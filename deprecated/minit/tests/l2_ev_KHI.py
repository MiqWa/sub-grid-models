#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 19 16:21:16 2022

@author: miquelmiravet
"""

#####################################
#                                   #
#               L2 NORM             #
#                                   #
#####################################

"""
We're going to perform the L2 norm test.

"""
import os
import numpy as np
from scipy.optimize import minimize
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy.ctypeslib as npct
from ctypes import c_int, c_double
import h5py
from scipy.stats import pearsonr
from scipy.linalg import norm
from statistics import stdev

#=================================================================================
array_3d_float = npct.ndpointer(dtype=np.float64, ndim=3, flags='CONTIGUOUS')
array_1d_float = npct.ndpointer(dtype=np.float64, ndim=1, flags='CONTIGUOUS')

libenergyev = npct.load_library("/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/energy_dens_ev/KHI/libenergyevkhi", ".")
libenergyev.energy_ev.restype = None
libenergyev.energy_ev.argtypes = [c_double, c_int,c_double,c_double, array_1d_float]
#=================================================================================
def readh5files(arx):

    print('DATA FROM FILE:      ',arx)

    with h5py.File(arx, "r") as f:
    # List all groups
        print("Keys: %s" % f.keys())
        a_group_key = list(f.keys())

        Sf = np.array(f.get('S_f'))
        x = np.array(f.get('x'))
        y = np.array(f.get('y'))
        z = np.array(f.get('z'))
        xf = np.array(f.get('x_f'))
        yf = np.array(f.get('y_f'))
        zf = np.array(f.get('z_f'))
        rho = np.array(f.get('rho_mean'))
        Mxx = np.array(f.get('stress_Mxx'))
        Mxy = np.array(f.get('stress_Mxy'))
        Mxz = np.array(f.get('stress_Mxz'))
        Myy = np.array(f.get('stress_Myy'))
        Myz = np.array(f.get('stress_Myz'))
        Mzz = np.array(f.get('stress_Mzz'))
        Rxx = np.array(f.get('stress_Rxx'))
        Rxy = np.array(f.get('stress_Rxy'))
        Rxz = np.array(f.get('stress_Rxz'))
        Ryy = np.array(f.get('stress_Ryy'))
        Ryz = np.array(f.get('stress_Ryz'))
        Rzz = np.array(f.get('stress_Rzz'))
        Fyz = np.array(f.get('stress_Fyz'))
        Fxy = np.array(f.get('stress_Fxy'))
        Fxz = np.array(f.get('stress_Fxz'))

        time = np.array(f.get('time'))

        f.close()

    M = np.array([Mxx,Mxy,Mxz,Myy,Myz,Mzz])
    R = np.array([Rxx,Rxy,Rxz,Ryy,Ryz,Rzz])
    F = np.array([Fxy,Fxz,Fyz])

    print('Dimensions of the grid     :',len(x)," x ",len(y)," x ",len(z))
    print('Time           :',time)

    return [M, R, F, rho, x, y, z, xf, yf, zf]


#===========================================================================================

def read_coeffs(res):

    res = 'coeffs-KHI-final'

    path = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/coefficients_r0/'+res+'/'

    alpha = np.loadtxt(path+'alphaR_r0_av.txt')

    beta = np.loadtxt(path+'betaR_r0_av.txt')

    gamma = np.loadtxt(path+'gammaR_r0_av.txt')

    return alpha, beta, gamma

#===========================================================================================

def energy_ev(v0, scale,al,t, factor_c, energy_ini,rho):

    gamma_khi = 1/al*v0*0.1425
    print('gamma KHI:',gamma_khi)

    std_fact= factor_c/(rho**(0.5)*scale)*0.1425
    print('Std : ', std_fact)
    timestep= 5e-5
    tfinal = t[-1]
    lent = int(round((tfinal)/timestep))+1

    energy_t = np.zeros(lent)
    energy_t[0] = energy_ini

    libenergyev.energy_ev_khi(std_fact,lent,timestep,gamma_khi, energy_t)

    ratio = int((lent-1)/(len(t)-1))
    energy_model = np.zeros(len(t))

    for i in range(0,len(t)-1):
        energy_model[i] = energy_t[ratio*i]

    energy_model[-1] = energy_t[-1]

    return energy_model

#===========================================================================================

def energy_mod_space(v0, lenxf,lenyf,lenzf,scale,al,t, factor_c, energy_ini,rho):

    energy_mod = np.zeros((len(t), lenxf, lenyf, lenzf))

    for i in range(0,lenxf):

        for j in range(0,lenyf):

            for k in range(0,lenzf):

                energy_mod[:,i,j,k]= energy_ev(v0, scale,al,t, factor_c, energy_ini,rho[i,j,k])

    return energy_mod

#===========================================================================================

def stresses(res,direc,factor_c,energy_ini,v0,al,t,Sf):

    lent = len(t)
    inivars = readh5files(direc+'stresses_sim_res_'+res+'-0000.h5')



    xfilter = inivars[7]
    yfilter = inivars[8]
    zfilter = inivars[9]

    Msim = np.zeros((lent,6,len(xfilter),len(yfilter),len(zfilter)))
    Rsim = np.zeros((lent,6,len(xfilter),len(yfilter),len(zfilter)))
    Fsim = np.zeros((lent,3,len(xfilter),len(yfilter),len(zfilter)))
    rhomean = np.zeros((lent,len(xfilter),len(yfilter),len(zfilter)))

    for tt in range(0, len(t)):

        arx = format(int(10*t[tt]), "04")
        Msim[tt], Rsim[tt], Fsim[tt], rhomean[tt], x, y, z, xfilter, yfilter, zfilter = readh5files(direc+'stresses_sim_res_'+res+'-'+arx+'.h5')

    cellsize = x[1]-x[0]

    filtersize = Sf*cellsize
    lambda_khi = al/0.1425

    scale = min(filtersize, lambda_khi)

    alphaKHI, betaKHI, gammaKHI = read_coeffs(res)

    energy_sim = 0.5*rhomean*(Rsim[:,0]+Rsim[:,3]-2*Rsim[:,5])

    energyKHI= energy_mod_space(v0, len(xfilter),len(yfilter),len(zfilter),scale,al,t, factor_c, energy_ini,rhomean[0,:,:,:])

    Mmod = np.zeros((lent, 6, len(xfilter), len(yfilter), len(zfilter)))
    Rmod = np.zeros((lent, 6, len(xfilter), len(yfilter), len(zfilter)))
    Fmod = np.zeros((lent, 3, len(xfilter), len(yfilter), len(zfilter)))

    for i in range(0,6):
        Mmod[:,i,:,:,:] = alphaKHI[i]*energyKHI[:,:,:,:]
        Rmod[:,i,:,:,:] = betaKHI[i]*energyKHI[:,:,:,:]/rhomean[:,:,:,:]

    for j in range(0,3):
        Fmod[:,j,:,:,:] = gammaKHI[j]*energyKHI[:,:,:,:]/rhomean[:,:,:,:]**(0.5)

    return Msim, Rsim, Fsim, Mmod, Rmod, Fmod, energyMRI, energy_sim, xfilter, yfilter, zfilter

#===========================================================================================

def l2_log(Msim, Rsim , Fsim, Mmod, Rmod, Fmod,t,path):

    ind0 = 10

    t = t[ind0:]

    Msimt = Msim[ind0:]
    Mmodt = Mmod[ind0:]
    Rsimt = Rsim[ind0:]
    Rmodt = Rmod[ind0:]
    Fsimt = Fsim[ind0:]
    Fmodt = Fmod[ind0:]

    l2_M_t_comp = np.zeros((len(t),6))
    l2_R_t_comp = np.zeros((len(t),6))
    l2_F_t_comp = np.zeros((len(t),3))

    l2_M_t = np.zeros(len(t))
    l2_R_t = np.zeros(len(t))
    l2_F_t = np.zeros(len(t))



    for tt in range(0,len(t)):
        integrandM = (Msimt[tt,:]-Mmodt[tt,:])**2
        integrandR = (Rsimt[tt,:]-Rmodt[tt,:])**2
        integrandF = (Fsimt[tt,:]-Fmodt[tt,:])**2
        mean_harF = 0.5*(1/np.sum(Fmodt[tt,:]**2)+1/np.sum(Fsimt[tt,:]**2))
        mean_harM = 0.5*(1/np.sum(Mmodt[tt,:]**2)+1/np.sum(Msimt[tt,:]**2))
        mean_harR = 0.5*(1/np.sum(Rmodt[tt,:]**2)+1/np.sum(Rsimt[tt,:]**2))

        l2_M_t[tt] = np.sqrt(np.sum(integrandM)*mean_harM)
        l2_R_t[tt] = np.sqrt(np.sum(integrandR)*mean_harR)
        l2_F_t[tt] = np.sqrt(np.sum(integrandF)*mean_harF)

    l2Mv2 = (np.sum(l2_M_t[:]**2)/len(t))**(0.5)
    l2Rv2 = (np.sum(l2_R_t[:]**2)/len(t))**(0.5)
    l2Fv2 = (np.sum(l2_F_t[:]**2)/len(t))**(0.5)

    for tt in range(0,len(t)):

        for cc in range(0,6):
            integrandM = (Msimt[tt,cc,:]-Mmodt[tt,cc,:])**2
            integrandR = (Rsimt[tt,cc,:]-Rmodt[tt,cc,:])**2
            mean_harM = 0.5*(1/np.sum(Mmodt[tt,cc,:]**2)+1/np.sum(Msimt[tt,cc,:]**2))
            mean_harR = 0.5*(1/np.sum(Rmodt[tt,cc,:]**2)+1/np.sum(Rsimt[tt,cc,:]**2))

            l2_M_t_comp[tt,cc] = np.sqrt(np.sum(integrandM)*mean_harM)
            #l2_M_t_comp[tt,cc] = (np.sum((np.log10(abs(Msimflat_t_comp[tt,cc,:]/Mmodflat_t_comp[tt,cc,:])))**2)/np.sum(np.log10(abs(Msimflat_t_comp[tt,cc,:]))**2))**(0.5)

            #Rsimflat_t_comp[tt,cc,:] = Rsimt[tt,cc,:,:,:].flatten()
            #Rmodflat_t_comp[tt,cc,:] = Rmodt[tt,cc,:,:,:].flatten()
            l2_R_t_comp[tt,cc] = np.sqrt(np.sum(integrandR)*mean_harR)
            #l2_R_t_comp[tt,cc] = (np.sum((np.log10(abs(Rsimflat_t_comp[tt,cc,:]/Rmodflat_t_comp[tt,cc,:])))**2)/np.sum(np.log10(abs(Rsimflat_t_comp[tt,cc,:]))**2))**(0.5)

        for cc in range(0,3):
            #Fsimflat_t_comp[tt,cc,:] = Fsimt[tt,cc,:,:,:].flatten()
            #Fmodflat_t_comp[tt,cc,:] = Fmodt[tt,cc,:,:,:].flatten()
            #for x in range(0,len(dr)):
            integrandF = (Fsimt[tt,cc,:]-Fmodt[tt,cc,:])**2
            mean_harF = 0.5*(1/np.sum(Fmodt[tt,cc,:]**2)+1/np.sum(Fsimt[tt,cc,:]**2))
            l2_F_t_comp[tt,cc] = np.sqrt(np.sum(integrandF)*mean_harF)
            #l2_F_t_comp[tt,cc] = (np.sum((np.log10(abs(Fsimflat_t_comp[tt,cc,:]/Fmodflat_t_comp[tt,cc,:])))**2)/np.sum(np.log10(abs(Fsimflat_t_comp[tt,cc,:]))**2))**(0.5)

    l2M_t_mean = (np.sum(l2_M_t_comp[:,:]**2, axis = 0)/len(t))
    l2R_t_mean = (np.sum(l2_R_t_comp[:,:]**2, axis = 0)/len(t))
    l2F_t_mean = (np.sum(l2_F_t_comp[:,:]**2, axis = 0)/len(t))

    l2Mrms = (np.sum(l2M_t_mean**2)/len(l2M_t_mean))**(0.5)
    l2Rrms = (np.sum(l2R_t_mean**2)/len(l2R_t_mean))**(0.5)
    l2Frms = (np.sum(l2F_t_mean**2)/len(l2F_t_mean))**(0.5)

    l2Mrms_t = (np.sum(l2_M_t_comp**2, axis = 1)/6)**(0.5)
    l2Rrms_t = (np.sum(l2_R_t_comp**2, axis = 1)/6)**(0.5)
    l2Frms_t = (np.sum(l2_F_t_comp**2, axis = 1)/3)**(0.5)

    with open(path+'/l2log_time_ev_facC.txt', 'w') as f:
        f.write('# MAXWELL \t REYNOLDS \t FARADAY #\n')
        for x in range(0,len(t)):
            f.write('%f\t%f\t%f\t%f\n'%(t[x],l2Mrms_t[x], l2Rrms_t[x], l2Frms_t[x]))

    with open(path+'/l2_time_ev_minit.txt', 'w') as f:
        f.write('# MAXWELL \t REYNOLDS \t FARADAY #\n')
        for x in range(0,len(t)):
            f.write('%f\t%f\t%f\t%f\n'%(t[x],l2_M_t[x], l2_R_t[x], l2_F_t[x]))

    with open(path+'/l2_minit.txt', 'w') as f:
        f.write('# MAXWELL \t REYNOLDS \t FARADAY #\n')
        f.write('%f\t%f\t%f\n'%(l2Mv2, l2Rv2, l2Fv2))

    with open(path+'/l2Mlog_comp_time_ev_facC.txt', 'w') as f:
        f.write('# M_rr \t M_rphi \t M_rz \t M_phiphi \t M_phiz \t M_zz #\n')
        for x in range(0,len(t)):
            f.write('%f\t%f\t%f\t%f\t%f\t%f\t%f\n'%(t[x],l2_M_t_comp[x,0], l2_M_t_comp[x,1], l2_M_t_comp[x,2], l2_M_t_comp[x,3], l2_M_t_comp[x,4], l2_M_t_comp[x,5]))

    with open(path+'/l2Rlog_comp_time_ev_facC.txt', 'w') as f:
        f.write('# R_rr \t R_rphi \t R_rz \t R_phiphi \t R_phiz \t R_zz  #\n')
        for x in range(0,len(t)):
            f.write('%f\t%f\t%f\t%f\t%f\t%f\t%f\n'%(t[x],l2_R_t_comp[x,0], l2_R_t_comp[x,1], l2_R_t_comp[x,2],l2_R_t_comp[x,3], l2_R_t_comp[x,4], l2_R_t_comp[x,5]))

    with open(path+'/l2Flog_comp_time_ev_facC.txt', 'w') as f:
        f.write('# F_rphi \t F_rz \t F_phiz #\n')
        for x in range(0,len(t)):
            f.write('%f\t%f\t%f\t%f\n'%(t[x],l2_F_t_comp[x,0], l2_F_t_comp[x,1], l2_F_t_comp[x,2]))

    with open(path+'/l2_rms_facC.txt', 'w') as f:
        f.write('# MAXWELL \t REYNOLDS \t FARADAY #\n')
        f.write('%f\t%f\t%f\n'%(l2Mrms,l2Rrms,l2Frms))

    return()

#===========================================================================================

def l2_log_phase(Msim, Rsim , Fsim, Mmod, Rmod, Fmod,t,path,phase):

    ind0 = 10
    indsat = 21
    difearly = int(indsat-ind0)
    diflate = int(difearly+1)
    t = t[ind0:]

    Msimt = Msim[ind0:]
    Mmodt = Mmod[ind0:]
    Rsimt = Rsim[ind0:]
    Rmodt = Rmod[ind0:]
    Fsimt = Fsim[ind0:]
    Fmodt = Fmod[ind0:]

    if phase == 'early':
        Msimt = Msimt[:difearly]
        Rsimt = Rsimt[:difearly]
        Fsimt = Fsimt[:difearly]
        Mmodt = Mmodt[:difearly]
        Rmodt = Rmodt[:difearly]
        Fmodt = Fmodt[:difearly]
        t = t[:difearly]

    elif phase == 'late':
        Msimt = Msimt[diflate:]
        Rsimt = Rsimt[diflate:]
        Fsimt = Fsimt[diflate:]
        Mmodt = Mmodt[diflate:]
        Rmodt = Rmodt[diflate:]
        Fmodt = Fmodt[diflate:]
        t = t[diflate:]

    l2_M_t_comp = np.zeros((len(t),6))
    l2_R_t_comp = np.zeros((len(t),6))
    l2_F_t_comp = np.zeros((len(t),3))

    l2_M_t = np.zeros(len(t))
    l2_R_t = np.zeros(len(t))
    l2_F_t = np.zeros(len(t))


    for tt in range(0,len(t)):
        integrandM = (Msimt[tt,:]-Mmodt[tt,:])**2
        integrandR = (Rsimt[tt,:]-Rmodt[tt,:])**2
        integrandF = (Fsimt[tt,:]-Fmodt[tt,:])**2
        mean_harF = 0.5*(1/np.sum(Fmodt[tt,:]**2)+1/np.sum(Fsimt[tt,:]**2))
        mean_harM = 0.5*(1/np.sum(Mmodt[tt,:]**2)+1/np.sum(Msimt[tt,:]**2))
        mean_harR = 0.5*(1/np.sum(Rmodt[tt,:]**2)+1/np.sum(Rsimt[tt,:]**2))

        l2_M_t[tt] = np.sqrt(np.sum(integrandM)*mean_harM)
        l2_R_t[tt] = np.sqrt(np.sum(integrandR)*mean_harR)
        l2_F_t[tt] = np.sqrt(np.sum(integrandF)*mean_harF)

    l2Mv2 = (np.sum(l2_M_t[:]**2)/len(t))**(0.5)
    l2Rv2 = (np.sum(l2_R_t[:]**2)/len(t))**(0.5)
    l2Fv2 = (np.sum(l2_F_t[:]**2)/len(t))**(0.5)


    if phase=='early':
        with open(path+'/l2_minit_early.txt', 'w') as f:
            f.write('# MAXWELL \t REYNOLDS \t FARADAY #\n')
            f.write('%f\t%f\t%f\n'%(l2Mv2, l2Rv2, l2Fv2))
    elif phase=='late':
        with open(path+'/l2_minit_late.txt', 'w') as f:
            f.write('# MAXWELL \t REYNOLDS \t FARADAY #\n')
            f.write('%f\t%f\t%f\n'%(l2Mv2, l2Rv2, l2Fv2))

    return()

#===========================================================================================

def main():

    factor_c = float(sys.argv[3])
    #factor_c_non0 = float(sys.argv[4])
    energy_ini = 7e-31
    v0 = 0.5
    al = 0.01

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_sim_test/"

    Sf = int(sys.argv[1])

    lenphi = int(sys.argv[2])

    if lenphi == 400 :

        lenr = 100
        lenz = 100


    elif lenphi == 240 :

        lenr = 60
        lenz = 60

    elif lenphi == 304 :

        lenr = 76
        lenz = 76

    res = str(lenr)+'_'+str(lenphi)+'_'+str(lenz)
    direc = path_data+'res_'+res+'/CVP--bx1e-3--512/Sf_'+str(Sf)+'/'

    t_final = 40
    lent = 201
    t = np.linspace(0,t_final,lent)

    Msim, Rsim, Fsim, Mmod, Rmod, Fmod, energy_mod, energy_sim, rfilter, phif, zf = stresses(res,direc,factor_c,energy_ini,factor_en,t,Sf)

    save_path = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/final_eval_minit_khi/res_"+res+"/CVP--bx1e-3--512/Sf_"+str(Sf)

    try:
        os.makedirs(save_path)
    except OSError as error:
        print(error)

    l2_log(Msim, Rsim , Fsim, Mmod, Rmod, Fmod,t,save_path)
    l2_log_phase(Msim, Rsim , Fsim, Mmod, Rmod, Fmod,t,save_path,'early')
    l2_log_phase(Msim, Rsim , Fsim, Mmod, Rmod, Fmod,t,save_path,'late')

#    Msim, Rsim, Fsim, Mmod, Rmod, Fmod, energy_mod, energy_sim, rfilter, phif, zf = stresses(res,direc,factor_c_non0,energy_ini,factor_en,t,Sf)

    #l2_log_non0(Msim, Rsim , Fsim, Mmod, Rmod, Fmod,t, len(rfilter),len(phif),len(zf),difr,phif,zf,save_path)


    return()

main()
