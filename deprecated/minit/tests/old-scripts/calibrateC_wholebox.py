#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 29 11:39:42 2021

@author: miquelmiravet
"""

#####################################
#                                   #
#       MODEL TEST WHOLE BOX        #       
#                                   #
#####################################

"""
We're going to test the performance of our model by computing the energy of the
stresses and comparing it to the one from our model. 

"""
import os
import numpy as np 
from scipy.optimize import minimize
from scipy.optimize import least_squares
import matplotlib.pyplot as plt
import numpy.ctypeslib as npct
from ctypes import c_int, c_double 
import h5py
from scipy.linalg import norm
from matplotlib.colors import LogNorm
import statistics

#=================================================================================
array_3d_float = npct.ndpointer(dtype=np.float64, ndim=3, flags='CONTIGUOUS')
array_1d_float = npct.ndpointer(dtype=np.float64, ndim=1, flags='CONTIGUOUS')

libenergyev = npct.load_library("/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/energy_dens_ev/libenergyev", ".")
libenergyev.energy_ev.restype = None
libenergyev.energy_ev.argtypes = [c_double, c_int,c_double,c_double, c_double,array_1d_float, array_1d_float]
#=================================================================================

def readh5files(arx):
    
    print('DATA FROM FILE:      ',arx)
    
    with h5py.File(arx, "r") as f:
    # List all groups
        print("Keys: %s" % f.keys())
        a_group_key = list(f.keys())
        for i in a_group_key:
            print(i)

        Fmean = np.array(f.get('Fmean'))
        Rmean = np.array(f.get('Rmean'))
        Mmean = np.array(f.get('Mmean'))
        rhomean = np.array(f.get('rhomean'))
        r0 = np.array(f.get('r0'))
        time = np.array(f.get('time'))
        
        M = np.array([Mmean[0,0],Mmean[0,1],Mmean[0,2], Mmean[1,1], Mmean[1,2], Mmean[2,2]])
        R = np.array([Rmean[0,0],Rmean[0,1],Rmean[0,2], Rmean[1,1], Rmean[1,2], Rmean[2,2]])
        F = np.array([Fmean[0,1], Fmean[0,2], Fmean[1,2]])
        
        f.close()

        print('Time           :',time,' ms')
    
    return [M, R, F, rhomean, r0]

#===========================================================================================

def read_coeffs(res):

    res = '100_400_100'

    path = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/coefficients_r0/res_'+res+'/'

    alpha = np.loadtxt(path+'alphaR_r0_av.txt')

    beta = np.loadtxt(path+'betaR_r0_av.txt')

    gamma = np.loadtxt(path+'gammaR_r0_av.txt')

    e_alpha = np.loadtxt(path+'sigma_alphaR_r0_av.txt')

    e_beta = np.loadtxt(path+'sigma_betaR_r0_av.txt')

    e_gamma = np.loadtxt(path+'sigma_gammaR_r0_av.txt')

    return alpha, beta, gamma, e_alpha, e_beta, e_gamma

#===========================================================================================


def energy_ev(alphatrace,t,q,omega, factor_c, energy_ini, factor_en):
    
    sigma = 0.27
    
    b0 = 4.6e13*2.874214371e-25/(4*3.14159265359)**(0.5)
    
    gamma_pi_fac = 2*sigma*(q/(4-q))**(0.5)*(1-(2-q)**2/4)**(0.5)*omega/b0*alphatrace**(0.5)
    
    std_fact= 1/factor_c**(0.5)*gamma_pi_fac
     
    timestep= 5e-5
    
    timestep = 0.001*2.99792458e10*timestep
    tfinal = t[-1]*0.001*2.99792458e10
    lent = int(round((tfinal)/timestep))+1

    energy_t = np.zeros(lent)
    energy_t[0] = energy_ini
   
    energy_pi = np.zeros(lent)
    energy_pi[0] =energy_ini*factor_en
   
    gamma_pi = np.zeros(lent)
    gamma_pi[0] = gamma_pi_fac*energy_ini**(0.5)

    gamma_mri = q*omega
     
    libenergyev.energy_ev(std_fact,lent,timestep,gamma_mri, gamma_pi_fac,energy_t,energy_pi)
    
    ratio = int((lent-1)/(len(t)-1))
    energy_model = np.zeros(len(t))

    for i in range(0,len(t)-1):
        energy_model[i] = energy_t[ratio*i]

    energy_model[-1] = energy_t[-1]
    
    return energy_model

#===========================================================================================
     
def fun_minimize_M(factor_c):
    
    energy_ini = 1e-30
    factor_en = 100
    omega = 1824*3.33564095e-11
    q = 1.25

    print('C factor     : ', factor_c)
    print('energy_ini      : ', energy_ini)
    print('ratio energies   : ', factor_en)

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_r0/"

    lenphi = 304

    if lenphi == 400 :
        
        lenr = 100
        lenz = 100

        
    elif lenphi == 240 :

        lenr = 60
        lenz = 60
        
    elif lenphi == 304 :

        lenr = 76
        lenz = 76

    res = str(lenr)+'_'+str(lenphi)+'_'+str(lenz)
    direc = path_data+'res_'+res+'/'
    t_final = 30
    lent = 61
    t = np.linspace(0,t_final,lent)
    Msim = np.zeros((len(t),6))
    Rsim = np.zeros((len(t),6))
    Fsim = np.zeros((len(t),3))
    rhomean = np.zeros(len(t))
    
    for tt in range(0, len(t)):

        arx = format(25*tt, "04")
        Msim[tt], Rsim[tt], Fsim[tt], rhomean[tt], r0 = readh5files(direc+'stresses_sigma_r0-'+arx+'.h5')
    
    alpha, beta, gamma, e_alpha, e_beta, e_gamma = read_coeffs(res)

    alpha_arr = np.array([alpha, alpha+0.5*e_alpha, alpha-0.5*e_alpha])
    beta_arr = np.array([beta, beta+0.5*e_beta, beta-0.5*e_beta])
    gamma_arr = np.array([gamma, gamma+0.5*e_gamma, gamma-0.5*e_gamma])
    alpha = alpha_arr[0]
    beta = beta_arr[0]
    gamma = gamma_arr[0]

    alphatrace = abs(alpha[0]+alpha[3]+alpha[5])

    energy_mod = energy_ev(alphatrace,t,q,omega, factor_c, energy_ini, factor_en)
     
    Mmod = np.zeros((len(t), 6))

    for i in range(0,6):
        Mmod[:,i] = alpha[i]*energy_mod[:]
    
    ind_sat = 29


    Msim = Msim[ind_sat:,:]
    Mmod = Mmod[ind_sat:,:]

    Msim = np.delete(Msim,[1,2,4], axis =1)
    Mmod = np.delete(Mmod,[1,2,4], axis = 1)

    Msim = Msim.flatten()
    Mmod = Mmod.flatten()

    Msim = np.log10(Msim)
    Mmod = np.log10(Mmod)
    
    l2norm = np.sum((Msim-Mmod)**2/((Msim)**2))*1/len(Msim)
    l2norm = l2norm**0.5
    
    return l2norm

#===========================================================================================
     
def fun_minimize_R(factor_c):
   
    energy_ini = 1e-30
    factor_en = 100
    omega = 1824*3.33564095e-11
    q = 1.25

    print('C factor     : ', factor_c)
    print('energy_ini      : ', energy_ini)
    print('ratio energies   : ', factor_en)

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_r0/"

    lenphi = 304

    if lenphi == 400 :
        
        lenr = 100
        lenz = 100

        
    elif lenphi == 240 :

        lenr = 60
        lenz = 60
        
    elif lenphi == 304 :

        lenr = 76
        lenz = 76

    res = str(lenr)+'_'+str(lenphi)+'_'+str(lenz)
    direc = path_data+'res_'+res+'/'
    t_final = 30
    lent = 61
    t = np.linspace(0,t_final,lent)
    Msim = np.zeros((len(t),6))
    Rsim = np.zeros((len(t),6))
    Fsim = np.zeros((len(t),3))
    rhomean = np.zeros(len(t))
    
    for tt in range(0, len(t)):

        arx = format(25*tt, "04")
        Msim[tt], Rsim[tt], Fsim[tt], rhomean[tt], r0 = readh5files(direc+'stresses_sigma_r0-'+arx+'.h5')
    
    alpha, beta, gamma, e_alpha, e_beta, e_gamma = read_coeffs(res)

    alpha_arr = np.array([alpha, alpha+0.5*e_alpha, alpha-0.5*e_alpha])
    beta_arr = np.array([beta, beta+0.5*e_beta, beta-0.5*e_beta])
    gamma_arr = np.array([gamma, gamma+0.5*e_gamma, gamma-0.5*e_gamma])
    alpha = alpha_arr[0]
    beta = beta_arr[0]
    gamma = gamma_arr[0]

    alphatrace = abs(alpha[0]+alpha[3]+alpha[5])

    energy_mod = energy_ev(alphatrace,t,q,omega, factor_c, energy_ini, factor_en)
     
    Rmod = np.zeros((len(t), 6))

    for i in range(0,6):
        Rmod[:,i] = beta[i]*energy_mod[:]/rhomean[:]
    
    ind_sat = 29


    Rsim = Rsim[ind_sat:,:]
    Rmod = Rmod[ind_sat:,:]

    Rsim = np.delete(Rsim,[1,2,4], axis =1)
    Rmod = np.delete(Rmod,[1,2,4], axis = 1)

    Rsim = Rsim.flatten()
    Rmod = Rmod.flatten()

    Rsim = np.log10(Rsim)
    Rmod = np.log10(Rmod)
    
    l2norm = np.sum((Rsim-Rmod)**2/((Rsim)**2))*1/len(Rsim)
    l2norm = l2norm**0.5
    
    return l2norm

#===========================================================================================
     
def fun_minimize_en(factor_c):
   
    energy_ini = 1e-30
    factor_en = 100
    omega = 1824*3.33564095e-11
    q = 1.25

    print('C factor     : ', factor_c)
    print('energy_ini      : ', energy_ini)
    print('ratio energies   : ', factor_en)

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_r0/"

    lenphi = 304

    if lenphi == 400 :
        
        lenr = 100
        lenz = 100

        
    elif lenphi == 240 :

        lenr = 60
        lenz = 60
        
    elif lenphi == 304 :

        lenr = 76
        lenz = 76

    res = str(lenr)+'_'+str(lenphi)+'_'+str(lenz)
    direc = path_data+'res_'+res+'/'
    t_final = 30
    lent = 61
    t = np.linspace(0,t_final,lent)
    Msim = np.zeros((len(t),6))
    Rsim = np.zeros((len(t),6))
    Fsim = np.zeros((len(t),3))
    rhomean = np.zeros(len(t))
    
    for tt in range(0, len(t)):

        arx = format(25*tt, "04")
        Msim[tt], Rsim[tt], Fsim[tt], rhomean[tt], r0 = readh5files(direc+'stresses_sigma_r0-'+arx+'.h5')
    
    alpha, beta, gamma, e_alpha, e_beta, e_gamma = read_coeffs(res)

    alpha_arr = np.array([alpha, alpha+0.5*e_alpha, alpha-0.5*e_alpha])
    beta_arr = np.array([beta, beta+0.5*e_beta, beta-0.5*e_beta])
    gamma_arr = np.array([gamma, gamma+0.5*e_gamma, gamma-0.5*e_gamma])
    alpha = alpha_arr[0]
    beta = beta_arr[0]
    gamma = gamma_arr[0]

    alphatrace = abs(alpha[0]+alpha[3]+alpha[5])

    energy_mod = energy_ev(alphatrace,t,q,omega, factor_c, energy_ini, factor_en)
    
    energy_sim = 0.5*rhomean[:]*(Rsim[:,0]+Rsim[:,3]+Rsim[:,5])

    ind = 29

    energy_mod = np.log10(energy_mod[ind:])
    energy_sim = np.log10(energy_sim[ind:])
    energy_mod = energy_mod.flatten()
    energy_sim = energy_sim.flatten()

    l2norm = np.sum((energy_sim-energy_mod)**2/((energy_sim)**2))*1/len(energy_sim)
    l2norm = l2norm**0.5
    
    return l2norm

#===========================================================================================

def stresses_mod_r0(factor_c,energy_ini,res,t,q,omega, factor_en):

    print('C factor     : ', factor_c)
    print('energy_ini      : ', energy_ini)

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_r0/res_"+res
     
    Msim = np.zeros((len(t),6))
    Rsim = np.zeros((len(t),6))
    Fsim = np.zeros((len(t),3))
    rhomean = np.zeros(len(t))
    
    for tt in range(0, len(t)):

        arx = format(25*tt, "04")
        Msim[tt], Rsim[tt], Fsim[tt], rhomean[tt], r0 = readh5files(path_data+'/stresses_sigma_r0-'+arx+'.h5')

    
    alpha, beta, gamma = read_coeffs(res)

    alphatrace = alpha[0]+alpha[3]+alpha[5]

    energy_mod = energy_ev(alphatrace,t,q,omega, factor_c, energy_ini, factor_en)
     
    Mmod = np.zeros((len(t), 6))
    Rmod = np.zeros((len(t), 6))
    Fmod = np.zeros((len(t), 3))

    for i in range(0,6):
        Mmod[:,i] = alpha[i]*energy_mod[:]
        Rmod[:,i] = beta[i]*energy_mod[:]/rhomean[:]
    
    for j in range(0,3):
        Fmod[:,j] = gamma[j]*energy_mod[:]/rhomean[:]**(0.5)
      
    return Msim, Rsim, Fsim, Mmod, Rmod, Fmod, energy_mod, rhomean

#===========================================================================================

def fun_min_sat_mean(factor_c):
   
    energy_ini = 1e-31
    factor_en = 1000
    omega = 1824*3.33564095e-11
    q = 1.25

    print('C factor     : ', factor_c)
    print('energy_ini      : ', energy_ini)
    print('ratio energies   : ', factor_en)

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_r0/"

    lenphi = 400

    if lenphi == 400 :
        
        lenr = 100
        lenz = 100

        
    elif lenphi == 240 :

        lenr = 60
        lenz = 60
        
    elif lenphi == 304 :

        lenr = 76
        lenz = 76

    res = str(lenr)+'_'+str(lenphi)+'_'+str(lenz)
    direc = path_data+'res_'+res+'/'
    t_final = 30
    lent = 61
    t = np.linspace(0,t_final,lent)
    Msim = np.zeros((len(t),6))
    Rsim = np.zeros((len(t),6))
    Fsim = np.zeros((len(t),3))
    rhomean = np.zeros(len(t))
    
    for tt in range(0, len(t)):

        arx = format(25*tt, "04")
        Msim[tt], Rsim[tt], Fsim[tt], rhomean[tt], r0 = readh5files(direc+'stresses_sigma_r0-'+arx+'.h5')
    
    alpha, beta, gamma, e_alpha, e_beta, e_gamma = read_coeffs(res)

    alpha_arr = np.array([alpha, alpha+0.5*e_alpha, alpha-0.5*e_alpha])
    beta_arr = np.array([beta, beta+0.5*e_beta, beta-0.5*e_beta])
    gamma_arr = np.array([gamma, gamma+0.5*e_gamma, gamma-0.5*e_gamma])
    alpha = alpha_arr[0]
    beta = beta_arr[0]
    gamma = gamma_arr[0]

    alphatrace = abs(alpha[0]+alpha[3]+alpha[5])

    energy_mod = energy_ev(alphatrace,t,q,omega, factor_c, energy_ini, factor_en)
     
    Mmod = np.zeros((len(t), 6))
    Rmod = np.zeros((len(t), 6))
    Fmod = np.zeros((len(t), 3))

    for i in range(0,6):
        Mmod[:,i] = alpha[i]*energy_mod[:]
        Rmod[:,i] = beta[i]*energy_mod[:]/rhomean[:]
    
    for j in range(0,3):
        Fmod[:,j] = gamma[j]*energy_mod[:]/rhomean[:]**(0.5)
    
    ind_sat = 29

    Msim = Msim[ind_sat:,:]
    Rsim = Rsim[ind_sat:,:]
    Fsim = Fsim[ind_sat:,:]
    Mmod = Mmod[ind_sat:,:]
    Rmod = Rmod[ind_sat:,:]
    Fmod = Fmod[ind_sat:,:]

    Rsim = np.delete(Rsim,[1,2,4], axis =1)
    Rmod = np.delete(Rmod,[1,2,4], axis = 1)
    Msim = np.delete(Msim,[1,2,4], axis = 1)
    Mmod = np.delete(Mmod,[1,2,4], axis = 1)

    Msim = Msim.flatten()
    Rsim = Rsim.flatten()
    Mmod = Mmod.flatten()
    Rmod = Rmod.flatten()

    Msim = np.log10(Msim)
    Rsim = np.log10(Rsim)
    Mmod = np.log10(Mmod)
    Rmod = np.log10(Rmod)
    
    a = np.concatenate((Msim,Rsim))
    b = np.concatenate((Mmod,Rmod))

    l2norm = np.sum((a-b)**2/((a)**2))*1/len(a)
    l2norm = l2norm**0.5

    return l2norm

#===========================================================================================

def comparison_growth(Msim, Rsim, Mmod, Rmod):

    Rsim = np.delete(Rsim,[1,2,4], axis = 1)
    Rmod = np.delete(Rmod,[1,2,4], axis = 1)
    Msim = np.delete(Msim,[1,2,4], axis = 1)
    Mmod = np.delete(Mmod,[1,2,4], axis = 1)

    ind_ini = 21
    ind_sat = 26

#    Msim = np.log10(Msim[ind_ini:ind_sat,:])
#    Rsim = np.log10(Rsim[ind_ini:ind_sat,:])
#    Mmod = np.log10(Mmod[ind_ini:ind_sat,:])
#    Rmod = np.log10(Rmod[ind_ini:ind_sat,:])

    Msim = Msim.flatten()
    Rsim = Rsim.flatten()
    Mmod = Mmod.flatten()
    Rmod = Rmod.flatten()

    a = np.concatenate((Msim,Rsim), axis= None)
    b = np.concatenate((Mmod,Rmod), axis = None)

    l2norm = np.sum((a-b)**2/((a)**2))*1/len(a)
    l2norm = l2norm**0.5

    return l2norm

#===========================================================================================

def fun_min_growth(energy_ini, factor_en):

    factor_c = 0.25
    
    omega = 1824*3.33564095e-11
    q = 1.25

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_r0/"

    lenphi = 400

    if lenphi == 400 :

        lenr = 100
        lenz = 100


    elif lenphi == 240 :

        lenr = 60
        lenz = 60

    elif lenphi == 304 :

        lenr = 76
        lenz = 76

    res = str(lenr)+'_'+str(lenphi)+'_'+str(lenz)
    direc = path_data+'res_'+res+'/'
    t_final = 30
    lent = 61
    t = np.linspace(0,t_final,lent)
    
    Msim = np.zeros((len(t),6))
    Rsim = np.zeros((len(t),6))
    Fsim = np.zeros((len(t),3))
    rhomean = np.zeros(len(t))

    for tt in range(0, len(t)):

        arx = format(25*tt, "04")
        Msim[tt], Rsim[tt], Fsim[tt], rhomean[tt], r0 = readh5files(direc+'stresses_sigma_r0-'+arx+'.h5')

    alpha, beta, gamma, e_alpha, e_beta, e_gamma = read_coeffs(res)

    alphatrace = alpha[0]+alpha[3]+alpha[5]

    energy_mod = energy_ev(alphatrace,t,q,omega, factor_c, energy_ini, factor_en)

    Mmod = np.zeros((lent, 6))
    Rmod = np.zeros((lent, 6))
    Fmod = np.zeros((lent, 3))

    for i in range(0,6):
        Mmod[:,i] = alpha[i]*energy_mod[:]
        Rmod[:,i] = beta[i]*energy_mod[:]/rhomean[:]

    for j in range(0,3):
        Fmod[:,j] = gamma[j]*energy_mod[:]/rhomean[:]**(0.5)

    l2norm = comparison_growth(Msim, Rsim, Mmod, Rmod)

    return l2norm

#===========================================================================================

def growth_norml2(e0, k0,path):

    l2cont = np.zeros((len(e0), len(k0)))

    for k in range(0,len(e0)):
        for j in range(0,len(k0)):
            l2cont[k,j] = fun_min_growth(e0[k],k0[j])

    print('MIN L2 NORM  :', np.amin(l2cont[:,:]))
    fig, ax = plt.subplots()
    c = ax.pcolor(k0,e0,l2cont[:,:],norm = LogNorm(vmin=np.min(l2cont[:,:]), vmax = np.max(l2cont[:,:])), cmap = 'viridis')
    cbar=fig.colorbar(c, ax=ax, label = r'$L_2$')
    cbar.ax.tick_params(labelsize=12)
    ax.set_yscale('log')
    ax.set_xlabel(r'$K_0$')
    ax.set_xscale('log')
    ax.set_ylabel(r'$e_{\rm T}(0)$')
    plt.savefig(path+'/l2norm_growth_whole_box.pdf')
    ind = np.where(l2cont == np.min(l2cont))
    print('index :', ind)
    inde0 = ind[0]
    indk0 = ind[1]
    e0opt = e0[inde0[0]]
    k0opt = k0[indk0[0]]
    l2min = l2cont[inde0,indk0]
    print('e0opt     = ', e0opt)
    print('k0opt     = ', k0opt)
    print('L2 norm min  = ', l2min)

    return e0opt, k0opt, l2min

#===========================================================================================

def contour_norml2_sat(factor_carr,path):

    l2cont = np.zeros(len(factor_carr))

    for k in range(0,len(factor_carr)):
        l2cont[k] = fun_min_sat_mean(factor_carr[k])
        
    ind = np.where(l2cont == np.min(l2cont))
    print('index :', ind)
    indc = ind[0]
    Copt = factor_carr[indc[0]]
    print('Copt     = ', Copt)
    print('L2 norm min  = ', l2cont[indc])
    plt.plot(factor_carr,l2cont, color = 'blue')
    plt.xlabel(r'$K_{\rm sat}$')
    plt.ylabel(r'$|L_2|$')
    
    plt.savefig(path+'/norml2test_whole_box.pdf')

    return()
    
#===========================================================================================

def main():    

    lenphi = 304

    if lenphi == 400 :
        
        lenr = 100
        lenz = 100

        
    elif lenphi == 240 :

        lenr = 60
        lenz = 60
        
    elif lenphi == 304 :

        lenr = 76
        lenz = 76

    res = str(lenr)+'_'+str(lenphi)+'_'+str(lenz)
    
    #WITH ALL STRESSES 
    #Initial guesses for C and K:
    x0 = [0.2]

    #bounds for the parameters: 
    bounds = ((0.17,0.3),)

    sol = minimize(fun_min_sat_mean, x0, bounds = bounds)
    
    norml2min = fun_min_sat_mean(sol.x)
    
    Copt = sol.x[0]  

    print('C OPTIMAL             = ', Copt)
    print('min norm L2         = ', norml2min)
   
    return()
"""
    #pearson

#    Msim, Rsim, Fsim, Mmod, Rmod, Fmod, energy_mod, rho = stresses_mod_r0(Copt,energy0opt,res,t,q,omega, factor_enopt)
    
    pearson_path = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/pearson_l2norm/res_"+res+"/whole_box"
#    path_stresses_mod = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stresses_mod/res_"+res

    try:
        os.makedirs(pearson_path)
    except OSError as error:
        print(error)

#    try:
#        os.makedirs(path_stresses_mod)
#   except OSError as error:
#        print(error)
        
    
    with open(pearson_path+'/l2norm_all_sat_whole_box.txt', 'w') as f:
        f.write('# L_2 NORM \t FACTOR C  # \n')
        f.write('%.10f\t%.10f\n'%(norml2min,Copt))

    factor_c = np.linspace(0.1,0.4,100)
    contour_norml2_sat(factor_c, pearson_path)


#    for x in range(0,len(t)):
#        arxiu = format(25*x, "04")
#        hf = h5py.File(path_stresses_mod+'/stresses_model_res_'+res+'_whole_box-'+str(arxiu)+'.h5', 'w')
#        hf.create_dataset('energy_model', data = energy_mod[x])
#        hf.create_dataset('time', data = t[x])
#        hf.create_dataset('M', data = Mmod[x,:])
#        hf.create_dataset('R', data = Rmod[x])
#        hf.create_dataset('F', data = Fmod[x,:])
#        hf.close()

  #WITH STRESS M 

    sol_M = minimize(fun_minimize_M, x0, bounds = bounds)

    print('OPTIMAL PARAMS FOR M  = ', sol_M.x)
    l2norm_opt_M = fun_minimize_M(sol_M.x)

    with open(pearson_path+'/l2norm_M_sat_whole_box.txt', 'w') as f:
        f.write('# L_2 NORM \t FACTOR C # \n')
        f.write('%.10f\t%.10f\n'%(l2norm_opt_M,sol_M.x[0]))

    
    #WITH STRESS R
    
    sol_R = minimize(fun_minimize_R, x0, bounds = bounds)

    print('OPTIMAL PARAMS FOR R = ', sol_R.x)
    l2norm_opt_R = fun_minimize_R(sol_R.x)

    with open(pearson_path+'/l2norm_R_sat_whole_box.txt', 'w') as f:
        f.write('# L_2 NORM \t FACTOR C # \n')
        f.write('%.10f\t%.10f\n'%(l2norm_opt_R,sol_R.x[0]))


    #WITH ENERGY
    
    sol_en = minimize(fun_minimize_en, x0, bounds = bounds)

    print('OPTIMAL PARAMS FOR energy = ', sol_en.x)
    l2norm_opt_en = fun_minimize_R(sol_en.x)

    with open(pearson_path+'/l2norm_energy_sat_whole_box.txt', 'w') as f:
        f.write('# L_2 NORM \t FACTOR C # \n')
        f.write('%.10f\t%.10f\n'%(l2norm_opt_en,sol_en.x[0]))
    
    return()
"""
def main2():

    lenphi = 400

    if lenphi == 400 :

        lenr = 100
        lenz = 100


    elif lenphi == 240 :

        lenr = 60
        lenz = 60

    elif lenphi == 304 :

        lenr = 76
        lenz = 76

    res = str(lenr)+'_'+str(lenphi)+'_'+str(lenz)
    pearson_path = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/pearson_l2norm/res_"+res+"/whole_box"

        #WITH ALL STRESSES 
    #Initial guesses for e0 and K0:

    x0 = 0.25

#    e0 = np.logspace(-32,-30,50)
#    k0 = np.logspace(2,4,50)

    #e0opt, k0opt, norml2min = growth_norml2(e0,k0,pearson_path)
    #bounds for the parameters: 
    bounds = ((0.2,0.3),)

    sol = minimize(fun_min_sat_mean, x0, bounds = bounds)

    norml2min = fun_min_sat_mean(sol.x)
    Copt = sol.x
    #e0opt= sol.x[0]  
    #k0opt = sol.x[1]
   # print('energy0 OPTIMAL  = ', e0opt)
    print('C OPTIMAL             = ', Copt)
   # print('ratio OPTIMAL    = ', k0opt)
    print('min norm L2         = ', norml2min)

#    with open(pearson_path+'/l2norm_all_growth_whole_box.txt', 'w') as f:
#        f.write('# L_2 NORM \t K_sat \t e_T(0) \t K_0 # \n')
#        f.write('%.10f\t%.5f\t%.35f\t%.5f\n'%(norml2min,Copt,e0opt,k0opt))

    return()



main2()   
    
    
    
    
