#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 12 17:09:16 2022

@author: miquelmiravet
"""

#####################################
#                                   #
#            MODEL TEST             #
#                                   #
#####################################

"""
We're going to test the performance of our model by computing the energy of the
stresses and comparing it to the one from our model.

"""
import os
import numpy as np
from scipy.optimize import minimize
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy.ctypeslib as npct
from ctypes import c_int, c_double
import h5py
from scipy.stats import pearsonr
from scipy.linalg import norm

#=================================================================================
array_3d_float = npct.ndpointer(dtype=np.float64, ndim=3, flags='CONTIGUOUS')
array_1d_float = npct.ndpointer(dtype=np.float64, ndim=1, flags='CONTIGUOUS')

libenergyev = npct.load_library("/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/energy_dens_ev/libenergyev", ".")
libenergyev.energy_ev.restype = None
libenergyev.energy_ev.argtypes = [c_double, c_int,c_double,c_double, c_double,array_1d_float, array_1d_float]
#=================================================================================

def readh5files(arx):

    print('DATA FROM FILE:      ',arx)

    with h5py.File(arx, "r") as f:
    # List all groups
        print("Keys: %s" % f.keys())
        a_group_key = list(f.keys())
        for i in a_group_key:
            print(i)

        Sf = f[a_group_key[0]][...]
        print('Sf    =', Sf)
        phi = f[a_group_key[2]][...]
        print('phi shape    :', phi.shape)
        phif = f[a_group_key[3]][...]
        print('phi filter shape    :', phif.shape)
        r = f[a_group_key[4]][...]
        print('r shape    :', r.shape)
        rf = f[a_group_key[5]][...]
        print('r filter shape    :', rf.shape)
        rho = f[a_group_key[6]][...]
        print('rho mean shape    :', rho.shape)
        Fphiz = f[a_group_key[7]][...]
        print('F_phi,z shape    :', Fphiz.shape)
        Frphi = f[a_group_key[8]][...]
        print('F_r,phi shape    :', Frphi.shape)
        Frz = f[a_group_key[9]][...]
        print('F_r,z shape    :', Frz.shape)
        Mphiphi = f[a_group_key[10]][...]
        print('M_phi,phi shape    :', Mphiphi.shape)
        Mphiz = f[a_group_key[11]][...]
        print('M_phi,z shape    :', Mphiz.shape)
        Mrphi = f[a_group_key[12]][...]
        print('M_r,phi shape    :', Mrphi.shape)
        Mrr = f[a_group_key[13]][...]
        print('M_r,r shape    :', Mrr.shape)
        Mrz = f[a_group_key[14]][...]
        print('M_r,z shape    :', Mrz.shape)
        Mzz = f[a_group_key[15]][...]
        print('M_z,z shape    :', Mzz.shape)
        Rphiphi = f[a_group_key[16]][...]
        print('R_phi,phi shape    :', Rphiphi.shape)
        Rphiz = f[a_group_key[17]][...]
        print('R_phi,z shape    :', Rphiz.shape)
        Rrphi = f[a_group_key[18]][...]
        print('R_r,phi shape    :', Rrphi.shape)
        Rrr = f[a_group_key[19]][...]
        print('R_r,r shape    :', Rrr.shape)
        Rrz = f[a_group_key[20]][...]
        print('R_r,z shape    :', Rrz.shape)
        Rzz = f[a_group_key[21]][...]
        print('R_z,z shape    :', Rzz.shape)
        time = f[a_group_key[22]][...]
        print('time shape', time.shape)
        z = f[a_group_key[23]][...]
        print('z shape    :', z.shape)
        zf = f[a_group_key[24]][...]
        print('z filter shape    :', zf.shape)

        f.close()

    M = np.array([Mrr,Mrphi,Mrz,Mphiphi,Mphiz,Mzz])
    R = np.array([Rrr,Rrphi,Rrz,Rphiphi,Rphiz,Rzz])
    F = np.array([Frphi,Frz,Fphiz])

    print('Dimensions of the grid     :',len(r)," x ",len(phi)," x ",len(z))
    print('Time           :',time,' ms')

    return [M, R, F, rho, r, phi, z, rf, phif, zf]

#===========================================================================================

def read_coeffs(res):

    res = '100_400_100'

    path = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/coefficients_r0/res_'+res+'/b0z-4.6e13/'

    alpha = np.loadtxt(path+'alphaR_r0_av.txt')

    beta = np.loadtxt(path+'betaR_r0_av.txt')

    gamma = np.loadtxt(path+'gammaR_r0_av.txt')

    return alpha, beta, gamma

#===========================================================================================

def energy_ev(alphatrace,t, scale,q,omega, factor_c, energy_ini, factor_en,rho):

    sigma = 0.27

    b0 = 4.6e13*2.874214371e-25/(4*3.14159265359)**(0.5)

    gamma_pi_fac = 2*sigma*(q/(4-q))**(0.5)*(1-(2-q)**2/4)**(0.5)*omega/b0*alphatrace**(0.5)

    std_fact= factor_c/(rho**(0.5)*scale)

    timestep= 5e-5

    timestep = 0.001*2.99792458e10*timestep
    tfinal = t[-1]*0.001*2.99792458e10
    lent = int(round((tfinal)/timestep))+1

    energy_t = np.zeros(lent)
    energy_t[0] = energy_ini

    energy_pi = np.zeros(lent)
    energy_pi[0] =energy_ini*factor_en

    gamma_pi = np.zeros(lent)
    gamma_pi[0] = gamma_pi_fac*energy_ini**(0.5)

    gamma_mri = q*omega

    libenergyev.energy_ev(std_fact,lent,timestep,gamma_mri, gamma_pi_fac,energy_t,energy_pi)

    ratio = int((lent-1)/(len(t)-1))
    energy_model = np.zeros(len(t))

    for i in range(0,len(t)-1):
        energy_model[i] = energy_t[ratio*i]

    energy_model[-1] = energy_t[-1]

    return energy_model

#===========================================================================================

def energy_mod_space(alphatrace,rf,lenphif,lenzf,factor_c,t, scale, energy_ini,res, factor_en, rho):

    r0tilde = 15.5e5
    omega0tilde = 1824*3.33564095e-11
    q = 1.25

    energy_mod = np.zeros((len(t), len(rf), lenphif, lenzf))

    for i in range(0,len(rf)):

        omega = omega0tilde*(rf[i]/r0tilde)**(-q)

        for j in range(0,lenphif):

            for k in range(0,lenzf):

                energy_mod[:,i,j,k] = energy_ev(alphatrace, t, scale, q, omega, factor_c, energy_ini, factor_en,rho[i,j,k])

    return energy_mod

#===========================================================================================

def comparison_stresses(Msim, Rsim, Fsim, Mmod, Rmod, Fmod):

#    Rsim = np.delete(Rsim,[1,2,4], axis = 1)
#    Rmod = np.delete(Rmod,[1,2,4], axis = 1)
#    Msim = np.delete(Msim, [1,2,4], axis = 1)
#    Mmod = np.delete(Mmod, [1,2,4], axis = 1)

    ind = 33

    Msim = Msim[ind:,:]
    Rsim = Rsim[ind:,:]
    Fsim = Fsim[ind:,:]
    Mmod = Mmod[ind:,:]
    Rmod = Rmod[ind:,:]
    Fmod = Fmod[ind:,:]

    Msim = Msim.flatten()
    Rsim = Rsim.flatten()
    Fsim = Fsim.flatten()
    Mmod = Mmod.flatten()
    Rmod = Rmod.flatten()
    Fmod = Fmod.flatten()

    a = np.concatenate((Msim,Rsim,Fsim), axis= None)
    b = np.concatenate((Mmod,Rmod,Fmod), axis = None)

    l2norm = np.sum((a-b)**2)/np.sum((a)**2)
    l2norm = l2norm**0.5

    return l2norm

#===========================================================================================

def comparison_stress(sim,mod,t, comp):

    ind = 33
    compdel = comp
    #if comp == 6:
    #    sim = np.delete(sim, [1,2,4], axis = 1)
    #    mod = np.delete(mod, [1,2,4], axis = 1)
    t = t[ind:]
    sim = sim[ind:,:]
    mod = mod[ind:,:]
    simflat = np.zeros((len(t),compdel,np.size(sim, axis = 2)*np.size(sim,axis = 3)*np.size(sim, axis=4)))
    modflat = np.zeros((len(t),compdel,np.size(mod, axis = 2)*np.size(mod,axis = 3)*np.size(mod, axis=4)))
    l2norm = np.zeros((len(t),compdel))

    for tt in range(0,len(t)):
        for cc in range(0,compdel):
            simflat[tt,cc,:] = sim[tt,cc,:,:,:].flatten()
            modflat[tt,cc,:] = mod[tt,cc,:,:,:].flatten()

            l2norm[tt,cc] = (np.sum((np.log10(abs(simflat[tt,cc,:]))-np.log10(abs(modflat[tt,cc,:])))**2)/np.sum((np.log10(abs(simflat[tt,cc,:])))**2))**(0.5)
    l2norm_tmean = np.mean(l2norm, axis = 0)
    l2norm_rms = (np.sum(l2norm_tmean**2)/compdel)**(0.5)

    return l2norm_rms

#===========================================================================================

def comparison_stress_diag(sim,mod,t, comp):

    ind = 33
    compdel = comp-2
    if comp == 6:
        sim = np.delete(sim, [2,4], axis = 1)
        mod = np.delete(mod, [2,4], axis = 1)
    t = t[ind:]
    sim = sim[ind:,:]
    mod = mod[ind:,:]
    simflat = np.zeros((len(t),compdel,np.size(sim, axis = 2)*np.size(sim,axis = 3)*np.size(sim, axis=4)))
    modflat = np.zeros((len(t),compdel,np.size(mod, axis = 2)*np.size(mod,axis = 3)*np.size(mod, axis=4)))
    l2norm = np.zeros((len(t),compdel))

    for tt in range(0,len(t)):
        for cc in range(0,compdel):
            simflat[tt,cc,:] = sim[tt,cc,:,:,:].flatten()
            modflat[tt,cc,:] = mod[tt,cc,:,:,:].flatten()

            l2norm[tt,cc] = (np.sum((np.log10(abs(simflat[tt,cc,:]))-np.log10(abs(modflat[tt,cc,:])))**2)/np.sum((np.log10(abs(simflat[tt,cc,:])))**2))**(0.5)
    l2norm_tmean = np.mean(l2norm, axis = 0)
    l2norm_rms = (np.sum(l2norm_tmean**2)/compdel)**(0.5)

    return l2norm_rms

#===========================================================================================

def comparison_growth(sim,mod,t,comp):

    ind_ini = 11
    ind_sat = 26
    if comp == 6:
        sim = np.delete(sim, [1,2,4], axis = 1)
        mod = np.delete(mod, [1,2,4], axis = 1)
    t = t[ind_ini:ind_sat]
    sim = sim[ind_ini:ind_sat,:]
    mod = mod[ind_ini:ind_sat,:]
    simflat = np.zeros((len(t),3,np.size(sim, axis = 2)*np.size(sim,axis = 3)*np.size(sim, axis=4)))
    modflat = np.zeros((len(t),3,np.size(sim, axis = 2)*np.size(sim,axis = 3)*np.size(sim, axis=4)))
    l2norm = np.zeros((len(t),3))

    for tt in range(0,len(t)):
        for cc in range(0,3):
            simflat[tt,cc,:] = sim[tt,cc,:,:,:].flatten()
            modflat[tt,cc,:] = mod[tt,cc,:,:,:].flatten()

            l2norm[tt,cc] = (np.sum((np.log10(simflat[tt,cc,:])-np.log10(modflat[tt,cc,:]))**2)/np.sum((np.log10(simflat[tt,cc,:]))**2))**(0.5)
    l2norm_tmean = np.mean(l2norm, axis = 0)
    l2norm_rms = (np.sum(l2norm_tmean**2)/3)**(0.5)

    return l2norm_rms

#===========================================================================================

def fun_min_growth_M(inp):

    energy_ini = inp[0]
    factor_en = inp[1]
    factor_c = 0.275

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_sim_test/"

    Sf = int(sys.argv[1])

    lenphi = 400

    if lenphi == 400 :

        lenr = 100
        lenz = 100

    elif lenphi == 240 :

        lenr = 60
        lenz = 60

    elif lenphi == 304 :

        lenr = 76
        lenz = 76

    res = str(lenr)+'_'+str(lenphi)+'_'+str(lenz)
    direc = path_data+'res_'+res+'/Sf_'+str(Sf)+'/'
    t_final = 30
    lent = 61
    t = np.linspace(0,t_final,lent)
    inivars = readh5files(direc+'stresses_sim_res_'+res+'-0000.h5')

    rfilter = inivars[7]
    phifilter = inivars[8]
    zfilter = inivars[9]

    Msim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Rsim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Fsim = np.zeros((lent,3,len(rfilter),len(phifilter),len(zfilter)))
    rhomean = np.zeros((lent,len(rfilter),len(phifilter),len(zfilter)))

    for tt in range(0, lent):

        arx = format(25*tt, "04")
        Msim[tt], Rsim[tt], Fsim[tt], rhomean[tt], r, phi, z, rfilter, phifilter, zfilter = readh5files(direc+'stresses_sim_res_'+res+'-'+arx+'.h5')

    cellsize = (r[1]-r[0]+(r[-1]+r[0])*0.5*(phi[1]-phi[0])+z[1]-z[0])/3

    filtersize = Sf*cellsize

    alpha, beta, gamma = read_coeffs(res)

    alphatrace = alpha[0]+alpha[3]+alpha[5]

    energy_mod = energy_mod_space(alphatrace,rfilter,len(phifilter),len(zfilter),factor_c,t, filtersize, energy_ini,res, factor_en)

    Mmod = np.zeros((lent, 6, len(rfilter), len(phifilter), len(zfilter)))

    for i in range(0,6):
        Mmod[:,i,:,:,:] = alpha[i]*energy_mod[:,:,:,:]

    l2norm = comparison_growth(Msim, Mmod,t,6)

    return l2norm


#===========================================================================================

def fun_min_growth_R(inp):

    energy_ini = inp[0]
    factor_en = inp[1]
    factor_c = 0.275

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_sim_test/"

    Sf = int(sys.argv[1])

    lenphi = 400

    if lenphi == 400 :

        lenr = 100
        lenz = 100

    elif lenphi == 240 :

        lenr = 60
        lenz = 60

    elif lenphi == 304 :

        lenr = 76
        lenz = 76

    res = str(lenr)+'_'+str(lenphi)+'_'+str(lenz)
    direc = path_data+'res_'+res+'/Sf_'+str(Sf)+'/'
    t_final = 30
    lent = 61
    t = np.linspace(0,t_final,lent)
    inivars = readh5files(direc+'stresses_sim_res_'+res+'-0000.h5')

    rfilter = inivars[7]
    phifilter = inivars[8]
    zfilter = inivars[9]

    Msim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Rsim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Fsim = np.zeros((lent,3,len(rfilter),len(phifilter),len(zfilter)))
    rhomean = np.zeros((lent,len(rfilter),len(phifilter),len(zfilter)))

    for tt in range(0, lent):

        arx = format(25*tt, "04")
        Msim[tt], Rsim[tt], Fsim[tt], rhomean[tt], r, phi, z, rfilter, phifilter, zfilter = readh5files(direc+'stresses_sim_res_'+res+'-'+arx+'.h5')

    cellsize = (r[1]-r[0]+(r[-1]+r[0])*0.5*(phi[1]-phi[0])+z[1]-z[0])/3

    filtersize = Sf*cellsize

    alpha, beta, gamma = read_coeffs(res)

    alphatrace = alpha[0]+alpha[3]+alpha[5]

    energy_mod = energy_mod_space(alphatrace,rfilter,len(phifilter),len(zfilter),factor_c,t, filtersize, energy_ini,res, factor_en)

    Rmod = np.zeros((lent, 6, len(rfilter), len(phifilter), len(zfilter)))

    for i in range(0,6):
        Rmod[:,i,:,:,:] = beta[i]*energy_mod[:,:,:,:]/rhomean[:,:,:,:]

    l2norm = comparison_growth(Rsim, Rmod,t,6)

    return l2norm

#===========================================================================================

def fun_min_growth_F(energy_ini, factor_en):

    factor_c = 0.275

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_sim_test/"

    Sf = int(sys.argv[1])

    lenphi = 400

    if lenphi == 400 :

        lenr = 100
        lenz = 100

    elif lenphi == 240 :

        lenr = 60
        lenz = 60

    elif lenphi == 304 :

        lenr = 76
        lenz = 76

    res = str(lenr)+'_'+str(lenphi)+'_'+str(lenz)
    direc = path_data+'res_'+res+'/Sf_'+str(Sf)+'/'
    t_final = 30
    lent = 61
    t = np.linspace(0,t_final,lent)
    inivars = readh5files(direc+'stresses_sim_res_'+res+'-0000.h5')

    rfilter = inivars[7]
    phifilter = inivars[8]
    zfilter = inivars[9]

    Msim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Rsim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Fsim = np.zeros((lent,3,len(rfilter),len(phifilter),len(zfilter)))
    rhomean = np.zeros((lent,len(rfilter),len(phifilter),len(zfilter)))

    for tt in range(0, lent):

        arx = format(25*tt, "04")
        Msim[tt], Rsim[tt], Fsim[tt], rhomean[tt], r, phi, z, rfilter, phifilter, zfilter = readh5files(direc+'stresses_sim_res_'+res+'-'+arx+'.h5')

    cellsize = (r[1]-r[0]+(r[-1]+r[0])*0.5*(phi[1]-phi[0])+z[1]-z[0])/3

    filtersize = Sf*cellsize

    alpha, beta, gamma = read_coeffs(res)

    alphatrace = alpha[0]+alpha[3]+alpha[5]

    energy_mod = energy_mod_space(alphatrace,rfilter,len(phifilter),len(zfilter),factor_c,t, filtersize, energy_ini,res, factor_en)

    Fmod = np.zeros((lent, 3, len(rfilter), len(phifilter), len(zfilter)))

    for j in range(0,3):
        Fmod[:,j,:,:,:] = gamma[j]*energy_mod[:,:,:,:]/rhomean[:,:,:,:]**(0.5)

    l2norm = comparison_growth(Fsim, Fmod, t,3)

    return l2norm

#==============================================================================================

def fun_minimize_all(x):

    factor_c = x
    energy_ini = 7e-31
    factor_en = 10

    print('C factor     : ', factor_c)

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_sim_test/"

    Sf = int(sys.argv[1])

    lenphi = 400

    if lenphi == 400 :

        lenr = 100
        lenz = 100


    elif lenphi == 240 :

        lenr = 60
        lenz = 60

    elif lenphi == 304 :

        lenr = 76
        lenz = 76

    res = str(lenr)+'_'+str(lenphi)+'_'+str(lenz)
    direc = path_data+'res_'+res+'/Sf_'+str(Sf)+'/'
    t_final = 30
    lent = 61
    t = np.linspace(0,t_final,lent)
    inivars = readh5files(direc+'stresses_sim_res_'+res+'-0000.h5')

    rfilter = inivars[7]
    phifilter = inivars[8]
    zfilter = inivars[9]

    Msim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Rsim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Fsim = np.zeros((lent,3,len(rfilter),len(phifilter),len(zfilter)))
    rhomean = np.zeros((lent,len(rfilter),len(phifilter),len(zfilter)))

    for tt in range(0, lent):

        arx = format(25*tt, "04")
        Msim[tt], Rsim[tt], Fsim[tt], rhomean[tt], r, phi, z, rfilter, phifilter, zfilter = readh5files(direc+'stresses_sim_res_'+res+'-'+arx+'.h5')

    cellsize = (r[1]-r[0]+(r[-1]+r[0])*0.5*(phi[1]-phi[0])+z[1]-z[0])/3

    filtersize = Sf*cellsize

    alpha, beta, gamma = read_coeffs(res)

    alphatrace = alpha[0]+alpha[3]+alpha[5]

    energy_mod = energy_mod_space(alphatrace,rfilter,len(phifilter),len(zfilter),factor_c,t, filtersize, energy_ini,res, factor_en)

    Mmod = np.zeros((lent, 6, len(rfilter), len(phifilter), len(zfilter)))
    Rmod = np.zeros((lent, 6, len(rfilter), len(phifilter), len(zfilter)))
    Fmod = np.zeros((lent, 3, len(rfilter), len(phifilter), len(zfilter)))


    for i in range(0,6):
        Mmod[:,i,:,:,:] = alpha[i]*energy_mod[:,:,:,:]
        Rmod[:,i,:,:,:] = beta[i]*energy_mod[:,:,:,:]/rhomean[:,:,:,:]

    for j in range(0,3):
        Fmod[:,j,:,:,:] = gamma[j]*energy_mod[:,:,:,:]/rhomean[:,:,:,:]**(0.5)

    l2norm = comparison_stresses(Msim, Rsim, Fsim, Mmod, Rmod, Fmod)

    return l2norm

#===========================================================================================

def fun_minimize_M(x):

    factor_c = x
    energy_ini = 7e-31
    factor_en = 10
    lambda_mri = 33300

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_sim_test/"

    Sf = int(sys.argv[1])

    lenphi = 400

    if lenphi == 400 :

        lenr = 100
        lenz = 100


    elif lenphi == 240 :

        lenr = 60
        lenz = 60

    elif lenphi == 304 :

        lenr = 76
        lenz = 76

    res = str(lenr)+'_'+str(lenphi)+'_'+str(lenz)
    direc = path_data+'res_'+res+'/Sf_'+str(Sf)+'/'
    t_final = 30
    lent = 61
    t = np.linspace(0,t_final,lent)
    inivars = readh5files(direc+'stresses_sim_res_'+res+'-0000.h5')

    rfilter = inivars[7]
    phifilter = inivars[8]
    zfilter = inivars[9]

    Msim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Rsim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Fsim = np.zeros((lent,3,len(rfilter),len(phifilter),len(zfilter)))
    rhomean = np.zeros((lent,len(rfilter),len(phifilter),len(zfilter)))

    for tt in range(0, lent):

        arx = format(25*tt, "04")
        Msim[tt], Rsim[tt], Fsim[tt], rhomean[tt], r, phi, z, rfilter, phifilter, zfilter = readh5files(direc+'stresses_sim_res_'+res+'-'+arx+'.h5')

    cellsize = (r[1]-r[0]+(r[-1]+r[0])*0.5*(phi[1]-phi[0])+z[1]-z[0])/3

    filtersize = Sf*cellsize
    scale = min(filtersize,lambda_mri)

    alpha, beta, gamma = read_coeffs(res)
    alphatrace = alpha[0]+alpha[3]+alpha[5]

    energy_mod = energy_mod_space(alphatrace, rfilter,len(phifilter),len(zfilter),factor_c,t, scale, energy_ini,res, factor_en,rhomean[0,:,:,:])

    Mmod = np.zeros((lent, 6, len(rfilter), len(phifilter), len(zfilter)))

    for i in range(0,6):
        Mmod[:,i,:,:,:] = alpha[i]*energy_mod[:,:,:,:]

    l2norm = comparison_stress(Msim,Mmod, t, 6)

    return l2norm

#===========================================================================================

def fun_minimize_R(x):

    factor_c = x
    energy_ini = 7e-31
    factor_en = 10
    lambda_mri = 33300

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_sim_test/"

    Sf = int(sys.argv[1])

    lenphi = 400

    if lenphi == 400 :

        lenr = 100
        lenz = 100


    elif lenphi == 240 :

        lenr = 60
        lenz = 60

    elif lenphi == 304 :

        lenr = 76
        lenz = 76

    res = str(lenr)+'_'+str(lenphi)+'_'+str(lenz)
    direc = path_data+'res_'+res+'/Sf_'+str(Sf)+'/'
    t_final = 30
    lent = 61
    t = np.linspace(0,t_final,lent)
    inivars = readh5files(direc+'stresses_sim_res_'+res+'-0000.h5')

    rfilter = inivars[7]
    phifilter = inivars[8]
    zfilter = inivars[9]

    Msim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Rsim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Fsim = np.zeros((lent,3,len(rfilter),len(phifilter),len(zfilter)))
    rhomean = np.zeros((lent,len(rfilter),len(phifilter),len(zfilter)))

    for tt in range(0, lent):

        arx = format(25*tt, "04")
        Msim[tt], Rsim[tt], Fsim[tt], rhomean[tt], r, phi, z, rfilter, phifilter, zfilter = readh5files(direc+'stresses_sim_res_'+res+'-'+arx+'.h5')

    cellsize = (r[1]-r[0]+(r[-1]+r[0])*0.5*(phi[1]-phi[0])+z[1]-z[0])/3

    filtersize = Sf*cellsize

    scale =min(filtersize,lambda_mri)

    alpha, beta, gamma = read_coeffs(res)

    alphatrace = alpha[0]+alpha[3]+alpha[5]

    energy_mod = energy_mod_space(alphatrace,rfilter,len(phifilter),len(zfilter),factor_c,t, scale, energy_ini,res, factor_en,rhomean[0,:,:,:])

    Rmod = np.zeros((lent, 6, len(rfilter), len(phifilter), len(zfilter)))

    for i in range(0,6):
        Rmod[:,i,:,:,:] = beta[i]*energy_mod[:,:,:,:]/rhomean[:,:,:,:]

    l2norm = comparison_stress(Rsim,Rmod,t,6)

    return l2norm

#===========================================================================================

def fun_minimize_F(x):

    factor_c = x
    energy_ini = 7e-31
    factor_en = 10
    lambda_mri = 33300

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_sim_test/"

    Sf = int(sys.argv[1])

    lenphi = 400

    if lenphi == 400 :

        lenr = 100
        lenz = 100


    elif lenphi == 240 :

        lenr = 60
        lenz = 60

    elif lenphi == 304 :

        lenr = 76
        lenz = 76

    res = str(lenr)+'_'+str(lenphi)+'_'+str(lenz)
    direc = path_data+'res_'+res+'/Sf_'+str(Sf)+'/'
    t_final = 30
    lent = 61
    t = np.linspace(0,t_final,lent)
    inivars = readh5files(direc+'stresses_sim_res_'+res+'-0000.h5')

    rfilter = inivars[7]
    phifilter = inivars[8]
    zfilter = inivars[9]

    Msim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Rsim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Fsim = np.zeros((lent,3,len(rfilter),len(phifilter),len(zfilter)))
    rhomean = np.zeros((lent,len(rfilter),len(phifilter),len(zfilter)))

    for tt in range(0, lent):

        arx = format(25*tt, "04")
        Msim[tt], Rsim[tt], Fsim[tt], rhomean[tt], r, phi, z, rfilter, phifilter, zfilter = readh5files(direc+'stresses_sim_res_'+res+'-'+arx+'.h5')

    cellsize = (r[1]-r[0]+(r[-1]+r[0])*0.5*(phi[1]-phi[0])+z[1]-z[0])/3

    filtersize = Sf*cellsize

    scale = min(filtersize,lambda_mri)

    alpha, beta, gamma = read_coeffs(res)

    alphatrace = alpha[0]+alpha[3]+alpha[5]

    energy_mod = energy_mod_space(alphatrace,rfilter,len(phifilter),len(zfilter),factor_c,t, scale, energy_ini,res, factor_en,rhomean[0,:,:,:])

    Fmod = np.zeros((lent, 3, len(rfilter), len(phifilter), len(zfilter)))

    for j in range(0,3):
        Fmod[:,j,:,:,:] = gamma[j]*energy_mod[:,:,:,:]/rhomean[:,:,:,:]**(0.5)

    l2norm = comparison_stress(Fsim,Fmod,t,3)

    return l2norm

#===========================================================================================

def plot_norml2(factor_carr,Sf, path):

    l2cont = np.zeros((3,len(factor_carr)))

    for k in range(0,len(factor_carr)):
        l2cont[0,k] = fun_minimize_M(factor_carr[k])
        l2cont[1,k] = fun_minimize_R(factor_carr[k])
        l2cont[2,k] = fun_minimize_F(factor_carr[k])

    fig, ax = plt.subplots(1,3, sharex = 'all', sharey = 'all', figsize = (15,4))

    for i in range(0,3):
        ax[i].plot(factor_carr,l2cont[i,:], color = 'blue')
        ax[i].set_xlabel('C')

    ax[0].set_ylabel('M')
    ax[1].set_ylabel('R')
    ax[2].set_ylabel('F')
    plt.savefig(path+'/l2norm_Sf_'+str(Sf)+'.pdf')
    return()

#===========================================================================================

def growth_norml2(e0, k0,Sf, path):

    l2cont = np.zeros((len(e0), len(k0)))

    for k in range(0,len(e0)):
        for j in range(0,len(k0)):
            l2cont[k,j] = fun_min_growth(e0[k],k0[j])

    print('MIN L2 NORM  :', np.amin(l2cont[:,:]))
    fig, ax = plt.subplots()
    c = ax.pcolor(e0,k0, l2cont[:,:],norm = LogNorm(vmin=np.min(l2cont[:,:]), vmax = np.max(l2cont[:,:])), cmap = 'viridis')
    cbar=fig.colorbar(c, ax=ax, label = r'$L_2$')
    cbar.ax.tick_params(labelsize=12)
    ax.set_yscale('log')
    ax.set_ylabel(r'$K_0$')
    ax.set_xscale('log')
    ax.set_xlabel(r'$e_{\rm T}(0)$')
    plt.savefig(path+'/l2norm_growth_Sf_'+str(Sf)+'.pdf')
    ind = np.where(l2cont == np.min(l2cont))
    print('index :', ind)
    inde0 = ind[0]
    indk0 = ind[1]
    e0opt = e0[inde0[0]]
    k0opt = k0[indk0[0]]
    l2min = l2cont[inde0,indk0]
    print('e0opt     = ', e0opt)
    print('k0opt     = ', k0opt)
    print('L2 norm min  = ', l2min)

    return e0opt, k0opt, l2min

#===========================================================================================

def main():

    Sf = int(sys.argv[1])
    plot = True

    lenphi = 400

    if lenphi == 400 :

        lenr = 100
        lenz = 100

    elif lenphi == 240 :

        lenr = 60
        lenz = 60

    elif lenphi == 304 :

        lenr = 76
        lenz = 76

    res = str(lenr)+'_'+str(lenphi)+'_'+str(lenz)

    #Initial guesses for C:

    x0 = 10

    #bounds for the parameters:
    bounds = ((5,15),)

    test_path = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/test_l2norm/res_"+res+"/b0z-4.6e13/Sf_"+str(Sf)

    try:
        os.makedirs(test_path)
    except OSError as error:
        print(error)

    # plot :
    if plot:
        factor_c = np.linspace(10,15,30)

        plot_norml2(factor_c,Sf, test_path)

        exit

  #WITH STRESS M

    sol_M = minimize(fun_minimize_M, x0, bounds = bounds)

    print('OPTIMAL C FOR M  = ', sol_M.x)
    l2norm_opt_M = fun_minimize_M(sol_M.x)

    with open(test_path+'/l2normMsatSf'+str(Sf)+'_facC.txt', 'w') as f:
        f.write('# L_2 NORM \t FACTOR C  # \n')
        f.write('%.10f\t%.10f\n'%(l2norm_opt_M,sol_M.x[0]))


    #WITH STRESS R

    sol_R = minimize(fun_minimize_R, x0, bounds = bounds)

    print('OPTIMAL C FOR R  = ', sol_R.x)
    l2norm_opt_R = fun_minimize_R(sol_R.x)

    with open(test_path+'/l2normRsatSf'+str(Sf)+'_facC.txt', 'w') as f:
        f.write('# L_2 NORM \t FACTOR C  # \n')
        f.write('%.10f\t%.10f\n'%(l2norm_opt_R,sol_R.x[0]))


    #WITH STRESS F

    sol_F = minimize(fun_minimize_F, x0, bounds = bounds)

    print('OPTIMAL C FOR F = ', sol_F.x)
    l2norm_opt_F = fun_minimize_F(sol_F.x)

    with open(test_path+'/l2normFsatSf'+str(Sf)+'_facC.txt', 'w') as f:
        f.write('# L_2 NORM \t FACTOR C  # \n')
        f.write('%.10f\t%.10f\n'%(l2norm_opt_F,sol_F.x[0]))

    return()


def main2():

    Sf = int(sys.argv[1])

    lenphi = 400

    if lenphi == 400 :

        lenr = 100
        lenz = 100


    elif lenphi == 240 :

        lenr = 60
        lenz = 60

    elif lenphi == 304 :

        lenr = 76
        lenz = 76

    res = str(lenr)+'_'+str(lenphi)+'_'+str(lenz)
    test_path = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/test_l2norm/res_"+res+"/Sf_"+str(Sf)

    try:
        os.makedirs(test_path)
    except OSError as error:
        print(error)

    #Initial guesses for e0 and K0:

    x0 = [5e-31,2000]

    e0 = np.logspace(-33,-30,50)
    k0 = np.logspace(1,4,50)

#    e0opt, k0opt, norml2min = growth_norml2(e0,k0,Sf,test_path)
    #bounds for the parameters:
    bounds = ((1e-33,1e-31),(100,10000),)

  #WITH STRESS M

    sol_M = minimize(fun_min_growth_M, x0, bounds = bounds)

    print('OPTIMAL C FOR M  = ', sol_M.x)
    l2norm_opt_M = fun_min_growth_M(sol_M.x)

    with open(test_path+'/l2norm_M_growth_Sf_'+str(Sf)+'.txt', 'w') as f:
        f.write('# L_2 NORM \t FACTOR C  # \n')
        f.write('%.10f\t%.10f\n'%(l2norm_opt_M,sol_M.x[0]))


    #WITH STRESS R

    sol_R = minimize(fun_min_growth_R, x0, bounds = bounds)

    print('OPTIMAL C FOR R  = ', sol_R.x)
    l2norm_opt_R = fun_min_growth_R(sol_R.x)

    with open(test_path+'/l2norm_R_growth_Sf_'+str(Sf)+'.txt', 'w') as f:
        f.write('# L_2 NORM \t FACTOR C  # \n')
        f.write('%.10f\t%.10f\n'%(l2norm_opt_R,sol_R.x[0]))


    #WITH STRESS F

  #  sol_F = minimize(fun_min_growth_F, x0, bounds = bounds)

  #  print('OPTIMAL C FOR F = ', sol_F.x)
  #  l2norm_opt_F = fun_min_growth_F(sol_F.x)

  #  with open(test_path+'/l2norm_F_growth_Sf_'+str(Sf)+'.txt', 'w') as f:
  #      f.write('# L_2 NORM \t FACTOR C  # \n')
  #      f.write('%.10f\t%.10f\n'%(l2norm_opt_F,sol_F.x[0]))

    #norml2min = fun_min_growth(sol.x)
    #Copt = 0.25
    #e0opt= sol.x[0]
    #k0opt = sol.x[1]

    return()

main()
