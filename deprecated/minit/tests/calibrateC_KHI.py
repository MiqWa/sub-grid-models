#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 12 17:09:16 2022

@author: miquelmiravet
"""

#####################################
#                                   #
#            MODEL TEST             #
#                                   #
#####################################

"""
We're going to test the performance of our model by computing the energy of the
stresses and comparing it to the one from our model.

"""
import os
import numpy as np
from scipy.optimize import minimize
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy.ctypeslib as npct
from ctypes import c_int, c_double
import h5py
from scipy.stats import pearsonr
from scipy.linalg import norm

#=================================================================================
array_3d_float = npct.ndpointer(dtype=np.float64, ndim=3, flags='CONTIGUOUS')
array_1d_float = npct.ndpointer(dtype=np.float64, ndim=1, flags='CONTIGUOUS')

libenergyev = npct.load_library("/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/energy_dens_ev/KHI/libenergyevkhi", ".")
libenergyev.energy_ev.restype = None
libenergyev.energy_ev.argtypes = [c_double, c_int,c_double,c_double, array_1d_float]
#=================================================================================

def readh5files(arx):

    print('DATA FROM FILE:      ',arx)

    with h5py.File(arx, "r") as f:
    # List all groups
        print("Keys: %s" % f.keys())
        a_group_key = list(f.keys())

        Sf = np.array(f.get('S_f'))
        x = np.array(f.get('x'))
        y = np.array(f.get('y'))
        z = np.array(f.get('z'))
        xf = np.array(f.get('x_f'))
        yf = np.array(f.get('y_f'))
        zf = np.array(f.get('z_f'))
        rho = np.array(f.get('rho_mean'))
        Mxx = np.array(f.get('stress_Mxx'))
        Mxy = np.array(f.get('stress_Mxy'))
        Mxz = np.array(f.get('stress_Mxz'))
        Myy = np.array(f.get('stress_Myy'))
        Myz = np.array(f.get('stress_Myz'))
        Mzz = np.array(f.get('stress_Mzz'))
        Rxx = np.array(f.get('stress_Rxx'))
        Rxy = np.array(f.get('stress_Rxy'))
        Rxz = np.array(f.get('stress_Rxz'))
        Ryy = np.array(f.get('stress_Ryy'))
        Ryz = np.array(f.get('stress_Ryz'))
        Rzz = np.array(f.get('stress_Rzz'))
        Fyz = np.array(f.get('stress_Fyz'))
        Fxy = np.array(f.get('stress_Fxy'))
        Fxz = np.array(f.get('stress_Fxz'))

        time = np.array(f.get('time'))

        f.close()

    M = np.array([Mxx,Mxy,Mxz,Myy,Myz,Mzz])
    R = np.array([Rxx,Rxy,Rxz,Ryy,Ryz,Rzz])
    F = np.array([Fxy,Fxz,Fyz])

    print('Dimensions of the grid     :',len(x)," x ",len(y)," x ",len(z))
    print('Time           :',time)

    return [M, R, F, rho, x, y, z, xf, yf, zf]

#===========================================================================================

def read_coeffs(res):

    path = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/coefficients_r0/coeffs-KHI-final/'

    alpha = np.loadtxt(path+'alphaR_r0_av.txt')

    beta = np.loadtxt(path+'betaR_r0_av.txt')

    gamma = np.loadtxt(path+'gammaR_r0_av.txt')

    return alpha, beta, gamma

#===========================================================================================

def energy_ev(v0, scale,al,t, factor_c, energy_ini,rho):

    gamma_khi = 1/al*v0*0.1425
    print('gamma KHI:',gamma_khi)

    std_fact= factor_c/(rho**(0.5)*scale)*0.1425
    print('Std : ', std_fact)
    timestep= 5e-5
    tfinal = t[-1]
    lent = int(round((tfinal)/timestep))+1

    energy_t = np.zeros(lent)
    energy_t[0] = energy_ini

    libenergyev.energy_ev_khi(std_fact,lent,timestep,gamma_khi, energy_t)

    ratio = int((lent-1)/(len(t)-1))
    energy_model = np.zeros(len(t))

    for i in range(0,len(t)-1):
        energy_model[i] = energy_t[ratio*i]

    energy_model[-1] = energy_t[-1]

    return energy_model

#===========================================================================================

def energy_mod_space(v0, lenxf,lenyf,lenzf,scale,al,t, factor_c, energy_ini,rho):

    energy_mod = np.zeros((len(t), lenxf, lenyf, lenzf))

    for i in range(0,lenxf):

        for j in range(0,lenyf):

            for k in range(0,lenzf):

                energy_mod[:,i,j,k]= energy_ev(v0, scale,al,t, factor_c, energy_ini,rho[i,j,k])

    return energy_mod

#===========================================================================================

def comparison_stress(sim,mod,t,ind):

    t = t[ind:]
    sim = sim[ind:]
    mod = mod[ind:]

    l2norm = np.zeros(len(t))

    for tt in range(0,len(t)):
        integrand = (sim[tt,:]-mod[tt,:])**2
        mean_har = 0.5*(1/np.sum(mod[tt,:]**2)+1/np.sum(sim[tt,:]**2))
        l2norm[tt] = np.sqrt(np.sum(integrand)*mean_har)

    l2norm_tmean = (np.sum(l2norm**2)/len(t))**(0.5)

    return l2norm_tmean

#===========================================================================================

def comparison_stress_diag(sim,mod,t, ind, comp):

    if comp == 6:
        sim = np.delete(sim, [2,4,5], axis = 1)
        mod = np.delete(mod, [2,4,5], axis = 1)

    t = t[ind:]
    sim = sim[ind:]
    mod = mod[ind:]

    l2norm = np.zeros(len(t))

    for tt in range(0,len(t)):
        integrand = (sim[tt,:]-mod[tt,:])**2
        mean_har = 0.5*(1/np.sum(mod[tt,:]**2)+1/np.sum(sim[tt,:]**2))
        l2norm[tt] = np.sqrt(np.sum(integrand)*mean_har)

    l2norm_tmean = (np.sum(l2norm**2)/len(t))**(0.5)

    return l2norm_tmean

#===========================================================================================

def fun_minimize_M(x):

    factor_c = x
    energy_ini = 5e-12
    al = 0.01
    lambda_khi = al/0.1425
    v0 = 0.5

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_sim_test/"

    Sf = int(sys.argv[1])

    len = 512

    res = str(len)+'_'+str(len)+'_'+str(len)
    direc = path_data+'res_'+res+'/CVP--bx1e-3--512/Sf_'+str(Sf)+'/'
    t_final = 40
    lent = 201
    t = np.linspace(0,t_final,lent)
    inivars = readh5files(direc+'stresses_sim_res_'+res+'-0000.h5')

    xfilter = inivars[7]
    yfilter = inivars[8]
    zfilter = inivars[9]

    Msim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Rsim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Fsim = np.zeros((lent,3,len(rfilter),len(phifilter),len(zfilter)))
    rhomean = np.zeros((lent,len(rfilter),len(phifilter),len(zfilter)))

    for tt in range(0, lent):

        arx = format(int(10*t[tt]), "04")
        Msim[tt], Rsim[tt], Fsim[tt], rhomean[tt], x, y, z, xfilter, yfilter, zfilter = readh5files(direc+'stresses_sim_res_'+res+'-'+arx+'.h5')

    cellsize = x[1]-x[0]

    filtersize = Sf*cellsize
    scale = min(filtersize,lambda_khi)

    alphaKHI, betaKHI, gammaKHI = read_coeffs(res)

    energyKHI = energy_mod_space(v0, len(xfilter),len(yfilter),len(zfilter),scale,al,t, factor_c, energy_ini,rho[0,:,:,:])

    Mmod = np.zeros((lent, 6, len(xfilter), len(yfilter), len(zfilter)))

    for i in range(0,6):
        Mmod[:,i,:,:,:] = alphaKHI[i]*energyKHI[:,:,:,:]

    l2norm = comparison_stress(Msim,Mmod, t,ind=0)

    return l2norm

#===========================================================================================

def fun_minimize_M_diag(x):

    factor_c = x
    energy_ini = 5e-12
    al = 0.01
    lambda_khi = al/0.1425
    v0 = 0.5

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_sim_test/"

    Sf = int(sys.argv[1])

    len = 512

    res = str(len)+'_'+str(len)+'_'+str(len)
    direc = path_data+'res_'+res+'/CVP--bx1e-3--512/Sf_'+str(Sf)+'/'
    t_final = 40
    lent = 201
    t = np.linspace(0,t_final,lent)
    inivars = readh5files(direc+'stresses_sim_res_'+res+'-0000.h5')

    xfilter = inivars[7]
    yfilter = inivars[8]
    zfilter = inivars[9]

    Msim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Rsim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Fsim = np.zeros((lent,3,len(rfilter),len(phifilter),len(zfilter)))
    rhomean = np.zeros((lent,len(rfilter),len(phifilter),len(zfilter)))

    for tt in range(0, lent):

        arx = format(int(10*t[tt]), "04")
        Msim[tt], Rsim[tt], Fsim[tt], rhomean[tt], x, y, z, xfilter, yfilter, zfilter = readh5files(direc+'stresses_sim_res_'+res+'-'+arx+'.h5')

    cellsize = x[1]-x[0]

    filtersize = Sf*cellsize
    scale = min(filtersize,lambda_khi)

    alphaKHI, betaKHI, gammaKHI = read_coeffs(res)

    energyKHI = energy_mod_space(v0, len(xfilter),len(yfilter),len(zfilter),scale,al,t, factor_c, energy_ini,rho[0,:,:,:])

    Mmod = np.zeros((lent, 6, len(xfilter), len(yfilter), len(zfilter)))

    for i in range(0,6):
        Mmod[:,i,:,:,:] = alphaKHI[i]*energyKHI[:,:,:,:]

    l2norm = comparison_stress_diag(Msim,Mmod, t,ind=0, 6)

    return l2norm

#===========================================================================================

def fun_minimize_R(x):

    factor_c = x
    energy_ini = 5e-12
    al = 0.01
    lambda_khi = al/0.1425
    v0 = 0.5

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_sim_test/"

    Sf = int(sys.argv[1])

    len = 512

    res = str(len)+'_'+str(len)+'_'+str(len)
    direc = path_data+'res_'+res+'/CVP--bx1e-3--512/Sf_'+str(Sf)+'/'
    t_final = 40
    lent = 201
    t = np.linspace(0,t_final,lent)
    inivars = readh5files(direc+'stresses_sim_res_'+res+'-0000.h5')

    xfilter = inivars[7]
    yfilter = inivars[8]
    zfilter = inivars[9]

    Msim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Rsim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Fsim = np.zeros((lent,3,len(rfilter),len(phifilter),len(zfilter)))
    rhomean = np.zeros((lent,len(rfilter),len(phifilter),len(zfilter)))

    for tt in range(0, lent):

        arx = format(int(10*t[tt]), "04")
        Msim[tt], Rsim[tt], Fsim[tt], rhomean[tt], x, y, z, xfilter, yfilter, zfilter = readh5files(direc+'stresses_sim_res_'+res+'-'+arx+'.h5')

    cellsize = x[1]-x[0]

    filtersize = Sf*cellsize
    scale = min(filtersize,lambda_khi)

    alphaKHI, betaKHI, gammaKHI = read_coeffs(res)

    energyKHI = energy_mod_space(v0, len(xfilter),len(yfilter),len(zfilter),scale,al,t, factor_c, energy_ini,rho[0,:,:,:])

    Rmod = np.zeros((lent, 6, len(xfilter), len(yfilter), len(zfilter)))

    for i in range(0,6):
        Rmod[:,i,:,:,:] = betaKHI[i]*energyKHI[:,:,:,:]/rhomean[:,:,:,:]

    l2norm = comparison_stress(Rsim,Rmod,t,ind=0)

    return l2norm

#===========================================================================================

def fun_minimize_R_diag(x):

    factor_c = x
    energy_ini = 5e-12
    al = 0.01
    lambda_khi = al/0.1425
    v0 = 0.5

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_sim_test/"

    Sf = int(sys.argv[1])

    len = 512

    res = str(len)+'_'+str(len)+'_'+str(len)
    direc = path_data+'res_'+res+'/CVP--bx1e-3--512/Sf_'+str(Sf)+'/'
    t_final = 40
    lent = 201
    t = np.linspace(0,t_final,lent)
    inivars = readh5files(direc+'stresses_sim_res_'+res+'-0000.h5')

    xfilter = inivars[7]
    yfilter = inivars[8]
    zfilter = inivars[9]

    Msim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Rsim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Fsim = np.zeros((lent,3,len(rfilter),len(phifilter),len(zfilter)))
    rhomean = np.zeros((lent,len(rfilter),len(phifilter),len(zfilter)))

    for tt in range(0, lent):

        arx = format(int(10*t[tt]), "04")
        Msim[tt], Rsim[tt], Fsim[tt], rhomean[tt], x, y, z, xfilter, yfilter, zfilter = readh5files(direc+'stresses_sim_res_'+res+'-'+arx+'.h5')

    cellsize = x[1]-x[0]

    filtersize = Sf*cellsize
    scale = min(filtersize,lambda_khi)

    alphaKHI, betaKHI, gammaKHI = read_coeffs(res)

    energyKHI = energy_mod_space(v0, len(xfilter),len(yfilter),len(zfilter),scale,al,t, factor_c, energy_ini,rho[0,:,:,:])

    Rmod = np.zeros((lent, 6, len(xfilter), len(yfilter), len(zfilter)))

    for i in range(0,6):
        Rmod[:,i,:,:,:] = betaKHI[i]*energyKHI[:,:,:,:]/rhomean[:,:,:,:]
    factor_c = x
    energy_ini = 5e-12
    al = 0.01
    lambda_khi = al/0.1425
    v0 = 0.5

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_sim_test/"

    Sf = int(sys.argv[1])

    len = 512

    res = str(len)+'_'+str(len)+'_'+str(len)
    direc = path_data+'res_'+res+'/CVP--bx1e-3--512/Sf_'+str(Sf)+'/'
    t_final = 40
    lent = 201
    t = np.linspace(0,t_final,lent)
    inivars = readh5files(direc+'stresses_sim_res_'+res+'-0000.h5')

    xfilter = inivars[7]
    yfilter = inivars[8]
    zfilter = inivars[9]

    Msim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Rsim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Fsim = np.zeros((lent,3,len(rfilter),len(phifilter),len(zfilter)))
    rhomean = np.zeros((lent,len(rfilter),len(phifilter),len(zfilter)))

    for tt in range(0, lent):

        arx = format(int(10*t[tt]), "04")
        Msim[tt], Rsim[tt], Fsim[tt], rhomean[tt], x, y, z, xfilter, yfilter, zfilter = readh5files(direc+'stresses_sim_res_'+res+'-'+arx+'.h5')

    cellsize = x[1]-x[0]

    filtersize = Sf*cellsize
    scale = min(filtersize,lambda_khi)

    alphaKHI, betaKHI, gammaKHI = read_coeffs(res)

    energyKHI = energy_mod_space(v0, len(xfilter),len(yfilter),len(zfilter),scale,al,t, factor_c, energy_ini,rho[0,:,:,:])

    Rmod = np.zeros((lent, 6, len(xfilter), len(yfilter), len(zfilter)))

    for i in range(0,6):
        Rmod[:,i,:,:,:] = betaKHI[i]*energyKHI[:,:,:,:]/rhomean[:,:,:,:]

    l2norm = comparison_stress_diag(Rsim,Rmod,t,ind=0,6)

    return l2norm

#===========================================================================================

def fun_minimize_F(x):

    factor_c = x
    energy_ini = 5e-12
    al = 0.01
    lambda_khi = al/0.1425
    v0 = 0.5

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_sim_test/"

    Sf = int(sys.argv[1])

    len = 512

    res = str(len)+'_'+str(len)+'_'+str(len)
    direc = path_data+'res_'+res+'/CVP--bx1e-3--512/Sf_'+str(Sf)+'/'
    t_final = 40
    lent = 201
    t = np.linspace(0,t_final,lent)
    inivars = readh5files(direc+'stresses_sim_res_'+res+'-0000.h5')

    xfilter = inivars[7]
    yfilter = inivars[8]
    zfilter = inivars[9]

    Msim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Rsim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Fsim = np.zeros((lent,3,len(rfilter),len(phifilter),len(zfilter)))
    rhomean = np.zeros((lent,len(rfilter),len(phifilter),len(zfilter)))

    for tt in range(0, lent):

        arx = format(int(10*t[tt]), "04")
        Msim[tt], Rsim[tt], Fsim[tt], rhomean[tt], x, y, z, xfilter, yfilter, zfilter = readh5files(direc+'stresses_sim_res_'+res+'-'+arx+'.h5')

    cellsize = x[1]-x[0]

    filtersize = Sf*cellsize
    scale = min(filtersize,lambda_khi)

    alphaKHI, betaKHI, gammaKHI = read_coeffs(res)

    energyKHI = energy_mod_space(v0, len(xfilter),len(yfilter),len(zfilter),scale,al,t, factor_c, energy_ini,rho[0,:,:,:])

    Fmod = np.zeros((lent, 3, len(xfilter), len(yfilter), len(zfilter)))

    for j in range(0,3):
        Fmod[:,j,:,:,:] = gammaKHI[j]*energyKHI[:,:,:,:]/rhomean[:,:,:,:]**(0.5)

    l2norm = comparison_stress(Fsim,Fmod,t,ind=0)

    return l2norm

#===========================================================================================

def plot_norml2(factor_carr,Sf, path):

    l2cont = np.zeros((3,len(factor_carr)))

    for k in range(0,len(factor_carr)):
        l2cont[0,k] = fun_minimize_M(factor_carr[k])
        l2cont[1,k] = fun_minimize_R(factor_carr[k])
        l2cont[2,k] = fun_minimize_F(factor_carr[k])

    fig, ax = plt.subplots(1,3, sharex = 'all', sharey = 'all', figsize = (15,4))

    for i in range(0,3):
        ax[i].plot(factor_carr,l2cont[i,:], color = 'blue')
        ax[i].set_xlabel('C')

    ax[0].set_ylabel('L2')
    ax[1].set_title('R')
    ax[2].set_title('F')
    ax[0].set_title('M')
#    ax[0].set_yscale('log')
#    ax[1].set_yscale('log')
#    ax[2].set_yscale('log')

    plt.savefig(path+'/l2norm_Sf_'+str(Sf)+'.pdf')

    l2diag = np.zeros((2,len(factor_carr)))

    for k in range(0,len(factor_carr)):
        l2diag[0,k] = fun_minimize_M_diag(factor_carr[k])
        l2diag[1,k] = fun_minimize_R_diag(factor_carr[k])

    fig, ax = plt.subplots(1,2, sharex = 'all', sharey = 'all', figsize = (8,4))

    for i in range(0,2):
        ax[i].plot(factor_carr,l2diag[i,:], color = 'blue')
        ax[i].set_xlabel('C')

    ax[0].set_ylabel('L2')
    ax[1].set_title('R')
    ax[0].set_title('M')
#    ax[0].set_yscale('log')
#    ax[1].set_yscale('log')

    plt.savefig(path+'/l2norm_diag_Sf_'+str(Sf)+'.pdf')

    minl2M = np.amin(l2cont[0])
    minl2R = np.amin(l2cont[1])

    indmin = np.where(l2cont[0] == minl2M)[0][0]
    copt = factor_carr[indmin]

    indexesM = []
    for i in range(0,len(l2cont[0])):
        if l2cont[0,i] > minl2M*(1.1):
            indexesM.append(i)

    cnomM = factor_carr[indexesM]

    cnomM = cnomM-copt
    cneg = cnomM[cnomM<0]
    cminM = np.amin(abs(cneg))
    cminM = -cminM+copt
    cpos = cnomM[cnomM>0]
    cmaxM = np.amin(cpos)
    cmaxM = cmaxM+copt

    with open(path+'/c_opt_interval'+str(Sf)+'_M.txt', 'w') as f:
        f.write('# C OPT \t C+ \t C- # \n')
        f.write('%.10f\t%.10f\t%.10f\n'%(copt,cmaxM,cminM))

    indmin = np.where(l2cont[1] == minl2R)[0][0]
    copt = factor_carr[indmin]

    indexesR = [i for i in range(0,len(l2cont[1])) if l2cont[1,i] > minl2R*(1.1)]

    cnomR = factor_carr[indexesR]

    cnomR = cnomR-copt
    cneg = cnomR[cnomR<0]
    cminR = np.amin(abs(cneg))
    cminR = -cminR+copt
    cpos = cnomR[cnomR>0]
    cmaxR = np.amin(cpos)
    cmaxR = cmaxR+copt

    with open(path+'/c_opt_interval'+str(Sf)+'_R.txt', 'w') as f:
        f.write('# C OPT \t C+ \t C- # \n')
        f.write('%.10f\t%.10f\t%.10f\n'%(copt,cmaxR,cminR))


    minl2Md = np.amin(l2diag[0])
    minl2Rd = np.amin(l2diag[1])

    indmind = np.where(l2diag[0] == minl2Md)[0][0]
    coptd = factor_carr[indmind]

    indexesMd = [i for i in range(0,len(l2diag[0])) if l2diag[0,i] > minl2Md*(1.1)]

    cnomM = factor_carr[indexesMd]

    cnomM = cnomM-coptd
    cneg = cnomM[cnomM<0]
    cminM = np.amin(abs(cneg))
    cminM = -cminM+coptd
    cpos = cnomM[cnomM>0]
    cmaxM = np.amin(cpos)
    cmaxM = cmaxM+coptd

    with open(path+'/c_opt_interval'+str(Sf)+'_diagM.txt', 'w') as f:
        f.write('# C OPT \t C+ \t C- # \n')
        f.write('%.10f\t%.10f\t%.10f\n'%(coptd,cmaxM,cminM))

    indmind = np.where(l2diag[1] == minl2Rd)[0][0]
    coptd = factor_carr[indmind]

    indexesRd = [i for i in range(0,len(l2diag[1])) if l2diag[1,i] > minl2Rd*(1.1)]

    cnomR = factor_carr[indexesRd]

    cnomR = cnomR-coptd
    cneg = cnomR[cnomR<0]
    cminR = np.amin(abs(cneg))
    cminR = -cminR+coptd
    cpos = cnomR[cnomR>0]
    cmaxR = np.amin(cpos)
    cmaxR = cmaxR+coptd

    with open(path+'/c_opt_interval'+str(Sf)+'_diagR.txt', 'w') as f:
        f.write('# C OPT \t C+ \t C- # \n')
        f.write('%.10f\t%.10f\t%.10f\n'%(coptd,cmaxR,cminR))

    return()

#===========================================================================================

def main():

    Sf = int(sys.argv[1])
    plot = True

    lenphi = 400

    if lenphi == 400 :

        lenr = 100
        lenz = 100

    elif lenphi == 240 :

        lenr = 60
        lenz = 60

    elif lenphi == 304 :

        lenr = 76
        lenz = 76

    res = str(lenr)+'_'+str(lenphi)+'_'+str(lenz)

    #Initial guesses for C:

    x0 = 3

    #bounds for the parameters:
    bounds = ((1,8),)

    test_path = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/test_khi/res_"+res+"/CVP--bx1e-3--512/Sf_"+str(Sf)

    try:
        os.makedirs(test_path)
    except OSError as error:
        print(error)

    # plot :
    if plot:
        factor_c = np.linspace(1,6,25)

        plot_norml2(factor_c,Sf, test_path)

        exit

  #WITH STRESS M

    sol_M = minimize(fun_minimize_M, x0, bounds = bounds)

    l2norm_opt_M = fun_minimize_M(sol_M.x)

    with open(test_path+'/l2normM--Sf'+str(Sf)+'_facC.txt', 'w') as f:
        f.write('# L_2 NORM \t FACTOR C  # \n')
        f.write('%.10f\t%.10f\n'%(l2norm_opt_M,sol_M.x[0]))

    sol_Mdiag = minimize(fun_minimize_M_diag, x0, bounds = bounds)

    l2normdiag_opt_M = fun_minimize_M_diag(sol_Mdiag.x)

    with open(test_path+'/l2norm_diag_M--Sf'+str(Sf)+'_facC.txt', 'w') as f:
        f.write('# L_2 NORM \t FACTOR C  # \n')
        f.write('%.10f\t%.10f\n'%(l2norm_opt_M,sol_M.x[0]))


    #WITH STRESS R

    sol_R = minimize(fun_minimize_R, x0, bounds = bounds)

    l2norm_opt_R = fun_minimize_R(sol_R.x)

    with open(test_path+'/l2normR--Sf'+str(Sf)+'_facC.txt', 'w') as f:
        f.write('# L_2 NORM \t FACTOR C  # \n')
        f.write('%.10f\t%.10f\n'%(l2norm_opt_R,sol_R.x[0]))

    sol_Rdiag = minimize(fun_minimize_R_diag, x0, bounds = bounds)

    l2normdiag_opt_R = fun_minimize_R_diag(sol_Rdiag.x)

    with open(test_path+'/l2norm_diag_R--Sf'+str(Sf)+'_facC.txt', 'w') as f:
        f.write('# L_2 NORM \t FACTOR C  # \n')
        f.write('%.10f\t%.10f\n'%(l2norm_opt_R,sol_R.x[0]))


    #WITH STRESS F

    sol_F = minimize(fun_minimize_F, x0, bounds = bounds)

    l2norm_opt_F = fun_minimize_F(sol_F.x)

    with open(test_path+'/l2normF--Sf'+str(Sf)+'_facC.txt', 'w') as f:
        f.write('# L_2 NORM \t FACTOR C  # \n')
        f.write('%.10f\t%.10f\n'%(l2norm_opt_F,sol_F.x[0]))

    return()


main()
