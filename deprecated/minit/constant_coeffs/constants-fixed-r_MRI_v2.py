#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 28 18:05:04 2021

@author: miquelmiravet

PABLO'S MODEL TESTING
"""
#############################################

# Let's model the stress tensors Mij, Rij and Tij as functions of the total energy density.
# First approach: Consider that the tensors are proportional to the energy density.

#############################################

import os
import numpy as np
import h5py
import csv
from statistics import stdev

def readh5files(arxiv):

    print('DATA FROM FILE:      ',arxiv)

    with h5py.File(arxiv, "r") as f:
    # List all groups
        print("Keys: %s" % f.keys())
        a_group_key = list(f.keys())
        for i in a_group_key:
            print(i)

        F9mean = f[a_group_key[0]][...]
        print('F9 stress shape    :', F9mean.shape)
        Fmean = f[a_group_key[1]][...]
        print('F stress shape    :', Fmean.shape)
        Fnew = np.array([Fmean[0,1],Fmean[0,2],Fmean[1,2]])
        Mmean = f[a_group_key[2]][...]
        print('M stress shape    :', Mmean.shape)
        Mnew = np.array([Mmean[0,0],Mmean[0,1], Mmean[0,2], Mmean[1,1], Mmean[1,2], Mmean[2,2]])
        Rmean = f[a_group_key[3]][...]
        print('R stress shape    :', Rmean.shape)
        Rnew = np.array([Rmean[0,0],Rmean[0,1], Rmean[0,2], Rmean[1,1], Rmean[1,2], Rmean[2,2]])
        r0 = f[a_group_key[4]][...]
        print('r0 shape    :', r0.shape)
        rhomean = f[a_group_key[5]][...]
        print('filtered rho shape    :', rhomean.shape)
        sigmaF = f[a_group_key[6]][...]
        print('sigma F shape    :', sigmaF.shape)
        sFnew = np.array([sigmaF[0,1], sigmaF[0,2], sigmaF[1,2]])
        sigmaF9 = f[a_group_key[7]][...]
        print('sigma F9 shape    :', sigmaF9.shape)
        sigmaM = f[a_group_key[8]][...]
        print('sigma M shape    :', sigmaM.shape)
        sMnew = np.array([sigmaM[0,0],sigmaM[0,1],sigmaM[0,2],sigmaM[1,1],sigmaM[1,2],sigmaM[2,2]])
        sigmaR = f[a_group_key[9]][...]
        print('sigma R shape    :', sigmaR.shape)
        sRnew = np.array([sigmaR[0,0],sigmaR[0,1],sigmaR[0,2],sigmaR[1,1],sigmaR[1,2],sigmaR[2,2]])
        sigma_rho= f[a_group_key[10]][...]
        print('sigma rho shape    :', sigma_rho.shape)
        time = f[a_group_key[11]][...]
        print('time shape    :', time.shape)
        f.close()
#    print('F9_rr    = ', sigmaF9[1,0])
    print('Evaluation of the filtered quantities at r0  =',r0)
    print('Time           =',time,' ms')

    return time, Mnew, Rnew, Fnew, sMnew**(0.5), sRnew**(0.5), sFnew**(0.5), rhomean, sigma_rho**(0.5), r0

def read_files_whole_box(arxiv):

    print('DATA FROM FILE:      ',arxiv)
    with h5py.File(arxiv, "r") as f:
        Mmean = np.array(f.get('Mmean'))
        Mnew = np.array([Mmean[0,0],Mmean[0,1], Mmean[0,2], Mmean[1,1], Mmean[1,2], Mmean[2,2]])
        Rmean = np.array(f.get('Rmean'))
        Rnew = np.array([Rmean[0,0],Rmean[0,1], Rmean[0,2], Rmean[1,1], Rmean[1,2], Rmean[2,2]])
        Fmean = np.array(f.get('Fmean'))
        Fnew = np.array([Fmean[0,1], Fmean[0,2], Fmean[1,2],])
        rhomean = np.array(f.get('rhomean'))
        sigmaM = np.array(f.get('sigma_M'))
        sMnew = np.array([sigmaM[0,0],sigmaM[0,1],sigmaM[0,2],sigmaM[1,1],sigmaM[1,2],sigmaM[2,2]])
        sigmaR = np.array(f.get('sigma_R'))
        sRnew = np.array([sigmaR[0,0],sigmaR[0,1],sigmaR[0,2],sigmaR[1,1],sigmaR[1,2],sigmaR[2,2]])
        sigmaF = np.array(f.get('sigma_F'))
        sFnew = np.array([sigmaF[0,1], sigmaF[0,2], sigmaF[1,2]])
        sigma_rho = np.array(f.get('sigma_rho'))
        time = np.array(f.get('time'))

    print('Time         = ',time,' ms')

    return time, Mnew, Rnew, Fnew, sMnew, sRnew, sFnew, rhomean, sigma_rho


def energy_dens(Rtensor,rho):

    energyMRI = 0.5*rho*(Rtensor[0]+Rtensor[3]-2*Rtensor[5])
    energyPI = 1.5*rho*Rtensor[5]
    return energyMRI, energyPI

def error_propagation(rho,err_rho,M,err_M,R,err_R,F,err_F,alphaMRI,betaMRI,energyMRI,energyPI,lent):

    traceR = R[0]+R[3]+R[5]
    e_alpha = np.zeros((6,lent))
    e_beta = np.zeros((6,lent))
    e_gamma = np.zeros((3,lent))
    e_enMRI = np.sqrt((0.5*err_rho[:]*(R[0,:]+R[3,:]-2*R[5,:]))**2+(0.5*rho[:]*err_R[1,:])**2+(0.5*rho[:]*err_R[3,:])**2+(rho[:]*err_R[5,:])**2)
    e_enPI = np.sqrt((1.5*R[5,:]*err_rho[:])**2+(1.5*rho*err_R[5,:])**2)

    for i in range(0,6):
        e_alpha[i,:] = ((err_M[i,:]/energyPI[:])**2+(alphaMRI[i]*e_enMRI[:]/energyPI[:])**2+((M[i,:]-alphaMRI[i]*energyMRI[:])*e_enPI/energyPI**2)**2)**(0.5)
        e_beta[i,:] = (((rho[:]*err_R[i,:])**2+(R[i,:]*err_rho[:])**2)/energyPI[:]**2+(betaMRI[i]*e_enMRI[:]/energyPI[:])**2+((rho[:]*R[i,:]-betaMRI[i]*energyMRI[:])*e_enPI[:]/energyPI[:]**2)**2)**(0.5)
    for j in range(0,3):
        e_gamma[j] = (((np.sqrt(rho[:])*err_F[j,:])**2+(F[j,:]*err_rho[:]/(2*np.sqrt(rho[:])))**2)/energyPI[:]**2+(F[j,:]*np.sqrt(rho[:])/energyPI[:]**2*e_enPI[:])**2)**(0.5)

    return e_alpha, e_beta, e_gamma

def constants_files_6comp(t,constant,econstant,constant_av,econstant_av,arxiu):

    with open(arxiu+'_r0.csv', 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        cols = zip(t,constant[0,:],constant[1,:],constant[2,:],constant[3,:],constant[4,:],constant[5,:])
        writer.writerows(cols)

    with open('sigma_'+arxiu+'_r0.csv', 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        cols = zip(t,econstant[0,:],econstant[1,:],econstant[2,:],econstant[3,:],econstant[4,:],econstant[5,:])
        writer.writerows(cols)

    with open(arxiu+'_r0_av.txt','w') as f:
        f.write('%f\t%f\t%f\t%f\t%f\t%f\n'%(constant_av[0],constant_av[1],constant_av[2],constant_av[3],constant_av[4],constant_av[5]))

    with open('sigma_'+arxiu+'_r0_av.txt','w') as f:
        f.write('%f\t%f\t%f\t%f\t%f\t%f\n'%(econstant_av[0],econstant_av[1],econstant_av[2],econstant_av[3],econstant_av[4],econstant_av[5]))

    return()

def constants_files_3comp(t,constant,econstant,constant_av,econstant_av,arxiu):

    with open(arxiu+'_r0.csv', 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        cols = zip(t,constant[0,:],constant[1,:],constant[2,:])
        writer.writerows(cols)

    with open('sigma_'+arxiu+'_r0.csv', 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        cols = zip(t,econstant[0,:],econstant[1,:],econstant[2,:])
        writer.writerows(cols)

    with open(arxiu+'_r0_av.txt','w') as f:
        f.write('%f\t%f\t%f\n'%(constant_av[0],constant_av[1],constant_av[2]))

    with open('sigma_'+arxiu+'_r0_av.txt','w') as f:
        f.write('%f\t%f\t%f\n'%(econstant_av[0],econstant_av[1],econstant_av[2]))
    return()

def energy_files(t,energy_dens):

    with open('energy_sim_r0.csv', 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        cols = zip(t,energy_dens[:])
        writer.writerows(cols)

    return()

#%%

def main():

    resu = ['100_400_100','60_240_60','76_304_76']
    ind_ini = 26
    told = np.linspace(0,30,61)
    t = told[1:]
    volume = False

    for j in range(0,3):

        print('RESOLUTION : ',resu[j])

        #stress tensors:

        F = np.zeros((3,len(told)))
        M = np.zeros((6,len(told)))
        R = np.zeros((6,len(told)))
        F9 = np.zeros((3,3,len(told)))
        sigmaF = np.zeros((3,len(told)))
        sigmaM = np.zeros((6,len(told)))
        sigmaR = np.zeros((6,len(told)))
        sigmaF9 = np.zeros((3,3,len(told)))
        rho = np.zeros(len(told))
        sigmarho = np.zeros(len(told))

        for x in range(0,len(told)):
            arx = format(25*x, "04")
            if volume == True:
                path = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_r0/res_'+resu[j]+'/volume_av/b0z-4.6e13/'
                files = 'stresses_sigma_whole_box-'+str(arx)+'.h5'
                filesdir = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/coefficients_r0/res_'+resu[j]+'/volume_av/b0z-4.6e13'
                path_energy = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/energy_dens/res_'+resu[j]+'/volume_av/b0z-4.6e13'

                listvar = read_files_whole_box(path+files)

                M[:,x] = listvar[1]
                R[:,x] = listvar[2]
                F[:,x] = listvar[3]
                sigmaM[:,x] = listvar[4]
                sigmaR[:,x] = listvar[5]
                sigmaF[:,x] = listvar[6]
                rho[x] = listvar[7]
                sigmarho[x] = listvar[8]

            else:
                path = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_r0/res_'+resu[j]+'/surface_av/b0z-4.6e13/'
                files = 'stresses_sigma_r0-'+str(arx)+'.h5'
                filesdir = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/coefficients_r0/res_'+resu[j]+'/surface_av/b0z-4.6e13/v2'
                path_energy = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/energy_dens/res_'+resu[j]+'/surface_av/b0z-4.6e13/v2'

                listvar = readh5files(path+files)

                M[:,x] = listvar[1]
                R[:,x] = listvar[2]
                F[:,x] = listvar[3]
                sigmaM[:,x] = listvar[4]
                sigmaR[:,x] = listvar[5]
                sigmaF[:,x] = listvar[6]
                rho[x] = listvar[7]
                sigmarho[x] = listvar[8]

        F = F[:,1:]
        M = M[:,1:]
        R = R[:,1:]
        sigmaF = sigmaF[:,1:]
        sigmaM = sigmaM[:,1:]
        sigmaR = sigmaR[:,1:]
        rho = rho[1:]
        sigmarho = sigmarho[1:]

        energyMRI,energyPI = energy_dens(R,rho)

        #constants:

        q = 1.25
        alphaMRI = np.zeros(6)
        alphaMRI[0] = (4-q)/q
        alphaMRI[3] = (4-q)/q
        alphaMRI[1] = -(4-q)/q

        betaMRI = np.zeros(6)
        betaMRI[0] = 1
        betaMRI[3] = 1
        betaMRI[1] = 1

        alphaPI = np.zeros((6,len(t)))
        betaPI = np.zeros((6,len(t)))
        gammaPI = np.zeros((3,len(t)))

        for i in range(0,6):
            alphaPI[i,:] = (M[i,:]-alphaMRI[i]*energyMRI[:])/energyPI[:]
            betaPI[i,:] = (rho*R[i,:]-betaMRI[i]*energyMRI[:])/energyPI[:]
        for i in range(0,3):
            gammaPI[i,:] = (rho)**(0.5)*F[i,:]/energyPI[:]

        #errors:

        e_alphaPI, e_betaPI, e_gammaPI = error_propagation(rho,sigmarho,M,sigmaM,R,sigmaR,F,sigmaF,alphaMRI,betaMRI,energyMRI,energyPI,len(t))

        #alphaPI = np.delete(alphaPI,(15,16,17,18,19,20),axis = 1)
        #betaPI = np.delete(betaPI,(15,16,17,18,19,20),axis = 1)
        #gammaPI = np.delete(gammaPI,(15,16,17,18,19,20),axis = 1)
        #e_alphaPI = np.delete(e_alphaPI,(15,16,17,18,19,20),axis = 1)
        #e_betaPI = np.delete(e_betaPI,(15,16,17,18,19,20),axis = 1)
        #e_gammaPI = np.delete(e_gammaPI,(15,16,17,18,19,20),axis = 1)
        #t = np.delete(t,(15,16,17,18,19,20))

        #averages:

        alpha_av = np.sum(alphaPI[:,ind_ini:]/e_alphaPI[:,ind_ini:]**2, axis = 1)/np.sum(1/e_alphaPI[:,ind_ini:]**2, axis = 1)
        beta_av = np.sum(betaPI[:,ind_ini:]/e_betaPI[:,ind_ini:]**2, axis = 1)/np.sum(1/e_betaPI[:,ind_ini:]**2, axis = 1)
        gamma_av = np.sum(gammaPI[:,ind_ini:]/e_gammaPI[:,ind_ini:]**2, axis = 1)/np.sum(1/e_gammaPI[:,ind_ini:]**2, axis = 1)

#        sist_err_alpha = np.mean(e_alphaR, axis = 1)
#        sist_err_beta = np.mean(e_betaR, axis = 1)
#        sist_err_gamma = np.mean(e_gammaR, axis = 1)

#        err_av_alpha = np.zeros(6)
#        err_av_beta = np.zeros(6)
#        err_av_gamma = np.zeros(3)

#        std_alpha = np.zeros(6)
#        std_beta = np.zeros(6)
#        std_gamma = np.zeros(3)

#        for i in range(0,6):
#            std_alpha[i] = stdev(alphaR[i,:])
#            std_beta[i] = stdev(betaR[i,:])
#            err_av_alpha[i] = (std_alpha[i]**2+sist_err_alpha[i]**2)**(0.5)
#            err_av_beta[i] = (std_beta[i]**2+sist_err_beta[i]**2)**(0.5)
#        for i in range(0,3):
#            std_gamma[i] = stdev(gammaR[i,:])
#            err_av_gamma[i] = (std_gamma[i]**2+sist_err_gamma[i]**2)**(0.5)


        err_av_alpha = (1/np.sum(1/e_alphaPI[:,ind_ini:]**2,axis = 1))**(0.5)
        err_av_beta = (1/np.sum(1/e_betaPI[:,ind_ini:]**2,axis = 1))**(0.5)
        err_av_gamma = (1/np.sum(1/e_gammaPI[:,ind_ini:]**2,axis = 1))**(0.5)

        #files:

        constants_files_6comp(t,alphaPI,e_alphaPI,alpha_av,err_av_alpha,'alphaPI')
        constants_files_6comp(t,betaPI,e_betaPI,beta_av,err_av_beta,'betaPI')
        constants_files_3comp(t,gammaPI,e_gammaPI,gamma_av,err_av_gamma,'gammaPI')

        try:
            os.makedirs(filesdir)
        except OSError as error:
            print(error)

        os.system('mv *.csv '+filesdir+'/.')
        os.system('mv *av.txt '+filesdir+'/.')

    return()




#%%

if __name__ == "__main__":
    main()
