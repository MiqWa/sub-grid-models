 #!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 16 13:46:41 2021

@author: miquelmiravet
"""

##################################################
#
#       ENERGY DENSITY OF TURBULENT STRESSES
#
##################################################

"""
    We are going to solve the evolution equations for the energy density in turbulent stresses
    and the energy density of the parasitic instabilities. To do so, we need an averaged value of the alpha coefficient and its trace, in
    order to get M_ij and therefore s_max = 0.2*B_0 = 0.2*sqrt(e_t*Tr(alpha_ij)). We also need the initial value for e_T, which is given by
    the initial value of 0.5*rho*traceR. e_pi(0) = 0. The source term S_TD depends on e_pi. tau_mri is constant over time.

"""

import os
import numpy as np
import h5py
import sys
from matplotlib import pyplot as plt
from matplotlib import rc
import math as m
import numpy.ctypeslib as npct
from numpy.ctypeslib import ndpointer
from ctypes import c_int, c_double
import statistics
import seaborn as sns

rc('text', usetex=True)
rc('font', family='serif')

#------------------------------------------------------------
# input type for the function
# must be a double array, with single dimension that is contiguous
array_1d_float = npct.ndpointer(dtype=np.float64, ndim=1, flags='CONTIGUOUS')

libenergyev = npct.load_library("KHI/libenergyevkhi.so", ".")
libenergyev.energy_ev_khi.restype = None
libenergyev.energy_ev_khi.argtypes = [c_double, c_int,c_double,c_double, array_1d_float]

#===============================================================================================

def readh5files(arx):

    print('DATA FROM FILE:      ',arx)

    with h5py.File(arx, "r") as f:
    # List all groups
        print("Keys: %s" % f.keys())
        a_group_key = list(f.keys())
        for i in a_group_key:
            print(i)

        Fmean = np.array(f.get('Fmean'))
        sigmaF = np.array(f.get('sigma_F'))
        Rmean = np.array(f.get('Rmean'))
        sigmaR = np.array(f.get('sigma_R'))
        Mmean = np.array(f.get('Mmean'))
        sigmaM = np.array(f.get('sigma_M'))
        rhomean = np.array(f.get('rhomean'))
        time = np.array(f.get('time'))

        M = np.array([Mmean[0,0],Mmean[0,1],Mmean[0,2], Mmean[1,1], Mmean[1,2], Mmean[2,2]])
        errM = np.array([sigmaM[0,0],sigmaM[0,1],sigmaM[0,2],sigmaM[1,1],sigmaM[1,2],sigmaM[2,2]])
        R = np.array([Rmean[0,0],Rmean[0,1],Rmean[0,2], Rmean[1,1], Rmean[1,2], Rmean[2,2]])
        errR = np.array([sigmaR[0,0],sigmaR[0,1],sigmaR[0,2],sigmaR[1,1],sigmaR[1,2],sigmaR[2,2]])
        F = np.array([Fmean[0,1], Fmean[0,2], Fmean[1,2]])
        errF = np.array([sigmaF[0,1],sigmaF[0,2],sigmaF[1,2]])
        f.close()

        print('Time           :',time)

    return [M, R, F, errM, errR, errF, rhomean]


#===============================================================================================

def energy_ev(v0, scale,al,t, factor_c, energy_ini,rho):

   # knumber = 1.2785/al #from Chandrasekhar (1969)
#    knumber = 1/(2*al)
    #gamma_khi = 0.45*knumber*2*v0
    #gamma_khi = 1.2785*0.5/al
    gamma_khi = 1/al*v0*0.1425
    print('gamma KHI:',gamma_khi)

    std_fact= factor_c/(rho**(0.5)*scale)*0.1425
    print('Std : ', std_fact)
    timestep= 5e-5
    tfinal = t[-1]
    lent = int(round((tfinal)/timestep))+1

    energy_t = np.zeros(lent)
    energy_t[0] = energy_ini

    libenergyev.energy_ev_khi(std_fact,lent,timestep,gamma_khi, energy_t)

    ratio = int((lent-1)/(len(t)-1))
    energy_model = np.zeros(len(t))

    for i in range(0,len(t)-1):
        energy_model[i] = energy_t[ratio*i]

    energy_model[-1] = energy_t[-1]

    return energy_model, gamma_khi

def stresses_r0(v0,al,factor_c,energy_ini,res,t, scale):

    print('C factor     : ', factor_c)
    print('energy_ini      : ', energy_ini)

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/DATA/KHI/"+res
    Msim = np.zeros((len(t),6))
    Rsim = np.zeros((len(t),6))
    Fsim = np.zeros((len(t),3))
    errMsq = np.zeros((len(t),6))
    errRsq = np.zeros((len(t),6))
    errFsq = np.zeros((len(t),3))
    rhomean = np.zeros(len(t))
    Mmod_arr = np.zeros((3,len(t), 6))
    Rmod_arr = np.zeros((3,len(t), 6))
    Fmod_arr = np.zeros((3,len(t), 3))

    for tt in range(0, len(t)):

        arx = format(int(10*t[tt]), "04")
        Msim[tt], Rsim[tt], Fsim[tt], errMsq[tt], errRsq[tt], errFsq[tt], rhomean[tt] = readh5files(path_data+'/stresses_sigma_r0_KHI-'+arx+'.h5')

    return Msim, Rsim, Fsim, rhomean

def stresses_r0_lowres(v0,al,factor_c,energy_ini,res,t, scale):

    print('C factor     : ', factor_c)
    print('energy_ini      : ', energy_ini)

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_r0/"+res
    Msim = np.zeros((len(t),6))
    Rsim = np.zeros((len(t),6))
    Fsim = np.zeros((len(t),3))
    errMsq = np.zeros((len(t),6))
    errRsq = np.zeros((len(t),6))
    errFsq = np.zeros((len(t),3))
    rhomean = np.zeros(len(t))
    Mmod_arr = np.zeros((3,len(t), 6))
    Rmod_arr = np.zeros((3,len(t), 6))
    Fmod_arr = np.zeros((3,len(t), 3))

    for tt in range(0, len(t)):

        arx = format(int(100*t[tt]), "04")
        Msim[tt], Rsim[tt], Fsim[tt], errMsq[tt], errRsq[tt], errFsq[tt], rhomean[tt] = readh5files(path_data+'/stresses_sigma_r0_KHI-'+arx+'.h5')

    return Msim, Rsim, Fsim, rhomean


#===========================================================================================

def main():

    factor_c = [2,4,6,8,10]
#    energy_ini = [1e-5,1e-4,1e-3,1e-2]
    #factor_c = 5
    energy_ini = 0.2
    leng = 512

#    res = 'res_128_128_128/CVP--bx3e-2--128--rndom'
#    tfinal = 40
#    lent = 801
#    t = np.linspace(0,tfinal,lent)

#    res2 = 'res_256_256_256/CVP--bx3e-4--256--rndom'
#    tfinal2 = 7.475
#    lent2 = 300
#    t2 = np.linspace(0,tfinal2,lent2)

    #res = 'res_512_512_512/CVP--bx1e-3--512--rndom/stress_tensors_global'
    res2 ='res_128_128_128/CVP--bx3e-2--128--rndom/stress_tensors_global'
    res3 ='res_256_256_256/CVP--bx3e-2--256--rndom/stress_tensors_global'
    tfinal = 40
    lent = 201
    t = np.linspace(0,tfinal,lent)
    #t = t[1:]
    #t2 = np.linspace(0,tfinal,401)

#================================================
    v0 = 0.5
    al = 0.01
    filtersize = 100000000
    num_fac = [1,10,100]
    scale = al
#=================================================

    fig, ax = plt.subplots(1,sharex = 'all', sharey = 'row', figsize = (8,5.5))

    #color = ['tab:red','tab:blue','tab:purple', 'tab:olive', 'tab:orange']
    color = sns.color_palette('magma',5)

   # for i in range(0,3):
    ax.tick_params(axis='x',labelsize = 18)
    ax.tick_params(axis='y',labelsize = 18)

    #for axs in ax.flat:
    #    axs.label_outer()

    #fig.subplots_adjust(wspace = 0.1)
    j = 0
    if j == 0:
 #   for j in range(0,3):
        for k in range(0,5):
       # k = 1
       # if k ==1:
            #Msim,Rsim,Fsim,rho = stresses_r0(v0,al*num_fac[j],factor_c[k],energy_ini,res,t,scale)
            #energy_trndm, gamma_khirndm = energy_ev(v0, scale,al*num_fac[j],t, factor_c[k], energy_ini,rho[0])

            #energy_tracerndm =0.5*rho*(Rsim[:,0]+Rsim[:,3]+Rsim[:,5])

            Msim2,Rsim2,Fsim2,rho2 = stresses_r0(v0,al*num_fac[j],factor_c[k],energy_ini,res2,t,scale)

            energy_tracerndm2 =0.5*rho2*(Rsim2[:,0]+Rsim2[:,3]+Rsim2[:,5])

            Msim3,Rsim3,Fsim3,rho3 = stresses_r0(v0,al*num_fac[j],factor_c[k],energy_ini,res3,t,scale)

            energy_tracerndm3 =0.5*rho3*(Rsim3[:,0]+Rsim3[:,3]+Rsim3[:,5])
            energy_trndm, gamma_khirndm = energy_ev(v0, scale,al*num_fac[j],t, factor_c[k], energy_ini,rho3[0])

     #       Msim,Rsim,Fsim,rho = stresses_r0(v0,al*num_fac[j],factor_c[k],energy_ini,res2,t2,scale)
    #        energy_t256, gamma_khi256 = energy_ev(v0, scale,al*num_fac[j],t2, factor_c[k], energy_ini,rho[10])

   #         energy_trace256 =0.5*rho*(Rsim[:,0]+Rsim[:,3]+Rsim[:,5])

            ax.plot(t[:], energy_trndm[:],linewidth = .9, linestyle = 'solid', color = color[k], label = r'$C = $ '+str(factor_c[k]))
   #         ax.plot(t2[5:], energy_t256[5:],linewidth = .9, linestyle = 'dashed', color = color[k])

        ax.plot(t[:], energy_tracerndm2[:], linewidth = 1.2, linestyle = 'solid', color = 'k', label = '128--rndm')
        ax.plot(t[:], energy_tracerndm3[:], linewidth = 1.2, linestyle = 'dashed', color = 'k', label = '256--rndm')
    #    ax.plot(t[:], energy_tracerndm[:], linewidth = 1.2, linestyle = 'dashdot', color = 'k', label = '512--rndm')
     #   ax.plot(t2[5:], energy_trace256[5:], linewidth = 1.2, linestyle = 'dashed', color = 'k', label = '256--rndm')

        ax.set_yscale('log')
        ax.set_ylim(bottom = 1e-5)
        ax.set_xlim([0,38])
#        ax.text(3,1e-6,r'$\rm{num fac} =$ '+str(num_fac[j]), fontsize = 16)
#        ax[j].set_ylabel(r'$e_{\rm KHI}$', fontsize = '20')
        ax.set_xlabel('Time', fontsize = '20')

        ax.set_ylabel(r'$e_{\rm KHI}$', fontsize = '20')
        ax.legend(loc = 'lower right', ncol = 2, frameon = 'True', fontsize = '18')
        ax.set_title(r'$B_{x0}=3\times 10^{-2}$', fontsize = '24')

    #alfven = 3e-4/(4*np.pi*rho[0])**(0.5)
    #print('M_A^-1 = ', alfven/(2*v0))

    fig.savefig('KHI/en_evolution_KHI-TEST_t40_all-res--bx3e-2.pdf')

    return()

if __name__ == "__main__":
    main()
