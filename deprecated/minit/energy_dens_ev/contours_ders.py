##############################################
#
#		CONTOURS FOR THE EN DENSITIES
#
##############################################

"""
Contour plots to check the dependence of the derivative of e_T and e_PI on the energy densities. 

"""

import numpy as np
from matplotlib import pyplot as plt
import matplotlib.colors as colors

def alpha_trace():
    
    path = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/coefficients/60_240_60/fixed-r/'
    var = np.loadtxt(path+'alphaR_r-1.csv', usecols=(1,2,3,4,5,6))
    var = np.swapaxes(var,0,1)
   
    alphatrace = var[0,:]+var[3,:]+var[5,:]
    
    alphatrace_time = np.sum(alphatrace)/len(alphatrace)
    
    print('TRACE ALPHA COEFF    = ', alphatrace_time)
    
    return alphatrace_time 

###################################


########	PARAMETERS	##########

q = 1.25
omega = 1824*3.33564095e-11
b0 = 4.6e13*2.874214371e-25/(4*3.14159265)**(0.5)
sigma = 0.27
alphatrace = alpha_trace()

etmin = 7.443877733711218e-24
etmax = 3.94e-21

epmin = 1.0888989687964432e-23
epmax = 1.653513286623542e-21
leng = 1000
e_T = np.linspace(etmin,etmax,leng)
e_PI = np.linspace(epmin,epmax,leng)

# GROWTH RATES :

gamma_mri = q*omega/2
gamma_pi = sigma**(q/(4-q))**(0.5)*(1-(2-q)**2/4)**(0.5)*omega/b0*alphatrace**(0.5)*e_T**(0.5)
	
# S_TD : 

std = sigma**(q/(4-q))**(0.5)*(1-(2-q)**2/4)**(0.5)*omega/b0*alphatrace**(0.5)*e_PI**(3/2)	
	
######		DERIVATIVES		########	
	
der_eT = np.zeros((leng,leng))
der_ePI = np.zeros((leng,leng))

for i in range(0,leng):
	for j in range(0,leng):
		
		der_eT[i,j] = e_T[i]*2*gamma_mri-e_PI[j]*2*gamma_pi[i]
		der_ePI[i,j] = e_PI[j]*2*gamma_pi[i]-2*std[j]


######		PLOTS		#########


fig, ax = plt.subplots()

vmin = np.amin(der_eT)
vmax = np.amax(der_eT)
norm = colors.TwoSlopeNorm(vmin=vmin, vcenter=0, vmax=vmax)

c = ax.pcolor(e_T,e_PI,der_eT,norm = norm,cmap = 'PRGn')
fig.colorbar(c, ax=ax, norm = norm, label = r'$\partial_t e_{\rm T}$')
ax.set_xlabel(r'$e_{\rm T}$')
ax.set_ylabel(r'$e_{\rm PI}$')
#ax.set_xscale('log')
#ax.set_yscale('log')


fig, ax = plt.subplots()

vmin = np.amin(der_ePI)
vmax = np.amax(der_ePI)
norm = colors.TwoSlopeNorm(vmin=vmin, vcenter=0, vmax=vmax)

c = ax.pcolor(e_T,e_PI,der_ePI, norm = norm, cmap = 'seismic')
fig.colorbar(c, ax=ax, norm = norm, label = r'$\partial_t e_{\rm PI}$')
ax.set_xlabel(r'$e_{\rm T}$')
ax.set_ylabel(r'$e_{\rm PI}$')
#ax.set_xscale('log')
#ax.set_yscale('log')

plt.show()









    
    
    
    
    
    
    
    
    
    
