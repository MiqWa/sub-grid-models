#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 16 13:46:41 2021

@author: miquelmiravet
"""

##################################################
#
#       ENERGY DENSITY OF TURBULENT STRESSES
#
##################################################

"""
    We are going to solve the evolution equations for the energy density in turbulent stresses
    and the energy density of the parasitic instabilities. To do so, we need an averaged value of the alpha coefficient and its trace, in
    order to get M_ij and therefore s_max = 0.2*B_0 = 0.2*sqrt(e_t*Tr(alpha_ij)). We also need the initial value for e_T, which is given by
    the initial value of 0.5*rho*traceR. e_pi(0) = 0. The source term S_TD depends on e_pi. tau_mri is constant over time.

"""

import os
import numpy as np
import h5py
import sys
from matplotlib import pyplot as plt
from matplotlib import rc
import math as m
import numpy.ctypeslib as npct
from numpy.ctypeslib import ndpointer
from ctypes import c_int, c_double
import statistics
import seaborn as sns

rc('text', usetex=True)
rc('font', family='serif')

#------------------------------------------------------------
# input type for the function
# must be a double array, with single dimension that is contiguous
array_1d_float = npct.ndpointer(dtype=np.float64, ndim=1, flags='CONTIGUOUS')

libenergyev = npct.load_library("libenergyev", ".")
libenergyev.energy_ev.restype = None
libenergyev.energy_ev.argtypes = [c_double, c_int,c_double,c_double, c_double,array_1d_float, array_1d_float]

#===============================================================================================

def readh5files(arx):

    print('DATA FROM FILE:      ',arx)

    with h5py.File(arx, "r") as f:
    # List all groups
        print("Keys: %s" % f.keys())
        a_group_key = list(f.keys())
        for i in a_group_key:
            print(i)

        Fmean = np.array(f.get('Fmean'))
        sigmaF = np.array(f.get('sigma_F'))
        Rmean = np.array(f.get('Rmean'))
        sigmaR = np.array(f.get('sigma_R'))
        Mmean = np.array(f.get('Mmean'))
        sigmaM = np.array(f.get('sigma_M'))
        rhomean = np.array(f.get('rhomean'))
        time = np.array(f.get('time'))

        M = np.array([Mmean[0,0],Mmean[0,1],Mmean[0,2], Mmean[1,1], Mmean[1,2], Mmean[2,2]])
        errM = np.array([sigmaM[0,0],sigmaM[0,1],sigmaM[0,2],sigmaM[1,1],sigmaM[1,2],sigmaM[2,2]])
        R = np.array([Rmean[0,0],Rmean[0,1],Rmean[0,2], Rmean[1,1], Rmean[1,2], Rmean[2,2]])
        errR = np.array([sigmaR[0,0],sigmaR[0,1],sigmaR[0,2],sigmaR[1,1],sigmaR[1,2],sigmaR[2,2]])
        F = np.array([Fmean[0,1], Fmean[0,2], Fmean[1,2]])
        errF = np.array([sigmaF[0,1],sigmaF[0,2],sigmaF[1,2]])
        f.close()

        print('Time           :',time,' ms')

    return [M, R, F, errM, errR, errF, rhomean]


#===============================================================================================

def read_coeffs(res):

    res = 'coeffs-MRI-final'

    path = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/coefficients_r0/'+res+'/'

    alpha = np.loadtxt(path+'alphaR_r0_av.txt')

    beta = np.loadtxt(path+'betaR_r0_av.txt')

    gamma = np.loadtxt(path+'gammaR_r0_av.txt')

    e_alpha = np.loadtxt(path+'sigma_alphaR_r0_av.txt')

    e_beta = np.loadtxt(path+'sigma_betaR_r0_av.txt')

    e_gamma = np.loadtxt(path+'sigma_gammaR_r0_av.txt')

    return alpha, beta, gamma, e_alpha, e_beta, e_gamma

def energy_ev(alphatrace,t, scale,q,omega, factor_c, energy_ini, factor_en,rho):

    sigma = 0.27

    b0 = 4.6e13*2.874214371e-25/(4*3.14159265359)**(0.5)

    gamma_pi_fac = 2*sigma*(q/(4-q))**(0.5)*(1-(2-q)**2/4)**(0.5)*omega/b0*alphatrace**(0.5)

    std_fact= factor_c/(rho**(0.5)*scale)

    timestep= 5e-5

    timestep = 0.001*2.99792458e10*timestep
    tfinal = t[-1]*0.001*2.99792458e10
    lent = int(round((tfinal)/timestep))+1

    energy_t = np.zeros(lent)
    energy_t[0] = energy_ini

    energy_pi = np.zeros(lent)
    energy_pi[0] =energy_ini*factor_en

    gamma_pi = np.zeros(lent)
    gamma_pi[0] = gamma_pi_fac*energy_ini**(0.5)

    gamma_mri = q*omega

    libenergyev.energy_ev(std_fact,lent,timestep,gamma_mri, gamma_pi_fac,energy_t,energy_pi)

    ratio = int((lent-1)/(len(t)-1))
    energy_model = np.zeros(len(t))
    energy_pi2 = np.zeros(len(t))

    for i in range(0,len(t)-1):
        energy_model[i] = energy_t[ratio*i]
        energy_pi2[i] = energy_pi[ratio*i]

    energy_model[-1] = energy_t[-1]
    energy_pi2[-1] = energy_pi[-1]

    return energy_model, energy_pi2

def stresses_mod_r0(factor_c,energy_ini,res,t,scale,q,omega, factor_en):

    print('C factor     : ', factor_c)
    print('energy_ini      : ', energy_ini)

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_r0/res_"+res+"/surface_av/b0z-4.6e13"

    Msim = np.zeros((len(t),6))
    Rsim = np.zeros((len(t),6))
    Fsim = np.zeros((len(t),3))
    errMsq = np.zeros((len(t),6))
    errRsq = np.zeros((len(t),6))
    errFsq = np.zeros((len(t),3))
    rhomean = np.zeros(len(t))
    Mmod_arr = np.zeros((3,len(t), 6))
    Rmod_arr = np.zeros((3,len(t), 6))
    Fmod_arr = np.zeros((3,len(t), 3))

    for tt in range(0, len(t)):

        arx = format(25*tt, "04")
        Msim[tt], Rsim[tt], Fsim[tt], errMsq[tt], errRsq[tt], errFsq[tt], rhomean[tt] = readh5files(path_data+'/stresses_sigma_r0-'+arx+'.h5')


    alpha, beta, gamma, e_alpha, e_beta, e_gamma = read_coeffs(res)

    alpha_arr = np.array([alpha, alpha+0.5*e_alpha, alpha-0.5*e_alpha])
    beta_arr = np.array([beta, beta+0.5*e_beta, beta-0.5*e_beta])
    gamma_arr = np.array([gamma, gamma+0.5*e_gamma, gamma-0.5*e_gamma])

    for xx in range(0,3):

        alpha = alpha_arr[xx]
        beta = beta_arr[xx]
        gamma = gamma_arr[xx]

        alphatrace = abs(alpha[0]+alpha[3]+alpha[5])

        energy_mod, en_pi = energy_ev(alphatrace,t, scale,q,omega, factor_c, energy_ini, factor_en,rhomean[0])

        for i in range(0,6):
            Mmod_arr[xx,:,i] = alpha[i]*energy_mod[:]
            Rmod_arr[xx,:,i] = beta[i]*energy_mod[:]/rhomean[:]

        for j in range(0,3):
            Fmod_arr[xx,:,j] = gamma[j]*energy_mod[:]/rhomean[:]**(0.5)

    ind_sat = 29

    errMmean = np.mean(errMsq[ind_sat:,:]**0.5, axis = 0)
    errRmean = np.mean(errRsq[ind_sat:,:]**0.5, axis = 0)
    errFmean = np.mean(errFsq[ind_sat:,:]**0.5, axis = 0)

    stdMsim = np.zeros(6)
    stdRsim = np.zeros(6)
    stdFsim = np.zeros(3)
    errM = np.zeros(6)
    errR = np.zeros(6)
    errF = np.zeros(3)

    for i in range(0,6):
        stdMsim[i] = statistics.stdev(Msim[ind_sat:,i])
#        Msim[:,i] = np.mean(Msim[ind_sat:,i])
        stdRsim[i] = statistics.stdev(Rsim[ind_sat:,i])
#        Rsim[:,i] = np.mean(Rsim[ind_sat:,i])
        errM[i] = (errMmean[i]**2+stdMsim[i]**2)**0.5
        errR[i] = (errRmean[i]**2+stdRsim[i]**2)**0.5
    for j in range(0,3):
        stdFsim[j] = statistics.stdev(Fsim[ind_sat:,j])
 #       Fsim[:,j] = np.mean(Fsim[ind_sat:,j])
        errF[j] = (errFmean[j]**2+stdFsim[j]**2)**0.5


    return Msim, Rsim, Fsim, errMmean, errRmean, errFmean, Mmod_arr, Rmod_arr, Fmod_arr, rhomean

#===========================================================================================

def main():

    factor_c = [8,9,10,11,12,13]
    energy_ini = [2e-31,7e-31,1e-30,5e-30]

    lenphi = 400

    if lenphi == 400:
       lenr = 100
       lenz = 100
       lent = 61
       tfinal = 30

    elif lenphi == 800 :

       lenr = 200
       lenz = 200
       lent = 26
       tfinal = 12.5

    elif lenphi == 240 :

       lenr = 60
       lenz = 60
       lent = 61
       tfinal = 30

    elif lenphi == 304 :

       lenr = 76
       lenz = 76
       lent = 61
       tfinal = 30

    res = str(lenr)+'_'+str(lenphi)+'_'+str(lenz)

    t = np.linspace(0,tfinal,lent)

#================================================
    omega0tilde = 1824*3.33564095e-11
    q = 1.25
    omega = omega0tilde
    filtersize = 460765
    lambda_mri = 33300
    scale = min(filtersize,lambda_mri)
#=================================================

    alpha, beta, gamma, e_alpha, e_beta, e_gamma = read_coeffs(res)

    alphatrace = abs(alpha[0]+alpha[3]+alpha[5])

#    energy_t = energy_ev(alphatrace,t, filtersize,q,omega, factor_c, energy_ini)


    fig, ax = plt.subplots(4,4,sharex = 'all', sharey = 'row', figsize = (22,18))

#    fig.suptitle(r'$e_{\rm PI}(0)/e_{\rm T}(0) = $'+str(factor_en), fontsize = '17')

#    figM, axM = plt.subplots(2,3,sharex = 'all', sharey = 'none', figsize = (27,15))

#    figM.suptitle(r'$e_{\rm PI}(0)/e_{\rm T}(0) = $'+str(factor_en), fontsize = '25')

#    figR, axR = plt.subplots(2,3,sharex = 'all', sharey = 'none', figsize = (27,15))

#    figR.suptitle(r'$e_{\rm PI}(0)/e_{\rm T}(0) = $'+str(factor_en), fontsize = '25')

#    figF, axF = plt.subplots(1,3,sharex = 'all', sharey = 'none', figsize = (27,8))

#    figF.suptitle(r'$e_{\rm PI}(0)/e_{\rm T}(0) = $'+str(factor_en), fontsize = '25')

    #color = ['tab:red','tab:blue','tab:green','tab:orange', 'tab:purple', 'tab:olive']

    color = sns.color_palette('winter',6)

    for i in range(0,4):
        for j in range(0,4):
            ax[j,i].tick_params(axis='x',labelsize = 20)
            ax[j,i].tick_params(axis='y',labelsize = 20)

#        for j in range(0,2):
#            axM[j,i].tick_params(axis='x',labelsize = 16)
#            axM[j,i].tick_params(axis='y',labelsize = 16)
#            axR[j,i].tick_params(axis='x',labelsize = 16)
#            axR[j,i].tick_params(axis='y',labelsize = 16)
#        axF[i].tick_params(axis='x',labelsize = 16)
#        axF[i].tick_params(axis='y',labelsize = 16)

    for axs in ax.flat:
        axs.label_outer()

#    for axs in axM.flat:
#        axs.label_outer()

#    for axs in axR.flat:
#        axs.label_outer()

#    for axs in axF.flat:
#        axs.label_outer()

    fig.subplots_adjust(wspace = 0.1)
    fig.subplots_adjust(hspace = 0.1)
#    figM.subplots_adjust(wspace = 0.1)
#    figR.subplots_adjust(wspace = 0.1)
#    figF.subplots_adjust(wspace = 0.1)
    ratio = [10,100,500,1e3]

    for ff in range(0,4):
        for j in range(0,4):

            for k in range(0,6):

                Msim,Rsim,Fsim,errM,errR,errF, Mmod, Rmod, Fmod, rho = stresses_mod_r0(factor_c[k],energy_ini[j],res,t,scale,q,omega,ratio[ff])
                energy_t, en_pi = energy_ev(alphatrace,t, scale,q,omega, factor_c[k], energy_ini[j],ratio[ff],rho[0])
                energy_trace =0.5*rho*(Rsim[:,0]+Rsim[:,3]-2*Rsim[:,5])
                energy_pisim = 1.5*rho*Rsim[:,5]


                ax[j,ff].plot(t[11:], energy_t[11:],linewidth = .8, linestyle = 'solid', color = color[k], label = r'$C = $ '+str(factor_c[k]))
                ax[j,ff].plot(t[10:], en_pi[10:],linewidth = .8, linestyle = 'dashdot', color = color[k])
            ax[j,ff].plot(t[11:], energy_trace[11:], linewidth = .8, linestyle = 'dashed', color = 'k', label = 'SIMULATION')
            ax[j,ff].plot(t[10:], energy_pisim[10:], linewidth = .9, linestyle = 'dashed', color = 'r', label = 'PI')
            ax[j,ff].set_yscale('log')
            ax[j,ff].set_ylim(bottom = 1e-23)
            ax[j,ff].set_xlim([0,30])
            ax[j,ff].text(15,1e-22,r'$e_{\rm MRI}(0) =$ '+str(energy_ini[j]), fontsize = 20)
            if ff  == 0 :
                ax[j, ff].set_ylabel(r'$e_{\rm MRI}$', fontsize = '20')
            if j == 3 :
                ax[j,ff].set_xlabel('Time [ms]', fontsize = '20')
            if j  == 0:
                ax[j,ff].set_title(r'$K_0 = $'+str(ratio[ff]),fontsize = '20')
            if j == 3 and ff == 3:
                ax[j, ff].legend( bbox_to_anchor=(1.75,0),loc = 'lower right', ncol = 1, frameon = 'True', fontsize = '20')

    fig.savefig('MRI/test_en_evolution_MRI_v2.pdf', bbox_inches='tight')

    fig, ax = plt.subplots(sharex = 'all', sharey = 'row', figsize = (9,6))

    ax.tick_params(axis='both',labelsize = 20)

    for k in range(0,6):

        Msim,Rsim,Fsim,errM,errR,errF, Mmod, Rmod, Fmod, rho = stresses_mod_r0(factor_c[k],energy_ini[1],res,t,scale,q,omega,ratio[ff])
        energy_t, en_pi = energy_ev(alphatrace,t, scale,q,omega, factor_c[k], energy_ini[1],ratio[0],rho[0])
        energy_trace =0.5*rho*(Rsim[:,0]+Rsim[:,3]-2*Rsim[:,5])
        energy_pisim = 1.5*rho*Rsim[:,5]

        ax.plot(t[10:], energy_t[10:],linewidth = .8, linestyle = 'solid', color = color[k], label = r'$C = $ '+str(factor_c[k]))
        ax.plot(t[10:], en_pi[10:],linewidth = .8, linestyle = 'dashdot', color = color[k])
    ax.plot(t[10:], energy_trace[10:], linewidth = .9, linestyle = 'dashed', color = 'k', label = 'SIMULATION')
    ax.plot(t[10:], energy_pisim[10:], linewidth = .9, linestyle = 'dashed', color = 'r', label = 'PI')
    ax.set_yscale('log')
    ax.set_ylim(bottom=1e-25)
    ax.set_xlim([5,30])
    ax.set_ylabel(r'$e_{\rm MRI}$', fontsize = '20')
    ax.set_xlabel('Time [ms]', fontsize = '20')
    ax.legend(loc = 'lower right', ncol = 1, frameon = 'True', fontsize = '16')
    fig.savefig('MRI/en_ev_MRI_v2.pdf', bbox_inches='tight')

    fig, ax = plt.subplots(sharex = 'all', sharey = 'row', figsize = (6,4))

    ax.tick_params(axis='both',labelsize = 20)

    energy_t, energy_pi = energy_ev(alphatrace,t, scale,q,omega, 11.5, energy_ini[1],0.1,rho[0])
    ax.plot(t[1:],energy_t[1:],linewidth=1.2,linestyle='solid', color = 'blue', label = r'$e_{\rm MRI}$')
    ax.plot(t[1:],energy_pi[1:],linewidth=1.2,linestyle='dashed',color = 'red', label = r'$e_{\rm PI}$')
    ax.set_yscale('log')
    ax.set_xlim([0,30])
    ax.set_ylabel(r'$e(t)$', fontsize = '20')
    ax.set_xlabel('Time [ms]', fontsize = '20')
    ax.legend(loc = 'lower right', ncol = 1, frameon = 'True', fontsize = '20')
    fig.savefig('/home/miquelmiravet/Desktop/figs_mri/en_evolution_MRI_with_pi.pdf', bbox_inches='tight')

#    comps1 = ['rr','\phi\phi','zz']
#    comps2 = ['r\phi','rz','\phi z']
#    style = ['solid','dotted','dashdot']

#    for k in range(0,6):

#        Msim, Rsim, Fsim, errM, errR, errF, Mmod_arr, Rmod_arr, Fmod_arr, rho = stresses_mod_r0(factor_c[k],2e-31,res,t,filtersize,q,omega,1000)
#        Msim_diag = np.array([Msim[:,0], Msim[:,3], Msim[:,5]])
#        errM_diag = np.array([errM[0], errM[3], errM[5]])
#        Rsim_diag = np.array([Rsim[:,0], Rsim[:,3], Rsim[:,5]])
#        errR_diag = np.array([errR[0], errR[3], errR[5]])
#        Msim_nodiag = np.array([Msim[:,1], Msim[:,2], Msim[:,4]])
#        errM_nodiag = np.array([errM[1], errM[2], errM[4]])
#        Rsim_nodiag = np.array([Rsim[:,1], Rsim[:,2], Rsim[:,4]])
#        errR_nodiag = np.array([errR[1], errR[2], errR[4]])
#        Mmod_diag = np.array([Mmod_arr[0,:,0], Mmod_arr[0,:,3], Mmod_arr[0,:,5]])
#        Rmod_diag = np.array([Rmod_arr[0,:,0], Rmod_arr[0,:,3], Rmod_arr[0,:,5]])
#        Mmod_nodiag = np.array([Mmod_arr[0,:,1], Mmod_arr[0,:,2], Mmod_arr[0,:,4]])
#        Rmod_nodiag = np.array([Rmod_arr[0,:,1], Rmod_arr[0,:,2], Rmod_arr[0,:,4]])

#        for l in range(0,3):

#            axM[0,l].plot(t[29:],(Mmod_diag[l,29:]), linestyle = 'solid', linewidth = .8, color = color[k], label = r'$K_{\rm sat} = $'+str(factor_c[k]))
#            axR[0,l].plot(t[29:],(Rmod_diag[l,29:]), linestyle = 'solid', linewidth = .8, color = color[k], label = r'$K_{\rm sat} = $'+str(factor_c[k]))
#            axF[l].plot(t[29:],(Fmod_arr[0,29:,l]), linestyle = 'solid', linewidth = .8, color = color[k], label = r'$K_{\rm sat} = $'+str(factor_c[k]))
#            axM[1,l].plot(t[29:],(Mmod_nodiag[l,29:]), linestyle = 'solid', linewidth = .8, color = color[k], label = r'$K_{\rm sat} = $'+str(factor_c[k]))
#            axR[1,l].plot(t[29:],(Rmod_nodiag[l,29:]), linestyle = 'solid', linewidth = .8, color = color[k], label = r'$K_{\rm sat} = $'+str(factor_c[k]))
#            axM[0,l].text(0.8,0.8,r'$M_{'+comps1[l]+'}$', fontsize = '16', transform=axM[0,l].transAxes)
#            axR[0,l].text(0.8, 0.8, r'$R_{'+comps1[l]+'}$', fontsize = '16', transform=axR[0,l].transAxes)
#            if l == 2:
#                axM[1,l].text(0.8,0.8,r'$M_{'+comps2[l]+'}$', fontsize = '16', transform=axM[1,l].transAxes)
#            else:
#                axM[1,l].text(0.8,0.2,r'$M_{'+comps2[l]+'}$', fontsize = '16', transform=axM[1,l].transAxes)
#            axR[1,l].text(0.5, 0.2, r'$R_{'+comps2[l]+'}$', fontsize = '16', transform=axR[1,l].transAxes)
#            if l == 1:
#                axF[l].text(0.8, 0.2, r'$F_{'+comps2[l]+'}$', fontsize = '16', transform=axF[l].transAxes)
#            else:
#                axF[l].text(0.8, 0.8, r'$F_{'+comps2[l]+'}$', fontsize = '16', transform=axF[l].transAxes)
#            axM[1,l].set_xlabel('Time [ms]', fontsize = '16')
#            axR[1,l].set_xlabel('Time [ms]', fontsize = '16')
#            axF[l].set_xlabel('Time [ms]', fontsize = '16')

#            if k == 5:
#                axM[0,l].plot(t[29:],(Msim_diag[l,29:]), linestyle = 'dashed', color = 'k', label = 'M SIM')
#                axM[0,l].fill_between(t[29:], Msim_diag[l,29:]-errM_diag[l], Msim_diag[l,29:]+errM_diag[l], color = 'grey', alpha = 0.1)
#                axR[0,l].plot(t[29:], (Rsim_diag[l,29:]), linestyle = 'dashed', color = 'k', label = 'R SIM')
#                axR[0,l].fill_between(t[29:], Rsim_diag[l,29:]-errR_diag[l], Rsim_diag[l,29:]+errR_diag[l], color = 'grey', alpha = 0.1)
#                axM[1,l].plot(t[29:],(Msim_nodiag[l,29:]), linestyle = 'dashed', color = 'k', label = 'M SIM')
#                axM[1,l].fill_between(t[29:], Msim_nodiag[l,29:]-errM_nodiag[l], Msim_nodiag[l,29:]+errM_nodiag[l], color = 'grey', alpha = 0.1)
#                axR[1,l].plot(t[29:], (Rsim_nodiag[l,29:]), linestyle = 'dashed', color = 'k', label = 'R SIM')
#                axR[1,l].fill_between(t[29:], Rsim_nodiag[l,29:]-errR_nodiag[l], Rsim_nodiag[l,29:]+errR_nodiag[l], color = 'grey', alpha = 0.1)
#                axF[l].plot(t[29:],(Fsim[29:,l]), linestyle = 'dashed', color = 'k', label = 'F SIM')
#                axF[l].fill_between(t[29:], Fsim[29:,l]-errF[l], Fsim[29:,l]+errF[l], color = 'grey', alpha = 0.1)

#                if l == 2 :
#                    axM[1,l].legend(bbox_to_anchor=(1.3,0),loc = 'lower right', ncol = 1, frameon = 'True', fontsize = '14')
#                    axR[1,l].legend(bbox_to_anchor=(1.3,0),loc = 'lower right', ncol = 1, frameon = 'True', fontsize = '14')
#                    axF[l].legend(bbox_to_anchor=(1.3,0),loc = 'lower right', ncol = 1, frameon = 'True', fontsize = '14')
#            axM[0,l].set_yscale('log')
#            axR[0,l].set_yscale('log')

#    figM.savefig('M_evolution_sat_mean.pdf')
#    figR.savefig('R_evolution_sat_mean.pdf')
#    figF.savefig('F_evolution_sat_mean.pdf')

    return()

    """
    comps1 = ['rr','\phi\phi','zz']
    comps2 = ['r\phi','rz','\phi z']
    style = ['solid','dotted','dashdot']

    for k in range(0,6):

        Msim_arr, Rsim_arr, Fsim_arr, Mmod_arr, Rmod_arr, Fmod_arr, rho = stresses_mod_r0(factor_c[k],2e-31,res,t,filtersize,q,omega,1000)
        Msim_diag = np.array([Msim_arr[0,:,0], Msim_arr[0,:,3], Msim_arr[0,:,5]])
        Rsim_diag = np.array([Rsim_arr[0,:,0], Rsim_arr[0,:,3], Rsim_arr[0,:,5]])
        Msim_nodiag = np.array([Msim_arr[0,:,1], Msim_arr[0,:,2], Msim_arr[0,:,4]])
        Rsim_nodiag = np.array([Rsim_arr[0,:,1], Rsim_arr[0,:,2], Rsim_arr[0,:,4]])
        Mmod_diag = np.array([Mmod_arr[0,:,0], Mmod_arr[0,:,3], Mmod_arr[0,:,5]])
        Rmod_diag = np.array([Rmod_arr[0,:,0], Rmod_arr[0,:,3], Rmod_arr[0,:,5]])
        Mmod_nodiag = np.array([Mmod_arr[0,:,1], Mmod_arr[0,:,2], Mmod_arr[0,:,4]])
        Rmod_nodiag = np.array([Rmod_arr[0,:,1], Rmod_arr[0,:,2], Rmod_arr[0,:,4]])

        for l in range(0,3):

            axM[0,l].plot(t[29:],(Mmod_diag[l,29:]), linestyle = 'solid', linewidth = .8, color = color[k], label = r'$K_{\rm sat} = $'+str(factor_c[k]))
            axR[0,l].plot(t[29:],(Rmod_diag[l,29:]), linestyle = 'solid', linewidth = .8, color = color[k], label = r'$K_{\rm sat} = $'+str(factor_c[k]))
            axF[l].plot(t[29:],(Fmod_arr[0,29:,l]), linestyle = 'solid', linewidth = .8, color = color[k], label = r'$K_{\rm sat} = $'+str(factor_c[k]))
            axM[1,l].plot(t[29:],(Mmod_nodiag[l,29:]), linestyle = 'solid', linewidth = .8, color = color[k], label = r'$K_{\rm sat} = $'+str(factor_c[k]))
            axR[1,l].plot(t[29:],(Rmod_nodiag[l,29:]), linestyle = 'solid', linewidth = .8, color = color[k], label = r'$K_{\rm sat} = $'+str(factor_c[k]))
            axM[0,l].text(0.8,0.8,r'$M_{'+comps1[l]+'}$', fontsize = '16', transform=axM[0,l].transAxes)
            axR[0,l].text(0.8, 0.8, r'$R_{'+comps1[l]+'}$', fontsize = '16', transform=axR[0,l].transAxes)
            if l == 2:
                axM[1,l].text(0.8,0.8,r'$M_{'+comps2[l]+'}$', fontsize = '16', transform=axM[1,l].transAxes)
            else:
                axM[1,l].text(0.8,0.2,r'$M_{'+comps2[l]+'}$', fontsize = '16', transform=axM[1,l].transAxes)
            axR[1,l].text(0.5, 0.2, r'$R_{'+comps2[l]+'}$', fontsize = '16', transform=axR[1,l].transAxes)
            if l == 1:
                axF[l].text(0.8, 0.2, r'$F_{'+comps2[l]+'}$', fontsize = '16', transform=axF[l].transAxes)
            else:
                axF[l].text(0.8, 0.8, r'$F_{'+comps2[l]+'}$', fontsize = '16', transform=axF[l].transAxes)
            axM[1,l].set_xlabel('Time [ms]', fontsize = '16')
            axR[1,l].set_xlabel('Time [ms]', fontsize = '16')
            axF[l].set_xlabel('Time [ms]', fontsize = '16')

            if k == 5:
                axM[0,l].plot(t[29:],(Msim_diag[l,29:]), linestyle = 'dashed', color = 'k', label = 'M SIM')
                axR[0,l].plot(t[29:], (Rsim_diag[l,29:]), linestyle = 'dashed', color = 'k', label = 'R SIM')
                axM[1,l].plot(t[29:],(Msim_nodiag[l,29:]), linestyle = 'dashed', color = 'k', label = 'M SIM')
                axR[1,l].plot(t[29:], (Rsim_nodiag[l,29:]), linestyle = 'dashed', color = 'k', label = 'R SIM')
                axF[l].plot(t[29:],(Fsim_arr[0,29:,l]), linestyle = 'dashed', color = 'k', label = 'F SIM')

                if l == 2 :
                    axM[1,l].legend(bbox_to_anchor=(1.3,0),loc = 'lower right', ncol = 1, frameon = 'True', fontsize = '14')
                    axR[1,l].legend(bbox_to_anchor=(1.3,0),loc = 'lower right', ncol = 1, frameon = 'True', fontsize = '14')
                    axF[l].legend(bbox_to_anchor=(1.3,0),loc = 'lower right', ncol = 1, frameon = 'True', fontsize = '14')
    figM.savefig('M_evolution_sat.pdf')
    figR.savefig('R_evolution_sat.pdf')
    figF.savefig('F_evolution_sat.pdf')

    return()
"""

if __name__ == "__main__":
    main()
#    plt.show()

#    fig, ax = plt.subplots()

#    ax.plot(t, energy_t, linestyle = 'solid', color = 'r', label = r'$e_T$')
#    ax.set_title('C = '+str(factor_c)+', e0 = '+str(energy_ini))
#    ax.set_yscale('log')
#    ax.set_xlabel('t [ms]')
#    ax.set_xlim([0,30])
#    ax.set_ylabel('energy dens')
#    plt.savefig('plots-tests/energy_evo.eps')
#    plt.show()
#    energy_files(time, energy_et,energy_pi,lent)

#    os.system('mv energy_dens_ev.csv '+path_energy+'/.')
