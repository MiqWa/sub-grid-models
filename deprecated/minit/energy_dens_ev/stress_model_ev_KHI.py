#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 16 13:46:41 2021

@author: miquelmiravet
"""

##################################################
#
#       ENERGY DENSITY OF TURBULENT STRESSES     
#                       
##################################################

"""
    We are going to solve the evolution equations for the energy density in turbulent stresses 
    and the energy density of the parasitic instabilities. To do so, we need an averaged value of the alpha coefficient and its trace, in 
    order to get M_ij and therefore s_max = 0.2*B_0 = 0.2*sqrt(e_t*Tr(alpha_ij)). We also need the initial value for e_T, which is given by 
    the initial value of 0.5*rho*traceR. e_pi(0) = 0. The source term S_TD depends on e_pi. tau_mri is constant over time.
    
"""

import os
import numpy as np
import h5py
import sys
from matplotlib import pyplot as plt
from matplotlib import rc
import math as m
import numpy.ctypeslib as npct
from numpy.ctypeslib import ndpointer
from ctypes import c_int, c_double
import statistics
import seaborn as sns

rc('text', usetex=True)
rc('font', family='serif')

#------------------------------------------------------------
# input type for the function
# must be a double array, with single dimension that is contiguous
array_1d_float = npct.ndpointer(dtype=np.float64, ndim=1, flags='CONTIGUOUS')

libenergyev = npct.load_library("KHI/libenergyevkhi.so", ".")
libenergyev.energy_ev_khi.restype = None
libenergyev.energy_ev_khi.argtypes = [c_double, c_int,c_double,c_double, array_1d_float]

#===============================================================================================

def readh5files(arx):
    
    print('DATA FROM FILE:      ',arx)
    
    with h5py.File(arx, "r") as f:
    # List all groups
        print("Keys: %s" % f.keys())
        a_group_key = list(f.keys())
        for i in a_group_key:
            print(i)

        Fmean = np.array(f.get('Fmean'))
        sigmaF = np.array(f.get('sigma_F'))
        Rmean = np.array(f.get('Rmean'))
        sigmaR = np.array(f.get('sigma_R'))
        Mmean = np.array(f.get('Mmean'))
        sigmaM = np.array(f.get('sigma_M'))
        rhomean = np.array(f.get('rhomean'))
        time = np.array(f.get('time'))
        
        M = np.array([Mmean[0,0],Mmean[0,1],Mmean[0,2], Mmean[1,1], Mmean[1,2], Mmean[2,2]])
        errM = np.array([sigmaM[0,0],sigmaM[0,1],sigmaM[0,2],sigmaM[1,1],sigmaM[1,2],sigmaM[2,2]])
        R = np.array([Rmean[0,0],Rmean[0,1],Rmean[0,2], Rmean[1,1], Rmean[1,2], Rmean[2,2]])
        errR = np.array([sigmaR[0,0],sigmaR[0,1],sigmaR[0,2],sigmaR[1,1],sigmaR[1,2],sigmaR[2,2]])
        F = np.array([Fmean[0,1], Fmean[0,2], Fmean[1,2]])
        errF = np.array([sigmaF[0,1],sigmaF[0,2],sigmaF[1,2]])
        f.close()

        print('Time           :',time)
    
    return [M, R, F, errM, errR, errF, rhomean]


#===============================================================================================

def read_coeffs(res):

    path = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/coefficients_r0/'+res+'/'

    alpha = np.loadtxt(path+'alphaR_r0_av.txt')

    beta = np.loadtxt(path+'betaR_r0_av.txt')

    gamma = np.loadtxt(path+'gammaR_r0_av.txt')

    e_alpha = np.loadtxt(path+'sigma_alphaR_r0_av.txt')

    e_beta = np.loadtxt(path+'sigma_betaR_r0_av.txt')

    e_gamma = np.loadtxt(path+'sigma_gammaR_r0_av.txt')

    return alpha, beta, gamma, e_alpha, e_beta, e_gamma

def energy_ev(v0, scale,al,t, factor_c, energy_ini,rho):
    
   # knumber = 1.2785/al #from Chandrasekhar (1969)
    knumber = 1/(2*al)
    #gamma_khi = 0.45*knumber*2*v0
    #gamma_khi = 1.2785*0.5/al
    gamma_khi = 1/al*v0*0.14
    print('gamma KHI:',gamma_khi)

    std_fact= 0.14*factor_c/(rho**(0.5)*scale)
    print('Std : ', std_fact)
    timestep= 5e-5
    tfinal = t[-1]
    lent = int(round((tfinal)/timestep))+1

    energy_t = np.zeros(lent)
    energy_t[0] = energy_ini

    libenergyev.energy_ev_khi(std_fact,lent,timestep,gamma_khi, energy_t)

    ratio = int((lent-1)/(len(t)-1))
    energy_model = np.zeros(len(t))

    for i in range(0,len(t)-1):
        energy_model[i] = energy_t[ratio*i]

    energy_model[-1] = energy_t[-1]

    return energy_model, gamma_khi

def stresses_mod_r0(v0,al,factor_c,energy_ini,res,t, scale):

    print('C factor     : ', factor_c)
    print('energy_ini      : ', energy_ini)

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_r0/"+res
    Msim = np.zeros((len(t),6))
    Rsim = np.zeros((len(t),6))
    Fsim = np.zeros((len(t),3))
    errMsq = np.zeros((len(t),6))
    errRsq = np.zeros((len(t),6))
    errFsq = np.zeros((len(t),3))
    rhomean = np.zeros(len(t))
    Mmod_arr = np.zeros((3,len(t), 6))
    Rmod_arr = np.zeros((3,len(t), 6))
    Fmod_arr = np.zeros((3,len(t), 3))

    for tt in range(0, len(t)):

        arx = format(10*tt, "04")
        Msim[tt], Rsim[tt], Fsim[tt], errMsq[tt], errRsq[tt], errFsq[tt], rhomean[tt] = readh5files(path_data+'/stresses_sigma_r0_KHI-'+arx+'.h5')

    
    alpha, beta, gamma, e_alpha, e_beta, e_gamma = read_coeffs(res)
    
    alpha_arr = np.array([alpha, alpha+0.5*e_alpha, alpha-0.5*e_alpha])
    beta_arr = np.array([beta, beta+0.5*e_beta, beta-0.5*e_beta])
    gamma_arr = np.array([gamma, gamma+0.5*e_gamma, gamma-0.5*e_gamma])
    
    for xx in range(0,3):
        
        alpha = alpha_arr[xx]
        beta = beta_arr[xx]
        gamma = gamma_arr[xx]

        energy_mod, gamma_khi = energy_ev(v0,scale,al,t, factor_c, energy_ini, rhomean[0])
     
        for i in range(0,6):
            Mmod_arr[xx,:,i] = alpha[i]*energy_mod[:]
            Rmod_arr[xx,:,i] = beta[i]*energy_mod[:]/rhomean[:]
    
        for j in range(0,3):
            Fmod_arr[xx,:,j] = gamma[j]*energy_mod[:]/rhomean[:]**(0.5)
      
    ind_sat = 29

    errMmean = np.mean(errMsq[ind_sat:,:]**0.5, axis = 0)
    errRmean = np.mean(errRsq[ind_sat:,:]**0.5, axis = 0)
    errFmean = np.mean(errFsq[ind_sat:,:]**0.5, axis = 0)
    
    stdMsim = np.zeros(6)
    stdRsim = np.zeros(6)
    stdFsim = np.zeros(3)
    errM = np.zeros(6)
    errR = np.zeros(6)
    errF = np.zeros(3)

    for i in range(0,6):
        stdMsim[i] = statistics.stdev(Msim[ind_sat:,i])
#        Msim[:,i] = np.mean(Msim[ind_sat:,i])
        stdRsim[i] = statistics.stdev(Rsim[ind_sat:,i])
#        Rsim[:,i] = np.mean(Rsim[ind_sat:,i])
        errM[i] = (errMmean[i]**2+stdMsim[i]**2)**0.5
        errR[i] = (errRmean[i]**2+stdRsim[i]**2)**0.5
    for j in range(0,3):
        stdFsim[j] = statistics.stdev(Fsim[ind_sat:,j])
 #       Fsim[:,j] = np.mean(Fsim[ind_sat:,j])
        errF[j] = (errFmean[j]**2+stdFsim[j]**2)**0.5    


    return Msim, Rsim, Fsim, errMmean, errRmean, errFmean, Mmod_arr, Rmod_arr, Fmod_arr, rhomean
    
#===========================================================================================

def main():

    factor_c = [1,3,5,8,11.75]
#    energy_ini = [1e-5,1e-4,1e-3,1e-2]
    #factor_c = 5
    energy_ini = 5e-12
    leng = 128
       
    res = 'res_128_128_128/CVP--bx3e-4--128--rndom'
    tfinal = 20
    lent = 801
    t = np.linspace(0,tfinal,lent)

    res2 = 'res_256_256_256/CVP--bx3e-4--256'
    tfinal2 = 2.8
    lent2 = 113
    t2 = np.linspace(0,tfinal2,lent2)

#================================================
    v0 = 0.5
    al = 0.01
    filtersize = 100000000
    num_fac = [1,10,100]
    scale = al
#=================================================
    
#    alpha, beta, gamma, e_alpha, e_beta, e_gamma = read_coeffs(res)
    
   # Msim,Rsim,Fsim,errM,errR,errF, Mmod, Rmod, Fmod, rho = stresses_mod_r0(v0,al,factor_c,energy_ini,res,t,scale)
   # print('computing model...')
   # energy_t, gamma_khi = energy_ev(v0, scale,al,t, factor_c, energy_ini,rho[5])
#    print(energy_t)
    
   # testen = np.zeros(30)
   # for i in range(0,30):
   #     testen[i] = energy_ini*m.exp(2*gamma_khi*t[i])
        
   # energy_trace =0.5*rho*(Rsim[:,0]+Rsim[:,3]+Rsim[:,5])
        
   # print(testen)
   # print(energy_trace)
   # fig, ax = plt.subplots()
    
    #ax.plot(t[20:], energy_t[20:],color = 'r', label='MODEL')
   # ax.plot(t[:30],testen,color = 'g', label= 'EXP TEST')
    #ax.plot(t[20:],energy_trace[20:], color = 'b', label = 'SIM')
    #ax.set_xlabel('Time')
    #ax.set_ylabel('Energy density')
    #ax.set_yscale('log')
    #ax.legend(loc = 'lower right')
    
    fig, ax = plt.subplots(1,3,sharex = 'all', sharey = 'row', figsize = (40,10))

#    fig.suptitle(r'$e_{\rm PI}(0)/e_{\rm T}(0) = $'+str(factor_en), fontsize = '17')

#    figM, axM = plt.subplots(2,3,sharex = 'all', sharey = 'none', figsize = (27,15))

#    figM.suptitle(r'$e_{\rm PI}(0)/e_{\rm T}(0) = $'+str(factor_en), fontsize = '25')

#    figR, axR = plt.subplots(2,3,sharex = 'all', sharey = 'none', figsize = (27,15))
    
#    figR.suptitle(r'$e_{\rm PI}(0)/e_{\rm T}(0) = $'+str(factor_en), fontsize = '25')

#    figF, axF = plt.subplots(1,3,sharex = 'all', sharey = 'none', figsize = (27,8))
    
#    figF.suptitle(r'$e_{\rm PI}(0)/e_{\rm T}(0) = $'+str(factor_en), fontsize = '25')

    color = ['tab:red','tab:blue','tab:purple', 'tab:olive', 'tab:orange']

    #color = sns.color_palette('winter',6)
    for i in range(0,3):
        ax[i].tick_params(axis='x',labelsize = 18)
        ax[i].tick_params(axis='y',labelsize = 18)
        
#        for j in range(0,2):
#            axM[j,i].tick_params(axis='x',labelsize = 16)
#            axM[j,i].tick_params(axis='y',labelsize = 16)
#            axR[j,i].tick_params(axis='x',labelsize = 16)
#            axR[j,i].tick_params(axis='y',labelsize = 16)
#        axF[i].tick_params(axis='x',labelsize = 16)
#        axF[i].tick_params(axis='y',labelsize = 16)

    for axs in ax.flat:
        axs.label_outer()
        
#    for axs in axM.flat:
#        axs.label_outer()
   
#    for axs in axR.flat:
#        axs.label_outer()
        
#    for axs in axF.flat:
#        axs.label_outer()
    
    fig.subplots_adjust(wspace = 0.1)
#    figM.subplots_adjust(wspace = 0.1)
#    figR.subplots_adjust(wspace = 0.1)
#    figF.subplots_adjust(wspace = 0.1)
    
    for j in range(0,3):
        for k in range(0,5):
#    k = 1
#    if k ==1:
            Msim,Rsim,Fsim,errM,errR,errF, Mmod, Rmod, Fmod, rho = stresses_mod_r0(v0,al*num_fac[j],factor_c[k],energy_ini,res,t,scale)
            energy_trndm, gamma_khirndm = energy_ev(v0, scale,al*num_fac[j],t, factor_c[k], energy_ini,rho[5])
                
            energy_tracerndm =0.5*rho*(Rsim[:,0]+Rsim[:,3]+Rsim[:,5])
        
          #  Msim,Rsim,Fsim,errM,errR,errF, Mmod, Rmod, Fmod, rho = stresses_mod_r0(v0,al*num_fac[j],factor_c[k],energy_ini,res2,t2,scale)
          #  energy_t256, gamma_khi256 = energy_ev(v0, scale,al*num_fac[1],t2, factor_c[k], energy_ini,rho[5])

          #  energy_trace256 =0.5*rho*(Rsim[:,0]+Rsim[:,3]+Rsim[:,5])
                        
            ax[j].plot(t[10:], energy_trndm[10:],linewidth = .9, linestyle = 'solid', color = color[k], label = r'$C = $'+str(factor_c[k]))
       # ax.plot(t2[10:], energy_t256[10:],linewidth = .9, linestyle = 'dashed', color = color[k])

        ax[j].plot(t[10:], energy_tracerndm[10:], linewidth = 1, linestyle = 'solid', color = 'k', label = '128--rndm')
 #   ax.plot(t2[10:], energy_trace256[10:], linewidth = 1, linestyle = 'dashed', color = 'g', label = '256')

        ax[j].set_yscale('log')    
        ax[j].set_ylim(bottom = 1e-7)
        ax[j].set_xlim([0,20])
        ax[j].text(3,1e-6,r'$\rm{num fac} =$ '+str(num_fac[j]), fontsize = 16)
#        ax[j].set_ylabel(r'$e_{\rm KHI}$', fontsize = '20')
        ax[j].set_xlabel('Time', fontsize = '20') 
    
    ax[0].set_ylabel(r'$e_{\rm KHI}$', fontsize = '20')    
    ax[2].legend(loc = 'lower right', ncol = 1, frameon = 'True', fontsize = '20')
            
            
    fig.savefig('KHI/en_evolution_KHI-TEST_NUM-FAC.pdf')


#    comps1 = ['rr','\phi\phi','zz']  
#    comps2 = ['r\phi','rz','\phi z']
#    style = ['solid','dotted','dashdot']

#    for k in range(0,6):
            
#        Msim, Rsim, Fsim, errM, errR, errF, Mmod_arr, Rmod_arr, Fmod_arr, rho = stresses_mod_r0(factor_c[k],2e-31,res,t,filtersize,q,omega,1000)
#        Msim_diag = np.array([Msim[:,0], Msim[:,3], Msim[:,5]])
#        errM_diag = np.array([errM[0], errM[3], errM[5]])
#        Rsim_diag = np.array([Rsim[:,0], Rsim[:,3], Rsim[:,5]])
#        errR_diag = np.array([errR[0], errR[3], errR[5]])
#        Msim_nodiag = np.array([Msim[:,1], Msim[:,2], Msim[:,4]])
#        errM_nodiag = np.array([errM[1], errM[2], errM[4]])
#        Rsim_nodiag = np.array([Rsim[:,1], Rsim[:,2], Rsim[:,4]])
#        errR_nodiag = np.array([errR[1], errR[2], errR[4]])
#        Mmod_diag = np.array([Mmod_arr[0,:,0], Mmod_arr[0,:,3], Mmod_arr[0,:,5]])
#        Rmod_diag = np.array([Rmod_arr[0,:,0], Rmod_arr[0,:,3], Rmod_arr[0,:,5]])
#        Mmod_nodiag = np.array([Mmod_arr[0,:,1], Mmod_arr[0,:,2], Mmod_arr[0,:,4]])
#        Rmod_nodiag = np.array([Rmod_arr[0,:,1], Rmod_arr[0,:,2], Rmod_arr[0,:,4]])

#        for l in range(0,3):
            
#            axM[0,l].plot(t[29:],(Mmod_diag[l,29:]), linestyle = 'solid', linewidth = .8, color = color[k], label = r'$K_{\rm sat} = $'+str(factor_c[k]))
#            axR[0,l].plot(t[29:],(Rmod_diag[l,29:]), linestyle = 'solid', linewidth = .8, color = color[k], label = r'$K_{\rm sat} = $'+str(factor_c[k]))
#            axF[l].plot(t[29:],(Fmod_arr[0,29:,l]), linestyle = 'solid', linewidth = .8, color = color[k], label = r'$K_{\rm sat} = $'+str(factor_c[k]))
#            axM[1,l].plot(t[29:],(Mmod_nodiag[l,29:]), linestyle = 'solid', linewidth = .8, color = color[k], label = r'$K_{\rm sat} = $'+str(factor_c[k]))
#            axR[1,l].plot(t[29:],(Rmod_nodiag[l,29:]), linestyle = 'solid', linewidth = .8, color = color[k], label = r'$K_{\rm sat} = $'+str(factor_c[k]))
#            axM[0,l].text(0.8,0.8,r'$M_{'+comps1[l]+'}$', fontsize = '16', transform=axM[0,l].transAxes)
#            axR[0,l].text(0.8, 0.8, r'$R_{'+comps1[l]+'}$', fontsize = '16', transform=axR[0,l].transAxes)
#            if l == 2:
#                axM[1,l].text(0.8,0.8,r'$M_{'+comps2[l]+'}$', fontsize = '16', transform=axM[1,l].transAxes)
#            else:
#                axM[1,l].text(0.8,0.2,r'$M_{'+comps2[l]+'}$', fontsize = '16', transform=axM[1,l].transAxes)
#            axR[1,l].text(0.5, 0.2, r'$R_{'+comps2[l]+'}$', fontsize = '16', transform=axR[1,l].transAxes)
#            if l == 1:
#                axF[l].text(0.8, 0.2, r'$F_{'+comps2[l]+'}$', fontsize = '16', transform=axF[l].transAxes)
#            else:
#                axF[l].text(0.8, 0.8, r'$F_{'+comps2[l]+'}$', fontsize = '16', transform=axF[l].transAxes)
#            axM[1,l].set_xlabel('Time [ms]', fontsize = '16')
#            axR[1,l].set_xlabel('Time [ms]', fontsize = '16')
#            axF[l].set_xlabel('Time [ms]', fontsize = '16')    

#            if k == 5:
#                axM[0,l].plot(t[29:],(Msim_diag[l,29:]), linestyle = 'dashed', color = 'k', label = 'M SIM')
#                axM[0,l].fill_between(t[29:], Msim_diag[l,29:]-errM_diag[l], Msim_diag[l,29:]+errM_diag[l], color = 'grey', alpha = 0.1)
#                axR[0,l].plot(t[29:], (Rsim_diag[l,29:]), linestyle = 'dashed', color = 'k', label = 'R SIM')
#                axR[0,l].fill_between(t[29:], Rsim_diag[l,29:]-errR_diag[l], Rsim_diag[l,29:]+errR_diag[l], color = 'grey', alpha = 0.1)
#                axM[1,l].plot(t[29:],(Msim_nodiag[l,29:]), linestyle = 'dashed', color = 'k', label = 'M SIM')
#                axM[1,l].fill_between(t[29:], Msim_nodiag[l,29:]-errM_nodiag[l], Msim_nodiag[l,29:]+errM_nodiag[l], color = 'grey', alpha = 0.1)
#                axR[1,l].plot(t[29:], (Rsim_nodiag[l,29:]), linestyle = 'dashed', color = 'k', label = 'R SIM')
#                axR[1,l].fill_between(t[29:], Rsim_nodiag[l,29:]-errR_nodiag[l], Rsim_nodiag[l,29:]+errR_nodiag[l], color = 'grey', alpha = 0.1)
#                axF[l].plot(t[29:],(Fsim[29:,l]), linestyle = 'dashed', color = 'k', label = 'F SIM')
#                axF[l].fill_between(t[29:], Fsim[29:,l]-errF[l], Fsim[29:,l]+errF[l], color = 'grey', alpha = 0.1)

#                if l == 2 :
#                    axM[1,l].legend(bbox_to_anchor=(1.3,0),loc = 'lower right', ncol = 1, frameon = 'True', fontsize = '14')
#                    axR[1,l].legend(bbox_to_anchor=(1.3,0),loc = 'lower right', ncol = 1, frameon = 'True', fontsize = '14')
#                    axF[l].legend(bbox_to_anchor=(1.3,0),loc = 'lower right', ncol = 1, frameon = 'True', fontsize = '14')
#            axM[0,l].set_yscale('log')
#            axR[0,l].set_yscale('log')
    
#    figM.savefig('M_evolution_sat_mean.pdf')    
#    figR.savefig('R_evolution_sat_mean.pdf')
#    figF.savefig('F_evolution_sat_mean.pdf')
    
    return()
    
    """
    comps1 = ['rr','\phi\phi','zz']  
    comps2 = ['r\phi','rz','\phi z']
    style = ['solid','dotted','dashdot']

    for k in range(0,6):
            
        Msim_arr, Rsim_arr, Fsim_arr, Mmod_arr, Rmod_arr, Fmod_arr, rho = stresses_mod_r0(factor_c[k],2e-31,res,t,filtersize,q,omega,1000)
        Msim_diag = np.array([Msim_arr[0,:,0], Msim_arr[0,:,3], Msim_arr[0,:,5]])
        Rsim_diag = np.array([Rsim_arr[0,:,0], Rsim_arr[0,:,3], Rsim_arr[0,:,5]])
        Msim_nodiag = np.array([Msim_arr[0,:,1], Msim_arr[0,:,2], Msim_arr[0,:,4]])
        Rsim_nodiag = np.array([Rsim_arr[0,:,1], Rsim_arr[0,:,2], Rsim_arr[0,:,4]])
        Mmod_diag = np.array([Mmod_arr[0,:,0], Mmod_arr[0,:,3], Mmod_arr[0,:,5]])
        Rmod_diag = np.array([Rmod_arr[0,:,0], Rmod_arr[0,:,3], Rmod_arr[0,:,5]])
        Mmod_nodiag = np.array([Mmod_arr[0,:,1], Mmod_arr[0,:,2], Mmod_arr[0,:,4]])
        Rmod_nodiag = np.array([Rmod_arr[0,:,1], Rmod_arr[0,:,2], Rmod_arr[0,:,4]])

        for l in range(0,3):
            
            axM[0,l].plot(t[29:],(Mmod_diag[l,29:]), linestyle = 'solid', linewidth = .8, color = color[k], label = r'$K_{\rm sat} = $'+str(factor_c[k]))
            axR[0,l].plot(t[29:],(Rmod_diag[l,29:]), linestyle = 'solid', linewidth = .8, color = color[k], label = r'$K_{\rm sat} = $'+str(factor_c[k]))
            axF[l].plot(t[29:],(Fmod_arr[0,29:,l]), linestyle = 'solid', linewidth = .8, color = color[k], label = r'$K_{\rm sat} = $'+str(factor_c[k]))
            axM[1,l].plot(t[29:],(Mmod_nodiag[l,29:]), linestyle = 'solid', linewidth = .8, color = color[k], label = r'$K_{\rm sat} = $'+str(factor_c[k]))
            axR[1,l].plot(t[29:],(Rmod_nodiag[l,29:]), linestyle = 'solid', linewidth = .8, color = color[k], label = r'$K_{\rm sat} = $'+str(factor_c[k]))
            axM[0,l].text(0.8,0.8,r'$M_{'+comps1[l]+'}$', fontsize = '16', transform=axM[0,l].transAxes)
            axR[0,l].text(0.8, 0.8, r'$R_{'+comps1[l]+'}$', fontsize = '16', transform=axR[0,l].transAxes)
            if l == 2:
                axM[1,l].text(0.8,0.8,r'$M_{'+comps2[l]+'}$', fontsize = '16', transform=axM[1,l].transAxes)
            else:
                axM[1,l].text(0.8,0.2,r'$M_{'+comps2[l]+'}$', fontsize = '16', transform=axM[1,l].transAxes)
            axR[1,l].text(0.5, 0.2, r'$R_{'+comps2[l]+'}$', fontsize = '16', transform=axR[1,l].transAxes)
            if l == 1:
                axF[l].text(0.8, 0.2, r'$F_{'+comps2[l]+'}$', fontsize = '16', transform=axF[l].transAxes)
            else:
                axF[l].text(0.8, 0.8, r'$F_{'+comps2[l]+'}$', fontsize = '16', transform=axF[l].transAxes)
            axM[1,l].set_xlabel('Time [ms]', fontsize = '16')
            axR[1,l].set_xlabel('Time [ms]', fontsize = '16')
            axF[l].set_xlabel('Time [ms]', fontsize = '16')    

            if k == 5:
                axM[0,l].plot(t[29:],(Msim_diag[l,29:]), linestyle = 'dashed', color = 'k', label = 'M SIM')
                axR[0,l].plot(t[29:], (Rsim_diag[l,29:]), linestyle = 'dashed', color = 'k', label = 'R SIM')
                axM[1,l].plot(t[29:],(Msim_nodiag[l,29:]), linestyle = 'dashed', color = 'k', label = 'M SIM')
                axR[1,l].plot(t[29:], (Rsim_nodiag[l,29:]), linestyle = 'dashed', color = 'k', label = 'R SIM')
                axF[l].plot(t[29:],(Fsim_arr[0,29:,l]), linestyle = 'dashed', color = 'k', label = 'F SIM')
                
                if l == 2 :
                    axM[1,l].legend(bbox_to_anchor=(1.3,0),loc = 'lower right', ncol = 1, frameon = 'True', fontsize = '14')
                    axR[1,l].legend(bbox_to_anchor=(1.3,0),loc = 'lower right', ncol = 1, frameon = 'True', fontsize = '14')
                    axF[l].legend(bbox_to_anchor=(1.3,0),loc = 'lower right', ncol = 1, frameon = 'True', fontsize = '14')
    figM.savefig('M_evolution_sat.pdf')    
    figR.savefig('R_evolution_sat.pdf')
    figF.savefig('F_evolution_sat.pdf')

    return()
"""

if __name__ == "__main__":
    main()  
#    plt.show()

#    fig, ax = plt.subplots()

#    ax.plot(t, energy_t, linestyle = 'solid', color = 'r', label = r'$e_T$')
#    ax.set_title('C = '+str(factor_c)+', e0 = '+str(energy_ini))
#    ax.set_yscale('log')
#    ax.set_xlabel('t [ms]')
#    ax.set_xlim([0,30])
#    ax.set_ylabel('energy dens')
#    plt.savefig('plots-tests/energy_evo.eps')
#    plt.show()
#    energy_files(time, energy_et,energy_pi,lent)
    
#    os.system('mv energy_dens_ev.csv '+path_energy+'/.')
