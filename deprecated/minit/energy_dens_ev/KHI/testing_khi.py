import numpy as np
from matplotlib import pyplot as plt
from matplotlib import rc
import math as m
import numpy.ctypeslib as npct
from numpy.ctypeslib import ndpointer
from ctypes import c_int, c_double
import statistics

rc('text', usetex=True)
rc('font', family='serif')

array_1d_float = npct.ndpointer(dtype=np.float64, ndim=1, flags='CONTIGUOUS')

libenergyev = npct.load_library("libenergyevkhi.so", ".")
libenergyev.energy_ev_khi.restype = None
libenergyev.energy_ev_khi.argtypes = [c_double, c_int,c_double,c_double, array_1d_float]


std_fact = 1e6
energy_t = np.zeros(40)
lent = len(energy_t)
gamma_khi = 1e-3
timestep = 1
energy_t[0] = 1e-21
t = np.linspace(0,39,40)

libenergyev.energy_ev_khi(std_fact,lent,timestep,gamma_khi, energy_t)

plt.plot(t, energy_t, color = 'r')
plt.xlabel('Time')
plt.ylabel('Energy density')
plt.title('TESTING CODE')
plt.yscale('log')
plt.show()
