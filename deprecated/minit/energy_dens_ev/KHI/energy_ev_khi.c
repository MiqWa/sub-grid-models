//
//  energy_ev_khi.c
//  
//
//  Created by Miquel Miravet on 11/01/2022.
//

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

void energy_ev_khi (double std, int lent, double timestep, double gamma_khi, double energy_t[]){

	/* STRANG-SPLITTING */
	
	int i ;
        
	double et1, et2; 

	double expot = timestep*0.5*2*gamma_khi ;

	for (i = 1; i < lent ; i++){

		et1 = energy_t[i-1]*exp(expot);

	    et2 = et1-timestep*std*et1*sqrt(et1);
       /* et2 = et1;*/
		
		energy_t[i] = et2*exp(expot);
        
       /* printf("ENERGY_T  = %.30f\n", energy_t[i]);*/
	}
}
		

