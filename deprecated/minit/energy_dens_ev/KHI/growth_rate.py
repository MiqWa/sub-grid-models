import os
import numpy as np
import h5py
import sys
#===============================================================================================

def readh5files2(file,leng):

    ind0 = 3
    ind1 = int(leng)+3

    print('DATA FROM FILE:      ',file)

    hf = h5py.File(file, 'r')
    g_params = hf.get('Parameters')
    t = np.array(g_params.get('t'))

    g_x = hf.get('X')
    x = np.array(g_x.get('znc'))
    x = x[ind0:ind1]
    g_y = hf.get('Y')
    y = np.array(g_y.get('znc'))
    y = y[ind0:ind1]
    g_z = hf.get('Z')
    z = np.array(g_z.get('znc'))
    z = z[ind0:ind1]

    g_hydro = hf.get('hydro')
    data_hydro = np.array(g_hydro.get('data'))
    data_hydro = data_hydro[ind0:ind1,ind0:ind1,ind0:ind1,:]
    rho = data_hydro[:,:,:,0]
    vx = data_hydro[:,:,:,2]/rho
    vy = data_hydro[:,:,:,3]/rho
    vz = data_hydro[:,:,:,4]/rho

    hf.close()

    print('Dimensions of the grid     :',len(x)," x ",len(y)," x ",len(z))
    print('Time           : ',t)

    return [x,y,z,vx.T, vy.T, vz.T,rho.T]

def readh5files(file,leng):

    print('DATA FROM FILE:      ',file)
    hf = h5py.File(file, 'r')

    rho = np.array(hf.get('rho'))
    vx = np.array(hf.get('vx'))
    vy = np.array(hf.get('vy'))
    vz = np.array(hf.get('vz'))
    Bx = np.array(hf.get('Bx'))
    By = np.array(hf.get('By'))
    Bz = np.array(hf.get('Bz'))

    a = np.linspace(-0.5,0.5,int(2*leng-2))
    dx = a[1]-a[0]
    x = np.linspace(a[0]-dx,a[-1]+dx,leng)
    y = np.linspace(x[0],x[-1],leng)
    z = np.linspace(x[0],x[-1],leng)

    hf.close()

    return [x,y,z,vx, vy, vz,rho]
#===============================================================================================
def deriv(varmean,x,y,z,lenx,leny,lenz):

    dx = x[2]-x[0]
    dy = y[2]-y[0]
    dz = z[2]-z[0]

    lenx = int(lenx/2)
    leny = int(leny/2)
    lenz = int(lenz/2)

    dvardx = np.zeros((lenx,leny,lenz))
    dvardy = np.zeros((lenx,leny,lenz))
    dvardz = np.zeros((lenx,leny,lenz))

    for i in range(0,lenx):
        for j in range(0,leny):
            for k in range(0,lenz-2):
                dvardz[i,j,k+1] = (varmean[i,j,k+2]-varmean[i,j,k])/(2*dz)
        for j in range(0,lenz):
            for k in range(0,leny-2):
                dvardy[i,k+1,j] = (varmean[i,k+2,j]-varmean[i,k,j])/(2*dy)
    for i in range(0,leny):
        for j in range(0,lenz):
            for k in range(0,lenx-2):
                dvardx[k+1,i,j] = (varmean[k+2,i,j]-varmean[k,i,j])/(2*dx)

    for i in range(0,lenx):
        for j in range(0,leny):
            dvardz[i,j,0] = (varmean[i,j,1]-varmean[i,j,0])/dz
            dvardz[i,j,lenz-1] = (varmean[i,j,lenz-1]-varmean[i,j,lenz-2])/dz
        for j in range(0,lenz):
            dvardy[i,0,j] = (varmean[i,1,j]-varmean[i,0,j])/dy
            dvardy[i,leny-1,j] = (varmean[i,leny-1,j]-varmean[i,leny-2,j])/dy
    for i in range(0,leny):
        for j in range(0,lenz):
            dvardx[0,i,j] = (varmean[1,i,j]-varmean[0,i,j])/dx
            dvardx[lenx-1,i,j] = (varmean[lenx-1,i,j]-varmean[lenx-2,i,j])/dx

    return dvardx,dvardy,dvardz

#===========================================================================================

def dv(varmean,lenx,leny,lenz):

    dvardx = np.zeros((lenx,leny,lenz))
    dvardy = np.zeros((lenx,leny,lenz))
    dvardz = np.zeros((lenx,leny,lenz))


    for i in range(0,lenx):
        for j in range(0,leny):
            for k in range(0,lenz-2):
                dvardz[i,j,k+1] = (varmean[i,j,k+2]-varmean[i,j,k])/2
        for j in range(0,lenz):
            for k in range(0,leny-2):
                dvardy[i,k+1,j] = (varmean[i,k+2,j]-varmean[i,k,j])/2
    for i in range(0,leny):
        for j in range(0,lenz):
            for k in range(0,lenx-2):
                dvardx[k+1,i,j] = (varmean[k+2,i,j]-varmean[k,i,j])/2

    for i in range(0,lenx):
        for j in range(0,leny):
            dvardz[i,j,0] = (varmean[i,j,1]-varmean[i,j,0])
            dvardz[i,j,lenz-1] = (varmean[i,j,lenz-1]-varmean[i,j,lenz-2])
        for j in range(0,lenz):
            dvardy[i,0,j] = (varmean[i,1,j]-varmean[i,0,j])
            dvardy[i,leny-1,j] = (varmean[i,leny-1,j]-varmean[i,leny-2,j])
    for i in range(0,leny):
        for j in range(0,lenz):
            dvardx[0,i,j] = (varmean[1,i,j]-varmean[0,i,j])
            dvardx[lenx-1,i,j] = (varmean[lenx-1,i,j]-varmean[lenx-2,i,j])

    #DIVERGENCE:

    div = dvardx+dvardy+dvardz

    return div

#===========================================================================================

def main():

    length = int(sys.argv[1])
    bx0 = '3e-4'
    path= '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/DATA/KHI/res_'+str(length)+'_'+str(length)+'_'+str(length)+'/CVP--bx'+bx0+'--'+str(length)+'--rndom/data'
    path_save = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/energy_dens_ev/KHI/growth_rates'

    file0 = path+'/KHI-00000000.h5'

    vars = readh5files(file0,length)

    t = np.linspace(0,40,201)

    x = vars[0].astype(np.float64)
    y = vars[1].astype(np.float64)
    z = vars[2].astype(np.float64)

    mid_index = int(len(x)/2)

    vx = np.array(vars[3])
    vy = np.array(vars[4])
    vz = np.array(vars[5])

    dvx = deriv(vx,x,y,z,len(x),len(y),len(z))
    dvy = deriv(vy,x,y,z,len(x),len(y),len(z))
    dvz = deriv(vz,x,y,z,len(x),len(y),len(z))

    curl = np.array([dvz[1]-dvy[2],dvx[2]-dvz[0],dvy[0]-dvx[1]])
    mod_curl = np.sqrt(curl[0]**2+curl[1]**2+curl[2]**2)

    #curl1 = mod_curl[:,:,:]
    #curl2 = mod_curl[:,:,:]

    #meancurl1 = np.mean(curl1)
    #meancurl2 = np.mean(curl2)

    #curl_tot = 0.5*(abs(meancurl1)+abs(meancurl2))

    curl_tot = np.mean(mod_curl)

    g_old = 1/0.01*0.5*0.1425

    ctt = g_old/curl_tot

    output = open(path_save+'/av_growth--bx'+bx0+'--'+str(length)+'.dat','w')
    output.write('# time \t rot \t growth \t vmean \t scale \t ctt #\n')

    for i in range(0,len(t)):
        arx = format(int(2*i), "08")
        vars = readh5files(path+'/KHI-'+arx+'.h5',length)
        vx = np.array(vars[3])
        vy = np.array(vars[4])
        vz = np.array(vars[5])

        dvx = deriv(vx,x,y,z,len(x),len(y),len(z))
        dvy = deriv(vy,x,y,z,len(x),len(y),len(z))
        dvz = deriv(vz,x,y,z,len(x),len(y),len(z))

        curl = np.array([dvz[1]-dvy[2],dvx[2]-dvz[0],dvy[0]-dvx[1]])
        mod_curl = np.sqrt(curl[0]**2+curl[1]**2+curl[2]**2)

    #    curl1 = mod_curl[:mid_index,:,:]
    #    curl2 = mod_curl[mid_index:,:,:]

    #    meancurl1 = np.mean(curl1)
    #    meancurl2 = np.mean(curl2)

    #    curl_tot = 0.5*(abs(meancurl1)+abs(meancurl2))

        curl_tot = np.mean(mod_curl)

        growth= ctt*curl_tot

        vmean = np.sqrt(np.sum(vx**2+vy**2+vz**2)/length**3)

        scale = vmean/curl_tot

        output.write('%f \t %.10f \t %.10f \t %.10f \t %.10f \t %.7f \n'%(t[i],curl_tot,growth,vmean,scale,ctt))

    output.close()

    return()

def main2():

    length = int(sys.argv[1])
    bx0 = '3e-4'
    path= '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/DATA/KHI/res_'+str(length)+'_'+str(length)+'_'+str(length)+'/CVP--bx'+bx0+'--'+str(length)+'--rndom/data'
    path_save = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/energy_dens_ev/KHI/growth_rates'

    file0 = path+'/KHI-00000000.h5'

    vars = readh5files(file0,length)

    t = np.linspace(0,40,201)

    x = vars[0].astype(np.float64)
    y = vars[1].astype(np.float64)
    z = vars[2].astype(np.float64)

    mid_index1 = int(len(y)/4)
    mid_index2 = int(len(y)/2)
    mid_index3 = int(len(y)*3/4)
    mid_box1 = int(len(y)/8)
    mid_box2 = int(len(y)*3/8)
    mid_box3 = int(len(y)*5/8)
    mid_box4 = int(len(y)*7/8)

    vx = np.array(vars[3])
    vy = np.array(vars[4])
    vz = np.array(vars[5])

    dvx1 = abs(np.mean(vx[:,:int(mid_index1),:])-np.mean(vx[:,int(mid_index1):mid_index2,:]))/abs(x[mid_box1]-x[mid_box2])
    dvx2 = abs(np.mean(vx[:,mid_index2:int(mid_index3),:])-np.mean(vx[:,int(mid_index3):,:]))/abs(x[mid_box3]-x[mid_box4])

    dv_tot = 0.5*(dvx1+dvx2)

    g_old = 1/0.01*0.5*0.1425

    ctt = g_old/dv_tot


    output = open(path_save+'/av_growth--bx'+bx0+'--'+str(length)+'--v2.dat','w')
    output.write('# time \t dv \t growth \t vmean \t scale \t ctt #\n')

    for i in range(0,len(t)):
        arx = format(int(2*i), "08")
        vars = readh5files(path+'/KHI-'+arx+'.h5',length)
        vx = np.array(vars[3])

        dvx1 = abs(np.mean(vx[:,:int(mid_index1),:])-np.mean(vx[:,int(mid_index1):mid_index2,:]))/abs(x[mid_box1]-x[mid_box2])
        dvx2 = abs(np.mean(vx[:,mid_index2:int(mid_index3),:])-np.mean(vx[:,int(mid_index3):,:]))/abs(x[mid_box3]-x[mid_box4])

        dv_tot = 0.5*(dvx1+dvx2)

        growth= ctt*dv_tot

        vmean = np.sqrt(np.sum(vx**2+vy**2+vz**2)/length**3)

        scale = vmean/dv_tot

        output.write('%f \t %.10f \t %.10f \t %.10f \t %.10f \t %.7f \n'%(t[i],dv_tot,growth,vmean,scale,ctt))

    output.close()

    return()

main()
