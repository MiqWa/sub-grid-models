//
//  energy_ev.c
//  
//
//  Created by Miquel Miravet on 16/04/2021.
//

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

void energy_ev (double std, int lent, double timestep, double gamma_mri, double gamma_pi_fact, double energy_t[], double energy_pi[]){

	/* STRANG-SPLITTING */
	
	int i ;
        
	double et1, et2, epi1, epi2; 

	double expot = timestep*0.5*gamma_mri ;
	
	/*printf("GAMMA_MRI	= %.15f\n", gamma_mri);*/
	for (i = 1; i < lent ; i++){


		et1 = energy_t[i-1]*exp(expot);
		
		epi1 = energy_pi[i-1]*exp(timestep*0.5*gamma_pi_fact*sqrt(energy_t[i-1]));
		

		et2 = et1-epi1*timestep*gamma_pi_fact*sqrt(et1);
		
		/*printf("GAMMA_PI	= %.15f\n", gamma_pi_fact*sqrt(et1));*/

		epi2 = epi1-timestep*std*epi1*sqrt(epi1);

		
		energy_t[i] = et2*exp(expot);
		
		/*printf("ENERGY_T	= %.30f\n", energy_t[i]);*/

		energy_pi[i] = epi2*exp(timestep*0.5*gamma_pi_fact*sqrt(energy_t[i-1]));


	}

	/* DIRECT INTEGRATION 

	int i;

	for (i = 1; i < lent ; i++){

		energy_t[i] = energy_t[i-1]*exp(timestep*(gamma_mri-gamma_pi_fact*energy_pi[i-1]/sqrt(energy_t[i-1])));
		energy_pi[i] = energy_pi[i-1]*exp(timestep*(gamma_pi_fact*sqrt(energy_t[i])-std*sqrt(energy_pi[i-1])));
	}*/
}
		

