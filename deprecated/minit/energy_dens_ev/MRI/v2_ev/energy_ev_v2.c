//
//  energy_ev.c
//  
//
//  Created by Miquel Miravet on 16/04/2021.
//

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

void energy_ev_v2 (double std, int lent, double timestep, double gamma_mri, double gamma_pi_fact, double energy_t[], double energy_pi[]){

	/* STRANG-SPLITTING */
	
	int i,j,k ;
        
	double et1, et2, epi1, epi2; 

	double expot = timestep*0.5*gamma_mri ;

	double gamma_pi, energy0, energy_pi0;
	
	for (i = 1; i < lent ; i++){
		
		/*energy_t and energy_pi*/

		et1 = energy_t[i-1]*exp(expot);
                epi1 = energy_pi[i-1]*exp(timestep*0.5*gamma_pi_fact*sqrt(et1));

                et2 = et1-timestep*epi1*gamma_pi_fact*sqrt(et1);
                epi2 = epi1-timestep*std*epi1*sqrt(epi1);

                energy_t[i] = et2*exp(expot);
                energy_pi[i] = epi2*exp(timestep*0.5*gamma_pi_fact*sqrt(energy_t[i]));
	
		gamma_pi = gamma_pi_fact*sqrt(energy_t[i]);

		j = i;

		if (gamma_pi/gamma_mri >= 1){
			
			energy0 = energy_t[i];
			energy_pi0 = energy_pi[i];

			break;
		}

	}

	for (k = j+1; k < lent ; k++){

		et1 = energy_t[k-1]*exp(expot);
		et2 = et1-timestep*energy_pi0*gamma_pi_fact*sqrt(et1);
		energy_t[k] = et2*exp(expot);
	}

}
		

