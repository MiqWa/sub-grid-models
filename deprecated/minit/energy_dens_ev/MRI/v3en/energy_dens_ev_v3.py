#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 16 13:46:41 2021

@author: miquelmiravet
"""

##################################################
#
#       ENERGY DENSITY OF TURBULENT STRESSES     
#                       
##################################################

"""
    We are going to solve the evolution equations for the energy density in turbulent stresses 
    and the energy density of the parasitic instabilities. To do so, we need an averaged value of the alpha coefficient and its trace, in 
    order to get M_ij and therefore s_max = 0.2*B_0 = 0.2*sqrt(e_t*Tr(alpha_ij)). We also need the initial value for e_T, which is given by 
    the initial value of 0.5*rho*traceR. e_pi(0) = 0. The source term S_TD depends on e_pi. tau_mri is constant over time.
    
"""

import os
import numpy as np
import h5py
import csv
from matplotlib import pyplot as plt
import math as m
import numpy.ctypeslib as npct
from numpy.ctypeslib import ndpointer
from ctypes import c_int, c_double

#------------------------------------------------------------
# input type for the function
# must be a double array, with single dimension that is contiguous
array_1d_float = npct.ndpointer(dtype=np.float64, ndim=1, flags='CONTIGUOUS')

libenergyev = npct.load_library("libenergyev", ".")
libenergyev.energy_ev_v3.restype = None
libenergyev.energy_ev_v3.argtypes = [c_double, c_int,c_int,c_double,c_double, c_double, c_double,c_double, array_1d_float, array_1d_float, array_1d_float, array_1d_float]

#===============================================================================================

#===============================================================================================

def alpha_trace(res,t):
    
    path = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/coefficients/'+res+'/fixed-r/'
    var = np.loadtxt(path+'alphaR_r-1.csv', usecols=(1,2,3,4,5,6))
    var = np.swapaxes(var,0,1)
   
    alphatrace = var[0,:]+var[3,:]+var[5,:]
    
    alphatrace_time = np.sum(alphatrace)/len(t[:])
    
    print('TRACE ALPHA COEFF    = ', alphatrace_time)
    
    return alphatrace_time 

def initial_value_energy_dens(path):
        
    fileen = path+'energy_traceR_r-1'
    energy= np.loadtxt(fileen+'.csv')
   
    energy0 = energy[1,1]
    energytrace = energy[:,1]
    
    filerho = path +'rho_mean_r-1'
    rho = np.loadtxt(filerho+'.csv')
    rho0 = rho[0,1]
    
    return energy0, energytrace, rho0

def energy_ev(std_fact,sigma,factor_en,tfinal, gamma_mri, gamma_pi_fact,filtersize, energy_ini, rho):
    
    print('\nSIGMA    = ', sigma)
#    print('C            = ', C)
    
    gamma_pi = gamma_pi_fact*(energy_ini)**(0.5)
    print('GAMMA_MRI    = ', gamma_mri)
    print('GAMMA_PI     = ', gamma_pi)
    
    timestep= 5e-8
    print('TIMESTEP     = ', timestep)
    
    timestep = 0.001*2.99792458e10*timestep
    tfinal = tfinal*0.001*2.99792458e10
    lent = int(round((tfinal)/timestep))+1
     
    print('Nº OF TIMESTEPS  = ', lent)

    time = np.linspace(0,tfinal*1000/2.99792458e10,lent)

    energy_pi0=energy_ini*factor_en
    
    energy_tc = np.zeros(lent)
    energy_pic = np.zeros(lent)

    lentplot = int((lent-1)/100000)
    
    energy_t = np.zeros(lentplot)
    energy_pi = np.zeros(lentplot)
    time_plot = np.zeros(lentplot)

    for l in range(0,lentplot):

        time_plot[l] = time[100000*l]

    print('ENERGY_T[0]  = ', energy_ini)
    print('ENERGY_PI[0] = ', energy_pi0)
    print('RHO          = ', rho)

    libenergyev.energy_ev_v3(std_fact,lent,lentplot,timestep,gamma_mri, gamma_pi_fact, energy_ini, factor_en, energy_t,energy_pi,energy_tc, energy_pic)
 

    return time_plot, lent, energy_t, energy_pi

def energy_files(time, energy_et, energy_pi, lent):
    
    with open('energy_dens_ev.csv', 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        cols = zip(time, energy_et,energy_pi)
        writer.writerows(cols)
    
    return
    
def main():

#    factor_pi = [0.1,1,10,1e2]
#    factor_c = [0.8,1,1.5,2,2.2]
#    factor_en = [1e-3,1e-2,1,10]

    sigma = 0.27
    factor_c = 1
    factor_en = 1

    lenphi = 240 
    
    if lenphi == 400:
       lenr = 100
       lenz = 100
       lent = 61
       tfinal = 30

    elif lenphi == 800 :

       lenr = 200
       lenz = 200
       lent = 26
       tfinal = 12.5

    elif lenphi == 240 :

       lenr = 60
       lenz = 60
       lent = 61
       tfinal = 30

    elif lenphi == 304 :

       lenr = 76
       lenz = 76
       lent = 61
       tfinal = 30
       
    res = str(lenr)+'_'+str(lenphi)+'_'+str(lenz)
    
    t = np.linspace(0,tfinal,lent)

#================================================

    r0 = 15.5e5
    omega0tilde = 1824*3.33564095e-11
    r0tilde = 31e5
    omega0 = 767*3.33564095e-11
    rho0 = 2.47e12
    rho0tilde = 2.47e13
#    scaling_en= (r0/r0tilde)**2*(omega0/omega0tilde)**2*(rho0/rho0tilde)
#    print(scaling_b)
    q = 1.25
    omega = omega0tilde
    gamma_mri = q*omega
    
    sigma = sigma*omega0tilde/omega0
    filtersize = 460765
    print('FILTERSIZE  : ', filtersize)

#=================================================

    path_energy = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/energy_dens_ev/'+res+'/'
    
    energy_ini, energy_trace, rho = initial_value_energy_dens(path_energy)
    
    alphatrace = alpha_trace(res,t)

    alphatrace = 14 

    gamma_pi_fac = 2*sigma/(filtersize)*(q/(4-q))**(0.5)*alphatrace**(0.5)/rho**(0.5)*2*3.14159265359
    
    std_fact= factor_c*2*sigma*(q/(4-q))**(0.5)*alphatrace**(0.5)/(rho**(0.5)*filtersize)*2*3.14159265359

    time, lent, energy_et, energy_pi = energy_ev(std_fact, sigma, factor_en,tfinal, gamma_mri, gamma_pi_fac, filtersize,energy_ini, rho)


       
#    fig, ax = plt.subplots(4,4,sharex = 'row', sharey = 'col', figsize = (24,20))

#    color = ['tab:red','tab:blue','tab:green','tab:orange', 'tab:purple']

#    for i in range(0,4):
#        for j in range(0,4):
        
#            ax[i,j].tick_params(axis='x',labelsize = 12)
#            ax[i,j].tick_params(axis='y',labelsize = 12)


#    for axs in ax.flat:
#        axs.label_outer()
   
#    plt.subplots_adjust(wspace = 0.1)
#    plt.suptitle('TIMESTEP: 9e-7 ms', fontsize = '30')

#    for i in range(0,4):
#        for j in range(0,4):
#            for k in range(0,5):
                 
#                gamma_pi_fac = factor_pi[i]*2*0.27*2*3.14159265359/filtersize*(q/(4-q))**(0.5)*alphatrace**(0.5)/(rho**(0.5)*scaling_b)

#                C = factor_c[k]*factor_pi[i]*0.52*2*3.14159265359*(q/(4-q))**(0.5)*alphatrace**(0.5)/scaling_b

    
#               time, lent, energy_et, energy_pi = energy_ev(C, factor_pi[i], factor_en[j],tfinal, gamma_mri, gamma_pi_fac, filtersize,energy_ini, rho)

#                lentplot = int((lent-1)/1000)

#                energy_etplot = np.zeros(lentplot)
#                energy_piplot = np.zeros(lentplot)
#                time_plot = np.zeros(lentplot)
                
#                for l in range(0,lentplot):

#                    energy_etplot[l] = energy_et[1000*l]
#                    energy_piplot[l] = energy_pi[1000*l]
#                    time_plot[l] = time[1000*l]
                
                
#                ax[i,j].plot(time_plot, energy_etplot, linestyle = 'solid', color = color[k], label = r'$e_T$ / $C_{\rm fact} = $'+str(factor_c[k]))
#                ax[i,j].plot(time_plot, energy_piplot, linestyle = 'dashed', color = color[k], label = r'$e_{PI}$ / $C_{\rm fact} = $'+str(factor_c[k]))
#                ax[i,j].set_yscale('log')
#                ax[i,j].set_ylim([1e-33,1e-15])
#                ax[i,j].set_xlim([0,30])
#                if i == 3 :
#                    ax[i,j].set_xlabel('t [ms]', fontsize = '15')
#                if j == 0 :    
#                    ax[i,j].set_ylabel('energy dens', fontsize = '15')
                
#                ax[i,j].set_title(' FACTOR_PI = '+str(factor_pi[i])+r' // $e_{\rm PI} = $'+str(factor_en[j])+r'$e_{\rm T}$',fontsize = '15')

#                if i ==3 and j == 3: 
#                    ax[i,j].legend( bbox_to_anchor=(1.5,0),loc = 'lower right', ncol = 1, frameon = 'True', fontsize = '12')
    
#    plt.savefig('energy_ev_delta_t_9e-7ms.eps')
#    plt.show()

    fig, ax = plt.subplots()
    
    t = np.linspace(0,tfinal,61)
    
    ax.plot(time, energy_et, linestyle = 'solid', color = 'r', label = r'$e_T$')
    ax.plot(time, energy_pi, linestyle = 'dashed', color = 'k', label = r'$e_{PI}$')

    ax.set_yscale('log')
    ax.set_xlabel('t [ms]')
    ax.set_xlim([0,30])
    ax.set_ylabel('energy dens')
#    plt.savefig('energy_newC.eps')
    plt.show()
#    energy_files(time, energy_et,energy_pi,lent)
    
#    os.system('mv energy_dens_ev.csv '+path_energy+'/.')


if __name__ == "__main__":
    main()    
