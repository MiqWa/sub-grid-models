//
//  energy_ev.c
//  
//
//  Created by Miquel Miravet on 16/04/2021.
//

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

void energy_ev_v3 (double std, int lent, int lent2, double timestep, double gamma_mri, double gamma_pi_fact, double energy_ini, double factor_en, double energy_t2[], double energy_pi2[], double energy_t[], double energy_pi[]){

	/* STRANG-SPLITTING */
	
	int i, j ;
        
	double et1, et2, epi1, epi2; 

	double expot = timestep*0.5*gamma_mri ;
	
	/*double *energy_t ;
	double *energy_pi; */

	energy_t[0] = energy_ini;
	energy_pi[0] = energy_ini*factor_en;
	
	printf("GAMMA_MRI	= %.15f\n", gamma_mri);
	for (i = 1; i < lent ; i++){
		
		/*energy_t and energy_pi*/

		et1 = energy_t[i-1]*exp(expot);
		
		epi1 = energy_pi[i-1]*exp(timestep*0.5*gamma_pi_fact*sqrt(energy_t[i-1]));
		

		et2 = et1-timestep*energy_pi[i-1]*gamma_pi_fact*sqrt(energy_t[i-1]);
		
		/*printf("GAMMA_PI	= %.15f\n", gamma_pi_fact*sqrt(et1));*/
		epi2 = epi1-timestep*std*energy_pi[i-1]*sqrt(energy_pi[i-1]);

		
		energy_t[i] = et2*exp(expot);


		energy_pi[i] = epi2*exp(timestep*0.5*gamma_pi_fact*sqrt(energy_t[i]));

		/*printf("%.30f\n",energy_t[i]);*/

	}

	for (j = 0; j < lent2 ; j++){

		energy_t2[j] = energy_t[100000*j];
		energy_pi2[j] = energy_pi[100000*j];
	}


	/*free(energy_t);
	free(energy_pi);*/

}
		

