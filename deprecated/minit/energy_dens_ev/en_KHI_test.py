 #!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 26 09:55:41 2022

@author: miquelmiravet
"""

##################################################
#
#             NEW GROWTH RATE FOR KHI
#
##################################################

import os
import numpy as np
import h5py
import sys
from scipy import interpolate
import matplotlib
from matplotlib import pyplot as plt
from matplotlib import rc
import math as m
import numpy.ctypeslib as npct
from numpy.ctypeslib import ndpointer
from ctypes import c_int, c_double
import statistics
import seaborn as sns
import cmocean

rc('text', usetex=True)
rc('font', family='serif')

#------------------------------------------------------------
# input type for the function
# must be a double array, with single dimension that is contiguous
array_1d_float = npct.ndpointer(dtype=np.float64, ndim=1, flags='CONTIGUOUS')

libenergyev = npct.load_library("KHI/lib_new_gamma.so", ".")
libenergyev.energy_ev_khi.restype = None
#libenergyev.energy_ev_khi.argtypes = [c_double, c_int,c_double,array_1d_float, array_1d_float]
libenergyev.energy_ev_khi.argtypes = [array_1d_float, c_int,c_double,array_1d_float, array_1d_float]

#===============================================================================================

def readh5files(arx):

    print('DATA FROM FILE:      ',arx)

    with h5py.File(arx, "r") as f:
    # List all groups
        print("Keys: %s" % f.keys())
        a_group_key = list(f.keys())

        Fmean = np.array(f.get('Fmean'))
        sigmaF = np.array(f.get('sigma_F'))
        Rmean = np.array(f.get('Rmean'))
        sigmaR = np.array(f.get('sigma_R'))
        Mmean = np.array(f.get('Mmean'))
        sigmaM = np.array(f.get('sigma_M'))
        rhomean = np.array(f.get('rhomean'))
        time = np.array(f.get('time'))

        M = np.array([Mmean[0,0],Mmean[0,1],Mmean[0,2], Mmean[1,1], Mmean[1,2], Mmean[2,2]])
        errM = np.array([sigmaM[0,0],sigmaM[0,1],sigmaM[0,2],sigmaM[1,1],sigmaM[1,2],sigmaM[2,2]])
        R = np.array([Rmean[0,0],Rmean[0,1],Rmean[0,2], Rmean[1,1], Rmean[1,2], Rmean[2,2]])
        errR = np.array([sigmaR[0,0],sigmaR[0,1],sigmaR[0,2],sigmaR[1,1],sigmaR[1,2],sigmaR[2,2]])
        F = np.array([Fmean[0,1], Fmean[0,2], Fmean[1,2]])
        errF = np.array([sigmaF[0,1],sigmaF[0,2],sigmaF[1,2]])
        f.close()

        print('Time           :',time)

    return [M, R, F, errM, errR, errF, rhomean]


#===============================================================================================

def energy_ev(sc,t, factor_c, energy_ini,gamma,rho):

    timestep= 5e-5
    tfinal = t[-1]
    lent = int(round((tfinal)/timestep))+1
    tnew = np.linspace(0,tfinal,lent)

    interpg = interpolate.interp1d(t, gamma, kind = "cubic")
    gamma_khi = interpg(tnew)
    interpsc = interpolate.interp1d(t,sc,kind = "cubic")
    scale = interpsc(tnew)
    std_fact= factor_c/(rho**(0.5)*scale)*0.14


    energy_t = np.zeros(lent)
    energy_t[0] = energy_ini

    libenergyev.energy_ev_khi(std_fact,lent,timestep,gamma_khi, energy_t)

    ratio = int((lent-1)/(len(t)-1))
    energy_model = np.zeros(len(t))

    for i in range(0,len(t)-1):
        energy_model[i] = energy_t[ratio*i]

    energy_model[-1] = energy_t[-1]

    return energy_model

def stresses_r0(res,t):

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/DATA/KHI/"+res
    Msim = np.zeros((len(t),6))
    Rsim = np.zeros((len(t),6))
    Fsim = np.zeros((len(t),3))
    errMsq = np.zeros((len(t),6))
    errRsq = np.zeros((len(t),6))
    errFsq = np.zeros((len(t),3))
    rhomean = np.zeros(len(t))
    Mmod_arr = np.zeros((3,len(t), 6))
    Rmod_arr = np.zeros((3,len(t), 6))
    Fmod_arr = np.zeros((3,len(t), 3))

    for tt in range(0, len(t)):

        arx = format(int(10*t[tt]), "04")
        Msim[tt], Rsim[tt], Fsim[tt], errMsq[tt], errRsq[tt], errFsq[tt], rhomean[tt] = readh5files(path_data+'/stresses_sigma_r0_KHI-'+arx+'.h5')

    return Msim, Rsim, Fsim, rhomean

#===========================================================================================

def main():

    factor_c = np.linspace(5,10,15)
    energy_ini = 2e-1
    factor_gamma = np.linspace(0.05,0.25,15)

    bx0 = sys.argv[1]

    res = 'res_512_512_512/CVP--bx'+bx0+'--512--rndom/stress_tensors_global'
    #res ='res_128_128_128/CVP--bx3e-4--128--rndom/stress_tensors_global'
    #res3 ='res_256_256_256/CVP--bx3e-2--256--rndom/stress_tensors_global'
    tfinal = 40
    lent = 201
    t = np.linspace(0,tfinal,lent)
    #t = t[1:]
    #t2 = np.linspace(0,tfinal,401)

    gamma_khi0 = np.loadtxt('KHI/growth_rates/av_growth--bx'+bx0+'--512--v2.dat', usecols=2)
    dv_tot = np.loadtxt('KHI/growth_rates/av_growth--bx'+bx0+'--512--v2.dat', usecols=1)
    scale0 = np.loadtxt('KHI/growth_rates/av_growth--bx'+bx0+'--512--v2.dat', usecols=4)
#    ctt = np.loadtxt('KHI/growth_rates/av_growth--bx3e-4--128--v2.dat', usecols=5)

    ctt = 1/0.01*0.5*0.14/dv_tot[0]

    gamma_khi0 = dv_tot

    #scale = scale/ctt
    Msim,Rsim,Fsim,rho = stresses_r0(res,t)
    energy_tracerndm =0.5*rho[0]*(Rsim[:,0]+Rsim[:,3]+Rsim[:,5])
#================================================
    v0 = 0.5
    al = 0.01
    filtersize = 100000000
    scale = 0.25*np.ones(lent)
#=================================================

    fig, ax = plt.subplots(1,sharex = 'all', sharey = 'row', figsize = (8,5.5))

    #color = ['tab:red','tab:blue','tab:purple', 'tab:olive', 'tab:orange']
    color = sns.color_palette('winter',len(factor_c))
    cmap =matplotlib.cm.winter

    ax.tick_params(axis='x',labelsize = 18)
    ax.tick_params(axis='y',labelsize = 18)

    for k in range(0,len(factor_c)):

        gamma_khi = gamma_khi0

        energy_trndm = energy_ev(scale,t, factor_c[k],energy_ini,gamma_khi,rho[0])

        ax.plot(t[:], energy_trndm[:],linewidth = .9, linestyle = 'solid', color = color[k])


    ax.plot(t[:], energy_tracerndm[:], linewidth = 1.2, linestyle = 'dashed', color = 'k', label = '512--rndm')

    energy_trndm = energy_ev(scale,t, 8.6,energy_ini,gamma_khi,rho[0])
    ax.plot(t[:], energy_trndm[:],linewidth = 1.2, linestyle = 'dashdot', color = 'r', label = r'$C = 8.6$ ')

    ax.set_yscale('log')
    ax.set_ylim(bottom = 1e-5)
#    ax.set_xlim([0,36])
    ax.set_xlabel('Time', fontsize = '20')

    ax.set_ylabel(r'$e_{\rm KHI}$', fontsize = '20')
    ax.legend(loc = 'lower right', ncol = 1, frameon = 'True', fontsize = '12')
    ax.set_title(r'$B_{x0}=$'+bx0, fontsize = '24')
    norm = matplotlib.colors.BoundaryNorm(factor_c, cmap.N, extend='both')

    fig.colorbar(matplotlib.cm.ScalarMappable(norm=norm, cmap=cmap),ax=ax, orientation='vertical',label=r'$C$')
#    fig.savefig('KHI/gnew--512--bx'+bx0+'--var_C.pdf')
    plt.close()
#    plt.show()
#================================================================================================
    fig, ax = plt.subplots(1,sharex = 'all', sharey = 'row', figsize = (8,5.5))

    color = sns.color_palette('winter',len(factor_gamma))
    cmap =matplotlib.cm.winter

    ax.tick_params(axis='x',labelsize = 18)
    ax.tick_params(axis='y',labelsize = 18)

    for k in range(0,len(factor_gamma)):

        gamma_khi = gamma_khi0/factor_gamma[k]

        energy_trndm = energy_ev(scale,t, 8.6,energy_ini,gamma_khi,rho[0])

        ax.plot(t[:], energy_trndm[:],linewidth = .9, linestyle = 'solid', color = color[k])

    energy_trndm = energy_ev(scale,t, 8.6,energy_ini,gamma_khi0,rho[0])
    ax.plot(t[:], energy_trndm[:],linewidth = .9, linestyle = 'dashed', color = 'r',label=r'$A_{\gamma}$=1')
    ax.plot(t[:], energy_tracerndm[:], linewidth = 1.2, linestyle = 'dashed', color = 'k', label = '512--rndm')

    ax.set_yscale('log')
    ax.set_ylim(bottom = 1e-5)
    ax.set_xlim([0,40])
    ax.set_xlabel('Time', fontsize = '20')

    ax.set_ylabel(r'$e_{\rm KHI}$', fontsize = '20')
    ax.legend(loc = 'lower right', ncol = 1, frameon = 'True', fontsize = '12')
    ax.set_title(r'$B_{x0}=$'+bx0, fontsize = '24')
    norm = matplotlib.colors.BoundaryNorm(factor_c, cmap.N, extend='both')
    fig.colorbar(matplotlib.cm.ScalarMappable(norm=norm, cmap=cmap),ax=ax, orientation='vertical',label=r'$A_{\gamma}$')
    #fig.savefig('KHI/gnew--512--bx'+bx0+'--var_gamma.pdf')
    plt.close()

#================================================================================================

    timestep= 5e-5
    tfinal = t[-1]
    lent = int(round((tfinal)/timestep))+1
    tnew = np.linspace(0,tfinal,lent)

    interpg = interpolate.interp1d(t,gamma_khi0, kind = "cubic")
    gamma_khi_int = interpg(tnew)
    interpsc = interpolate.interp1d(t,scale0,kind = "cubic")
    scale_int = interpsc(tnew)


    fig, ax = plt.subplots(1,sharex = 'all', sharey = 'row', figsize = (8,5.5))
    ax.tick_params(axis='x',labelsize = 18)
    ax.tick_params(axis='y',labelsize = 18)

    ax.plot(t,gamma_khi0/(1/al*v0*0.1425),marker = 'x',color = 'rebeccapurple')
    ax.plot(tnew,gamma_khi_int/(1/al*v0*0.1425),linestyle='solid',color='goldenrod')

    ax.hlines(y = 1, xmin = t[0],xmax=t[-1],linestyle='dashed',color='grey')

    ax.set_xlim([0,40])
    ax.set_ylabel(r'$\gamma_{\rm KH}(t)/\gamma_{\rm KH}(0)$', fontsize = 20)
    ax.set_xlabel('Time',fontsize = 20)
    ax.set_yscale('log')
    ax.set_title(r'$B_{x0}$='+bx0, fontsize = '24')
    #fig.savefig('KHI/growth_rates/plot_growth--512--bx'+bx0+'--v2.png')
    plt.close()

#================================================================================================

    fig, ax = plt.subplots(1,sharex = 'all', sharey = 'row', figsize = (8,5.5))
    ax.tick_params(axis='x',labelsize = 18)
    ax.tick_params(axis='y',labelsize = 18)

    ax.plot(t,scale0,marker = 'x',color = 'rebeccapurple')
    ax.plot(tnew,scale_int,linestyle='solid',color='goldenrod')
    ax.hlines(y = al, xmin = t[0],xmax=t[-1],linestyle='dashed',color='grey')
    ax.set_xlim([0,40])
    ax.set_ylabel('SCALE', fontsize = 20)
    ax.set_xlabel('Time',fontsize = 20)
    ax.set_yscale('log')
    ax.set_title(r'$B_{x0}$='+bx0, fontsize = '24')
    #fig.savefig('KHI/growth_rates/plot_scale--512--bx'+bx0+'--v2.png')
#    plt.close()


#================================================================================================

    fig, ax = plt.subplots(1,4,sharex = 'all', sharey = 'row', figsize = (30,5.5))

    new_c = np.array([6,7,8,9])

    for i in range(0,len(new_c)):
        ax[i].tick_params(axis='x',labelsize = 18)
        ax[i].tick_params(axis='y',labelsize = 18)

        for k in range(0,len(factor_gamma)):

            gamma_khi = gamma_khi0*factor_gamma[k]

            energy_trndm = energy_ev(scale,t, new_c[i],energy_ini,gamma_khi,rho[0])

            ax[i].plot(t[:], energy_trndm[:],linewidth = .9, linestyle = 'solid', color = color[k])

        energy_trndm = energy_ev(scale,t, new_c[i],energy_ini,gamma_khi0,rho[0])
        ax[i].plot(t[:], energy_trndm[:],linewidth = .9, linestyle = 'dashed', color = 'r',label=r'$A_{\gamma}$=1')
        ax[i].plot(t[:], energy_tracerndm[:], linewidth = 1.2, linestyle = 'dashed', color = 'k', label = '512--rndm')
        ax[i].set_title(r'$C=$ '+str(new_c[i]), fontsize = '24')
        ax[i].set_yscale('log')
        ax[i].set_ylim([1e-4,5])
        ax[i].set_xlim([0,40])
        ax[i].set_xlabel('Time', fontsize = '20')
        ax[i].legend(loc = 'lower right', ncol = 1, frameon = 'True', fontsize = '12')

    ax[0].set_ylabel(r'$e_{\rm KHI}$', fontsize = '20')
    fig.suptitle(r'$B_{x0}=$ '+bx0, fontsize = '30')
    norm = matplotlib.colors.BoundaryNorm(factor_gamma, cmap.N, extend='both')
    fig.colorbar(matplotlib.cm.ScalarMappable(norm=norm, cmap=cmap),ax=ax[3], orientation='vertical',label=r'$A_{\gamma}$')
#    plt.show()
    fig.savefig('KHI/playing/gnew--512--bx'+bx0+'--var_fac-c.pdf')
#    plt.close()

#================================================================================================

    fig, ax = plt.subplots(1,4,sharex = 'all', sharey = 'row', figsize = (30,5.5))

    #new_gamma = np.array([3,5,7,10])
    new_gamma = np.array([0.05,0.1,0.15,0.2])
    for i in range(0,len(new_gamma)):
        ax[i].tick_params(axis='x',labelsize = 18)
        ax[i].tick_params(axis='y',labelsize = 18)

        gamma_khi = gamma_khi0*new_gamma[i]

        for k in range(0,len(factor_c)):

            energy_trndm = energy_ev(scale,t, factor_c[k],energy_ini,gamma_khi,rho[0])

            ax[i].plot(t[:], energy_trndm[:],linewidth = .9, linestyle = 'solid', color = color[k])

        energy_trndm = energy_ev(scale,t, 8.6,energy_ini,gamma_khi,rho[0])
        ax[i].plot(t[:], energy_trndm[:],linewidth = .9, linestyle = 'dashed', color = 'r',label=r'$C$=8.6')
        ax[i].plot(t[:], energy_tracerndm[:], linewidth = 1.2, linestyle = 'dashed', color = 'k', label = '512--rndm')
        ax[i].set_title(r'$A_{\gamma}=$ '+str(new_gamma[i]), fontsize = '24')
        ax[i].set_yscale('log')
        ax[i].set_ylim([1e-4,5])
        ax[i].set_xlim([0,40])
        ax[i].set_xlabel('Time', fontsize = '20')
        ax[i].legend(loc = 'lower right', ncol = 1, frameon = 'True', fontsize = '12')

    ax[0].set_ylabel(r'$e_{\rm KHI}$', fontsize = '20')
    fig.suptitle(r'$B_{x0}=$'+bx0, fontsize = '30')
    norm = matplotlib.colors.BoundaryNorm(factor_c, cmap.N, extend='both')
    fig.colorbar(matplotlib.cm.ScalarMappable(norm=norm, cmap=cmap),ax=ax[3], orientation='vertical',label=r'$C$')
#    plt.show()
    fig.savefig('KHI/playing/gnew--512--bx'+bx0+'--var_fac-c--bis.pdf')
#    plt.close()

    return()

if __name__ == "__main__":
    main()
