#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stdbool.h>

void box_kernel_cart(int lenx, int leny, int lenz, double x[], double y[], double z[], int lenxfilter, int lenyfilter, int lenzfilter, double xfilter[], double yfilter[], double
zfilter[], float filtersize, double var[][leny][lenz], double varmean[][lenyfilter][lenzfilter]){

  float difx, dify, difz,diflx1,diflx2,difly1,difly2,diflz1,diflz2;
  int i, j, k, p, q, l;
  float dx = x[1]-x[0];
  float lbox = lenx*dx;

  double sum= 0;
  int sum2= 0;

/*  double *** G = (double ***)malloc(lenx*sizeof(double**));
    for (i = 0; i< lenx; i++) {
      G[i] = (double **) malloc(leny*sizeof(double *));
      for (j = 0; j < leny; j++) {
        G[i][j] = (double *)malloc(lenz*sizeof(double));
        }
      }
*/
  double deltax = filtersize*0.500001;
  double deltay = filtersize*0.500001;
  double deltaz = filtersize*0.500001;

  int G = 0;

  for (p = 0 ; p < lenxfilter ; p++){
    printf("xfilter = %6f\n", xfilter[p]);
    for (i = 0 ; i < lenx ; i++){
      //    printf("x_i = %6f\n",x[i]);
      //    printf("delta x = %6f\n", deltax);
          difx = fabs(xfilter[p]-x[i]);
    //      printf("difx = %6f\n", difx);
          diflx1 = fabs(xfilter[p]-(x[i]+lbox));
    //      printf("df1 = %6f\n", diflx1);
          diflx2 = fabs(xfilter[p]-(x[i]-lbox));
    //      printf("df2 = %6f\n", diflx2);

          if((difx <= deltax || diflx1 <= deltax || diflx2 <= deltax)){
                G = 1;
              }
				      else{
                G = 0;
				      }
    //          printf("G = %3d\n",G);
    //          printf("%3d\n",i);
    //          printf("======================================\n");
              sum2 += G;
              G = 0;
            }
        printf("N = %3d \n",sum2);
        printf("################################################################################################################################\n");
        sum = 0;
		    sum2 = 0;
        G = 0;
  }

/*  for (i = 0; i< lenx; i++) {
    for (j = 0; j < leny; j++) {
      free(G[i][j]);
    }
	  free(G[i]);
  }
  free(G);
  */
}
