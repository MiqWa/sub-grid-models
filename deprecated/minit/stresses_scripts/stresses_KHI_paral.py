#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 28 14:38:10 2021

@author: miquelmiravet
"""

#############################################
#                                           #
#       STRESS TENSORS EVOLUTION KHI        #
#                                           #
#############################################

"""
Calculation of the energy density of the stresses directly from the data of the
numerical simulations
"""

import os
import numpy as np
import sys
import csv
import numpy.ctypeslib as npct
from ctypes import c_int
import h5py
import concurrent.futures

#=================================================================================
#=================================================================================
# input type for the function
# must be a double array, with single dimension that is contiguous
array_3d_float = npct.ndpointer(dtype=np.float64, ndim=3, flags='CONTIGUOUS')
array_1d_float = npct.ndpointer(dtype=np.float64, ndim=1, flags='CONTIGUOUS')

libboxkernel = npct.load_library("/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/gradient-model-test/khi_kernels/libboxkernel", ".")
libboxkernel.box_kernel_cart.restype = None
libboxkernel.box_kernel_cart.argtypes = [c_int, c_int, c_int, array_1d_float, array_1d_float, array_1d_float, c_int, c_int, c_int, array_1d_float, array_1d_float, array_1d_float, array_1d_float, array_3d_float, array_3d_float]
#===========================================================================================

def readh5files(file):

    print('DATA FROM FILE:      ',file)
    hf = h5py.File(file, 'r')

    t = np.array(hf.get('time'))
    t0 = np.array(hf.get('t0'))
    tf = np.array(hf.get('tf'))
    x = np.array(hf.get('x'))
    y = np.array(hf.get('y'))
    z = np.array(hf.get('z'))
    rho = np.array(hf.get('rho'))
    vx = np.array(hf.get('vx'))
    vy = np.array(hf.get('vy'))
    vz = np.array(hf.get('vz'))
    Bx = np.array(hf.get('Bx'))
    By = np.array(hf.get('By'))
    Bz = np.array(hf.get('Bz'))


#    print('Dimensions of the grid     :',len(x)," x ",len(y)," x ",len(z))
    print('Time           : ',t)
    print('Initial time     : ',t0)
    print('Final time   : ',tf)

    return [t,x,y,z,Bx, By, Bz, vx, vy, vz,rho]

#===========================================================================================

def box_filter(items):

    var,filtersize,x,y,z,lenx,leny,lenz,xfilter,yfilter,zfilter = items

    var = np.asarray(var, order = 'C')
    varmean = np.zeros((len(xfilter),len(yfilter),len(zfilter)))

    libboxkernel.box_kernel_cart(lenx, leny,lenz, x, y, z, len(xfilter), len(yfilter), len(zfilter), xfilter, yfilter, zfilter, filtersize, var, varmean)

    return varmean

#===========================================================================================

def stresses(v, b, rho, filtersize, x,y, z, xfilter, yfilter, zfilter):

    vars = [rho,v[0],v[1],v[2],b[0],b[1],b[2]]

    items = ((var,filtersize,x,y,z,xfilter,yfilter,zfilter) for var in vars)
    items = tuple(items)

    with concurrent.futures.ProcessPoolExecutor() as executor:
        resmean = executor.map(stresses,items)

    rhomean = np.array(resmean[0])
    vmean = np.array([resmean[1],resmean[2],resmean[3]])
    bmean = np.array([resmean[4],resmean[5],resmean[6]])

    vsqmean = np.zeros((3,3,len(xfilter),len(yfilter),len(zfilter)))

    Rmean = np.zeros((3,3,len(xfilter),len(yfilter),len(zfilter)))

    bsqmean = np.zeros((3,3,len(xfilter),len(yfilter),len(zfilter)))

    Mmean = np.zeros((3,3,len(xfilter),len(yfilter),len(zfilter)))

    bvmean = np.zeros((3,3,len(xfilter),len(yfilter),len(zfilter)))

    Fmean = np.zeros((3,3,len(xfilter),len(yfilter),len(zfilter)))

    for i in range(0,3):
        for j in range(0,3):

            bsqmean[i,j] = box_filter(b[i,:,:,:]*b[j,:,:,:],filtersize,x,y,z, xfilter, yfilter, zfilter)
            Mmean[i,j] = bsqmean[i,j]-bmean[i]*bmean[j]

            vsqmean[i,j] = box_filter(v[i,:,:,:]*v[j,:,:,:],filtersize, dr, x,y, z, xfilter, yfilter, zfilter)
            Rmean[i,j] = vsqmean[i,j]-vmean[i]*vmean[j]

            bvmean[i,j] = box_filter(b[i,:,:,:]*v[j,:,:,:],filtersize,x,y, z, xfilter, yfilter, zfilter)


    for i in range(0,3):
    	for j in range(0,3):

            Fmean[i,j] = bvmean[j,i]-bmean[j]*vmean[i]-(bvmean[i,j]-bmean[i]*vmean[j])


    return Mmean, Rmean, Fmean, rhomean

#===========================================================================================

def stresses_ev(listvar):

    i,res,leng,direc,Sf,t, x,y,z,xfilter,yfilter,zfilter, b0x,boy,b0z,v0x,v0y,v0z = listvar

    arx = format(int(10*t(i)), "08")
    listvar = readh5files(direc+'h'+str(arx)+'.h5',leng)
    B = [listvar[4]-b0x, listvar[5]-b0y, listvar[6]-b0z]
    B = np.array(B)
    v = [listvar[7]-v0x,listvar[8]-v0y,listvar[9]-v0z]
    v = np.array(v)
    rho = listvar[10]
    rho = np.asarray(rho, order='C')

    Mmean, Rmean, Fmean, rhomean = stresses(v, B, rho, filtersize, x,y, z, xfilter, yfilter, zfilter)

    return()
#===========================================================================================

def main():

    path_data = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/DATA/KHI/'
    direc = path_data+'CVP--bx1e-3--128--rndom/'

    Sf = int(sys.argv[1])

    length = 128
    lenx = length
    leny = length
    lenz = length

    var = readh5files(direc+'KHI-00000000.h5')

    res = str(lenx)+'_'+str(leny)+'_'+str(lenz)
    x = np.linspace(0,1,lenz)
    y = np.linspace(0,1,lenz)
    z = np.linspace(0,1,lenz)
    b0x = var[4]
    b0y = var[5]
    b0z = var[6]
    v0x = var[7]
    v0y = var[8]
    v0z = var[9]

    t0 = 0
    tf = 40
    t = np.linspace(t0,tf,401)

    res = str(lenx)+'_'+str(leny)+'_'+str(lenz)

    cellsize = x[1]-x[0]

    filtersize = Sf*cellsize

    a = int(len(x)/2)
    amin = a-5
    amax = a+5
    xfilter= x[amin:amax]
    yfilter = y[amin:amax]
    zfilter = z[amin:amax]


    items = ((i,res,length,direc,Sf,t, z,y,z,xfilter,yfilter,zfilter, b0x,b0y,b0z,v0x,v0y,v0z) for i in t)
    items = tuple(items)

    print('Stresses...')

    with concurrent.futures.ProcessPoolExecutor() as executor:
        res = executor.map(stresses_ev,items)

    return()


if __name__ == "__main__":
    main()
