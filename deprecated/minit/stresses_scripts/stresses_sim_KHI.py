#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 28 14:38:10 2021

@author: miquelmiravet
"""

#############################################
#                                           #
#       STRESS TENSORS EVOLUTION KHI        #
#                                           #
#############################################

"""
Calculation of the energy density of the stresses directly from the data of the
numerical simulations
"""

import os
import numpy as np
import sys
import csv
import numpy.ctypeslib as npct
from ctypes import c_int
import h5py

#=================================================================================
#=================================================================================
# input type for the function
# must be a double array, with single dimension that is contiguous
array_3d_float = npct.ndpointer(dtype=np.float64, ndim=3, flags='CONTIGUOUS')
array_1d_float = npct.ndpointer(dtype=np.float64, ndim=1, flags='CONTIGUOUS')

libboxkernel = npct.load_library("/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/gradient-model-test/khi_kernels/libboxkernel", ".")
libboxkernel.box_kernel_cart.restype = None
libboxkernel.box_kernel_cart.argtypes = [c_int, c_int, c_int, array_1d_float, array_1d_float, array_1d_float, c_int, c_int, c_int, array_1d_float, array_1d_float, array_1d_float, array_1d_float, array_3d_float, array_3d_float]
#===========================================================================================

def readh5files(file):

    print('DATA FROM FILE:      ',file)
    hf = h5py.File(file, 'r')

    t = np.array(hf.get('time'))
    t0 = np.array(hf.get('t0'))
    tf = np.array(hf.get('tf'))
    x = np.array(hf.get('x'))
    y = np.array(hf.get('y'))
    z = np.array(hf.get('z'))
    rho = np.array(hf.get('rho'))
    vx = np.array(hf.get('vx'))
    vy = np.array(hf.get('vy'))
    vz = np.array(hf.get('vz'))
    Bx = np.array(hf.get('Bx'))
    By = np.array(hf.get('By'))
    Bz = np.array(hf.get('Bz'))


    print('Dimensions of the grid     :',len(x)," x ",len(y)," x ",len(z))
    print('Time           : ',t)
    print('Initial time     : ',t0)
    print('Final time   : ',tf)

    return [t,x,y,z,Bx, By, Bz, vx, vy, vz,rho]

#===========================================================================================

def box_filter(var,filtersize,x,y,z,lenx,leny,lenz,xfilter,yfilter,zfilter):

    var = np.asarray(var, order = 'C')
    varmean = np.zeros((len(xfilter),len(yfilter),len(zfilter)))

    libboxkernel.box_kernel_cart(lenx, leny,lenz, x, y, z, len(xfilter), len(yfilter), len(zfilter), xfilter, yfilter, zfilter, filtersize, var, varmean)

    return varmean

#===========================================================================================

def stresses(v, b, rho, filtersize, x,y, z, xfilter, yfilter, zfilter):

    rhomean = box_filter(rho,filtersize,x,y,z,xfilter,yfilter,zfilter)
    vmean = np.array([box_filter(v[0],filtersize,x,y,z,xfilter,yfilter,zfilter),box_filter(v[1],filtersize,x,y,z,xfilter,yfilter,zfilter),box_filter(v[2],filtersize,x,y,z,xfilter,yfilter,zfilter)])

    vsqmean = np.zeros((3,3,len(xfilter),len(yfilter),len(zfilter)))

    Rmean = np.zeros((3,3,len(xfilter),len(yfilter),len(zfilter)))

    bmean = np.array([box_filter(b[0],filtersize,x,y,z,xfilter,yfilter,zfilter),box_filter(b[1],filtersize,x,y,z,xfilter,yfilter,zfilter),box_filter(b[2],filtersize,x,y,z,xfilter,yfilter,zfilter)])

    bsqmean = np.zeros((3,3,len(xfilter),len(yfilter),len(zfilter)))

    Mmean = np.zeros((3,3,len(xfilter),len(yfilter),len(zfilter)))

    bvmean = np.zeros((3,3,len(xfilter),len(yfilter),len(zfilter)))

    Fmean = np.zeros((3,3,len(xfilter),len(yfilter),len(zfilter)))

    for i in range(0,3):
        for j in range(0,3):

            bsqmean[i,j] = box_filter(b[i,:,:,:]*b[j,:,:,:],filtersize,x,y,z, xfilter, yfilter, zfilter)
            Mmean[i,j] = bsqmean[i,j]-bmean[i]*bmean[j]

            vsqmean[i,j] = box_filter(v[i,:,:,:]*v[j,:,:,:],filtersize, dr, x,y, z, xfilter, yfilter, zfilter)
            Rmean[i,j] = vsqmean[i,j]-vmean[i]*vmean[j]

            bvmean[i,j] = box_filter(b[i,:,:,:]*v[j,:,:,:],filtersize,x,y, z, xfilter, yfilter, zfilter)


    for i in range(0,3):
    	for j in range(0,3):

            Fmean[i,j] = bvmean[j,i]-bmean[j]*vmean[i]-(bvmean[i,j]-bmean[i]*vmean[j])


    return Mmean, Rmean, Fmean, rhomean

#===========================================================================================

def stresses_ev(res,direc,Sf,t,path_files, leny, b0x,boy,b0z,v0x,v0y,v0z):

    var = readh5files(direc+'h00000000.h5 ')

    x = var[1].astype(np.float64)
    print('r TYPE : ', r.dtype)
    y = var[2].astype(np.float64)
    z = var[3].astype(np.float64)

    cellsize = x[1]-x[0]

    filtersize = Sf*cellsize

    a = int(len(x)/2)
    amin = a-5
    amax = a+5
    xfilter= x[amin:amax]
    yfilter = y[amin:amax]
    zfilter = z[amin:amax]

    Mmean = np.zeros((len(t),3,3,len(xfilter),len(yfilter),len(zfilter)))
    Rmean = np.zeros((len(t),3,3,len(xfilter),len(yfilter),len(zfilter)))
    Fmean = np.zeros((len(t),3,3,len(xfilter),len(yfilter),len(zfilter)))
    rhomean = np.zeros((len(t),len(xfilter),len(yfilter),len(zfilter)))

    for i in range(0,len(t)):

        arx = format(i, "08")
        listvar = readh5files(direc+'h'+str(arx)+'.h5')

        B = [listvar[4]-b0x, listvar[5]-b0y, listvar[6]-b0z]
        B = np.array(B)
        v = [listvar[7]-v0x,listvar[8]-v0y,listvar[9]-v0z]
        v = np.array(v)
        rho = listvar[10]
        rho = np.asarray(rho, order='C')

        Mmean[x], Rmean[x], Fmean[x], rhomean[x] = stresses(v, B, rho, filtersize, x,y, z, xfilter, yfilter, zfilter)

        arxiu = format(i, "08")
        hf = h5py.File(path_files+'/stresses_sim_res_'+res+'-'+str(arxiu)+'.h5', 'w')
        hf.create_dataset('time', data=t[x])
        hf.create_dataset('S_f', data = Sf)
        hf.create_dataset('x', data=x)
        hf.create_dataset('y', data=y)
        hf.create_dataset('z', data=z)
        hf.create_dataset('x_f', data=xfilter)
        hf.create_dataset('y_f', data=yfilter)
        hf.create_dataset('z_f', data=zfilter)
        hf.create_dataset('stress_Mxx', data=Mmean[x,0,0])
        hf.create_dataset('stress_Mxy', data=Mmean[x,0,1])
        hf.create_dataset('stress_Mxz', data=Mmean[x,0,2])
        hf.create_dataset('stress_Myy', data=Mmean[x,1,1])
        hf.create_dataset('stress_Myz', data=Mmean[x,1,2])
        hf.create_dataset('stress_Mzz', data=Mmean[x,2,2])
        hf.create_dataset('stress_Rxx', data=Rmean[x,0,0])
        hf.create_dataset('stress_Rxy', data=Rmean[x,0,1])
        hf.create_dataset('stress_Rxz', data=Rmean[x,0,2])
        hf.create_dataset('stress_Ryy', data=Rmean[x,1,1])
        hf.create_dataset('stress_Ryz', data=Rmean[x,1,2])
        hf.create_dataset('stress_Rzz', data=Rmean[x,2,2])
        hf.create_dataset('stress_Fxy', data=Fmean[x,0,1])
        hf.create_dataset('stress_Fxz', data=Fmean[x,0,2])
        hf.create_dataset('stress_Fyz', data=Fmean[x,1,2])
        hf.create_dataset('rho_mean', data=rhomean[x])

        hf.close()

    return()

#===========================================================================================

def main():

    path_data = "//storage/scratch/lv43/lv43072/KHI/Carrasco--512--Std--bx1e-3--rndm-1/"
    direc = path_data+"data/"

    Sf = int(sys.argv[1])

    length = 512
    lenx = length
    leny = length
    lenz = length

    var = readh5files(direc+'h00000000.h5')

    res = str(lenx)+'_'+str(leny)+'_'+str(lenz)
    x = var[1].astype(np.float64)
    y = var[2].astype(np.float64)
    z = var[3].astype(np.float64)
    b0x = var[4]
    b0y = var[5]
    b0z = var[6]
    v0x = var[7]
    v0y = var[8]
    v0z = var[9]

    t0 = 0
    tf = 40
    t = np.linspace(t0,tf,201)

    res = str(lenx)+'_'+str(leny)+'_'+str(lenz)

    path_files = path_data+"results/stress_tensors/Sf_"+str(Sf)

    try:
        os.makedirs(path_files)
    except OSError as error:
        print(error)

    stresses_ev(res,direc,Sf,t,path_files, leny, b0x,b0y,b0z,v0x,v0y,v0z)

    return()

main()
