import time
import concurrent.futures
from functools import partial

def useless_function(data):
    a,b=data
    return a+b

with concurrent.futures.ProcessPoolExecutor() as executor:
    secs = [5, 4, 3, 2, 1]
    a = 1
    items = [(a,b) for b in secs]
    
    pool = executor.map(useless_function,items)
    for res in pool:
        print(f'Return Value: {res}')
 
