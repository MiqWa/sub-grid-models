//
//  box_kernel_cart.c
//  
//
//  Created by Miquel Miravet on 23/06/2021.
//

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stdbool.h>

void box_kernel_cart(int lenx, int leny, int lenz, double x[], double y[], double z[], int lenxfilter, int lenyfilter, int lenzfilter, double xfilter[], double yfilter[], double zfilter[], double filtersize, double var[][leny][lenz], double varmean[][lenyfilter][lenzfilter]){

    double dx, dy, dz, difx, dify, difz;
    int i, j, k, p, q, l, c;
    double sum= 0, sum2= 0;

    double *** G = (double ***)malloc(lenx*sizeof(double**));
        for (i = 0; i< lenx; i++) {
         G[i] = (double **) malloc(leny*sizeof(double *));
          for (j = 0; j < leny; j++) {
              G[i][j] = (double *)malloc(lenz*sizeof(double));
          }
        }

    double deltax = filtersize*0.5001;
    double deltay = filtersize*0.5001;
    double deltaz = filtersize*0.5001;
	
    dx = x[1]-x[0];
    dy = y[1]-y[0];
    dz = z[1]-z[0];

    double dV = dx*dy*dz;

    for (p = 0 ; p < lenxfilter ; p++){
        for (q = 0 ; q < lenyfilter ; q++){
            for (l = 0 ; l < lenzfilter ; l++){       
    		for (i = 0 ; i < lenx ; i++){
                    for (j = 0; j < leny ; j++){
                        for (k = 0; k < lenz ; k++){
			    
			    difx = abs(xfilter[p]-x[i]);
                            dify = abs(yfilter[q]-y[j]);
			    difz = abs(zfilter[l]-z[k]);

			    if(difx <= deltax){
		                if(dify <= deltay){
				    if(difz <= deltaz){	    
			        	
		                        G[i][j][k] = 1;
				    }
				    else{

					G[i][j][k] = 0;
				    }
				}
				else{
					G[i][j][k] = 0;
				}
			    }
		            else{
			        
				G[i][j][k] = 0;

                            }
                        
		            sum2 += G[i][j][k]*dV;

                            sum += var[i][j][k]*G[i][j][k]*dV;

                        }
                    }
                }
			
                varmean[p][q][l] = sum/sum2;
          	sum = 0;
		sum2 = 0;
            }
        }
    }

    for (i = 0; i< lenx; i++) {
        for (j = 0; j < leny; j++) {
            free(G[i][j]);
        }
	free(G[i]);
    }
    free(G);
}   
