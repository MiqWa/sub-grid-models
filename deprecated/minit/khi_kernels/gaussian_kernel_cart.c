//
//  gaussian_kernel.c
//  
//
//  Created by Miquel Miravet on 11/03/2021.
//

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

void gaussian_kernel_cart(int lenx, int leny, int lenz, double x[], double y[], double z[], int lenxfilter, int lenyfilter, int lenzfilter, double xfilter[], double yfilter[], double zfilter[], double filtersize, double var[][leny][lenz], double varmean[][lenyfilter][lenzfilter]){


    double xi, dV;
    double dx, dy, dz;
    int i, j, k, p, q, l;
    double sum, sum2;

    double *** G = (double ***)malloc(lenx*sizeof(double**));
        for (i = 0; i< lenx; i++) {
         G[i] = (double **) malloc(leny*sizeof(double *));
          for (j = 0; j < leny; j++) {
              G[i][j] = (double *)malloc(lenz*sizeof(double));
          }
        }


    xi = filtersize*filtersize/24 ;
    dx = x[2]-x[1];
    dy = y[2]-y[1];
    dz = z[2]-z[1];
    dV = dx*dy*dz ;

    for (p = 0 ; p < lenxfilter ; p++){
        for (q = 0 ; q < lenyfilter ; q++){
            for (l = 0 ; l < lenzfilter ; l++){

                sum = 0;
                sum2 = 0;

                for (i = 0 ; i < lenx ; i++){
                    for (j = 0; j < leny ; j++){
                        for (k = 0; k < lenz ; k++){
                            G[i][j][k] = exp(-((x[p]-x[i])*(x[p]-x[i])+(y[q]-y[j])*(y[q]-y[j])+(z[l]-z[k])*(z[l]-z[k]))/(4*xi)) ;

                            sum += G[i][j][k]*dV;
                            sum2 += var[i][j][k]*G[i][j][k]*dV;

                        }
                    }
                }

                varmean[p][q][l] = sum2/sum;
     
            }
        }
    }


    for (i = 0; i< lenx; i++) {
        for (j = 0; j < leny; j++) {
            free(G[i][j]);
        }
	free(G[i]);
    }
    free(G);


}    
