#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 16 15:21:38 2021

@author: miquelmiravet
"""
import os 
import numpy as np
import matplotlib.pyplot as plt
import csv
import math as m
from pathlib import Path
from PyPDF2 import PdfFileMerger
from PyPDF2 import PdfFileReader

def get_constants(path,filename,m,l,n,lent):
    
    if filename == path+'/gammaR' or filename == path+'/gammaRrho' or filename == path+'/gammaM': 
        var = np.loadtxt(filename+'.csv', usecols=(1,2,3))
        sigmavar = np.loadtxt(filename+'.csv', usecols=(4,5,6))
        var = np.swapaxes(var,0,1)
        sigmavar = np.swapaxes(sigmavar,0,1)
            
        var = var.reshape(3,m,l,n,lent)
        sigmavar = sigmavar.reshape(3,m,l,n,lent)
        
        
    else:
        var = np.loadtxt(filename+'.csv', usecols=(1,2,3,4,5,6))
        sigmavar = np.loadtxt(filename+'.csv', usecols=(7,8,9,10,11,12))
        
        var = np.swapaxes(var,0,1)
        sigmavar = np.swapaxes(sigmavar,0,1)
        
        var = var.reshape(6,m,l,n,lent)
        sigmavar = sigmavar.reshape(6,m,l,n,lent)
        
    return var, sigmavar

def log_scale(var):

    logvar = (np.sign(var)*(m.log(abs(var)+1,10)))
    
    return logvar

def plot_constants(plotsdir,lent, tfinal, alphaR, ealphaR, alphaRrho, ealphaRrho, alphaM, ealphaM, betaR, ebetaR, betaRrho, ebetaRrho, betaM, ebetaM, betaMrho, ebetaMrho, gammaR, egammaR, gammaRrho, egammaRrho, gammaM, egammaM, res):
    
    t = np.linspace(0,tfinal,lent)
    t = t[1:]
    a = 2
    b = 0
    c = 0
    alphaR = alphaR[:,a,b,c,1:]
    ealphaR = ealphaR[:,a,b,c,1:]
    alphaRrho = alphaRrho[:,a,b,c,1:]
    ealphaRrho = ealphaRrho[:,a,b,c,1:]
    alphaM = alphaM[:,a,b,c,1:]
    ealphaM = ealphaM[:,a,b,c,1:]
    betaR = betaR[:,a,b,c,1:]
    ebetaR = ebetaR[:,a,b,c,1:]
    betaRrho = betaRrho[:,a,b,c,1:]
    ebetaRrho = ebetaRrho[:,a,b,c,1:]
    betaM = betaM[:,a,b,c,1:]
    ebetaM = ebetaM[:,a,b,c,1:]
    betaMrho = betaMrho[:,a,b,c,1:]
    ebetaMrho = ebetaMrho[:,a,b,c,1:]
    gammaR = gammaR[:,a,b,c,1:]
    egammaR = egammaR[:,a,b,c,1:]
    gammaRrho = gammaRrho[:,a,b,c,1:]
    egammaRrho = egammaRrho[:,a,b,c,1:]
    gammaM = gammaM[:,a,b,c,1:]
    egammaM = egammaM[:,a,b,c,1:]

    
    for x in range(0,len(t)):
        for i in range(0,6):
            betaM[i,x] = log_scale(betaM[i,x])
            ebetaM[i,x] = log_scale(ebetaM[i,x])
            betaMrho[i,x] = log_scale(betaMrho[i,x])
            ebetaMrho[i,x] = log_scale(ebetaMrho[i,x])

        if res != 0:
            for j in range(0,3):
                gammaM[j,x] = log_scale(gammaM[j,x])
                egammaM[j,x] = log_scale(egammaM[j,x])
        
    fig, ax = plt.subplots()
    
    ax.plot(t, alphaR[0,:], linestyle = 'None', marker = '.', color = 'm', label = r'$\alpha_{\rm{R},rr}$')
    ax.errorbar(t, alphaR[0,:], yerr=ealphaR[0,:], xerr=None, fmt= 'none', ecolor='m', capsize = 1.0)
    #ax.plot(t, -alphaR[0,:], linestyle = 'None', marker = 'x', color = 'm')
    #ax.errorbar(t, -alphaR[0,:], yerr=ealphaR[0,:], xerr=None, fmt='none', ecolor='m', capsize = 1.0)
    ax.plot(t, alphaR[1,:], linestyle = 'None', marker = '.', color = 'c', label = r'$\alpha_{\rm{R},r\phi}$')
    ax.errorbar(t, alphaR[1,:], yerr=ealphaR[1,:], xerr=None, fmt='none', ecolor='c', capsize = 1.0)
    #ax.plot(t, -alphaR[1,:], linestyle = 'None', marker = 'x', color = 'c')
    #ax.errorbar(t, -alphaR[1,:], yerr=ealphaR[1,:], xerr=None, fmt='none', ecolor='c', capsize = 1.0)
    ax.plot(t, alphaR[3,:], linestyle = 'None', marker = '.', color = 'k', label = r'$\alpha_{\rm{R},\phi\phi}$')
    ax.errorbar(t, alphaR[3,:], yerr=ealphaR[3,:], xerr=None, fmt='none', ecolor='k', capsize = 1.0)
    #ax.plot(t, -alphaR[3,:], linestyle = 'None', marker = 'x', color = 'k')
    #ax.errorbar(t, -alphaR[3,:], yerr=ealphaR[3,:], xerr=None, fmt='none', ecolor='k', capsize = 1.0)

    plt.xlabel('Time [ms]', fontsize = '14')
    plt.ylabel(r'$\alpha_{\rm{R},ij}$',fontsize = '14')
    plt.title('RESOLUTION : '+res)
   #plt.yscale('symlog')
    ax.legend(loc = 'lower right', ncol = 1, frameon = 'True',prop={"size":11})
    plt.savefig(plotsdir+'/alphaR_non_z.pdf',bbox_inches='tight')
    #plt.savefig(plotsdir+'/alphaR.eps',bbox_inches='tight')
    
    fig, ax = plt.subplots()
    
    ax.plot(t, alphaR[2,:], linestyle = 'None', marker = '.', color = 'y', label = r'$\alpha_{\rm{R},rz}$')
    ax.errorbar(t, alphaR[2,:], yerr=ealphaR[2,:], xerr=None, fmt='none', ecolor='y', capsize = 1.0)
    #ax.plot(t, -alphaR[2,:], linestyle = 'None', marker = 'x', color = 'y')
    #ax.errorbar(t, -alphaR[2,:], yerr=ealphaR[2,:], xerr=None, fmt='', ecolor='y', barsabove = True)
    ax.plot(t, alphaR[4,:], linestyle = 'None', marker = '.', color = 'r', label = r'$\alpha_{\rm{R},\phi z}$')
    ax.errorbar(t, alphaR[4,:], yerr=ealphaR[4,:], xerr=None, fmt='none', ecolor='r', capsize = 1.0)
    #ax.plot(t, -alphaR[4,:], linestyle = 'None', marker = 'x', color = 'r')
    #ax.errorbar(t, -alphaR[4,:], yerr=ealphaR[4,:], xerr=None, fmt='', ecolor='r', barsabove = True)
    ax.plot(t, alphaR[5,:], linestyle = 'None', marker = '.', color = 'g', label = r'$\alpha_{\rm{R},zz}$')
    ax.errorbar(t, alphaR[5,:], yerr=ealphaR[5,:], xerr=None, fmt='none', ecolor='g', capsize = 1.0)
    #ax.plot(t, -alphaR[5,:], linestyle = 'None', marker = 'x', color = 'g')
    #ax.errorbar(t, -alphaR[5,:], yerr=ealphaR[5,:], xerr=None, fmt='', ecolor='g', barsabove = True)
    
    plt.xlabel('Time [ms]', fontsize = '14')
    plt.ylabel(r'$\alpha_{\rm{R},ij}$',fontsize = '14')
    plt.title('RESOLUTION : '+res)
    #plt.yscale('symlog')
    ax.legend(loc = 'lower right', ncol = 1, frameon = 'True',prop={"size":11})
    plt.savefig(plotsdir+'/alphaR-z.pdf',bbox_inches='tight')
    
    fig, ax = plt.subplots()
    
    ax.plot(t, alphaRrho[0,:], linestyle = 'None', marker = '.', color = 'm', label = r'$\alpha^{\rho}_{\rm{R},rr}$')
    ax.errorbar(t, alphaRrho[0,:], yerr=ealphaRrho[0,:], xerr=None, fmt='none', ecolor='m', capsize = 1.0)
    #ax.plot(t, -alphaRrho[0,:], linestyle = 'None', marker = 'x', color = 'm')
    #ax.errorbar(t, -alphaRrho[0,:], yerr=ealphaRrho[0,:], xerr=None, fmt='', ecolor='m', barsabove = True)
    ax.plot(t, alphaRrho[1,:], linestyle = 'None', marker = '.', color = 'c', label = r'$\alpha^{\rho}_{\rm{R},r\phi}$')
    ax.errorbar(t, alphaRrho[1,:], yerr=ealphaRrho[1,:], xerr=None, fmt='none', ecolor='c', capsize = 1.0)
    #ax.plot(t, -alphaRrho[1,:], linestyle = 'None', marker = 'x', color = 'c')
    #ax.errorbar(t, -alphaRrho[1,:], yerr=ealphaRrho[1,:], xerr=None, fmt='', ecolor='c', barsabove = True)
    ax.plot(t, alphaRrho[3,:], linestyle = 'None', marker = '.', color = 'k', label = r'$\alpha^{\rho}_{\rm{R},\phi\phi}$')
    ax.errorbar(t, alphaRrho[3,:], yerr=ealphaRrho[3,:], xerr=None, fmt='none', ecolor='k', capsize = 1.0)
    #ax.plot(t, -alphaRrho[3,:], linestyle = 'None', marker = 'x', color = 'k')
    #ax.errorbar(t, -alphaRrho[3,:], yerr=ealphaRrho[3,:], xerr=None, fmt='', ecolor='k', barsabove = True)

    plt.xlabel('Time [ms]', fontsize = '14')
    plt.ylabel(r'$\alpha^{\rho}_{\rm{R},ij}$',fontsize = '14')
    plt.title('RESOLUTION : '+res)
    #plt.yscale('symlog')
    ax.legend(loc = 'lower right', ncol = 1, frameon = 'True',prop={"size":11})
    plt.savefig(plotsdir+'/alphaRrho_non_z.pdf',bbox_inches='tight')
    #plt.savefig(plotsdir+'/alphaR.eps',bbox_inches='tight')
    
    fig, ax = plt.subplots()
    
    ax.plot(t, alphaRrho[2,:], linestyle = 'None', marker = '.', color = 'y', label = r'$\alpha^{\rho}_{\rm{R},rz}$')
    ax.errorbar(t, alphaRrho[2,:], yerr=ealphaRrho[2,:], xerr=None, fmt='none', ecolor='y', capsize = 1.0)
    #ax.plot(t, -alphaRrho[2,:], linestyle = 'None', marker = 'x', color = 'y')
    #ax.errorbar(t, -alphaRrho[2,:], yerr=ealphaRrho[2,:], xerr=None, fmt='', ecolor='y', barsabove = True)
    ax.plot(t, alphaRrho[4,:], linestyle = 'None', marker = '.', color = 'r', label = r'$\alpha^{\rho}_{\rm{R},\phi z}$')
    ax.errorbar(t, alphaRrho[4,:], yerr=ealphaRrho[4,:], xerr=None, fmt='none', ecolor='r', capsize = 1.0)
    #ax.plot(t, -alphaRrho[4,:], linestyle = 'None', marker = 'x', color = 'r')
    #ax.errorbar(t, -alphaRrho[4,:], yerr=ealphaRrho[4,:], xerr=None, fmt='', ecolor='r', barsabove = True)
    ax.plot(t, alphaRrho[5,:], linestyle = 'None', marker = '.', color = 'g', label = r'$\alpha^{\rho}_{\rm{R},zz}$')
    ax.errorbar(t, alphaRrho[5,:], yerr=ealphaRrho[5,:], xerr=None, fmt='none', ecolor='g', capsize = 1.0)
    #ax.plot(t, -alphaRrho[5,:], linestyle = 'None', marker = 'x', color = 'g')
    #ax.errorbar(t, -alphaRrho[5,:], yerr=ealphaRrho[5,:], xerr=None, fmt='', ecolor='g', barsabove = True)
    
    plt.xlabel('Time [ms]', fontsize = '14')
    plt.ylabel(r'$\alpha^{\rho}_{\rm{R},ij}$',fontsize = '14')
    plt.title('RESOLUTION : '+res)
    #plt.yscale('symlog')
    ax.legend(loc = 'lower right', ncol = 1, frameon = 'True',prop={"size":11})
    plt.savefig(plotsdir+'/alphaRrho-z.pdf',bbox_inches='tight')
    
    fig, ax = plt.subplots()
    
    ax.plot(t, alphaM[0,:], linestyle = 'None', marker = '.', color = 'm', label = r'$\alpha_{\rm{M},rr}$')
    ax.errorbar(t, alphaM[0,:], yerr=ealphaM[0,:], xerr=None, fmt='none', ecolor='m', capsize = 1.0)
    #ax.plot(t, -alphaM[0,:], linestyle = 'None', marker = 'x', color = 'm')
    #ax.errorbar(t, -alphaM[0,:], yerr=ealphaM[0,:], xerr=None, fmt='', ecolor='m', barsabove = True)
    ax.plot(t, alphaM[1,:], linestyle = 'None', marker = '.', color = 'c', label = r'$\alpha_{\rm{M},r\phi}$')
    ax.errorbar(t, alphaM[1,:], yerr=ealphaM[1,:], xerr=None, fmt='none', ecolor='c', capsize = 1.0)
    #ax.plot(t, -alphaM[1,:], linestyle = 'None', marker = 'x', color = 'c')
    #ax.errorbar(t, -alphaM[1,:], yerr=ealphaM[1,:], xerr=None, fmt='', ecolor='c', barsabove = True)
    ax.plot(t, alphaM[3,:], linestyle = 'None', marker = '.', color = 'k', label = r'$\alpha_{\rm{M},\phi\phi}$')
    ax.errorbar(t, alphaM[3,:], yerr=ealphaM[3,:], xerr=None, fmt='none', ecolor='k', capsize = 1.0)
    #ax.plot(t, -alphaM[3,:], linestyle = 'None', marker = 'x', color = 'k')
    #ax.errorbar(t, -alphaM[3,:], yerr=ealphaM[3,:], xerr=None, fmt='', ecolor='k', barsabove = True)

    plt.xlabel('Time [ms]', fontsize = '14')
    plt.ylabel(r'$\alpha_{\rm{M},ij}$',fontsize = '14')
    plt.title('RESOLUTION : '+res)
    #plt.yscale('symlog')
    ax.legend(loc = 'lower right', ncol = 1, frameon = 'True',prop={"size":11})
    plt.savefig(plotsdir+'/alphaM_non_z.pdf',bbox_inches='tight')
    #plt.savefig(plotsdir+'/alphaR.eps',bbox_inches='tight')
    
    fig, ax = plt.subplots()
    
    ax.plot(t, alphaM[2,:], linestyle = 'None', marker = '.', color = 'y', label = r'$\alpha_{\rm{M},rz}$')
    ax.errorbar(t, alphaM[2,:], yerr=ealphaM[2,:], xerr=None, fmt='none', ecolor='y', capsize = 1.0)
    #ax.plot(t, -alphaM[2,:], linestyle = 'None', marker = 'x', color = 'y')
    #ax.errorbar(t, -alphaM[2,:], yerr=ealphaM[2,:], xerr=None, fmt='', ecolor='y', barsabove = True)
    ax.plot(t, alphaM[4,:], linestyle = 'None', marker = '.', color = 'r', label = r'$\alpha_{\rm{M},\phi z}$')
    ax.errorbar(t, alphaM[4,:], yerr=ealphaM[4,:], xerr=None, fmt='none', ecolor='r', capsize = 1.0)
    #ax.plot(t, -alphaM[4,:], linestyle = 'None', marker = 'x', color = 'r')
    #ax.errorbar(t, -alphaM[4,:], yerr=ealphaM[4,:], xerr=None, fmt='', ecolor='r', barsabove = True)
    ax.plot(t, alphaM[5,:], linestyle = 'None', marker = '.', color = 'g', label = r'$\alpha_{\rm{M},zz}$')
    ax.errorbar(t, alphaM[5,:], yerr=ealphaM[5,:], xerr=None, fmt='none', ecolor='g', capsize = 1.0)
    #ax.plot(t, -alphaM[5,:], linestyle = 'None', marker = 'x', color = 'g')
    #ax.errorbar(t, -alphaM[5,:], yerr=ealphaM[5,:], xerr=None, fmt='', ecolor='g', barsabove = True)
    
    plt.xlabel('Time [ms]', fontsize = '14')
    plt.ylabel(r'$\alpha_{\rm{M},ij}$',fontsize = '14')
    plt.title('RESOLUTION : '+res)
    #plt.yscale('symlog')
    ax.legend(loc = 'lower right', ncol = 1, frameon = 'True',prop={"size":11})
    plt.savefig(plotsdir+'/alphaM-z.pdf',bbox_inches='tight')
    
    fig, ax = plt.subplots()
    
    ax.plot(t, betaR[0,:], linestyle = 'None', marker = '.', color = 'm', label = r'$\beta_{\rm{R},rr}$')
    ax.errorbar(t, betaR[0,:], yerr=ebetaR[0,:], xerr=None, fmt='none', ecolor='m', capsize = 1.0)
    #ax.plot(t, -betaR[0,:], linestyle = 'None', marker = 'x', color = 'm')
    #ax.errorbar(t, -betaR[0,:], yerr=ebetaR[0,:], xerr=None, fmt='', ecolor='m', barsabove = True)
    ax.plot(t, betaR[1,:], linestyle = 'None', marker = '.', color = 'c', label = r'$\beta_{\rm{R},r\phi}$')
    ax.errorbar(t, betaR[1,:], yerr=ebetaR[1,:], xerr=None, fmt='none', ecolor='c', capsize = 1.0)
    #ax.plot(t, -betaR[1,:], linestyle = 'None', marker = 'x', color = 'c')
    #ax.errorbar(t, -betaR[1,:], yerr=ebetaR[1,:], xerr=None, fmt='', ecolor='c', barsabove = True)
    ax.plot(t, betaR[3,:], linestyle = 'None', marker = '.', color = 'k', label = r'$\beta_{\rm{R},\phi\phi}$')
    ax.errorbar(t, betaR[3,:], yerr=ebetaR[3,:], xerr=None, fmt='none', ecolor='k', capsize = 1.0)
    #ax.plot(t, -betaR[3,:], linestyle = 'None', marker = 'x', color = 'k')
    #ax.errorbar(t, -betaR[3,:], yerr=ebetaR[3,:], xerr=None, fmt='', ecolor='k', barsabove = True)

    plt.xlabel('Time [ms]', fontsize = '14')
    plt.ylabel(r'$\beta_{\rm{R},ij}$',fontsize = '14')
    plt.title('RESOLUTION : '+res)
    #plt.yscale('symlog')
    ax.legend(loc = 'lower right', ncol = 1, frameon = 'True',prop={"size":11})
    plt.savefig(plotsdir+'/betaR_non_z.pdf',bbox_inches='tight')
    #plt.savefig(plotsdir+'/alphaR.eps',bbox_inches='tight')
    
    fig, ax = plt.subplots()
    
    ax.plot(t, betaR[2,:], linestyle = 'None', marker = '.', color = 'y', label = r'$\beta_{\rm{R},rz}$')
    ax.errorbar(t, betaR[2,:], yerr=ebetaR[2,:], xerr=None, fmt='none', ecolor='y', capsize = 1.0)
    #ax.plot(t, -betaR[2,:], linestyle = 'None', marker = 'x', color = 'y')
    #ax.errorbar(t, -betaR[2,:], yerr=ebetaR[2,:], xerr=None, fmt='', ecolor='y', barsabove = True)
    ax.plot(t, betaR[4,:], linestyle = 'None', marker = '.', color = 'r', label = r'$\beta_{\rm{R},\phi z}$')
    ax.errorbar(t, betaR[4,:], yerr=ebetaR[4,:], xerr=None, fmt='none', ecolor='r', capsize = 1.0)
    #ax.plot(t, -betaR[4,:], linestyle = 'None', marker = 'x', color = 'r')
    #ax.errorbar(t, -betaR[4,:], yerr=ebetaR[4,:], xerr=None, fmt='', ecolor='r', barsabove = True)
    ax.plot(t, betaR[5,:], linestyle = 'None', marker = '.', color = 'g', label = r'$\beta_{\rm{R},zz}$')
    ax.errorbar(t, betaR[5,:], yerr=ebetaR[5,:], xerr=None, fmt='none', ecolor='g', capsize = 1.0)
    #ax.plot(t, -betaR[5,:], linestyle = 'None', marker = 'x', color = 'g')
    #ax.errorbar(t, -betaR[5,:], yerr=ebetaR[5,:], xerr=None, fmt='', ecolor='g', barsabove = True)
    
    plt.xlabel('Time [ms]', fontsize = '14')
    plt.ylabel(r'$\beta_{\rm{R},ij}$',fontsize = '14')
    plt.title('RESOLUTION : '+res)
    #plt.yscale('symlog')
    ax.legend(loc = 'lower right', ncol = 1, frameon = 'True',prop={"size":11})
    plt.savefig(plotsdir+'/betaR-z.pdf',bbox_inches='tight')
    
    fig, ax = plt.subplots()
    
    ax.plot(t, betaRrho[0,:], linestyle = 'None', marker = '.', color = 'm', label = r'$\beta^{\rho}_{\rm{R},rr}$')
    ax.errorbar(t, betaRrho[0,:], yerr=ebetaRrho[0,:], xerr=None, fmt='none', ecolor='m', capsize = 1.0)
    #ax.plot(t, -betaRrho[0,:], linestyle = 'None', marker = 'x', color = 'm')
    #ax.errorbar(t, -betaRrho[0,:], yerr=ebetaRrho[0,:], xerr=None, fmt='', ecolor='m', barsabove = True)
    ax.plot(t, betaRrho[1,:], linestyle = 'None', marker = '.', color = 'c', label = r'$\beta^{\rho}_{\rm{R},r\phi}$')
    ax.errorbar(t, betaRrho[1,:], yerr=ebetaRrho[1,:], xerr=None, fmt='none', ecolor='c', capsize = 1.0)
    #ax.plot(t, -betaRrho[1,:], linestyle = 'None', marker = 'x', color = 'c')
    #ax.errorbar(t, -betaRrho[1,:], yerr=ebetaRrho[1,:], xerr=None, fmt='', ecolor='c', barsabove = True)
    ax.plot(t, betaRrho[3,:], linestyle = 'None', marker = '.', color = 'k', label = r'$\beta^{\rho}_{\rm{R},\phi\phi}$')
    ax.errorbar(t, betaRrho[3,:], yerr=ebetaRrho[3,:], xerr=None, fmt='none', ecolor='k', capsize = 1.0)
    #ax.plot(t, -betaRrho[3,:], linestyle = 'None', marker = 'x', color = 'k')
    #ax.errorbar(t, -betaRrho[3,:], yerr=ebetaRrho[3,:], xerr=None, fmt='', ecolor='k', barsabove = True)

    plt.xlabel('Time [ms]', fontsize = '14')
    plt.ylabel(r'$\beta^{\rho}_{\rm{R},ij}$',fontsize = '14')
    plt.title('RESOLUTION : '+res)
    #plt.yscale('symlog')
    ax.legend(loc = 'lower right', ncol = 1, frameon = 'True',prop={"size":11})
    plt.savefig(plotsdir+'/betaRrho_non_z.pdf',bbox_inches='tight')
    #plt.savefig(plotsdir+'/alphaR.eps',bbox_inches='tight')
    
    fig, ax = plt.subplots()
    
    ax.plot(t, betaRrho[2,:], linestyle = 'None', marker = '.', color = 'y', label = r'$\beta^{\rho}_{\rm{R},rz}$')
    ax.errorbar(t, betaRrho[2,:], yerr=ebetaRrho[2,:], xerr=None, fmt='none', ecolor='y', capsize = 1.0)
    #ax.plot(t, -betaR[2,:], linestyle = 'None', marker = 'x', color = 'y')
    #ax.errorbar(t, -betaRrho[2,:], yerr=ebetaRrho[2,:], xerr=None, fmt='', ecolor='y', capsize = 1.0)
    ax.plot(t, betaRrho[4,:], linestyle = 'None', marker = '.', color = 'r', label = r'$\beta^{\rho}_{\rm{R},\phi z}$')
    ax.errorbar(t, betaRrho[4,:], yerr=ebetaRrho[4,:], xerr=None, fmt='none', ecolor='r', capsize = 1.0)
    #ax.plot(t, -betaRrho[4,:], linestyle = 'None', marker = 'x', color = 'r')
    #ax.errorbar(t, -betaRrho[4,:], yerr=ebetaRrho[4,:], xerr=None, fmt='', ecolor='r', barsabove = True)
    ax.plot(t, betaRrho[5,:], linestyle = 'None', marker = '.', color = 'g', label = r'$\beta^{\rho}_{\rm{R},zz}$')
    ax.errorbar(t, betaRrho[5,:], yerr=ebetaRrho[5,:], xerr=None, fmt='none', ecolor='g', capsize = 1.0)
    #ax.plot(t, -betaRrho[5,:], linestyle = 'None', marker = 'x', color = 'g')
    #ax.errorbar(t, -betaRrho[5,:], yerr=ebetaRrho[5,:], xerr=None, fmt='', ecolor='g', barsabove = True)
    
    plt.xlabel('Time [ms]', fontsize = '14')
    plt.ylabel(r'$\beta^{\rho}_{\rm{R},ij}$',fontsize = '14')
    plt.title('RESOLUTION : '+res)
    #plt.yscale('symlog')
    ax.legend(loc = 'lower right', ncol = 1, frameon = 'True',prop={"size":11})
    plt.savefig(plotsdir+'/betaRrho-z.pdf',bbox_inches='tight')
    
    
    fig, ax = plt.subplots()
    
    ax.plot(t, betaM[0,:], linestyle = 'None', marker = '.', color = 'm', label = r'$\beta_{\rm{M},rr}$')
    ax.errorbar(t, betaM[0,:], yerr=ebetaM[0,:], xerr=None, fmt='none', ecolor='m', capsize = 1.0)
    #ax.plot(t, -betaM[0,:], linestyle = 'None', marker = 'x', color = 'm')
    #ax.errorbar(t, -betaM[0,:], yerr=ebetaM[0,:], xerr=None, fmt='', ecolor='m', barsabove = True)
    ax.plot(t, betaM[1,:], linestyle = 'None', marker = '.', color = 'c', label = r'$\beta_{\rm{M},r\phi}$')
    ax.errorbar(t, betaM[1,:], yerr=ebetaM[1,:], xerr=None, fmt='none', ecolor='c', capsize = 1.0)
    #ax.plot(t, -betaM[1,:], linestyle = 'None', marker = 'x', color = 'c')
    #ax.errorbar(t, -betaM[1,:], yerr=ebetaM[1,:], xerr=None, fmt='', ecolor='c', barsabove = True)
    ax.plot(t, betaM[3,:], linestyle = 'None', marker = '.', color = 'k', label = r'$\beta_{\rm{M},\phi\phi}$')
    ax.errorbar(t, betaM[3,:], yerr=ebetaM[3,:], xerr=None, fmt='none', ecolor='k', capsize = 1.0)
    #ax.plot(t, -betaM[3,:], linestyle = 'None', marker = 'x', color = 'k')
    #ax.errorbar(t, -betaM[3,:], yerr=ebetaM[3,:], xerr=None, fmt='', ecolor='k', barsabove = True)

    plt.xlabel('Time [ms]', fontsize = '14')
    plt.ylabel(r'sign($\beta_{\rm{M},ij}) \log_{10}(|\beta_{\rm{M},ij}|+1)$',fontsize = '14')
    plt.title('RESOLUTION : '+res)
    #plt.yscale('symlog')
    ax.legend(loc = 'lower right', ncol = 1, frameon = 'True',prop={"size":11})
    plt.savefig(plotsdir+'/betaM_non_z.pdf',bbox_inches='tight')
    #plt.savefig(plotsdir+'/alphaR.eps',bbox_inches='tight')
    
    fig, ax = plt.subplots()
    
    ax.plot(t, betaM[2,:], linestyle = 'None', marker = '.', color = 'y', label = r'$\beta_{\rm{M},rz}$')
    ax.errorbar(t, betaM[2,:], yerr=ebetaM[2,:], xerr=None, fmt='none', ecolor='y', capsize = 1.0)
    #ax.plot(t, -betaM[2,:], linestyle = 'None', marker = 'x', color = 'y')
    #ax.errorbar(t, -betaM[2,:], yerr=ebetaM[2,:], xerr=None, fmt='', ecolor='y', barsabove = True)
    ax.plot(t, betaM[4,:], linestyle = 'None', marker = '.', color = 'r', label = r'$\beta_{\rm{M},\phi z}$')
    ax.errorbar(t, betaM[4,:], yerr=ebetaM[4,:], xerr=None, fmt='none', ecolor='r', capsize = 1.0)
    #ax.plot(t, -betaM[4,:], linestyle = 'None', marker = 'x', color = 'r')
    #ax.errorbar(t, -betaM[4,:], yerr=ebetaM[4,:], xerr=None, fmt='', ecolor='r', barsabove = True)
    ax.plot(t, betaM[5,:], linestyle = 'None', marker = '.', color = 'g', label = r'$\beta_{\rm{M},zz}$')
    ax.errorbar(t, betaM[5,:], yerr=ebetaM[5,:], xerr=None, fmt='none', ecolor='g', capsize = 1.0)
    #ax.plot(t, -betaM[5,:], linestyle = 'None', marker = 'x', color = 'g')
    #ax.errorbar(t, -betaM[5,:], yerr=ebetaM[5,:], xerr=None, fmt='', ecolor='g', barsabove = True)
    
    plt.xlabel('Time [ms]', fontsize = '14')
    plt.ylabel(r'sign($\beta_{\rm{M},ij}) \log_{10}(|\beta_{\rm{M},ij}|+1)$',fontsize = '14')
    plt.title('RESOLUTION : '+res)
    #plt.yscale('symlog')
    ax.legend(loc = 'lower right', ncol = 1, frameon = 'True',prop={"size":11})
    plt.savefig(plotsdir+'/betaM-z.pdf',bbox_inches='tight')
    
    fig, ax = plt.subplots()
    
    ax.plot(t, betaMrho[0,:], linestyle = 'None', marker = '.', color = 'm', label = r'$\beta^{\rho}_{\rm{M},rr}$')
    ax.errorbar(t, betaMrho[0,:], yerr=ebetaMrho[0,:], xerr=None, fmt='none', ecolor='m', capsize = 1.0)
    #ax.plot(t, -betaMrho[0,:], linestyle = 'None', marker = 'x', color = 'm')
    #ax.errorbar(t, -betaMrho[0,:], yerr=ebetaMrho[0,:], xerr=None, fmt='', ecolor='m', barsabove = True)
    ax.plot(t, betaMrho[1,:], linestyle = 'None', marker = '.', color = 'c', label = r'$\beta^{\rho}_{\rm{M},r\phi}$')
    ax.errorbar(t, betaMrho[1,:], yerr=ebetaMrho[1,:], xerr=None, fmt='none', ecolor='c', capsize = 1.0)
    #ax.plot(t, -betaMrho[1,:], linestyle = 'None', marker = 'x', color = 'c')
    #ax.errorbar(t, -betaMrho[1,:], yerr=ebetaMrho[1,:], xerr=None, fmt='', ecolor='c', barsabove = True)
    ax.plot(t, betaMrho[3,:], linestyle = 'None', marker = '.', color = 'k', label = r'$\beta^{\rho}_{\rm{M},\phi\phi}$')
    ax.errorbar(t, betaMrho[3,:], yerr=ebetaMrho[3,:], xerr=None, fmt='none', ecolor='k', capsize = 1.0)
    #ax.plot(t, -betaMrho[3,:], linestyle = 'None', marker = 'x', color = 'k')
    #ax.errorbar(t, -betaMrho[3,:], yerr=ebetaMrho[3,:], xerr=None, fmt='', ecolor='k', barsabove = True)

    plt.xlabel('Time [ms]', fontsize = '14')
    plt.ylabel(r'sign($\beta^{\rho}_{\rm{M},ij}) \log_{10}(|\beta^{\rho}_{\rm{M},ij}|+1)$',fontsize = '14')
    plt.title('RESOLUTION : '+res)
    #plt.yscale('symlog')
    ax.legend(loc = 'lower right', ncol = 1, frameon = 'True',prop={"size":11})
    plt.savefig(plotsdir+'/betaMrho_non_z.pdf',bbox_inches='tight')
    #plt.savefig(plotsdir+'/alphaR.eps',bbox_inches='tight')
    
    fig, ax = plt.subplots()
    
    ax.plot(t, betaMrho[2,:], linestyle = 'None', marker = '.', color = 'y', label = r'$\beta^{\rho}_{\rm{M},rz}$')
    ax.errorbar(t, betaMrho[2,:], yerr=ebetaMrho[2,:], xerr=None, fmt='none', ecolor='y', capsize = 1.0)
    #ax.plot(t, -betaM[2,:], linestyle = 'None', marker = 'x', color = 'y')
    #ax.errorbar(t, -betaMrho[2,:], yerr=ebetaMrho[2,:], xerr=None, fmt='', ecolor='y', barsabove = True)
    ax.plot(t, betaMrho[4,:], linestyle = 'None', marker = '.', color = 'r', label = r'$\beta^{\rho}_{\rm{M},\phi z}$')
    ax.errorbar(t, betaMrho[4,:], yerr=ebetaMrho[4,:], xerr=None, fmt='none', ecolor='r', capsize = 1.0)
    #ax.plot(t, -betaMrho[4,:], linestyle = 'None', marker = 'x', color = 'r')
    #ax.errorbar(t, -betaMrho[4,:], yerr=ebetaMrho[4,:], xerr=None, fmt='', ecolor='r', barsabove = True)
    ax.plot(t, betaMrho[5,:], linestyle = 'None', marker = '.', color = 'g', label = r'$\beta^{\rho}_{\rm{M},zz}$')
    ax.errorbar(t, betaMrho[5,:], yerr=ebetaMrho[5,:], xerr=None, fmt='none', ecolor='g', capsize = 1.0)
    #ax.plot(t, -betaMrho[5,:], linestyle = 'None', marker = 'x', color = 'g')
    #ax.errorbar(t, -betaMrho[5,:], yerr=ebetaMrho[5,:], xerr=None, fmt='', ecolor='g', barsabove = True)
    
    plt.xlabel('Time [ms]', fontsize = '14')
    plt.ylabel(r'sign($\beta^{\rho}_{\rm{M},ij}) \log_{10}(|\beta^{\rho}_{\rm{M},ij}|+1)$',fontsize = '14')
    plt.title('RESOLUTION : '+res)
    #plt.yscale('symlog')
    ax.legend(loc = 'lower right', ncol = 1, frameon = 'True',prop={"size":11})
    plt.savefig(plotsdir+'/betaMrho-z.pdf',bbox_inches='tight')
    
    fig, ax = plt.subplots()
    
    ax.plot(t, gammaR[0,:], linestyle = 'None', marker = '.', color = 'm', label = r'$\gamma_{\rm{R},r\phi}$')
    ax.errorbar(t,gammaR[0,:],yerr=egammaR[0,:], xerr=None, fmt='none', ecolor='m', capsize = 1.0)
    #ax.plot(t, -gammaR[0,:], linestyle = 'None', marker = 'x', color = 'm')
    #ax.errorbar(t,-gammaR[0,:],yerr=egammaR[0,:], xerr=None, fmt='', ecolor='m', barsabove = True)
    ax.plot(t, gammaR[1,:], linestyle = 'None', marker = '.', color = 'c', label = r'$\gamma_{\rm{R},rz}$')
    ax.errorbar(t,gammaR[1,:],yerr=egammaR[1,:], xerr=None, fmt='none', ecolor='c', capsize = 1.0)
    #ax.plot(t, -gammaR[1,:], linestyle = 'None', marker = 'x', color = 'c')
    #ax.errorbar(t,-gammaR[1,:],yerr=egammaR[1,:], xerr=None, fmt='', ecolor='c', barsabove = True)
    ax.plot(t, gammaR[2,:], linestyle = 'None', marker = '.', color = 'y', label = r'$\gamma_{\rm{R},\phi z}$')
    ax.errorbar(t,gammaR[2,:],yerr=egammaR[2,:], xerr=None, fmt='none', ecolor='y', capsize = 1.0)
    #ax.plot(t, -gammaR[2,:], linestyle = 'None', marker = 'x', color = 'y')
    #ax.errorbar(t,-gammaR[2,:],yerr=egammaR[2,:], xerr=None, fmt='', ecolor='c', barsabove = True)
    
    plt.xlabel('Time [ms]', fontsize = '14')
    plt.ylabel(r'$\gamma_{\rm{R},ij}$',fontsize = '14')
    plt.title('RESOLUTION : '+res)
    #plt.yscale('symlog')
    ax.legend(loc = 'lower right', ncol = 1, frameon = 'True',prop={"size":11})
    plt.savefig(plotsdir+'/gammaR.pdf',bbox_inches='tight')
    #plt.savefig(plotsdir+'/gammaR.eps',bbox_inches='tight')
    
    fig, ax = plt.subplots()
    
    ax.plot(t, gammaRrho[0,:], linestyle = 'None', marker = '.', color = 'm', label = r'$\gamma^{\rho}_{\rm{R},r\phi}$')
    ax.errorbar(t,gammaRrho[0,:],yerr=egammaRrho[0,:], xerr=None, fmt='none', ecolor='m', capsize = 1.0)
    #ax.plot(t, -gammaRrho[0,:], linestyle = 'None', marker = 'x', color = 'm')
    #ax.errorbar(t,-gammaRrho[0,:],yerr=egammaRrho[0,:], xerr=None, fmt='', ecolor='m', barsabove = True)
    ax.plot(t, gammaRrho[1,:], linestyle = 'None', marker = '.', color = 'c', label = r'$\gamma^{\rho}_{\rm{R},rz}$')
    ax.errorbar(t,gammaRrho[1,:],yerr=egammaRrho[1,:], xerr=None, fmt='none', ecolor='c', capsize = 1.0)
    #ax.plot(t, -gammaRrho[1,:], linestyle = 'None', marker = 'x', color = 'c')
    #ax.errorbar(t,-gammaRrho[1,:],yerr=egammaRrho[1,:], xerr=None, fmt='', ecolor='c', barsabove = True)
    ax.plot(t, gammaRrho[2,:], linestyle = 'None', marker = '.', color = 'y', label = r'$\gamma^{\rho}_{\rm{R},\phi z}$')
    ax.errorbar(t,gammaRrho[2,:],yerr=egammaRrho[2,:], xerr=None, fmt='none', ecolor='y', capsize = 1.0)
    #ax.plot(t, -gammaRrho[2,:], linestyle = 'None', marker = 'x', color = 'y')
    #ax.errorbar(t,-gammaRrho[2,:],yerr=egammaRrho[2,:], xerr=None, fmt='', ecolor='c', barsabove = True)
    
    plt.xlabel('Time [ms]', fontsize = '14')
    plt.ylabel(r'$\gamma^{\rho}_{\rm{R},ij}$',fontsize = '14')
    plt.title('RESOLUTION : '+res)
    #plt.yscale('symlog')
    ax.legend(loc = 'lower right', ncol = 1, frameon = 'True',prop={"size":11})
    plt.savefig(plotsdir+'/gammaRrho.pdf',bbox_inches='tight')
    
    fig, ax = plt.subplots()
    
    ax.plot(t, gammaM[0,:], linestyle = 'None', marker = '.', color = 'm', label = r'$\gamma_{\rm{M},r\phi}$')
    ax.errorbar(t,gammaM[0,:],yerr=egammaM[0,:], xerr=None, fmt='none', ecolor='m', capsize = 1.0)
    #ax.plot(t, -gammaM[0,:], linestyle = 'None', marker = 'x', color = 'm')
    #ax.errorbar(t,-gammaM[0,:],yerr=egammaM[0,:], xerr=None, fmt='', ecolor='m', barsabove = True)
    ax.plot(t, gammaM[1,:], linestyle = 'None', marker = '.', color = 'c', label = r'$\gamma_{\rm{M},rz}$')
    ax.errorbar(t,gammaM[1,:],yerr=egammaM[1,:], xerr=None, fmt='none', ecolor='c', capsize = 1.0)
    #ax.plot(t, -gammaM[1,:], linestyle = 'None', marker = 'x', color = 'c')
    #ax.errorbar(t,-gammaM[1,:],yerr=egammaM[1,:], xerr=None, fmt='', ecolor='c', barsabove = True)
    ax.plot(t, gammaM[2,:], linestyle = 'None', marker = '.', color = 'y', label = r'$\gamma_{\rm{M},\phi z}$')
    ax.errorbar(t,gammaM[2,:],yerr=egammaM[2,:], xerr=None, fmt='none', ecolor='y', capsize = 1.0)
    #ax.plot(t, -gammaM[2,:], linestyle = 'None', marker = 'x', color = 'y')
    #ax.errorbar(t,-gammaM[2,:],yerr=egammaM[2,:], xerr=None, fmt='', ecolor='c', barsabove = True)
    
    plt.xlabel('Time [ms]', fontsize = '14')
    if res == 0 :
        plt.ylabel(r'$\gamma_{\rm{M},ij}$',fontsize = '14')
    else : 
        plt.ylabel(r'sign($\gamma_{\rm{M},ij}) \log_{10}(|\gamma_{\rm{M},ij}|+1)$',fontsize = '14')
    plt.title('RESOLUTION : '+res)
    #plt.yscale('symlog')
    ax.legend(loc = 'lower right', ncol = 1, frameon = 'True',prop={"size":11})
    plt.savefig(plotsdir+'/gammaM.pdf',bbox_inches='tight')
    #plt.savefig(plotsdir+'/gammaM.eps',bbox_inches='tight')
    
    #merge into one file
    pdf_mer = PdfFileMerger()
    fig1 = PdfFileReader(plotsdir+'/alphaR_non_z.pdf','rb')
    fig2 = PdfFileReader(plotsdir+'/alphaR-z.pdf','rb')
    fig3 = PdfFileReader(plotsdir+'/alphaRrho_non_z.pdf','rb')
    fig4 = PdfFileReader(plotsdir+'/alphaRrho-z.pdf','rb')
    fig5 = PdfFileReader(plotsdir+'/alphaM_non_z.pdf','rb')
    fig6 = PdfFileReader(plotsdir+'/alphaM-z.pdf','rb')
    
    pdf_mer.append(fig1)
    pdf_mer.append(fig2)
    pdf_mer.append(fig3)
    pdf_mer.append(fig4)
    pdf_mer.append(fig5)
    pdf_mer.append(fig6)

    with Path(plotsdir+'/all_alpha-'+res+'.pdf').open(mode='wb') as output_file1:
        pdf_mer.write(output_file1)

    pdf_mer = PdfFileMerger()
    fig7 = PdfFileReader(plotsdir+'/betaR_non_z.pdf','rb')
    fig8 = PdfFileReader(plotsdir+'/betaR-z.pdf','rb')
    fig9 = PdfFileReader(plotsdir+'/betaRrho_non_z.pdf','rb')
    fig10 = PdfFileReader(plotsdir+'/betaRrho-z.pdf','rb')
    fig11 = PdfFileReader(plotsdir+'/betaM_non_z.pdf','rb')
    fig12 = PdfFileReader(plotsdir+'/betaM-z.pdf','rb')
    fig13 = PdfFileReader(plotsdir+'/betaMrho_non_z.pdf','rb')
    fig14 = PdfFileReader(plotsdir+'/betaMrho-z.pdf','rb')
    
    pdf_mer.append(fig7)
    pdf_mer.append(fig8)
    pdf_mer.append(fig9)
    pdf_mer.append(fig10)
    pdf_mer.append(fig11)
    pdf_mer.append(fig12)
    pdf_mer.append(fig13)
    pdf_mer.append(fig14)
   
    with Path(plotsdir+'/all_beta-'+res+'.pdf').open(mode='wb') as output_file2:
        pdf_mer.write(output_file2)  
        
    pdf_mer = PdfFileMerger()
    fig15 = PdfFileReader(plotsdir+'/gammaR.pdf','rb')
    fig16 = PdfFileReader(plotsdir+'/gammaRrho.pdf','rb')
    fig17 = PdfFileReader(plotsdir+'/gammaM.pdf','rb')
       
    pdf_mer.append(fig15)
    pdf_mer.append(fig16)
    pdf_mer.append(fig17)

    with Path(plotsdir+'/all_gamma-'+res+'.pdf').open(mode='wb') as output_file3:
        pdf_mer.write(output_file3) 
    
    return()
    
 #%%   
    
    
def main():
    
    lent = [61,18,61,26]
    tfinal = [6,8.5,30,12.5]
    resu = ['100_100_34','100_400_100','60_240_60','200_800_200']
    m = 4
    l = 1
    n = 1

    #for j in range(0,3):
    
    j = 2
    
    if j==2:
        
        print('RESOLUTION : ',resu[j])
        
        filesdir = '/scr/miquel/Projects/sub-grid/pablo-model/constant-case/'+resu[j]+'/new-files'
        
        plotsdir = '/scr/miquel/Projects/sub-grid/pablo-model/constant-case/'+resu[j]+'/new-plots/log-scale'
        
        try:
            os.makedirs(plotsdir)
        except OSError as error:
            print(error)

        lentit = lent[j]
        tfinalit = tfinal[j]
        
        #constants:
        
        alphaR, ealphaR = get_constants(filesdir,filesdir+'/alphaR',m,l,n,lentit)
        alphaRrho, ealphaRrho = get_constants(filesdir,filesdir+'/alphaRrho',m,l,n,lentit)
        alphaM, ealphaM = get_constants(filesdir,filesdir+'/alphaM',m,l,n,lentit)
        
        betaR, ebetaR = get_constants(filesdir,filesdir+'/betaR',m,l,n,lentit)
        betaRrho, ebetaRrho = get_constants(filesdir,filesdir+'/betaRrho',m,l,n,lentit)
        betaM, ebetaM = get_constants(filesdir,filesdir+'/betaM',m,l,n,lentit)
        betaMrho, ebetaMrho = get_constants(filesdir,filesdir+'/betaMrho',m,l,n,lentit)
        
        gammaR, egammaR = get_constants(filesdir,filesdir+'/gammaR',m,l,n,lentit)
        gammaRrho, egammaRrho = get_constants(filesdir,filesdir+'/gammaRrho',m,l,n,lentit)
        gammaM, egammaM = get_constants(filesdir,filesdir+'/gammaM',m,l,n,lentit)
        
        #plots:
        
        plot_constants(plotsdir,lentit, tfinalit, alphaR, ealphaR, alphaRrho, ealphaRrho, alphaM, ealphaM, betaR, ebetaR, betaRrho, ebetaRrho, betaM, ebetaM, betaMrho, ebetaMrho, gammaR, egammaR, gammaRrho, egammaRrho, gammaM, egammaM, resu[j])
        
    return()
    
#%%
        
main()
        
