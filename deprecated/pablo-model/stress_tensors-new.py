#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 19 12:07:40 2020

@author: miquelmiravet
"""

"""

COMPUTATION OF THE STRESS TENSORS

"""

import os 
import numpy as np
import matplotlib.pyplot as plt
plt.style.use('seaborn-white')
import csv
from scipy.io import FortranFile
import h5py

#READ DATA FUNCTIONS: 


def readh5files(file):

    print('DATA FROM FILE:      ',file)

    with h5py.File(file, "r") as f:
    # List all groups
        print("Keys: %s" % f.keys())
        a_group_key = list(f.keys())
        for i in a_group_key:
            print(i)

        Pgas = f[a_group_key[0]][...]
        print('Pgas shape    :', Pgas.shape)
        bphi = f[a_group_key[1]][...]
        print('Bphi shape    :', bphi.shape)
        br = f[a_group_key[2]][...]
        print('Br shape    :', br.shape)
        bz = f[a_group_key[3]][...]
        print('Bz shape    :', bz.shape)
        gravpot = f[a_group_key[4]][...]
        print('gravpot shape    :', gravpot.shape)
        phi = f[a_group_key[5]][...]
        print('Phi shape    :', phi.shape)
        r = f[a_group_key[6]][...]
        print('r shape    :', r.shape)
        rho = f[a_group_key[7]][...]
        print('Rho shape    :', rho.shape)
        time = f[a_group_key[8]][...]
        print('Time shape    :', time.shape)
        vphi = f[a_group_key[9]][...]
        print('vphi shape    :', vphi.shape)
        vr = f[a_group_key[10]][...]
        print('vr shape    :', vr.shape)
        vz = f[a_group_key[11]][...]
        print('vz shape    :', vz.shape)
        z = f[a_group_key[12]][...]
        print('z shape    :', z.shape)
        #time = file.replace('.h5','')
        #time = float(time[-4:])/250
        
        
        f.close()

    print('Dimensions of the grid     :',len(r)," x ",len(phi)," x ",len(z))
    print('Time           :',time,' s')
    
    return [time, r, phi, z, br, bphi, bz, vr, vphi, vz, rho, Pgas, gravpot]

def read_data(arxiu,direc,tyrion):
    
    """
    This function reads the data from
    the files, by choosing the file we 
    are interested to study.
    -Arguments: 
        arxiu: number of the file.
        
    """
    
    print('DATA FROM FILE:      ',arxiu)
    if tyrion == True:
        str1 = direc+'/mriquel-0file.dat' 
    else : 
        str1 = 'A100_100_34/mriquel-0file.dat'
        
    ubi=str1.replace('file',arxiu)
    with FortranFile(ubi,'r','>u4') as f: 
        # read time
        time=f.read_reals(dtype='>f8')
        print ("time                     = ", time)
        # read dimensions of the grid
        nr = f.read_reals(dtype='>i4')[0]
        nphi = f.read_reals(dtype='>i4')[0]
        nz = f.read_reals(dtype='>i4')[0]
        print ("nr x nphi x nz           = ",nr," x ",nphi," x ",nz)
        # read grid arrays (1D)
        r = f.read_reals(dtype='>f8')
        phi = f.read_reals(dtype='>f8')
        z = f.read_reals(dtype='>f8')
        print ("len(r), len(phi), len(z) = ",len(r), len(phi), len(z))

        # read variables on the grid (3D arrays)
        Br = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("Br                       : ",Br.shape)
        Bphi = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("Bphi                     : ",Bphi.shape)
        Bz = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("Bz                       : ",Bz.shape)
        vr = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("vr                       : ",vr.shape)
        vphi = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("vphi                     : ",vphi.shape)
        vz = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("vz                       : ",vz.shape)
        rho = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("rho                      : ",rho.shape)
        Pgas = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("Pgas                     : ",Pgas.shape)
        phi2 = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("phi                      : ",phi2.shape)
         
        
    return [time, r, phi, z, Br, Bphi, Bz, vr, vphi, vz, rho, Pgas, phi2] 


#AVERAGING AND STRESS TENSORS FUNCTION: 

def averaging(r,phi,z,B,v,rho,p):
    
    
    dr = (r[1]-r[0])
    print('dr   =', dr,' cm')
    dphi = phi[1]-phi[0]
    print('dphi =', dphi,' rads')
    dz = (z[1]-z[0])
    print('dz   =', dz,' cm')
    dV = r*dr*dphi*dz
    dVmatrix = np.ones((len(r),len(phi),len(z)))
    for x in range(0,len(r)):
        dVmatrix[x,:,:] = dV[x]*dVmatrix[x,:,:]
    print('Volume element dim:', dVmatrix.shape)
    #total volume: 
    V = np.sum(dVmatrix)
    print('Volume:', V*10**(-15), 'km3')
    print('Volume check:', 1*1*0.3,'km3')

    #averaging:
    var_dec = [B[0],B[1],B[2],v[0],v[1],v[2],rho,p]
    meanvar=np.zeros(8)
    for y in range(0,8):
        integrand = dVmatrix*var_dec[y]
        print('Integrand dimension:',integrand.shape)
        meanvar[y] = np.sum(integrand)/V
        
    #Bmean = np.array([meanvar[0],meanvar[1],meanvar[2]])
    #vmean = np.array([meanvar[3],meanvar[4],meanvar[5]])
    #rhomean = meanvar[6]
    #pmean = meanvar[7]
    
    #turbulent parts:
    turb = np.zeros((6,len(r),len(phi),len(z)))
    for x in range(0,6):
        turb[x] = var_dec[x]-meanvar[x]
    print('Turbulent matrix shape:', turb.shape)
    brturb = turb[0]
    bphiturb = turb[1]
    bzturb = turb[2]
    vrturb = turb[3] 
    vphiturb = turb[4]
    vzturb = turb[5]

    #stress tensors:
    bturb = [brturb, bphiturb, bzturb]
    vturb = [vrturb, vphiturb, vzturb]
    M = np.zeros((3,3,len(r),len(phi),len(z)))
    R = np.zeros((3,3,len(r),len(phi),len(z)))
    F = np.zeros((3,3,len(r),len(phi),len(z)))
    for x in range(0,3):
        for y in range(0,3):
            M[x][y] = bturb[x]*bturb[y]
            R[x][y] = vturb[x]*vturb[y]
            F[x][y] = bturb[x]*vturb[y]        
    print('Maxwell stress tensor :', M.shape)
    print('Reynolds stress tensor:', R.shape)
    print('Faraday stress tensor :', F.shape)
    #averaging of the turbulent stress tensors:
    stresses = [M,R,F]
    stresses = np.array(stresses)
    print('Stresses shape:', stresses.shape)
    print('dV matrix shape:', dVmatrix.shape)
    mean_stresses = np.zeros((3,3,3))
    for x in range(0,3):
        print('STRESS TENSOR Nº           :', x+1)
        int_stress = dVmatrix*stresses[x]
        int_stress = np.sum(int_stress,axis=4)
        print('Summation in nz. Dimensions:', int_stress.shape)
        int_stress = np.sum(int_stress,axis=3)
        print('Summation in nphi. Dimensions:', int_stress.shape)
        int_stress = np.sum(int_stress,axis=2)
        print('Summation in nr. Dimensions:', int_stress.shape)
        mean_stresses[x] = int_stress/V
    Mmean = mean_stresses[0]
    Rmean = mean_stresses[1]
    Fmean = mean_stresses[2]
        
    return meanvar, Mmean, Rmean, Fmean

def splitting(arr,n):
    """
    Function that splits the array in order to 
    have a number of components proportional to n
    """
    frac = len(arr)/n
    if frac.is_integer() == False: 
        while True: 
            arr = np.delete(arr,len(arr)-1)
            frac_new = len(arr)/n
            if frac_new.is_integer() == True:
                break
    return arr


def averaging_subboxes(n,m,l,r,phi,z,B,v,rho,p): 
    """
    This function averages over (m,l,n) subboxes, 
    in order to determine the partial derivatives. 
    """
    
    #reshaping of the coordinate arrays into m/n subarrays of equal length
    r_split = r.reshape(m,int(len(r)/m))
    phi_split = phi.reshape(l,int(len(phi)/l))
    z_split = z.reshape(n,int(len(z)/n))
    
    var = np.array([B[0],B[1],B[2],v[0],v[1],v[2],rho,p])
    varnew = np.zeros((8,len(r),len(phi),len(z)))
    
    #splitting of the variable arrays
    for x in range(0,8):
        for i in range(0,len(r)):
            for j in range(0,len(phi)):
                varnew[x,i,j,:] = splitting(var[x,i,j,:],n)
            for k in range(0,len(z)):
                varnew[x,i,:,k] = splitting(var[x,i,:,k],l)
        for j in range(0,len(phi)):
            for k in range(0,len(z)):
                varnew[x,:,j,k] = splitting(var[x,:,j,k],m)
    
    #reshaping of the variable arrays into subarrays of equal length, corresponding to each sub-box.
    var_split = varnew.reshape(8,m,int(len(r)/m),l,int(len(phi)/l),n,int(len(z)/n))
    
    #AVERAGING PROCEDURE
    
    #differentials
    dr = r_split[0,1]-r_split[0,0]
    dphi = phi_split[0,1]-phi_split[0,0]
    dz = z_split[0,1]-z_split[0,0]
    
    varmean = np.zeros((8,m,l,n))
    
    #construction of the matrix that represents the integral differentials. It will be different for each sub-box
    dVmatrix = np.ones((m,int(len(r)/m),l,int(len(phi)/l),n,int(len(z)/n)))
    dVmatrixdef = np.zeros((m,int(len(r)/m),l,int(len(phi)/l),n,int(len(z)/n)))   
    for i in range(0,m):
        dV = r_split[i,:]*dr*dphi*dz
        for j in range(0,l):
            for k in range(0,n):
                for x in range(0,int(len(r)/m)):
                    dVmatrixdef[i,x,j,:,k,:] = dV[x]*dVmatrix[i,x,j,:,k,:]
    
    #volume of each sub-box                
    V = np.zeros((m,l,n))
    for i in range(0,m):
        for j in range(0,l):
            for k in range(0,n):
                V[i,j,k] = np.sum(dVmatrixdef[i,:,j,:,k,:])
    
    #averaging
    for x in range(0,8):
        for i in range(0,m):
            for j in range(0,l):
                for k in range(0,n):
                    varmean[x,i,j,k] = np.sum(dVmatrixdef[i,:,j,:,k,:]*var_split[x,i,:,j,:,k,:])/V[i,j,k] 
    
    varturb = np.zeros((8,m,int(len(r)/m),l,int(len(phi)/l),n,int(len(z)/n)))
    
    for x in range(0,8):
        for i in range(0,m):
            for j in range(0,l):
                for k in range(0,n):
                    varturb[x,i,:,j,:,k,:] = var_split[x,i,:,j,:,k,:]-varmean[x,i,j,k]
    
    bturb = np.array([varturb[0],varturb[1],varturb[2]])
    vturb = np.array([varturb[3],varturb[4],varturb[5]])
    rhoturb = varturb[6]
    rhosplit = var_split[6]
    bmean = np.array([varmean[0],varmean[1],varmean[2]])
    vmean = np.array([varmean[3],varmean[4],varmean[5]])
    rhomean = varmean[6]
    
    sigmarhosq = np.zeros((m,l,n))
    
    for i in range(0,m):
        for j in range(0,l):
            for k in range(0,n):
                sigmarhosq = np.sum(dVmatrixdef[i,:,j,:,k,:]*rhoturb[i,:,j:,k,:]**2)/V[i,j,k]
    
    M = np.zeros((3,3,m,int(len(r)/m),l,int(len(phi)/l),n,int(len(z)/n)))
    R = np.zeros((3,3,m,int(len(r)/m),l,int(len(phi)/l),n,int(len(z)/n)))
    F = np.zeros((3,3,m,int(len(r)/m),l,int(len(phi)/l),n,int(len(z)/n)))
    Rrho = np.zeros((3,3,m,int(len(r)/m),l,int(len(phi)/l),n,int(len(z)/n)))
    
    Mmean = np.zeros((3,3,m,l,n))
    Rmean = np.zeros((3,3,m,l,n))
    Fmean = np.zeros((3,3,m,l,n))
    Rrhomean = np.zeros((3,3,m,l,n))
    
    sigmaMsq = np.zeros((3,3,m,l,n))   
    sigmaRsq = np.zeros((3,3,m,l,n))  
    sigmaRrhosq = np.zeros((3,3,m,l,n))
    sigmaFsq = np.zeros((3,3,m,l,n))
    
    for x in range(0,3):
        for y in range(0,3):
            M[x,y] = bturb[x]*bturb[y]
            R[x,y] = vturb[x]*vturb[y]
            Rrho[x,y] = rhosplit*vturb[x]*vturb[y]
            F[x,y] = bturb[y]*vturb[x]-bturb[x]*vturb[y] 
            
    #averaging stress tensors: 
        
            for i in range(0,m):
                for j in range(0,l):
                    for k in range(0,n):
                        Mmean[x,y,i,j,k] = np.sum(dVmatrixdef[i,:,j,:,k,:]*M[x,y,i,:,j,:,k,:])/V[i,j,k] 
                        Rmean[x,y,i,j,k] = np.sum(dVmatrixdef[i,:,j,:,k,:]*R[x,y,i,:,j,:,k,:])/V[i,j,k]
                        Rrhomean[x,y,i,j,k] = np.sum(dVmatrixdef[i,:,j,:,k,:]*Rrho[x,y,i,:,j,:,k,:])/V[i,j,k]
                        Fmean[x,y,i,j,k] = np.sum(dVmatrixdef[i,:,j,:,k,:]*F[x,y,i,:,j,:,k,:])/V[i,j,k]
                        sigmaMsq[x,y,i,j,k] = np.sum(dVmatrixdef[i,:,j,:,k,:]*(M[x,y,i,:,j,:,k,:]-Mmean[x,y,i,j,k])**2)/V[i,j,k]
                        sigmaRsq[x,y,i,j,k] = np.sum(dVmatrixdef[i,:,j,:,k,:]*(R[x,y,i,:,j,:,k,:]-Rmean[x,y,i,j,k])**2)/V[i,j,k]
                        sigmaFsq[x,y,i,j,k] = np.sum(dVmatrixdef[i,:,j,:,k,:]*(F[x,y,i,:,j,:,k,:]-Fmean[x,y,i,j,k])**2)/V[i,j,k]
                        sigmaRrhosq[x,y,i,j,k] = np.sum(dVmatrixdef[i,:,j,:,k,:]*(Rrho[x,y,i,:,j,:,k,:]-Rrhomean[x,y,i,j,k])**2)/V[i,j,k]
                        
    return bmean, vmean, rhomean, Mmean, Rmean, Rrhomean, Fmean, sigmaMsq, sigmaRsq, sigmaRrhosq, sigmaFsq, sigmarhosq

#Time evolution: 


def time_evolution_sub_boxes(direc,lent,lenr,lenphi,lenz,stresses,tyrion,m,l,n):
    
    bmean_t = np.zeros((lent,3,m,l,n))
    vmean_t = np.zeros((lent,3,m,l,n))
    rhomean_t = np.zeros((lent,m,l,n))
    Mmean_t = np.zeros((lent,3,3,m,l,n))
    Rmean_t = np.zeros((lent,3,3,m,l,n))
    Rrhomean_t = np.zeros((lent,3,3,m,l,n))
    Fmean_t = np.zeros((lent,3,3,m,l,n))
    sigmaMsq_t = np.zeros((lent,3,3,m,l,n))
    sigmaRsq_t = np.zeros((lent,3,3,m,l,n))
    sigmaFsq_t = np.zeros((lent,3,3,m,l,n))
    sigmarhosq_t = np.zeros((lent,m,l,n))
    sigmaRrhosq_t = np.zeros((lent,3,3,m,l,n))
    
    

    for x in range(0,lent):
        
        if direc == str(os.getcwd())+'/MRI/Miquel/A100_100_34/':
            arx = format(10*x, "03")
            listvar=read_data(arx,direc,tyrion)
        else :
            arx = format(25*x, "04")
            listvar = readh5files(direc+'mri-'+str(arx)+'.h5')
        
        r = listvar[1]
        phi = listvar[2]
        z = listvar[3]
        B = np.array([listvar[4],listvar[5],listvar[6]])*2.874214371*10**(-25)/((4*3.141592653589793)**(0.5))
        v = np.array([listvar[7],listvar[8],listvar[9]])*3.33564095*10**(-11)
        rho = listvar[10]*7.42471382*10**(-29)
        p = listvar[11]*8.26110825*10**(-50)
        
        #extract Omega*r to v_phi:
        
        r0 = 31*10**(5)
        omega0 = 767*3.33564095*10**(-11)
        q = 1.25
        omega = omega0*(r/r0)**(-q)
        for i in range(0,len(r)):
            v[1,i,:,:] = v[1,i,:,:]-omega[i]*r[i]
            
        #averaging:
        
        bmean_t[x], vmean_t[x], rhomean_t[x], Mmean_t[x], Rmean_t[x], Rrhomean_t[x], Fmean_t[x], sigmaMsq_t[x], sigmaRsq_t[x], sigmaRrhosq_t[x],sigmaFsq_t[x], sigmarhosq_t[x] = averaging_subboxes(n,m,l,r,phi,z,B,v,rho,p)

    return Mmean_t, Rmean_t, Rrhomean_t, Fmean_t,rhomean_t, sigmaMsq_t, sigmaRsq_t, sigmaRrhosq_t, sigmaFsq_t, sigmarhosq_t

def write_file(stresses,Mmean_t,Rmean_t,Rrhomean_t,Fmean_t,rhomean_t,sigmaMsq_t,sigmaRsq_t,sigmaRrhosq_t,sigmaFsq_t,sigmarhosq_t,m,l,n,lent):
    
    if stresses == True :
        
        with open('stress_M.csv', 'w') as f:
            writer = csv.writer(f, delimiter='\t')
            for a in range(0,m):
                for b in range(0,l):
                    for c in range(0,n):
                        if lent == 61:
                             col = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59','60']
                        elif lent == 26:
                             col = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25']
                        elif lent == 18:
                             col = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17']
                        
                        cols = zip(col,Mmean_t[:,0,0,a,b,c],Mmean_t[:,0,1,a,b,c],Mmean_t[:,0,2,a,b,c],Mmean_t[:,1,1,a,b,c],Mmean_t[:,1,2,a,b,c],Mmean_t[:,2,2,a,b,c],sigmaMsq_t[:,0,0,a,b,c],sigmaMsq_t[:,0,1,a,b,c],sigmaMsq_t[:,0,2,a,b,c],sigmaMsq_t[:,1,1,a,b,c],sigmaMsq_t[:,1,2,a,b,c],sigmaMsq_t[:,2,2,a,b,c])
                        writer.writerows(cols)
    
        with open('stress_R.csv', 'w') as f:
            writer = csv.writer(f, delimiter='\t')
            for a in range(0,m):
                for b in range(0,l):
                    for c in range(0,n):
                        if lent == 61:
                             col = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59','60']
                        elif lent == 26:
                             col = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25']
                        elif lent == 18:
                             col = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17']
                        
                        cols = zip(col,Rmean_t[:,0,0,a,b,c],Rmean_t[:,0,1,a,b,c],Rmean_t[:,0,2,a,b,c],Rmean_t[:,1,1,a,b,c],Rmean_t[:,1,2,a,b,c],Rmean_t[:,2,2,a,b,c],sigmaRsq_t[:,0,0,a,b,c],sigmaRsq_t[:,0,1,a,b,c],sigmaRsq_t[:,0,2,a,b,c],sigmaRsq_t[:,1,1,a,b,c],sigmaRsq_t[:,1,2,a,b,c],sigmaRsq_t[:,2,2,a,b,c])
                        writer.writerows(cols)
                        
        with open('stress_Rrho.csv', 'w') as f:
            writer = csv.writer(f, delimiter='\t')
            for a in range(0,m):
                for b in range(0,l):
                    for c in range(0,n):
                        if lent == 61:
                             col = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59','60']
                        elif lent == 26:
                             col = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25']
                        elif lent == 18:
                             col = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17']
                        
                        cols = zip(col,Rrhomean_t[:,0,0,a,b,c],Rrhomean_t[:,0,1,a,b,c],Rrhomean_t[:,0,2,a,b,c],Rrhomean_t[:,1,1,a,b,c],Rrhomean_t[:,1,2,a,b,c],Rrhomean_t[:,2,2,a,b,c],sigmaRrhosq_t[:,0,0,a,b,c],sigmaRrhosq_t[:,0,1,a,b,c],sigmaRrhosq_t[:,0,2,a,b,c],sigmaRrhosq_t[:,1,1,a,b,c],sigmaRrhosq_t[:,1,2,a,b,c],sigmaRrhosq_t[:,2,2,a,b,c])
                        writer.writerows(cols)
    
    
        with open('stress_F.csv', 'w') as f:
            writer = csv.writer(f, delimiter='\t')
            for a in range(0,m):
                for b in range(0,l):
                    for c in range(0,n):
                        if lent == 61:
                             col = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59','60']
                        elif lent == 26:
                             col = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25']
                        elif lent == 18:
                             col = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17']
                        
                        cols = zip(col,Fmean_t[:,0,1,a,b,c],Fmean_t[:,0,2,a,b,c],Fmean_t[:,1,2,a,b,c],sigmaFsq_t[:,0,1,a,b,c],sigmaFsq_t[:,0,2,a,b,c],sigmaFsq_t[:,1,2,a,b,c])
                        writer.writerows(cols)
    
            
        with open('rho_mean.csv','w') as f: 
             writer = csv.writer(f, delimiter='\t')
             for a in range(0,m):
                for b in range(0,l):
                    for c in range(0,n):
                        if lent == 61:
                             col = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59','60']
                        elif lent == 26:
                             col = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25']
                        elif lent == 18:
                             col = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17']
                        
                        cols = zip(col,rhomean_t[:,a,b,c],sigmarhosq_t[:,a,b,c])
                        writer.writerows(cols)
    
    else : 
        
        with open('rho_mean.csv','w') as f: 
             writer = csv.writer(f, delimiter='\t')
             for a in range(0,m):
                for b in range(0,l):
                    for c in range(0,n):
                        if lent == 61:
                             col = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59','60']
                        elif lent == 26:
                             col = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25']
                        elif lent == 18:
                             col = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17']
                        
                        cols = zip(col,rhomean_t[:,a,b,c],sigmarhosq_t[:,a,b,c])
                        writer.writerows(cols)
            
    return()
#%%

def main():
    
    stresses = True
    tyrion = True
    m = 4
    l = 1
    n = 1
    lentv = [61,18,61,26]
    #tfinal = [6,8.5,30,12.5]
    lenrv=[100,100,60,200]
    lenphiv=[100,400,240,800]
    lenzv=[34,100,60,200]
    resu = ['100_100_34','100_400_100','60_240_60','200_800_200']
    direcv = ['A100_100_34/','A100_400_100_B_4.6e13_v_4.45d8_r_4.45d8_Lz_1.0_INTEL_stencil/','A60_240_60_B_4.6e13_v_4.45d8_r_4.45d8_Lz_1.0_INTEL_stencil_flex/','A200_800_200_B_4.6e13_v_4.45d8_r_4.45d8_Lz_1.0_INTEL_stencil/']

    for j in range(0,4): 
        
        print('RESOLUTION : ',resu[j])
        
        lent = lentv[j]
        lenr = lenrv[j]
        lenphi = lenphiv[j]
        lenz = lenzv[j]
    
        direc = direcv[j]
        
    
        if tyrion == True:
        
            direc = str(os.getcwd())+'/MRI/Miquel/'+direc
        
        Mmean_t, Rmean_t, Rrhomean_t, Fmean_t,rhomean_t, sigmaMsq_t, sigmaRsq_t, sigmaRrhosq_t, sigmaFsq_t, sigmarhosq_t = time_evolution_sub_boxes(direc,lent,lenr,lenphi,lenz,stresses,tyrion,m,l,n)
        
        write_file(stresses,Mmean_t,Rmean_t,Rrhomean_t,Fmean_t,rhomean_t,sigmaMsq_t,sigmaRsq_t,sigmaRrhosq_t,sigmaFsq_t,sigmarhosq_t,m,l,n,lent)
    
        if stresses == True:
        
            path = str(os.getcwd())+'/stress_tensors/'+str(lenr)+'_'+str(lenphi)+'_'+str(lenz)+'/new-version/text_files'
    
            try:
                os.makedirs(path)
            except OSError as error:
                    print(error)
    
            os.system('mv stress*.csv '+path+'/.')
            
            path = str(os.getcwd())+'/rho_mean/'+str(lenr)+'_'+str(lenphi)+'_'+str(lenz)+'/new-version'
            
            try:
                os.makedirs(path)
            except OSError as error:
                    print(error)
                
            os.system('mv rho*.csv '+path+'/.')
    
    
        else:
            
            path = str(os.getcwd())+'/rho_mean/'+str(lenr)+'_'+str(lenphi)+'_'+str(lenz)+'/new-version'
            
            try:
                os.makedirs(path)
            except OSError as error:
                    print(error)
                
            os.system('mv rho*.csv '+path+'/.')
    
    return()
    

        
    
    
  #%%

main()  
    
    
