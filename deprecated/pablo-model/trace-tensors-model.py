#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 28 18:05:04 2021

@author: miquelmiravet

PABLO'S MODEL TESTING
"""
#############################################

# Let's model the stress tensors Mij, Rij and Tij as functions of the total energy density.
# First approach: Consider that the tensors are proportional to the energy density. 

#############################################

import os 
import numpy as np
import matplotlib.pyplot as plt
from scipy.io import FortranFile
import h5py
from pathlib import Path
from PyPDF2 import PdfFileMerger
from PyPDF2 import PdfFileReader

def readh5files(file):

    print('DATA FROM FILE:      ',file)

    with h5py.File(file, "r") as f:
    # List all groups
        print("Keys: %s" % f.keys())
        a_group_key = list(f.keys())
        for i in a_group_key:
            print(i)

        Pgas = f[a_group_key[0]][...]
        print('Pgas shape    :', Pgas.shape)
        bphi = f[a_group_key[1]][...]
        print('Bphi shape    :', bphi.shape)
        br = f[a_group_key[2]][...]
        print('Br shape    :', br.shape)
        bz = f[a_group_key[3]][...]
        print('Bz shape    :', bz.shape)
        gravpot = f[a_group_key[4]][...]
        print('gravpot shape    :', gravpot.shape)
        phi = f[a_group_key[5]][...]
        print('Phi shape    :', phi.shape)
        r = f[a_group_key[6]][...]
        print('r shape    :', r.shape)
        rho = f[a_group_key[7]][...]
        print('Rho shape    :', rho.shape)
        time = f[a_group_key[8]][...]
        print('Time shape    :', time.shape)
        vphi = f[a_group_key[9]][...]
        print('vphi shape    :', vphi.shape)
        vr = f[a_group_key[10]][...]
        print('vr shape    :', vr.shape)
        vz = f[a_group_key[11]][...]
        print('vz shape    :', vz.shape)
        z = f[a_group_key[12]][...]
        print('z shape    :', z.shape)
        #time = file.replace('.h5','')
        #time = float(time[-4:])/250
        
        
        f.close()

    print('Dimensions of the grid     :',len(r)," x ",len(phi)," x ",len(z))
    print('Time           :',time,' s')
    
    return [time, r, phi, z, br, bphi, bz, vr, vphi, vz, rho, Pgas, gravpot]

def read_data(arxiu,direc,tyrion):
    
    """
    This function reads the data from
    the files, by choosing the file we 
    are interested to study.
    -Arguments: 
        arxiu: number of the file.
        
    """
    
    print('DATA FROM FILE:      ',arxiu)
    if tyrion == True:
        str1 = direc+'/mriquel-0file.dat' 
    else : 
        str1 = 'A100_100_34/mriquel-0file.dat'
        
    ubi=str1.replace('file',arxiu)
    with FortranFile(ubi,'r','>u4') as f: 
        # read time
        time=f.read_reals(dtype='>f8')
        print ("time                     = ", time)
        # read dimensions of the grid
        nr = f.read_reals(dtype='>i4')[0]
        nphi = f.read_reals(dtype='>i4')[0]
        nz = f.read_reals(dtype='>i4')[0]
        print ("nr x nphi x nz           = ",nr," x ",nphi," x ",nz)
        # read grid arrays (1D)
        r = f.read_reals(dtype='>f8')
        phi = f.read_reals(dtype='>f8')
        z = f.read_reals(dtype='>f8')
        print ("len(r), len(phi), len(z) = ",len(r), len(phi), len(z))

        # read variables on the grid (3D arrays)
        Br = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("Br                       : ",Br.shape)
        Bphi = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("Bphi                     : ",Bphi.shape)
        Bz = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("Bz                       : ",Bz.shape)
        vr = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("vr                       : ",vr.shape)
        vphi = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("vphi                     : ",vphi.shape)
        vz = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("vz                       : ",vz.shape)
        rho = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("rho                      : ",rho.shape)
        Pgas = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("Pgas                     : ",Pgas.shape)
        phi2 = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("phi                      : ",phi2.shape)
         
        
    return [time, r, phi, z, Br, Bphi, Bz, vr, vphi, vz, rho, Pgas, phi2] 

def averaging(var,r,phi,z): 
    """
    This function averages over the whole box.  
    """
   
    dr = r[1]-r[0]
    dphi = phi[1]-phi[0]
    dz = z[1]-z[0]
    dV = r*dr*dphi*dz
    dVmatrix = np.ones((len(r),len(phi),len(z)))
    for x in range(0,len(r)):
        dVmatrix[x,:,:] = dV[x]*dVmatrix[x,:,:]
    
    #total volume: 
    V = np.sum(dVmatrix)
    

    #averaging:
    integrand = dVmatrix*var

    meanvar = np.sum(integrand)/V


    
    return meanvar

def stresses(bmean,vmean,b,v,lenr,lenphi,lenz):

    #turbulent parts:

    brturb = b[0,:,:,:]-bmean[0]
    bphiturb = b[1,:,:,:]-bmean[1]
    bzturb = b[2,:,:,:]-bmean[2]
    vrturb = v[0,:,:,:]-vmean[0] 
    vphiturb = v[1,:,:,:]-vmean[1]
    vzturb = v[2,:,:,:]-vmean[2]

    #stress tensors:
    bturb = np.array([brturb, bphiturb, bzturb])
    vturb = np.array([vrturb, vphiturb, vzturb])
    M = np.zeros((3,3,lenr,lenphi,lenz))
    R = np.zeros((3,3,lenr,lenphi,lenz))
    F = np.zeros((3,3,lenr,lenphi,lenz))
    for x in range(0,3):
        for y in range(0,3):
            M[x,y,:,:,:] = bturb[x,:,:,:]*bturb[y,:,:,:]
            R[x,y,:,:,:] = vturb[x,:,:,:]*vturb[y,:,:,:]
            F[x,y,:,:,:] = bturb[x,:,:,:]*vturb[y,:,:,:]        
    print('Maxwell stress tensor :', M.shape)
    print('Reynolds stress tensor:', R.shape)
    print('Faraday stress tensor :', F.shape)

    
    #vmean in units of c
    #for y in range(0,3):
     #   meanvar[y+3] = meanvar[y+3]*10**(-2)/299792458
    
    return (M, R, F)


def get_mean_stress_sym(filename):
    names = ['rr','rphi','rz','phiphi','phiz','zz']
    data = np.recfromtxt(filename,names=names)
    return (data['rr'],data['rphi'],data['rz'],data['phiphi'],data['phiz'],data['zz'])

def get_mean_stress(filename):
    names = ['rr','phir','zr','phiphi','phiz','zz','rphi','rz','zphi']
    data = np.recfromtxt(filename,names=names)
    return (data['rr'],data['phir'],data['zr'],data['phiphi'],data['phiz'],data['zz'],data['rphi'],data['rz'],data['zphi'])
        
def energy_dens(Rtensor,Mtensor,rho):
    rho = 1
    traceR = Rtensor[:,0]+Rtensor[:,3]+Rtensor[:,5]
    traceM = Mtensor[:,0]+Mtensor[:,3]+Mtensor[:,5]
    energyR = 0.5*rho*traceR
    energyM = 0.5*rho*traceM
    return (energyR,energyM)

def energy_dens_bis(R,M,rho):
    traceR = R[0,0]+R[1,1]+R[2,2]
    traceM = M[0,0]+M[1,1]+M[2,2]
    energyR = 0.5*rho[:,:,:]*traceR[:,:,:]
    energyM = 0.5*rho[:,:,:]*traceM[:,:,:]
    return(energyR,energyM)

def plot_constants(plotsdir,lent, tfinal, alphaR, alphaM,betaR, betaM, gammaR, gammaM,res):
    
    t = np.linspace(0,tfinal,lent)
    t = t[1:]
    alphaR = alphaR[1:,:]
    alphaM = alphaM[1:,:]
    betaR = betaR[1:,:]
    betaM = betaM[1:,:]
    gammaR = gammaR[1:,:]
    gammaM = gammaM[1:,:]

    fig, ax = plt.subplots()
    
    ax.plot(t, alphaR[:,0], linestyle = 'None', marker = '.', color = 'm', label = r'$\alpha_{\rm{R},rr}$')
    ax.plot(t, -alphaR[:,0], linestyle = 'None', marker = 'x', color = 'm')
    ax.plot(t, alphaR[:,1], linestyle = 'None', marker = '.', color = 'c', label = r'$\alpha_{\rm{R},r\phi}$')
    ax.plot(t, -alphaR[:,1], linestyle = 'None', marker = 'x', color = 'c')
    ax.plot(t, alphaR[:,2], linestyle = 'None', marker = '.', color = 'y', label = r'$\alpha_{\rm{R},rz}$')
    ax.plot(t, -alphaR[:,2], linestyle = 'None', marker = 'x', color = 'y')
    ax.plot(t, alphaR[:,3], linestyle = 'None', marker = '.', color = 'k', label = r'$\alpha_{\rm{R},\phi\phi}$')
    ax.plot(t, -alphaR[:,3], linestyle = 'None', marker = 'x', color = 'k')
    ax.plot(t, alphaR[:,4], linestyle = 'None', marker = '.', color = 'r', label = r'$\alpha_{\rm{R},\phi z}$')
    ax.plot(t, -alphaR[:,4], linestyle = 'None', marker = 'x', color = 'r')
    ax.plot(t, alphaR[:,5], linestyle = 'None', marker = '.', color = 'g', label = r'$\alpha_{\rm{R},zz}$')
    ax.plot(t, -alphaR[:,5], linestyle = 'None', marker = 'x', color = 'g')
    
    plt.xlabel('Time [ms]', fontsize = '14')
    plt.ylabel(r'$\alpha_{\rm{R},ij}$',fontsize = '14')
    plt.title('RESOLUTION : '+res)
    plt.yscale('log')
    ax.legend(loc = 'lower right', ncol = 2, frameon = 'True',prop={"size":11})
    #plt.savefig(plotsdir+'/alphaR.pdf',bbox_inches='tight')
    plt.savefig(plotsdir+'/alphaR.eps',bbox_inches='tight')
    
    fig, ax = plt.subplots()
    
    ax.plot(t, alphaM[:,0], linestyle = 'None', marker = '.', color = 'm', label = r'$\alpha_{\rm{M},rr}$')
    ax.plot(t, -alphaM[:,0], linestyle = 'None', marker = 'x', color = 'm')
    ax.plot(t, alphaM[:,1], linestyle = 'None', marker = '.', color = 'c', label = r'$\alpha_{\rm{M},r\phi}$')
    ax.plot(t, -alphaM[:,1], linestyle = 'None', marker = 'x', color = 'c')
    ax.plot(t, alphaM[:,2], linestyle = 'None', marker = '.', color = 'y', label = r'$\alpha_{\rm{M},rz}$')
    ax.plot(t, -alphaM[:,2], linestyle = 'None', marker = 'x', color = 'y')
    ax.plot(t, alphaM[:,3], linestyle = 'None', marker = '.', color = 'k', label = r'$\alpha_{\rm{M},\phi\phi}$')
    ax.plot(t, -alphaM[:,3], linestyle = 'None', marker = 'x', color = 'k')
    ax.plot(t, alphaM[:,4], linestyle = 'None', marker = '.', color = 'r', label = r'$\alpha_{\rm{M},\phi z}$')
    ax.plot(t, -alphaM[:,4], linestyle = 'None', marker = 'x', color = 'r')
    ax.plot(t, alphaM[:,5], linestyle = 'None', marker = '.', color = 'g', label = r'$\alpha_{\rm{M},zz}$')
    ax.plot(t, -alphaM[:,5], linestyle = 'None', marker = 'x', color = 'g')
    
    plt.xlabel('Time [ms]', fontsize = '14')
    plt.ylabel(r'$\alpha_{\rm{M},ij}$',fontsize = '14')
    plt.title('RESOLUTION : '+res)
    plt.yscale('log')
    ax.legend(loc = 'lower right', ncol = 2, frameon = 'True',prop={"size":11})
   # plt.savefig(plotsdir+'/alphaM.pdf',bbox_inches='tight')
    plt.savefig(plotsdir+'/alphaM.eps',bbox_inches='tight')
    
    fig, ax = plt.subplots()
    
    ax.plot(t, betaR[:,0], linestyle = 'None', marker = '.', color = 'm', label = r'$\beta_{\rm{R},rr}$')
    ax.plot(t, -betaR[:,0], linestyle = 'None', marker = 'x', color = 'm')
    ax.plot(t, betaR[:,1], linestyle = 'None', marker = '.', color = 'c', label = r'$\beta_{\rm{R},r\phi}$')
    ax.plot(t, -betaR[:,1], linestyle = 'None', marker = 'x', color = 'c')
    ax.plot(t, betaR[:,2], linestyle = 'None', marker = '.', color = 'y', label = r'$\beta_{\rm{R},rz}$')
    ax.plot(t, -betaR[:,2], linestyle = 'None', marker = 'x', color = 'y')
    ax.plot(t, betaR[:,3], linestyle = 'None', marker = '.', color = 'k', label = r'$\beta_{\rm{R},\phi\phi}$')
    ax.plot(t, -betaR[:,3], linestyle = 'None', marker = 'x', color = 'k')
    ax.plot(t, betaR[:,4], linestyle = 'None', marker = '.', color = 'r', label = r'$\beta_{\rm{R},\phi z}$')
    ax.plot(t, -betaR[:,4], linestyle = 'None', marker = 'x', color = 'r')
    ax.plot(t, betaR[:,5], linestyle = 'None', marker = '.', color = 'g', label = r'$\beta_{\rm{R},zz}$')
    ax.plot(t, -betaR[:,5], linestyle = 'None', marker = 'x', color = 'g')
    
    plt.xlabel('Time [ms]', fontsize = '14')
    plt.ylabel(r'$\beta_{\rm{R},ij}$',fontsize = '14')
    plt.title('RESOLUTION : '+res)
    plt.yscale('log')
    ax.legend(loc = 'lower right', ncol = 2, frameon = 'True',prop={"size":11})
    #plt.savefig(plotsdir+'/betaR.pdf',bbox_inches='tight')
    plt.savefig(plotsdir+'/betaR.eps',bbox_inches='tight')
    
    fig, ax = plt.subplots()
    
    ax.plot(t, betaM[:,0], linestyle = 'None', marker = '.', color = 'm', label = r'$\beta_{\rm{M},rr}$')
    ax.plot(t, -betaM[:,0], linestyle = 'None', marker = 'x', color = 'm')
    ax.plot(t, betaM[:,1], linestyle = 'None', marker = '.', color = 'c', label = r'$\beta_{\rm{M},r\phi}$')
    ax.plot(t, -betaM[:,1], linestyle = 'None', marker = 'x', color = 'c')
    ax.plot(t, betaM[:,2], linestyle = 'None', marker = '.', color = 'y', label = r'$\beta_{\rm{M},rz}$')
    ax.plot(t, -betaM[:,2], linestyle = 'None', marker = 'x', color = 'y')
    ax.plot(t, betaM[:,3], linestyle = 'None', marker = '.', color = 'k', label = r'$\beta_{\rm{M},\phi\phi}$')
    ax.plot(t, -betaM[:,3], linestyle = 'None', marker = 'x', color = 'k')
    ax.plot(t, betaM[:,4], linestyle = 'None', marker = '.', color = 'r', label = r'$\beta_{\rm{M},\phi z}$')
    ax.plot(t, -betaM[:,4], linestyle = 'None', marker = 'x', color = 'r')
    ax.plot(t, betaM[:,5], linestyle = 'None', marker = '.', color = 'g', label = r'$\beta_{\rm{M},zz}$')
    ax.plot(t, -betaM[:,5], linestyle = 'None', marker = 'x', color = 'g')
    
    plt.xlabel('Time [ms]', fontsize = '14')
    plt.ylabel(r'$\beta_{\rm{M},ij}$',fontsize = '14')
    plt.title('RESOLUTION : '+res)
    plt.yscale('log')
    ax.legend(loc = 'lower right', ncol = 2, frameon = 'True',prop={"size":11})
    #plt.savefig(plotsdir+'/betaM.pdf',bbox_inches='tight')
    plt.savefig(plotsdir+'/betaM.eps',bbox_inches='tight')
    
    fig, ax = plt.subplots()
    
    ax.plot(t, gammaR[:,0], linestyle = 'None', marker = '.', color = 'm', label = r'$\gamma_{\rm{R},r\phi}$')
    ax.plot(t, -gammaR[:,0], linestyle = 'None', marker = 'x', color = 'm')
    ax.plot(t, gammaR[:,1], linestyle = 'None', marker = '.', color = 'c', label = r'$\gamma_{\rm{R},rz}$')
    ax.plot(t, -gammaR[:,1], linestyle = 'None', marker = 'x', color = 'c')
    ax.plot(t, gammaR[:,2], linestyle = 'None', marker = '.', color = 'y', label = r'$\gamma_{\rm{R},\phi z}$')
    ax.plot(t, -gammaR[:,2], linestyle = 'None', marker = 'x', color = 'y')
    
    plt.xlabel('Time [ms]', fontsize = '14')
    plt.ylabel(r'$\gamma_{\rm{R},ij}$',fontsize = '14')
    plt.title('RESOLUTION : '+res)
    plt.yscale('log')
    ax.legend(loc = 'lower right', ncol = 1, frameon = 'True',prop={"size":11})
    #plt.savefig(plotsdir+'/gammaR.pdf',bbox_inches='tight')
    plt.savefig(plotsdir+'/gammaR.eps',bbox_inches='tight')
    
    fig, ax = plt.subplots()
    
    ax.plot(t, gammaM[:,0], linestyle = 'None', marker = '.', color = 'm', label = r'$\gamma_{\rm{M},r\phi}$')
    ax.plot(t, -gammaM[:,0], linestyle = 'None', marker = 'x', color = 'm')
    ax.plot(t, gammaM[:,1], linestyle = 'None', marker = '.', color = 'c', label = r'$\gamma_{\rm{M},rz}$')
    ax.plot(t, -gammaM[:,1], linestyle = 'None', marker = 'x', color = 'c')
    ax.plot(t, gammaM[:,2], linestyle = 'None', marker = '.', color = 'y', label = r'$\gamma_{\rm{M},\phi z}$')
    ax.plot(t, -gammaM[:,2], linestyle = 'None', marker = 'x', color = 'y')
    
    plt.xlabel('Time [ms]', fontsize = '14')
    plt.ylabel(r'$\gamma_{\rm{M},ij}$',fontsize = '14')
    plt.title('RESOLUTION : '+res)
    plt.yscale('log')
    ax.legend(loc = 'lower right', ncol = 1, frameon = 'True',prop={"size":11})
   # plt.savefig(plotsdir+'/gammaM.pdf',bbox_inches='tight')
    plt.savefig(plotsdir+'/gammaM.eps',bbox_inches='tight')
    
    #merge into one file
    #pdf_mer = PdfFileMerger()
    #fig1 = PdfFileReader(plotsdir+'/alphaR.pdf','rb')
    #fig2 = PdfFileReader(plotsdir+'/alphaM.pdf','rb')
    #fig3 = PdfFileReader(plotsdir+'/betaR.pdf','rb')
    #fig4 = PdfFileReader(plotsdir+'/betaM.pdf','rb')
    #fig5 = PdfFileReader(plotsdir+'/gammaR.pdf','rb')
    #fig6 = PdfFileReader(plotsdir+'/gammaM.pdf','rb')

    #pdf_mer.append(fig1)
    #pdf_mer.append(fig2)
    #pdf_mer.append(fig3)
    #pdf_mer.append(fig4)
    #pdf_mer.append(fig5)
    #pdf_mer.append(fig6)


    #with Path(plotsdir+'/all_constants.pdf').open(mode='wb') as output_file:
     #   pdf_mer.write(output_file)
    
    return()
    
    
def plot_constants_bis(plotsdir,lent, tfinal, alphaR, alphaM,betaR, betaM, gammaR, gammaM,res):
    
    t = np.linspace(0,tfinal,lent)
    t = t[1:]
    alphaR = alphaR[1:,:,:]
    alphaM = alphaM[1:,:,:]
    betaR = betaR[1:,:,:]
    betaM = betaM[1:,:,:]
    gammaR = gammaR[1:,:,:]
    gammaM = gammaM[1:,:,:]
    
    
    fig, ax = plt.subplots()
    
    ax.plot(t, alphaR[:,0,0], marker = 'x', color = 'm', label = r'$\alpha_{\rm{R},rr}$')
    ax.plot(t, alphaR[:,0,1], marker = 'x', color = 'c', label = r'$\alpha_{\rm{R},r\phi}$')
    ax.plot(t, alphaR[:,0,2], marker = 'x', color = 'y', label = r'$\alpha_{\rm{R},rz}$')
    ax.plot(t, alphaR[:,1,1], marker = 'x', color = 'k', label = r'$\alpha_{\rm{R},\phi\phi}$')
    ax.plot(t, alphaR[:,1,2], marker = 'x', color = 'r', label = r'$\alpha_{\rm{R},\phi z}$')
    ax.plot(t, alphaR[:,2,2], marker = 'x', color = 'g', label = r'$\alpha_{\rm{R},zz}$')
    
    plt.xlabel('Time [ms]', fontsize = '14')
    plt.ylabel(r'$\alpha_{\rm{R},ij}$',fontsize = '14')
    plt.title('RESOLUTION : '+res)
    ax.legend(loc = 'lower right', ncol = 2, frameon = 'True',prop={"size":11})
    plt.savefig(plotsdir+'/alphaR.eps',bbox_inches='tight')
    
    fig, ax = plt.subplots()
    
    ax.plot(t, alphaM[:,0,0], marker = 'x', color = 'm', label = r'$\alpha_{\rm{M},rr}$')
    ax.plot(t, alphaM[:,0,1], marker = 'x', color = 'c', label = r'$\alpha_{\rm{M},r\phi}$')
    ax.plot(t, alphaM[:,0,2], marker = 'x', color = 'y', label = r'$\alpha_{\rm{M},rz}$')
    ax.plot(t, alphaM[:,1,1], marker = 'x', color = 'k', label = r'$\alpha_{\rm{M},\phi\phi}$')
    ax.plot(t, alphaM[:,1,2], marker = 'x', color = 'r', label = r'$\alpha_{\rm{M},\phi z}$')
    ax.plot(t, alphaM[:,2,2], marker = 'x', color = 'g', label = r'$\alpha_{\rm{M},zz}$')
    
    plt.xlabel('Time [ms]', fontsize = '14')
    plt.ylabel(r'$\alpha_{\rm{M},ij}$',fontsize = '14')
    plt.title('RESOLUTION : '+res)
    ax.legend(loc = 'lower right', ncol = 2, frameon = 'True',prop={"size":11})
    plt.savefig(plotsdir+'/alphaM.eps',bbox_inches='tight')
    
    fig, ax = plt.subplots()
    
    ax.plot(t, betaR[:,0,0], marker = 'x', color = 'm', label = r'$\beta_{\rm{R},rr}$')
    ax.plot(t, betaR[:,0,1], marker = 'x', color = 'c', label = r'$\beta_{\rm{R},r\phi}$')
    ax.plot(t, betaR[:,0,2], marker = 'x', color = 'y', label = r'$\beta_{\rm{R},rz}$')
    ax.plot(t, betaR[:,1,1], marker = 'x', color = 'k', label = r'$\beta_{\rm{R},\phi\phi}$')
    ax.plot(t, betaR[:,1,2], marker = 'x', color = 'r', label = r'$\beta_{\rm{R},\phi z}$')
    ax.plot(t, betaR[:,2,2], marker = 'x', color = 'g', label = r'$\beta_{\rm{R},zz}$')
    
    plt.xlabel('Time [ms]', fontsize = '14')
    plt.ylabel(r'$\beta_{\rm{R},ij}$',fontsize = '14')
    plt.title('RESOLUTION : '+res)
    ax.legend(loc = 'lower right', ncol = 2, frameon = 'True',prop={"size":11})
    plt.savefig(plotsdir+'/betaR.eps',bbox_inches='tight')
    
    fig, ax = plt.subplots()
    
    ax.plot(t, betaM[:,0,0], marker = 'x', color = 'm', label = r'$\beta_{\rm{M},rr}$')
    ax.plot(t, betaM[:,0,1], marker = 'x', color = 'c', label = r'$\beta_{\rm{M},r\phi}$')
    ax.plot(t, betaM[:,0,2], marker = 'x', color = 'y', label = r'$\beta_{\rm{M},rz}$')
    ax.plot(t, betaM[:,1,1], marker = 'x', color = 'k', label = r'$\beta_{\rm{M},\phi\phi}$')
    ax.plot(t, betaM[:,1,2], marker = 'x', color = 'r', label = r'$\beta_{\rm{M},\phi z}$')
    ax.plot(t, betaM[:,2,2], marker = 'x', color = 'g', label = r'$\beta_{\rm{M},zz}$')
    
    plt.xlabel('Time [ms]', fontsize = '14')
    plt.ylabel(r'$\beta_{\rm{M},ij}$',fontsize = '14')
    plt.title('RESOLUTION : '+res)
    ax.legend(loc = 'lower right', ncol = 2, frameon = 'True',prop={"size":11})
    plt.savefig(plotsdir+'/betaM.eps',bbox_inches='tight')
    
    fig, ax = plt.subplots()
    
    ax.plot(t, gammaR[:,0,1], marker = 'x', color = 'm', label = r'$\gamma_{\rm{R},r\phi}$')
    ax.plot(t, gammaR[:,0,2], marker = 'x', color = 'c', label = r'$\gamma_{\rm{R},rz}$')
    ax.plot(t, gammaR[:,1,2], marker = 'x', color = 'y', label = r'$\gamma_{\rm{R},\phi z}$')
    
    plt.xlabel('Time [ms]', fontsize = '14')
    plt.ylabel(r'$\gamma_{\rm{R},ij}$',fontsize = '14')
    plt.title('RESOLUTION : '+res)
    ax.legend(loc = 'lower right', ncol = 1, frameon = 'True',prop={"size":11})
    plt.savefig(plotsdir+'/gammaR.eps',bbox_inches='tight')
    
    fig, ax = plt.subplots()
    
    ax.plot(t, gammaM[:,0,1], marker = 'x', color = 'm', label = r'$\gamma_{\rm{M},r\phi}$')
    ax.plot(t, gammaM[:,0,2], marker = 'x', color = 'c', label = r'$\gamma_{\rm{M},rz}$')
    ax.plot(t, gammaM[:,1,2], marker = 'x', color = 'y', label = r'$\gamma_{\rm{M},\phi z}$')
    
    plt.xlabel('Time [ms]', fontsize = '14')
    plt.ylabel(r'$\gamma_{\rm{M},ij}$',fontsize = '14')
    plt.title('RESOLUTION : '+res)
    ax.legend(loc = 'lower right', ncol = 1, frameon = 'True',prop={"size":11})
    plt.savefig(plotsdir+'/gammaM.eps',bbox_inches='tight')
    
    return()
        
    
    
def main_mean():
    
    lent = [61,18,61,26]
    tfinal = [6,8.5,30,12.5]
    resu = ['100_100_34','100_400_100','60_240_60','200_800_200']
    
    
    
    for j in range(0,4):
        
        print('RESOLUTION : ',resu[j])
        
        plotsdir = '/scr/miquel/Projects/sub-grid/pablo-model/constant-case/'+resu[j]+'/plots'

        lentit = lent[j]
        
        F = np.zeros((lentit,9))
        M = np.zeros((lentit,6))
        R = np.zeros((lentit,6))
        
        path = str(os.getcwd())+'/stress_tensors/'+resu[j]+'/text_files/'
        
        F[:,0], F[:,1], F[:,2], F[:,3], F[:,4], F[:,5], F[:,6], F[:,7], F[:,8] = get_mean_stress(path+'stress_F.csv')
        M[:,0], M[:,1], M[:,2], M[:,3], M[:,4], M[:,5] = get_mean_stress_sym(path+'stress_M.csv') 
        R[:,0], R[:,1], R[:,2], R[:,3], R[:,4], R[:,5] = get_mean_stress_sym(path+'stress_R.csv')
        
        rhofile = str(os.getcwd())+'/rho_mean/'+resu[j]+'/rho_mean.txt'
        
        rho = np.loadtxt(rhofile)*7.42471382*10**(-29)
        
        energyR, energyM = energy_dens(R,M,rho)
        
        # Faraday tensor defined as : F_ij = v'_i b'_j -b'_i v'_j
        
        Fanti = np.array([F[:,1]-F[:,6],F[:,2]-F[:,7],F[:,8]-F[:,4]]) #[Frphi, Frz, Fphiz]
        
        #change units to geometric (vel is in m/s)
        
        M = M*(2.874214371*10**(-25)/((4*3.141592653589793)**(0.5)))**2
        R = R*1e-4*(3.33564095*10**(-11))**2
        Fanti = Fanti*1e-2*3.33564095*10**(-11)*2.874214371*10**(-25)/((4*3.141592653589793)**(0.5))
        
        # let's see how proportional the tensors are to the energy dens

        alphaR = np.zeros((lentit,6))
        alphaM = np.zeros((lentit,6))

        betaR = np.zeros((lentit,6))
        betaM = np.zeros((lentit,6))

        gammaR = np.zeros((lentit,3))
        gammaM = np.zeros((lentit,3))
        
        for i in range(0,lentit):

            alphaR[i,:] = M[i,:]/energyR[i]
            alphaM[i,:] = M[i,:]/energyM[i]
        
            betaR[i,:] = R[i,:]/energyR[i]
            betaM[i,:] = R[i,:]/energyM[i]
    
            gammaR[i,:] = Fanti[:,i]/energyR[i]
            gammaM[i,:] = Fanti[:,i]/energyM[i]
        
        #print('alpha_R coefficient: ')
        #print(alphaR)
        #print('alpha_M coefficient: ')
        #print(alphaM)
        #print('beta_R coefficient: ')
        #print(betaR)
        #print('beta_M coefficient: ')
        #print(betaM)
        #print('gamma_R coefficient: ')
        #print(gammaR)
        #print('gamma_M coefficient: ')
        #print(gammaM)
        
        plot_constants(plotsdir,lent[j], tfinal[j], alphaR, alphaM,betaR, betaM, gammaR, gammaM,resu[j])
        
    
        
    return()
    
    
def main():
    
    lent = [61,18,61,26]
    tfinal = [6,8.5,30,12.5]
    resu = ['100_100_34','100_400_100','60_240_60','200_800_200']
    direc = ['A100_100_34','A100_400_100_B_4.6e13_v_4.45d8_r_4.45d8_Lz_1.0_INTEL_stencil','A60_240_60_B_4.6e13_v_4.45d8_r_4.45d8_Lz_1.0_INTEL_stencil_flex','A200_800_200_B_4.6e13_v_4.45d8_r_4.45d8_Lz_1.0_INTEL_stencil']
    tyrion = True
        
    for j in range(0,4):
        
        print('RESOLUTION : ',resu[j])
        
        direcit = '/scr/miquel/Projects/sub-grid/MRI/Miquel/'+direc[j]+'/' 
        lentit = lent[j]
        plotsdir = '/scr/miquel/Projects/sub-grid/pablo-model/constant-case/'+resu[j]+'/plots'

        if j == 0:
            arx = '000'
            listvar = read_data(arx,direcit,tyrion)

        else :
            arx = '0000'
            listvar = readh5files(direcit+'mri-'+str(arx)+'.h5')

        r = listvar[1]
        lenr = len(r)
        phi = listvar[2]
        lenphi = len(phi)
        z = listvar[3]
        lenz = len(z)
        
        alphaRav = np.zeros((lentit,3,3))
        alphaMav = np.zeros((lentit,3,3))
        betaRav = alphaRav
        betaMav = alphaRav
        gammaRav = alphaRav
        gammaMav = alphaRav

        alphaR = np.zeros((lentit,3,3,lenr,lenphi,lenz))
        alphaM = alphaR
        betaR = alphaR
        betaM = alphaR
        gammaR = alphaR
        gammaM = alphaR
            
        for x in range(0,lentit):
            
            if j == 0:
                arx = format(10*x, "03")
                listvar=read_data(arx,direcit,tyrion)
            else :
                arx = format(25*x, "04")
                listvar = readh5files(direcit+'mri-'+str(arx)+'.h5')
            
            r = listvar[1]
            lenr = len(r)
            phi = listvar[2]
            lenphi = len(phi)
            z = listvar[3]
            lenz = len(z)
            
            b = np.array([listvar[4],listvar[5],listvar[6]])*2.874214371*10**(-25)/((4*3.141592653589793)**(0.5))
            v = np.array([listvar[7],listvar[8],listvar[9]])*3.33564095*10**(-11)
            bmean = np.array([averaging(b[0],r,phi,z),averaging(b[1],r,phi,z),averaging(b[2],r,phi,z)])
            vmean = np.array([averaging(v[0],r,phi,z),averaging(v[1],r,phi,z),averaging(v[2],r,phi,z)])
            
            rho = listvar[10]*7.42471382*10**(-29)
            
            
            M,R,F = stresses(bmean,vmean,b,v,lenr,lenphi,lenz)
            
            Fanti = np.zeros((3,3,lenr,lenphi,lenz))
        
            
            for l in range(0,3):
                for m in range(0,3):
                    Fanti[m,l] = F[l,m]-F[m,l]
                    
            energyR, energyM = energy_dens_bis(R,M,rho)       
            
            for l in range(0,lenr):
                for m in range(0,lenphi):
                    for n in range(0,lenz):
                        alphaR[x,:,:,l,m,n] = M[:,:,l,m,n]/energyR[l,m,n]
                        alphaM[x,:,:,l,m,n] = M[:,:,l,m,n]/energyM[l,m,n]
        
                        betaR[x,:,:,l,m,n] = R[:,:,l,m,n]/energyR[l,m,n]
                        betaM[x,:,:,l,m,n] = R[:,:,l,m,n]/energyM[l,m,n]
    
                        gammaR[x,:,:,l,m,n] = Fanti[:,:,l,m,n]/energyR[l,m,n]
                        gammaM[x,:,:,l,m,n] = Fanti[:,:,l,m,n]/energyM[l,m,n]
        
        #average of the ctts
                     
            for l in range(0,3):
                for m in range(0,3):
                    
                    alphaRav[x,l,m] = averaging(alphaR[x,l,m],r,phi,z)
                    alphaMav[x,l,m] = averaging(alphaM[x,l,m],r,phi,z)
                    betaRav[x,l,m] = averaging(betaR[x,l,m],r,phi,z)
                    betaMav[x,l,m] = averaging(betaM[x,l,m],r,phi,z)
                    gammaRav[x,l,m] = averaging(gammaR[x,l,m],r,phi,z)
                    gammaMav[x,l,m] = averaging(gammaM[x,l,m],r,phi,z)
            
        
        #plots
        
        plot_constants_bis(plotsdir,lentit, tfinal[j], alphaR, alphaM,betaR, betaM, gammaR, gammaM,resu[j])
        
        
#%%
        
main_mean()
    
        
        
        
        
