#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 28 18:05:04 2021

@author: miquelmiravet

PABLO'S MODEL TESTING
"""
#############################################

# Let's model the stress tensors Mij, Rij and Tij as functions of the total energy density.
# First approach: Consider that the tensors are proportional to the energy density. 

#############################################

import os 
import numpy as np
from scipy.io import FortranFile
import h5py
import csv

def readh5files(file):

    print('DATA FROM FILE:      ',file)

    with h5py.File(file, "r") as f:
    # List all groups
        print("Keys: %s" % f.keys())
        a_group_key = list(f.keys())
        for i in a_group_key:
            print(i)

        Pgas = f[a_group_key[0]][...]
        print('Pgas shape    :', Pgas.shape)
        bphi = f[a_group_key[1]][...]
        print('Bphi shape    :', bphi.shape)
        br = f[a_group_key[2]][...]
        print('Br shape    :', br.shape)
        bz = f[a_group_key[3]][...]
        print('Bz shape    :', bz.shape)
        gravpot = f[a_group_key[4]][...]
        print('gravpot shape    :', gravpot.shape)
        phi = f[a_group_key[5]][...]
        print('Phi shape    :', phi.shape)
        r = f[a_group_key[6]][...]
        print('r shape    :', r.shape)
        rho = f[a_group_key[7]][...]
        print('Rho shape    :', rho.shape)
        time = f[a_group_key[8]][...]
        print('Time shape    :', time.shape)
        vphi = f[a_group_key[9]][...]
        print('vphi shape    :', vphi.shape)
        vr = f[a_group_key[10]][...]
        print('vr shape    :', vr.shape)
        vz = f[a_group_key[11]][...]
        print('vz shape    :', vz.shape)
        z = f[a_group_key[12]][...]
        print('z shape    :', z.shape)
        #time = file.replace('.h5','')
        #time = float(time[-4:])/250
        
        
        f.close()

    print('Dimensions of the grid     :',len(r)," x ",len(phi)," x ",len(z))
    print('Time           :',time,' s')
    
    return [time, r, phi, z, br, bphi, bz, vr, vphi, vz, rho, Pgas, gravpot]

def read_data(arxiu,direc,tyrion):
    
    """
    This function reads the data from
    the files, by choosing the file we 
    are interested to study.
    -Arguments: 
        arxiu: number of the file.
        
    """
    
    print('DATA FROM FILE:      ',arxiu)
    if tyrion == True:
        str1 = direc+'/mriquel-0file.dat' 
    else : 
        str1 = 'A100_100_34/mriquel-0file.dat'
        
    ubi=str1.replace('file',arxiu)
    with FortranFile(ubi,'r','>u4') as f: 
        # read time
        time=f.read_reals(dtype='>f8')
        print ("time                     = ", time)
        # read dimensions of the grid
        nr = f.read_reals(dtype='>i4')[0]
        nphi = f.read_reals(dtype='>i4')[0]
        nz = f.read_reals(dtype='>i4')[0]
        print ("nr x nphi x nz           = ",nr," x ",nphi," x ",nz)
        # read grid arrays (1D)
        r = f.read_reals(dtype='>f8')
        phi = f.read_reals(dtype='>f8')
        z = f.read_reals(dtype='>f8')
        print ("len(r), len(phi), len(z) = ",len(r), len(phi), len(z))

        # read variables on the grid (3D arrays)
        Br = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("Br                       : ",Br.shape)
        Bphi = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("Bphi                     : ",Bphi.shape)
        Bz = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("Bz                       : ",Bz.shape)
        vr = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("vr                       : ",vr.shape)
        vphi = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("vphi                     : ",vphi.shape)
        vz = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("vz                       : ",vz.shape)
        rho = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("rho                      : ",rho.shape)
        Pgas = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("Pgas                     : ",Pgas.shape)
        phi2 = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("phi                      : ",phi2.shape)
         
        
    return [time, r, phi, z, Br, Bphi, Bz, vr, vphi, vz, rho, Pgas, phi2]     

def get_mean_stresses(path,filename,m,l,n,lent):
    
    f2 = filename+'.csv'
    if filename == path+'stress_F': 
        var = np.loadtxt(f2, usecols=(1,2,3))
        sigmavar = np.loadtxt(f2, usecols=(4,5,6))
        var = np.swapaxes(var,0,1)
        sigmavar = np.swapaxes(sigmavar,0,1)
        
        var = var.reshape(3,m,l,n,lent)
        sigmavar = sigmavar.reshape(3,m,l,n,lent)
        
        
    else:
        var = np.loadtxt(f2, usecols=(1,2,3,4,5,6))
        sigmavar = np.loadtxt(f2, usecols=(7,8,9,10,11,12))
        
        var = np.swapaxes(var,0,1)
        sigmavar = np.swapaxes(sigmavar,0,1)
        
        var = var.reshape(6,m,l,n,lent)
        sigmavar = sigmavar.reshape(6,m,l,n,lent)
        
    
    return var, sigmavar

def get_rho(filename,m,l,n,lent):
    
    file = filename+'.csv'
    
    var = np.loadtxt(file,usecols=1)
    sigmavar = np.loadtxt(file,usecols=2)
    
    var = var.reshape(m,l,n,lent)
    sigmavar = sigmavar.reshape(m,l,n,lent)
    
    return var, sigmavar

        
def energy_dens(Rtensor,Rrhotensor,Mtensor,rho):
    traceR = Rtensor[0]+Rtensor[3]+Rtensor[5]
    traceRrho = Rrhotensor[0]+Rrhotensor[3]+Rrhotensor[5]
    traceM = Mtensor[0]+Mtensor[3]+Mtensor[5]
    energyR = 0.5*rho*traceR
    energyRrho = 0.5*traceRrho
    energyM = 0.5*traceM
    return energyR,energyRrho,energyM

def error_propagation(rho,err_rho,M,err_M,R,err_R,Rrho,err_Rrho,F,err_F,alphaR,alphaRrho,betaR,betaRrho,gammaR,gammaRrho,alphaM,betaM,betaMrho,gammaM):
    
    traceR = R[0]+R[3]+R[5]
    traceRrho = Rrho[0]+Rrho[3]+Rrho[5]
    traceM = M[0]+M[3]+M[5]
    
    e_alphaR = (err_M/(0.5*rho*traceR)**2+(M/(0.5*traceR*rho**2))**2*err_rho+(M/(0.5*rho*traceR**2))**2*(err_R[0]+err_R[3]+err_R[5]))**(0.5)
    e_alphaRrho = (err_M/(0.5*traceRrho)**2+(M/(0.5*traceRrho**2))**2*(err_Rrho[0]+err_Rrho[3]+err_Rrho[5]))**(0.5)
    e_alphaM = (err_M/(0.5*traceM)**2+(M/(0.5*traceM**2))**2*(err_M[0]+err_M[3]+err_M[5]))**(0.5)
    e_betaR = (err_R/(0.5*traceR)**2+(R/(0.5*traceR**2))**2*(err_R[0]+err_R[3]+err_R[5]))**(0.5)
    e_betaRrho = (err_Rrho/(0.5*traceRrho)**2+(Rrho/(0.5*traceRrho**2))**2*(err_Rrho[0]+err_Rrho[3]+err_Rrho[5]))**(0.5)
    e_betaM = (err_rho*(R/(0.5*traceM))**2+err_R*(rho/(0.5*traceM))**2+(rho*R/(0.5*traceM**2))**2*(err_M[0]+err_M[3]+err_M[5]))**(0.5)
    e_betaMrho = (err_Rrho/(0.5*traceM)**2+(Rrho/(0.5*traceM**2))**2*(err_M[0]+err_M[3]+err_M[5])**2)**(0.5)
    e_gammaR = (err_F/(0.5*rho**(0.5)*traceR)**2+(F/(0.25*rho**(-1.5)*traceR))**2*err_rho+(F/(0.5*rho**(0.5)*traceR**2))**2*(err_R[0]+err_R[3]+err_R[5]))**(0.5)
    e_gammaRrho = (rho*err_F/(0.5*traceRrho)**2+(F/(rho**(0.5)*traceRrho))**2*err_rho+rho*(F/(0.5*traceRrho**2))**2*(err_Rrho[0]+err_Rrho[3]+err_Rrho[5]))**(0.5)
    e_gammaM = (rho*err_F/(0.5*traceM)**2+(F/(rho**(0.5)*traceM))**2*err_rho+rho*(F/(0.5*traceM**2))**2*(err_M[0]+err_M[3]+err_M[5]))**(0.5)
    
    return e_alphaR, e_alphaRrho, e_alphaM, e_betaR, e_betaRrho, e_betaM, e_betaMrho, e_gammaR, e_gammaRrho, e_gammaM
    
    
def constants_files_6comp(m,l,n,lent,constant,econstant,arxiu):
    
    with open(arxiu+'.csv', 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        for a in range(0,m):
            for b in range(0,l):
                for c in range(0,n):
                    if lent == 61:
                        col = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59','60']
                    elif lent == 26:
                        col = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25']
                    elif lent == 18:
                        col = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17']
                    
                    cols = zip(col,constant[0,a,b,c,:],constant[1,a,b,c,:],constant[2,a,b,c,:],constant[3,a,b,c,:],constant[4,a,b,c,:],constant[5,a,b,c,:],econstant[0,a,b,c,:],econstant[1,a,b,c,:],econstant[2,a,b,c,:],econstant[3,a,b,c,:],econstant[4,a,b,c,:],econstant[5,a,b,c,:])
                    writer.writerows(cols)
    return()
    
def constants_files_3comp(m,l,n,lent,constant,econstant,arxiu):
    
    with open(arxiu+'.csv', 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        for a in range(0,m):
            for b in range(0,l):
                for c in range(0,n):
                    if lent == 61:
                        col = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59','60']
                    elif lent == 26:
                        col = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25']
                    elif lent == 18:
                        col = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17']
                    
                    cols = zip(col,constant[0,a,b,c,:],constant[1,a,b,c,:],constant[2,a,b,c,:],econstant[0,a,b,c,:],econstant[1,a,b,c,:],econstant[2,a,b,c,:])
                    writer.writerows(cols)
    return()
    
#%% 
                 
def main():
    
    lent = [61,18,61,26]
    resu = ['100_100_34','100_400_100','60_240_60','200_800_200']
    m = 4
    l = 1
    n = 1

    for j in range(0,4):
        
        print('RESOLUTION : ',resu[j])
        

        lentit = lent[j]
        
        F = np.zeros((lentit,3))
        M = np.zeros((lentit,6))
        R = np.zeros((lentit,6))
        Rrho = np.zeros((lentit,6))
        sigmaF = np.zeros((lentit,3))
        sigmaM = np.zeros((lentit,6))
        sigmaR = np.zeros((lentit,6))
        sigmaRrho = np.zeros((lentit,6))
        
        path = str(os.getcwd())+'/stress_tensors/'+resu[j]+'/new-version/text_files/'
        
        F, sigmaF = get_mean_stresses(path,path+'stress_F',m,l,n,lentit)
        M, sigmaM = get_mean_stresses(path,path+'stress_M',m,l,n,lentit) 
        R, sigmaR = get_mean_stresses(path,path+'stress_R',m,l,n,lentit)
        Rrho, sigmaRrho = get_mean_stresses(path,path+'stress_Rrho',m,l,n,lentit)
        
        rhofile = str(os.getcwd())+'/rho_mean/'+resu[j]+'/new-version/rho_mean'
        
        rho, sigmarho = get_rho(rhofile,m,l,n,lentit)
        
        energyR, energyRrho, energyM = energy_dens(R,Rrho,M,rho)
        
        #constants:

        alphaRrho = M[:]/energyRrho
        alphaR = M[:]/energyR
        alphaM = M[:]/energyM
        
        betaRrho = Rrho[:]/energyRrho
        betaR = rho*R[:]/energyR
        betaM = rho*R[:]/energyM
        betaMrho = Rrho[:]/energyM
    
        gammaR = (rho)**(0.5)*F[:]/energyR
        gammaRrho = (rho)**(0.5)*F[:]/energyRrho
        gammaM = (rho)**(0.5)*F[:]/energyM
        
        #errors:
        
        e_alphaR, e_alphaRrho, e_alphaM, e_betaR, e_betaRrho, e_betaM, e_betaMrho, e_gammaR, e_gammaRrho, e_gammaM = error_propagation(rho,sigmarho,M,sigmaM,R,sigmaR,Rrho,sigmaRrho,F,sigmaF,alphaR,alphaRrho,betaR,betaRrho,gammaR,gammaRrho,alphaM,betaM,betaMrho,gammaM)
        
        #files:
        
        constants_files_6comp(m,l,n,lentit,alphaR,e_alphaR,'alphaR')
        constants_files_6comp(m,l,n,lentit,alphaRrho,e_alphaRrho,'alphaRrho')
        constants_files_6comp(m,l,n,lentit,alphaM,e_alphaM,'alphaM')
        constants_files_6comp(m,l,n,lentit,betaR,e_betaR,'betaR')
        constants_files_6comp(m,l,n,lentit,betaRrho,e_betaRrho,'betaRrho')
        constants_files_6comp(m,l,n,lentit,betaM,e_betaM,'betaM')
        constants_files_6comp(m,l,n,lentit,betaMrho,e_betaMrho,'betaMrho')
        constants_files_3comp(m,l,n,lentit,gammaR,e_gammaR,'gammaR')
        constants_files_3comp(m,l,n,lentit,gammaRrho,e_gammaRrho,'gammaRrho')
        constants_files_3comp(m,l,n,lentit,gammaM,e_gammaM,'gammaM')
        
        filesdir = '/scr/miquel/Projects/sub-grid/pablo-model/constant-case/'+resu[j]+'/new-files'
        
        try:
            os.makedirs(filesdir)
        except OSError as error:
            print(error)
    
        os.system('mv *.csv '+filesdir+'/.')
        
    return()
    
    

        
#%%
        
main()
    
        
        
        
        
