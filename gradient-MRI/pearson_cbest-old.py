#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  1 16:26:50 2020

@author: miquelmiravet
"""

"""

Now we can compute the Pearson correlation coefficient as done in the paper to 
check the correlation between the SFS and SGS tensors. We can do it for each component.

We also calculate de C best fit parameter. 

"""

import os 
import numpy as np
from scipy.stats import pearsonr
import h5py
import csv 
from scipy.io import FortranFile

def readh5files(file):

    print('DATA FROM FILE:      ',file)

    with h5py.File(file, "r") as f:
    # List all groups
        print("Keys: %s" % f.keys())
        a_group_key = list(f.keys())
        for i in a_group_key:
            print(i)

        Pgas = f[a_group_key[0]][...]
        print('Pgas shape    :', Pgas.shape)
        bphi = f[a_group_key[1]][...]
        print('Bphi shape    :', bphi.shape)
        br = f[a_group_key[2]][...]
        print('Br shape    :', br.shape)
        bz = f[a_group_key[3]][...]
        print('Bz shape    :', bz.shape)
        gravpot = f[a_group_key[4]][...]
        print('gravpot shape    :', gravpot.shape)
        phi = f[a_group_key[5]][...]
        print('Phi shape    :', phi.shape)
        r = f[a_group_key[6]][...]
        print('r shape    :', r.shape)
        rho = f[a_group_key[7]][...]
        print('Rho shape    :', rho.shape)
        time = f[a_group_key[8]][...]
        print('Time shape    :', time.shape)
        vphi = f[a_group_key[9]][...]
        print('vphi shape    :', vphi.shape)
        vr = f[a_group_key[10]][...]
        print('vr shape    :', vr.shape)
        vz = f[a_group_key[11]][...]
        print('vz shape    :', vz.shape)
        z = f[a_group_key[12]][...]
        print('z shape    :', z.shape)
        #time = file.replace('.h5','')
        #time = float(time[-4:])/250
        
        
        f.close()

    print('Dimensions of the grid     :',len(r)," x ",len(phi)," x ",len(z))
    print('Time           :',time,' s')
    
    return [time, r, phi, z, br, bphi, bz, vr, vphi, vz, rho, Pgas, gravpot]

def read_data(arxiu):
    
    """
    This function reads the data from
    the files, by choosing the file we 
    are interested to study.
    -Arguments: 
        arxiu: number of the file.
        
    """
    
    print('DATA FROM FILE:      ',arxiu)
    str1 = "A100_100_34/mriquel-0file.dat" 
    ubi=str1.replace('file',arxiu)
    with FortranFile(ubi,'r','>u4') as f: 
        # read time
        time=f.read_reals(dtype='>f8')
        print ("time                     = ", time)
        # read dimensions of the grid
        nr = f.read_reals(dtype='>i4')[0]
        nphi = f.read_reals(dtype='>i4')[0]
        nz = f.read_reals(dtype='>i4')[0]
        print ("nr x nphi x nz           = ",nr," x ",nphi," x ",nz)
        # read grid arrays (1D)
        r = f.read_reals(dtype='>f8')
        phi = f.read_reals(dtype='>f8')
        z = f.read_reals(dtype='>f8')
        print ("len(r), len(phi), len(z) = ",len(r), len(phi), len(z))

        # read variables on the grid (3D arrays)
        Br = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("Br                       : ",Br.shape)
        Bphi = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("Bphi                     : ",Bphi.shape)
        Bz = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("Bz                       : ",Bz.shape)
        vr = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("vr                       : ",vr.shape)
        vphi = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("vphi                     : ",vphi.shape)
        vz = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("vz                       : ",vz.shape)
        rho = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("rho                      : ",rho.shape)
        Pgas = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("Pgas                     : ",Pgas.shape)
        phi2 = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("phi                      : ",phi2.shape)
         
        
    return [time, r, phi, z, Br, Bphi, Bz, vr, vphi, vz, rho, Pgas, phi2]
         

def read_file(arx,path,m,l,n,lent):
        
    with open(path+'/'+arx+'.csv', 'r') as inp, open(path+'/'+arx+'.txt', 'w') as out:
        for line in inp:
            line = line.replace(',', ':')
            out.write(line)
        
    with open(path+'/'+arx+'.txt','r') as f:
        f2 = f.readlines()
        f.close()
     
    if arx == 'tauTteo' or arx == 'tauTgrad':   
        var = np.loadtxt(f2, usecols=(1,2,3,4,5,6))
        
        var = np.swapaxes(var,0,1)
        
        var = var.reshape(6,m,l,n,lent)
        
        out = np.zeros((3,3,m,l,n,lent))
        out[0,0,:,:,:,:] = var[0,:,:,:,:]
        out[0,1,:,:,:,:] = var[1,:,:,:,:]
        out[0,2,:,:,:,:] = var[2,:,:,:,:]
        out[1,1,:,:,:,:] = var[3,:,:,:,:]
        out[1,2,:,:,:,:] = var[4,:,:,:,:]
        out[2,2,:,:,:,:] = var[5,:,:,:,:]
        out[1,0,:,:,:,:] = var[1,:,:,:,:]
        out[2,0,:,:,:,:] = var[2,:,:,:,:]
        out[2,1,:,:,:,:] = var[4,:,:,:,:]
        
    elif arx == 'tauSteo' or arx == 'tauSgrad':
        var = np.loadtxt(f2, usecols=(1,2,3))
        
        var = np.swapaxes(var,0,1)
        
        var = var.reshape(3,m,l,n,lent)
        
        out = var
    
    elif arx == 'tauMteo' or arx == 'tauMgrad': 
        var = np.loadtxt(f2, usecols=(1,2,3))
        
        var = np.swapaxes(var,0,1)
        
        var = var.reshape(3,m,l,n,lent)
        
        out = np.zeros((3,3,m,l,n,lent))
        out[0,1,:,:,:,:] = var[0,:,:,:,:]
        out[0,2,:,:,:,:] = var[1,:,:,:,:]
        out[1,2,:,:,:,:] = var[2,:,:,:,:]
        out[1,0,:,:,:,:] = -var[0,:,:,:,:]
        out[2,0,:,:,:,:] = -var[1,:,:,:,:]
        out[2,1,:,:,:,:] = -var[2,:,:,:,:]
        
    return out

def splitting(arr,n):
    """
    Function that splits the array in order to 
    have a number of components proportional to n
    """
    frac = len(arr)/n
    if frac.is_integer() == False: 
        while True: 
            arr = np.delete(arr,len(arr)-1)
            frac_new = len(arr)/n
            if frac_new.is_integer() == True:
                break
    return arr

def averaging(var,n,m,l,r,phi,z): 
    """
    This function averages over the whole box.  
    """
    
    #reshaping of the coordinate arrays into m/l/n subarrays of equal length
    
    #print('lenr :',r[-1]-r[0])
    #print('lenz :',z[-1]-z[0])
    r = splitting(r,m)
    r = np.array(r)
    phi = splitting(phi,l)
    phi = np.array(phi)
    z = splitting(z,n)
    z = np.array(z)
    
    r_split = r.reshape(m,int(len(r)/m))
    #print(r_split)
    phi_split = phi.reshape(l,int(len(phi)/l))
    z_split = z.reshape(n,int(len(z)/n))
    r_subbox = np.zeros(m)
    for i in range(0,m):
        r_subbox[i] = (r_split[i,-1]+r_split[i,0])/2
    

    #AVERAGING PROCEDURE
    
    #differentials
    dr = r_split[1,0]-r_split[0,0]
    dphi = phi_split[1,0]-phi_split[0,0]
    dz = z_split[1,0]-z_split[0,0]
    
    
    #construction of the matrix that represents the integral differentials. It will be different for each sub-box
    dVmatrix = np.ones((m,l,n))
    dVmatrixdef = np.zeros((m,l,n))   
    
    dV = r_subbox[:]*dr*dphi*dz
    for x in range(0,m):
        dVmatrixdef[x,:,:] = dV[x]*dVmatrix[x,:,:]
    
    #volume                
    V = np.sum(dVmatrixdef[:,:,:])
    
    #print('V :', V)
    #print('V teo : ', 100000*400000*100000)
    
    #averaging
    varmean = np.sum(dVmatrixdef[:,:,:]*var[:,:,:])/V 
    
    return varmean

def pearson(Sf,lenr,lenphi,lenz,m,l,n,lent,tau_T_theo,tau_T_grad,tau_S_theo,tau_S_grad,tau_M_theo,tau_M_grad,r,phi,z,t_turb,t_final):
 
    corr_tauT = np.zeros((lent,3,3))
    corr_tauS = np.zeros((lent,3))
    corr_tauM = np.zeros((lent,3,3))
    
    tau_S_theo_corr = np.zeros((lent,m*l*n,3))
    tau_S_grad_corr = np.zeros((lent,m*l*n,3))        
    tau_T_theo_corr = np.zeros((lent,m*l*n,3,3))
    tau_T_grad_corr = np.zeros((lent,m*l*n,3,3))
    tau_M_theo_corr = np.zeros((lent,m*l*n,3,3))
    tau_M_grad_corr = np.zeros((lent,m*l*n,3,3))
     
    
    for x in range(0,lent):
        for i in range(0,3):
            tau_S_theo_corr[x,:,i] = tau_S_theo[i,:,:,:,x].reshape(m*l*n)
            tau_S_grad_corr[x,:,i] = tau_S_grad[i,:,:,:,x].reshape(m*l*n)

            for j in range(0,3):
                tau_T_theo_corr[x,:,i,j] = tau_T_theo[i,j,:,:,:,x].reshape(m*l*n)
                tau_T_grad_corr[x,:,i,j] = tau_T_grad[i,j,:,:,:,x].reshape(m*l*n)
                tau_M_theo_corr[x,:,i,j] = tau_M_theo[i,j,:,:,:,x].reshape(m*l*n)
                tau_M_grad_corr[x,:,i,j] = tau_M_grad[i,j,:,:,:,x].reshape(m*l*n)

                
                

    corr_tauT_av = np.zeros(lent)
    corr_tauS_av = np.zeros(lent)
    corr_tauM_av = np.zeros(lent)

    for x in range(0,lent):
        for i in range(0,3):
            corr_tauS[x,i], ps = pearsonr(tau_S_theo_corr[x,:,i],tau_S_grad_corr[x,:,i])
            for j in range(0,3):
                corr_tauT[x,i,j], pt = pearsonr(tau_T_theo_corr[x,:,i,j],tau_T_grad_corr[x,:,i,j])
                corr_tauM[x,i,j], pm = pearsonr(tau_M_theo_corr[x,:,i,j],tau_M_grad_corr[x,:,i,j])
                
        corr_tauT_av[x] = (corr_tauT[x,0,0]+corr_tauT[x,1,1]+corr_tauT[x,2,2]+corr_tauT[x,0,1]+corr_tauT[x,0,2]+corr_tauT[x,1,2])/6
        corr_tauM_av[x] = (corr_tauM[x,0,1]+corr_tauM[x,0,2]+corr_tauM[x,1,2])/3
        corr_tauS_av[x] = np.sum(corr_tauS[x,:])/3
 
    #let's divide now between turbulent regime and the growth of the turbulence
    
    t = np.linspace(0,t_final*10,lent)
    print(t_final)
    index = np.where(t == t_turb*10)
    print('INDEX : ', index[0])
    index = int(index[0])
    
    tau_T_theo_turb = np.zeros((lent-index,m*l*n,3,3))
    tau_T_grad_turb = np.zeros((lent-index,m*l*n,3,3))
    tau_S_theo_turb = np.zeros((lent-index,m*l*n,3))
    tau_S_grad_turb = np.zeros((lent-index,m*l*n,3))
    tau_M_theo_turb = np.zeros((lent-index,m*l*n,3,3))
    tau_M_grad_turb = np.zeros((lent-index,m*l*n,3,3))
    
    tau_T_theo_nonturb = np.zeros((index,m*l*n,3,3))
    tau_T_grad_nonturb = np.zeros((index,m*l*n,3,3))
    tau_S_theo_nonturb = np.zeros((index,m*l*n,3))
    tau_S_grad_nonturb = np.zeros((index,m*l*n,3))
    tau_M_theo_nonturb = np.zeros((index,m*l*n,3,3))
    tau_M_grad_nonturb = np.zeros((index,m*l*n,3,3))
    
    for x in range(0,lent-index):
        tau_S_theo_turb[x,:,:] = tau_S_theo_corr[x+index,:,:]
        tau_S_grad_turb[x,:,:] = tau_S_grad_corr[x+index,:,:]
        tau_T_theo_turb[x,:,:,:] = tau_T_theo_corr[x+index,:,:,:]
        tau_T_grad_turb[x,:,:,:] = tau_T_grad_corr[x+index,:,:,:]
        tau_M_theo_turb[x,:,:,:] = tau_M_theo_corr[x+index,:,:,:]
        tau_M_grad_turb[x,:,:,:] = tau_M_grad_corr[x+index,:,:,:]
                
    for x in range(0,index):
        tau_S_theo_nonturb[x,:,:] = tau_S_theo_corr[x,:,:]
        tau_S_grad_nonturb[x,:,:] = tau_S_grad_corr[x,:,:]
        tau_T_theo_nonturb[x,:,:,:] = tau_T_theo_corr[x,:,:,:]
        tau_T_grad_nonturb[x,:,:,:] = tau_T_grad_corr[x,:,:,:]
        tau_M_theo_nonturb[x,:,:,:] = tau_M_theo_corr[x,:,:,:]
        tau_M_grad_nonturb[x,:,:,:] = tau_M_grad_corr[x,:,:,:]


    corr_tauT3 = np.zeros((3,3))
    corr_tauS3 = np.zeros(3)
    corr_tauM3 = np.zeros((3,3))
    
    tau_S_theo_turb = tau_S_theo_turb.reshape((lent-index)*m*l*n,3)    
    tau_S_grad_turb = tau_S_grad_turb.reshape((lent-index)*m*l*n,3) 
    tau_T_theo_turb = tau_T_theo_turb.reshape((lent-index)*m*l*n,3,3) 
    tau_T_grad_turb = tau_T_grad_turb.reshape((lent-index)*m*l*n,3,3) 
    tau_M_theo_turb = tau_M_theo_turb.reshape((lent-index)*m*l*n,3,3) 
    tau_M_grad_turb = tau_M_grad_turb.reshape((lent-index)*m*l*n,3,3)
    
    tau_S_theo_nonturb = tau_S_theo_nonturb.reshape((index)*m*l*n,3)    
    tau_S_grad_nonturb = tau_S_grad_nonturb.reshape((index)*m*l*n,3) 
    tau_T_theo_nonturb = tau_T_theo_nonturb.reshape((index)*m*l*n,3,3) 
    tau_T_grad_nonturb = tau_T_grad_nonturb.reshape((index)*m*l*n,3,3) 
    tau_M_theo_nonturb = tau_M_theo_nonturb.reshape((index)*m*l*n,3,3) 
    tau_M_grad_nonturb = tau_M_grad_nonturb.reshape((index)*m*l*n,3,3)
                                               
    for i in range(0,3): 
        corr_tauS3[i], ps3 = pearsonr(tau_S_theo_turb[:,i],tau_S_grad_turb[:,i])
        for j in range(0,3):
            corr_tauT3[i,j], pt3 = pearsonr(tau_T_theo_turb[:,i,j],tau_T_grad_turb[:,i,j])
            corr_tauM3[i,j], pm3 = pearsonr(tau_M_theo_turb[:,i,j],tau_M_grad_turb[:,i,j])
            
    
    corr_tauT_av3 = (corr_tauT3[0,0]+corr_tauT3[1,1]+corr_tauT3[2,2]+corr_tauT3[0,1]+corr_tauT3[0,2]+corr_tauT3[1,2])/6
    corr_tauM_av3 = (corr_tauM3[0,1]+corr_tauM3[0,2]+corr_tauM3[1,2])/3
    corr_tauS_av3 = np.sum(corr_tauS3[:])/3
    
    
    corr_tauT4 = np.zeros((3,3))
    corr_tauS4 = np.zeros(3)
    corr_tauM4 = np.zeros((3,3))
    
        
    for i in range(0,3): 
        corr_tauS4[i], ps4 = pearsonr(tau_S_theo_nonturb[:,i],tau_S_grad_nonturb[:,i])
        for j in range(0,3):
            corr_tauT4[i,j], pt4 = pearsonr(tau_T_theo_nonturb[:,i,j],tau_T_grad_nonturb[:,i,j])
            corr_tauM4[i,j], pm4 = pearsonr(tau_M_theo_nonturb[:,i,j],tau_M_grad_nonturb[:,i,j])
            
    
    corr_tauT_av4 = (corr_tauT4[0,0]+corr_tauT4[1,1]+corr_tauT4[2,2]+corr_tauT4[0,1]+corr_tauT4[0,2]+corr_tauT4[1,2])/6
    corr_tauM_av4 = (corr_tauM4[0,1]+corr_tauM4[0,2]+corr_tauM4[1,2])/3
    corr_tauS_av4 = np.sum(corr_tauS4[:])/3

    return corr_tauT_av, corr_tauS_av, corr_tauM_av, corr_tauT_av3, corr_tauS_av3, corr_tauM_av3, corr_tauT_av4, corr_tauS_av4, corr_tauM_av4

#write files
def pearson_file(corrs,Sf,lenr,lenphi,lenz,lent):
    
    corr_tauT_av = corrs[0]
    corr_tauS_av = corrs[1]
    corr_tauM_av = corrs[2]
    corr_tauT_av3 = corrs[3]
    corr_tauS_av3 = corrs[4]
    corr_tauM_av3 = corrs[5]
    corr_tauT_av4 = corrs[6]
    corr_tauS_av4 = corrs[7]
    corr_tauM_av4 = corrs[8]

    
    
    with open('pearson.txt', 'w') as f:
        for t in range(0,lent):
            f.write('%f\t%f\t%f\n'%(corr_tauT_av[t],corr_tauS_av[t],corr_tauM_av[t]))
            
            
    
    
    with open('pearson_split.txt','w') as f3:
        f3.write('%f\t%f\t%f\n'%(corr_tauT_av3,corr_tauS_av3,corr_tauM_av3))
        f3.write('%f\t%f\t%f\n'%(corr_tauT_av4,corr_tauS_av4,corr_tauM_av4))
        
    
    
    return()
    
def cbest(Sf,lenr,lenphi,lenz,lent,tau_T_theo,tau_T_grad,tau_S_theo,tau_S_grad,tau_M_theo,tau_M_grad):
    
    c_tauT = np.zeros((lent,3,3))
    c_tauS = np.zeros((lent,3))
    c_tauM = np.zeros((lent,3,3))
    c_tauT_av = np.zeros(lent)
    c_tauM_av = np.zeros(lent)
    c_tauS_av = np.zeros(lent)

    for x in range(0,lent):
        for i in range(0,3):
            c_tauS[x,i] = np.sum(tau_S_theo[i,:,:,:,x]*tau_S_grad[i,:,:,:,x])/np.sum(tau_S_grad[i,:,:,:,x]**2)
            for j in range(0,3):
                c_tauT[x,i,j] = np.sum(tau_T_theo[i,j,:,:,:,x]*tau_T_grad[i,j,:,:,:,x])/np.sum(tau_T_grad[i,j,:,:,:,x]**2)
                c_tauM[x,i,j] = np.sum(tau_M_theo[i,j,:,:,:,x]*tau_M_grad[i,j,:,:,:,x])/np.sum(tau_M_grad[i,j,:,:,:,x]**2)
    
        c_tauT_av[x] = (c_tauT[x,0,0]+c_tauT[x,1,1]+c_tauT[x,2,2]+c_tauT[x,0,1]+c_tauT[x,0,2]+c_tauT[x,1,2])/6
        c_tauM_av[x] = (c_tauM[x,0,2]+c_tauM[x,0,1]+c_tauM[x,1,2])/3
        c_tauS_av[x] = np.sum(c_tauS[x,:])/3

    return c_tauT_av, c_tauS_av, c_tauM_av

def cbest_files(cparams,Sf,lenr,lenphi,lenz,lent):
     
    c_tauT_av = cparams[0]
    c_tauS_av = cparams[1]
    c_tauM_av = cparams[2]
    
    with open('c_best.txt', 'w') as f:
        for t in range(0,lent):
            f.write('%f\t%f\t%f\n'%(c_tauT_av[t],c_tauS_av[t],c_tauM_av[t]))
            
      
    
    return()
 
#%%
def main():
    Sf = 2
    lenr = 200
    lenphi = 800
    lenz = 200
    
    pathdata = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/'

    if lenphi == 800 :
        
        t_turb = 11
        lent = 26
        t_final = 12.5
        
    elif lenphi == 100 :
        
        t_turb = 4.8
        lent = 61
        t_final = 6
        
    elif lenphi == 240 : 
        
        t_turb = 11.5
        lent = 61
        t_final = 30
        
    elif lenphi == 400 :
        
        t_turb = 8.5
        lent = 17
        t_final = 8.5

    m = int(lenr/Sf)
    l = int(lenphi/Sf)
    n = int(lenz/Sf)
    
    if lenphi == 400 :
        
        direc = pathdata+'DATA/MRI/A100_400_100_B_4.6e13_v_4.45d8_r_4.45d8_Lz_1.0_INTEL_stencil/'
    
    elif lenphi == 800 : 
        
        direc = pathdata+'DATA/MRI/A200_800_200_B_4.6e13_v_4.45d8_r_4.45d8_Lz_1.0_INTEL_stencil/'
    
    elif lenphi == 240 :
        
        direc = pathdata+'DATA/MRI/A60_240_60_B_4.6e13_v_4.45d8_r_4.45d8_Lz_1.0_INTEL_stencil_flex/'
    
    elif lenphi == 100 :
        
        direc = pathdata+'DATA/MRI/A100_100_34/'
    
    
    entries = os.listdir(direc)
    print(entries)
    entries = sorted(entries)
    print(entries)

    if lenphi == 100 :
        var = read_data('000')
        
    else :
        var = readh5files(direc+'mri-0000.h5')
    
    
    r = var[1]
    phi = var[2]
    z = var[3]

    pathtensors = 'RESULTS/Sf_'+str(Sf)+'/'+str(lenr)+'_'+str(lenphi)+'_'+str(lenz)+'/sfs_tensors/text_files'

    pathfile = pathtensors
    tau_T_theo = read_file('tauTteo',pathfile,m,l,n,lent)
    tau_S_theo = read_file('tauSteo',pathfile,m,l,n,lent)
    tau_M_theo = read_file('tauMteo',pathfile,m,l,n,lent)
    tau_T_grad = read_file('tauTgrad',pathfile,m,l,n,lent)
    tau_S_grad = read_file('tauSgrad',pathfile,m,l,n,lent)
    tau_M_grad = read_file('tauMgrad',pathfile,m,l,n,lent)
    
    corrs = pearson(Sf,lenr,lenphi,lenz,m,l,n,lent,tau_T_theo,tau_T_grad,tau_S_theo,tau_S_grad,tau_M_theo,tau_M_grad,r,phi,z,t_turb,t_final)
    cparams = cbest(Sf,lenr,lenphi,lenz,lent,tau_T_theo,tau_T_grad,tau_S_theo,tau_S_grad,tau_M_theo,tau_M_grad)

    pearson_file(corrs,Sf,lenr,lenphi,lenz,lent)
    cbest_files(cparams,Sf,lenr,lenphi,lenz,lent)
    
    path = 'Sf_'+str(Sf)+'/'+str(lenr)+'_'+str(lenphi)+'_'+str(lenz)+'/pearson_coeff'
    
    try:
        os.makedirs(path)
    except OSError as error:
        print(error)
        
    os.system('mv pearson.txt '+path+'/.')
    #os.system('mv pearson_bis.txt '+path+'/.')
    os.system('mv pearson_split.txt '+path+'/.')
    
    

#cbest parameter

    path = 'Sf_'+str(Sf)+'/'+str(lenr)+'_'+str(lenphi)+'_'+str(lenz)+'/c_best'
    
    try:
        os.makedirs(path)
    except OSError as error:
        print(error)
        
    #os.system('scp miquel@tyrion24:/scr/miquel/MRI/Miquel/c_best.txt '+path+'/.')
        
    os.system('mv c_best.txt '+path+'/.')
                
    return()
    
    
#%%
    
main()


    
    
    
