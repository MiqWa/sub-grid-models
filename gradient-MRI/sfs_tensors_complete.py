#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 16:41:11 2020

@author: miquelmiravet
"""
import os 
import numpy as np
import h5py
import sys
from scipy.io import FortranFile
import numpy.ctypeslib as npct
from numpy.ctypeslib import ndpointer
from ctypes import c_int, c_double 

from pearson_cbest import pearson
#------------------------------------------------------------
# input type for the function
# must be a double array, with single dimension that is contiguous
array_3d_float = npct.ndpointer(dtype=np.float64, ndim=3, flags='CONTIGUOUS')
array_1d_float = npct.ndpointer(dtype=np.float64, ndim=1, flags='CONTIGUOUS')

libgkernel = npct.load_library("mri_kernels/libgkernel", ".")
libgkernel.gaussian_kernel.restype = None
libgkernel.gaussian_kernel.argtypes = [c_int, c_int, c_int, array_1d_float, array_1d_float, array_1d_float, array_1d_float, c_int, c_int, c_int, array_1d_float, array_1d_float, array_1d_float, c_double, array_3d_float, array_3d_float]

libboxkernel = npct.load_library("mri_kernels/libboxkernel", ".")
libboxkernel.box_kernel.restype = None
libboxkernel.box_kernel.argtypes = [c_int, c_int, c_int, array_1d_float, array_1d_float, array_1d_float, array_1d_float, c_int, c_int, c_int, array_1d_float, array_1d_float, array_1d_float, array_1d_float, array_3d_float, array_3d_float]
#===========================================================================================

def readh5files(file):

    print('DATA FROM FILE:      ',file)

    with h5py.File(file, "r") as f:
    # List all groups
        print("Keys: %s" % f.keys())
        a_group_key = list(f.keys())
        for i in a_group_key:
            print(i)

        Pgas = f[a_group_key[0]][...]
        print('Pgas shape    :', Pgas.shape)
        bphi = f[a_group_key[1]][...]
        print('Bphi shape    :', bphi.shape)
        br = f[a_group_key[2]][...]
        print('Br shape    :', br.shape)
        bz = f[a_group_key[3]][...]
        print('Bz shape    :', bz.shape)
        gravpot = f[a_group_key[4]][...]
        print('gravpot shape    :', gravpot.shape)
        phi = f[a_group_key[5]][...]
        print('Phi shape    :', phi.shape)
        r = f[a_group_key[6]][...]
        print('r shape    :', r.shape)
        rho = f[a_group_key[7]][...]
        print('Rho shape    :', rho.shape)
        time = f[a_group_key[8]][...]
        print('Time shape    :', time.shape)
        vphi = f[a_group_key[9]][...]
        print('vphi shape    :', vphi.shape)
        vr = f[a_group_key[10]][...]
        print('vr shape    :', vr.shape)
        vz = f[a_group_key[11]][...]
        print('vz shape    :', vz.shape)
        z = f[a_group_key[12]][...]
        print('z shape    :', z.shape)
        #time = file.replace('.h5','')
        #time = float(time[-4:])/250
        
        
        f.close()

    print('Dimensions of the grid     :',len(r)," x ",len(phi)," x ",len(z))
    print('Time           :',time,' s')
    
    return [time, r, phi, z, br, bphi, bz, vr, vphi, vz, rho, Pgas, gravpot]

#===========================================================================================

def readh5files_notime(file):

    print('DATA FROM FILE:      ',file)

    with h5py.File(file, "r") as f:
    # List all groups
        print("Keys: %s" % f.keys())
        a_group_key = list(f.keys())
        for i in a_group_key:
            print(i)

        Pgas = f[a_group_key[0]][...]
        print('Pgas shape    :', Pgas.shape)
        bphi = f[a_group_key[1]][...]
        print('Bphi shape    :', bphi.shape)
        br = f[a_group_key[2]][...]
        print('Br shape    :', br.shape)
        bz = f[a_group_key[3]][...]
        print('Bz shape    :', bz.shape)
        gravpot = f[a_group_key[4]][...]
        print('gravpot shape    :', gravpot.shape)
        phi = f[a_group_key[5]][...]
        print('Phi shape    :', phi.shape)
        r = f[a_group_key[6]][...]
        print('r shape    :', r.shape)
        rho = f[a_group_key[7]][...]
        print('Rho shape    :', rho.shape)
        vphi = f[a_group_key[8]][...]
        print('vphi shape    :', vphi.shape)
        vr = f[a_group_key[9]][...]
        print('vr shape    :', vr.shape)
        vz = f[a_group_key[10]][...]
        print('vz shape    :', vz.shape)
        z = f[a_group_key[11]][...]
        print('z shape    :', z.shape)
        #time = file.replace('.h5','')
        #time = float(time[-4:])/250


        f.close()

    print('Dimensions of the grid     :',len(r)," x ",len(phi)," x ",len(z))

    return [gravpot, r, phi, z, br, bphi, bz, vr, vphi, vz, rho, Pgas]

#===========================================================================================

def read_data(arxiu):
    
    """
    This function reads the data from
    the files, by choosing the file we 
    are interested to study.
    -Arguments: 
        arxiu: number of the file.
        
    """
    
    print('DATA FROM FILE:      ',arxiu)
    str1 = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/DATA/MRI/A100_100_34/mriquel-0file.dat" 
    ubi=str1.replace('file',arxiu)
    with FortranFile(ubi,'r','>u4') as f: 
        # read time
        time=f.read_reals(dtype='>f8')
        print ("time                     = ", time)
        # read dimensions of the grid
        nr = f.read_reals(dtype='>i4')[0]
        nphi = f.read_reals(dtype='>i4')[0]
        nz = f.read_reals(dtype='>i4')[0]
        print ("nr x nphi x nz           = ",nr," x ",nphi," x ",nz)
        # read grid arrays (1D)
        r = f.read_reals(dtype='>f8')
        phi = f.read_reals(dtype='>f8')
        z = f.read_reals(dtype='>f8')
        print ("len(r), len(phi), len(z) = ",len(r), len(phi), len(z))

        # read variables on the grid (3D arrays)
        Br = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("Br                       : ",Br.shape)
        Bphi = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("Bphi                     : ",Bphi.shape)
        Bz = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("Bz                       : ",Bz.shape)
        vr = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("vr                       : ",vr.shape)
        vphi = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("vphi                     : ",vphi.shape)
        vz = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("vz                       : ",vz.shape)
        rho = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("rho                      : ",rho.shape)
        Pgas = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("Pgas                     : ",Pgas.shape)
        phi2 = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("phi                      : ",phi2.shape)
         
        
    return [time, r, phi, z, Br, Bphi, Bz, vr, vphi, vz, rho, Pgas, phi2]

#===========================================================================================


#===========================================================================================
    
def gaussian_filter(var,filtersize,dr,r,phi,z,lenr,lenphi,lenz,rfilter,phifilter,zfilter,lenrfilter,lenphifilter,lenzfilter):

    varmean = np.zeros((lenrfilter,lenphifilter,lenzfilter)) 
    
    libgkernel.gaussian_kernel(lenr, lenphi,lenz, r, phi, z, dr,lenrfilter, lenphifilter, lenzfilter, rfilter, phifilter, zfilter, filtersize, var, varmean)
    
    return varmean

#===========================================================================================

def box_filter(var,filtersize,dr,r,phi,z,lenr,lenphi,lenz,rfilter,phifilter,zfilter,lenrfilter,lenphifilter,lenzfilter):

    varmean = np.zeros((lenrfilter,lenphifilter,lenzfilter))

    libboxkernel.box_kernel(lenr, lenphi,lenz, r, phi, z, dr, lenrfilter, lenphifilter, lenzfilter, rfilter, phifilter, zfilter, filtersize, var, varmean)

    return varmean

#===========================================================================================

    
def sfs_tensors_theo(dr,r,phi,z,lenr,lenphi,lenz,rnew,phinew,znew,lenrnew,lenphinew,lenznew,press,delta,Nmean, rhomean, vtilde, bmean, Umean,gammat,kappa,gamma1,filtersize,T,S,M,filtering):
    """
    Construction of the flux terms. Use of the averaging 
    function to get the SFS tensors. 
    """

    
    epstilde = Umean/rhomean-0.5*(vtilde[0]**2+vtilde[1]**2+vtilde[2]**2)-0.5*(bmean[0]**2+bmean[1]**2+bmean[2]**2)/rhomean
    pmean = kappa*rhomean**(gamma1)+(gammat-1)*rhomean*(epstilde-rhomean**(gamma1-1)*kappa/(gamma1-1))
    
    
    #flux terms in terms of averaged/tilde variables: 
    Ttilde = np.zeros((3,3,len(rnew),len(phinew),len(znew)))
    Stilde = np.zeros((3,len(rnew),len(phinew),len(znew)))
    Mtilde = np.zeros((3,3,len(rnew),len(phinew),len(znew)))
    for i in range(0,3):
        Stilde[i,:,:,:] = (Umean[:,:,:]+pmean[:,:,:]+0.5*(bmean[0,:,:,:]**2+bmean[1,:,:,:]**2+bmean[2,:,:,:]**2))*vtilde[i,:,:,:]-(vtilde[0,:,:,:]*bmean[0,:,:,:]+vtilde[1,:,:,:]*bmean[1,:,:,:]+vtilde[2,:,:,:]*bmean[2,:,:,:])*bmean[i,:,:,:]
        for j in range(0,3):
            Ttilde[j,i,:,:,:] = rhomean[:,:,:]*vtilde[i,:,:,:]*vtilde[j,:,:,:]-bmean[i,:,:,:]*bmean[j,:,:,:]+delta[j,i]*(pmean[:,:,:]+0.5*(bmean[0,:,:,:]**2+bmean[1,:,:,:]**2+bmean[2,:,:,:]**2))
            Mtilde[j,i,:,:,:] = -(bmean[j,:,:,:]*vtilde[i,:,:,:]-vtilde[j,:,:,:]*bmean[i,:,:,:])
    
    #averaged flux terms
    
    Tmean = np.zeros((3,3,len(rnew),len(phinew),len(znew)))
    Smean = np.zeros((3,len(rnew),len(phinew),len(znew)))
    Mmean = np.zeros((3,3,len(rnew),len(phinew),len(znew)))

    if filtering == 'gaussian':

        for i in range(0,3):
            Smean[i,:,:,:] = gaussian_filter(S[i],filtersize,dr,r,phi,z,lenr,lenphi,lenz,rnew,phinew,znew,lenrnew,lenphinew,lenznew)
            for j in range(0,3):
                Tmean[i,j,:,:,:] = gaussian_filter(T[i,j],filtersize,dr,r,phi,z,lenr,lenphi,lenz,rnew,phinew,znew,lenrnew,lenphinew,lenznew)
                Mmean[i,j,:,:,:] = gaussian_filter(M[i,j],filtersize,dr,r,phi,z,lenr,lenphi,lenz,rnew,phinew,znew,lenrnew,lenphinew,lenznew)
    
    elif filtering == 'box':

        for i in range(0,3):
            Smean[i,:,:,:] = box_filter(S[i],filtersize,dr,r,phi,z,lenr,lenphi,lenz,rnew,phinew,znew,lenrnew,lenphinew,lenznew)
            for j in range(0,3):
                Tmean[i,j,:,:,:] = box_filter(T[i,j],filtersize,dr,r,phi,z,lenr,lenphi,lenz,rnew,phinew,znew,lenrnew,lenphinew,lenznew)
                Mmean[i,j,:,:,:] = box_filter(M[i,j],filtersize,dr,r,phi,z,lenr,lenphi,lenz,rnew,phinew,znew,lenrnew,lenphinew,lenznew)

    #SFS tensors
    
    tau_T = Ttilde-Tmean
    tau_S = Stilde-Smean
    tau_M = Mtilde-Mmean

    return [tau_T, tau_S, tau_M] 

#===========================================================================================
    
def gradients_bis(varmean,r,phi,z,lenr,lenphi,lenz):
    """
    Determination of the gradient by using the finite difference method. 
    In order to do it, we need to average the variables over different sub-boxes.
    At the end, we average the gradients over the whole box in order to get a single value.
    """
    dr = r[1]-r[0]
    dphi = phi[1]-phi[0]
    dz = z[1]-z[0]
        
    dvardr = np.zeros((lenr,lenphi,lenz))
    dvardphi = np.zeros((lenr,lenphi,lenz))
    dvardz = np.zeros((lenr,lenphi,lenz))
        
        
    for i in range(0,lenr):
        for j in range(0,lenphi):
            for k in range(0,lenz-2):
                dvardz[i,j,k+1] = (varmean[i,j,k+2]-varmean[i,j,k])/(2*dz)
        for j in range(0,lenz):
            for k in range(0,lenphi-2):
                dvardphi[i,k+1,j] = (varmean[i,k+2,j]-varmean[i,k,j])/(2*dphi)
    for i in range(0,lenphi):
        for j in range(0,lenz):
            for k in range(0,lenr-2):
                dvardr[k+1,i,j] = (varmean[k+2,i,j]-varmean[k,i,j])/(2*dr)
                
    for i in range(0,lenr):
        for j in range(0,lenphi):
            dvardz[i,j,0] = (varmean[i,j,1]-varmean[i,j,0])/dz
            dvardz[i,j,lenz-1] = (varmean[i,j,lenz-1]-varmean[i,j,lenz-2])/dz    
        for j in range(0,lenz):
            dvardphi[i,0,j] = (varmean[i,1,j]-varmean[i,0,j])/dphi
            dvardphi[i,lenphi-1,j] = (varmean[i,lenphi-1,j]-varmean[i,lenphi-2,j])/dphi
    for i in range(0,lenphi):
        for j in range(0,lenz):
            dvardr[0,i,j] = (varmean[1,i,j]-varmean[0,i,j])/dr
            dvardr[lenr-1,i,j] = (varmean[lenr-1,i,j]-varmean[lenr-2,i,j])/dr
 
    #GRADIENT: 
    
    #second component of the gradient : we have to divide by r (medium value in each sub-box)
 
    dvardphir = np.zeros((lenr,lenphi,lenz))
    for i in range(0,len(r)):
        dvardphir[i,:,:] = dvardphi[i,:,:]/(r[i])
    
    grad = np.zeros((3,lenr,lenphi,lenz))
    
    grad[0,:,:,:] = dvardr
    grad[1,:,:,:] = dvardphir
    grad[2,:,:,:] = dvardz
        
    return grad

#===========================================================================================

def vars_filter(B,N,rho,U,filtersize,dr,r,phi,z,lenr,lenphi,lenz,rnew,phinew,znew,lenrnew,lenphinew,lenznew,filtering):
    
    if filtering == 'gaussian':

        print('Gaussian filtering !!')

        Nrmean = gaussian_filter(N[0,:,:,:],filtersize,dr,r,phi,z,lenr,lenphi,lenz,rnew,phinew,znew,lenrnew,lenphinew,lenznew)
        print('Nr filtered!')
        Nphimean = gaussian_filter(N[1,:,:,:],filtersize,dr,r,phi,z,lenr,lenphi,lenz,rnew,phinew,znew,lenrnew,lenphinew,lenznew)
        print('Nphi filtered!')
        Nzmean = gaussian_filter(N[2,:,:,:],filtersize,dr,r,phi,z,lenr,lenphi,lenz,rnew,phinew,znew,lenrnew,lenphinew,lenznew)
        print('Nz filtered!')
        Nmean = np.array([Nrmean,Nphimean,Nzmean])
        rhomean = gaussian_filter(rho,filtersize,dr,r,phi,z,lenr,lenphi,lenz,rnew,phinew,znew,lenrnew,lenphinew,lenznew)
        print('rho filtered!')
        vtilde = np.zeros((3,len(rnew),len(phinew),len(znew)))
        for i in range(0,3):
            vtilde[i,:,:,:] = Nmean[i,:,:,:]/rhomean[:,:,:] 
        bmean = np.array([gaussian_filter(B[0,:,:,:],filtersize,dr,r,phi,z,lenr,lenphi,lenz,rnew,phinew,znew,lenrnew,lenphinew,lenznew),gaussian_filter(B[1,:,:,:],filtersize,dr,r,phi,z,lenr,lenphi,lenz,rnew,phinew,znew,lenrnew,lenphinew,lenznew),gaussian_filter(B[2,:,:,:],filtersize,dr,r,phi,z,lenr,lenphi,lenz,rnew,phinew,znew,lenrnew,lenphinew,lenznew)])
        print('B filtered!')
        Umean = gaussian_filter(U,filtersize,dr,r,phi,z,lenr,lenphi,lenz,rnew,phinew,znew,lenrnew,lenphinew,lenznew)
        print('U filtered!')
    
    elif filtering == 'box':

        print('Box filtering !!')

        Nrmean = box_filter(N[0,:,:,:],filtersize,dr,r,phi,z,lenr,lenphi,lenz,rnew,phinew,znew,lenrnew,lenphinew,lenznew)
        print('Nr filtered!')
        Nphimean = box_filter(N[1,:,:,:],filtersize,dr,r,phi,z,lenr,lenphi,lenz,rnew,phinew,znew,lenrnew,lenphinew,lenznew)
        print('Nphi filtered!')
        Nzmean = box_filter(N[2,:,:,:],filtersize,dr,r,phi,z,lenr,lenphi,lenz,rnew,phinew,znew,lenrnew,lenphinew,lenznew)
        print('Nz filtered!')
        Nmean = np.array([Nrmean,Nphimean,Nzmean])
        rhomean = box_filter(rho,filtersize,dr,r,phi,z,lenr,lenphi,lenz,rnew,phinew,znew,lenrnew,lenphinew,lenznew)
        print('rho filtered!')
        vtilde = np.zeros((3,len(rnew),len(phinew),len(znew)))
        for i in range(0,3):
            vtilde[i,:,:,:] = Nmean[i,:,:,:]/rhomean[:,:,:]
        bmean = np.array([box_filter(B[0,:,:,:],filtersize,dr,r,phi,z,lenr,lenphi,lenz,rnew,phinew,znew,lenrnew,lenphinew,lenznew),box_filter(B[1,:,:,:],filtersize,dr,r,phi,z,lenr,lenphi,lenz,rnew,phinew,znew,lenrnew,lenphinew,lenznew),box_filter(B[2,:,:,:],filtersize,dr,r,phi,z,lenr,lenphi,lenz,rnew,phinew,znew,lenrnew,lenphinew,lenznew)])
        print('B filtered!')
        Umean = box_filter(U,filtersize,dr,r,phi,z,lenr,lenphi,lenz,rnew,phinew,znew,lenrnew,lenphinew,lenznew)
        print('U filtered!')


    return Nmean, rhomean, vtilde, bmean, Umean

#===========================================================================================
    
def sfs_tensors_gradient(filtersize,filtersize2,r,phi,z,lenr,lenphi,lenz,rnew,phinew,znew,lenrnew,lenphinew,lenznew,Nmean, rhomean, vtilde, bmean, Umean,delta,gammat,kappa,gamma1):
    """
    This function constructs the different SFS dictated
    by the gradient model. 
    """
    
    # chi = filter_size^2/24
    chi = (filtersize2**2)/24
    
    #mean and tilde variables on which gradient will be applied
    
    epstilde = Umean/rhomean-0.5*(vtilde[0]**2+vtilde[1]**2+vtilde[2]**2)-0.5*(bmean[0]**2+bmean[1]**2+bmean[2]**2)/rhomean
    pmean = kappa*rhomean**(gamma1)+(gammat-1)*rhomean*(epstilde-rhomean**(gamma1-1)*kappa/(gamma1-1))
    phitilde = Umean+pmean+0.5*(bmean[0]**2+bmean[1]**2+bmean[2]**2)
    dpdeps = (gammat-1)*rhomean
    dpdrho = kappa*gamma1*rhomean**(gamma1-1)+(gammat-1)*(epstilde-kappa*gamma1/(gamma1-1)*rhomean**(gamma1-1))
    

    #gradients
    vtildegrad = np.zeros((3,3,len(rnew),len(phinew),len(znew)))
    for i in range(0,3):
        vtildegrad[i,:,:,:,:] = gradients_bis(vtilde[i,:,:,:],rnew,phinew,znew,lenrnew,lenphinew,lenznew)
        
    bmeangrad = np.zeros((3,3,len(rnew),len(phinew),len(znew)))
    for i in range(0,3):        
        bmeangrad[i,:,:,:,:] = gradients_bis(bmean[i,:,:,:],rnew,phinew,znew,lenrnew,lenphinew,lenznew)
    
    dpdrhograd = gradients_bis(dpdrho,rnew,phinew,znew,lenrnew,lenphinew,lenznew)
    dpdepsgrad = gradients_bis(dpdeps,rnew,phinew,znew,lenrnew,lenphinew,lenznew)
    rhomeangrad = gradients_bis(rhomean,rnew,phinew,znew,lenrnew,lenphinew,lenznew)
    epstildegrad = gradients_bis(epstilde,rnew,phinew,znew,lenrnew,lenphinew,lenznew)
    phitildegrad = gradients_bis(phitilde,rnew,phinew,znew,lenrnew,lenphinew,lenznew)
    vbgrad = gradients_bis(vtilde[0]*bmean[0]+vtilde[1]*bmean[1]+vtilde[2]*bmean[2],rnew,phinew,znew,lenrnew,lenphinew,lenznew) #gradient of the scalar product
              
    #SFS tensors                  
                      
    tau_kin = np.zeros((3,3,len(rnew),len(phinew),len(znew)))
    tau_mag = np.zeros((3,3,len(rnew),len(phinew),len(znew)))
    tau_ener = np.zeros((3,len(rnew),len(phinew),len(znew)))
    tau_ind = np.zeros((3,3,len(rnew),len(phinew),len(znew)))
    tau_pres = np.zeros((len(rnew),len(phinew),len(znew)))
    

    for x in range(0,len(rnew)):
        for y in range(0,len(phinew)):
            for z in range(0,len(znew)):
                for i in range(0,3):
                    tau_ener[i,x,y,z] = -2*chi*(np.sum(phitildegrad[:,x,y,z]*vtildegrad[i,:,x,y,z])+(np.sum((bmean[i,x,y,z]*(bmean[0,x,y,z]*vtildegrad[0,:,x,y,z]+bmean[1,x,y,z]*vtildegrad[1,:,x,y,z]+bmean[2,x,y,z]*vtildegrad[2,:,x,y,z]))*rhomeangrad[:,x,y,z])-np.sum((phitilde[x,y,z]*vtildegrad[i,:,x,y,z])*rhomeangrad[:,x,y,z]))/rhomean[x,y,z]-bmean[i,x,y,z]*(bmeangrad[0,0,x,y,z]*vtildegrad[0,0,x,y,z]+bmeangrad[0,1,x,y,z]*vtildegrad[0,1,x,y,z]+bmeangrad[0,2,x,y,z]*vtildegrad[0,2,x,y,z]+bmeangrad[1,0,x,y,z]*vtildegrad[1,0,x,y,z]+bmeangrad[1,1,x,y,z]*vtildegrad[1,1,x,y,z]+bmeangrad[1,2,x,y,z]*vtildegrad[1,2,x,y,z]+bmeangrad[2,0,x,y,z]*vtildegrad[2,0,x,y,z]+bmeangrad[2,1,x,y,z]*vtildegrad[2,1,x,y,z]+bmeangrad[2,2,x,y,z]*vtildegrad[2,2,x,y,z])-np.sum(vbgrad[:,x,y,z]*bmeangrad[i,:,x,y,z]))
                    for j in range(0,3):
                        tau_kin[j,i,x,y,z] = -2*chi*rhomean[x,y,z]*np.sum(vtildegrad[i,:,x,y,z]*vtildegrad[j,:,x,y,z])
                        tau_mag[j,i,x,y,z] = -2*chi*np.sum(bmeangrad[i,:,x,y,z]*bmeangrad[j,:,x,y,z])
                        tau_ind[j,i,x,y,z] = 2*chi*(np.sum(vtildegrad[i,:,x,y,z]*bmeangrad[j,:,x,y,z])-np.sum(vtildegrad[j,:,x,y,z]*bmeangrad[i,:,x,y,z])+(bmean[i,x,y,z]*np.sum(vtildegrad[j,:,x,y,z]*rhomeangrad[:,x,y,z])-bmean[j,x,y,z]*np.sum(vtildegrad[i,:,x,y,z]*rhomeangrad[:,x,y,z]))/rhomean[x,y,z])
    
                tau_pres[x,y,z] = -chi*(np.sum(dpdrhograd[:,x,y,z]*rhomeangrad[:,x,y,z])+np.sum(dpdepsgrad[:,x,y,z]*epstildegrad[:,x,y,z])-2/(rhomean[x,y,z])*dpdeps[x,y,z]*np.sum(rhomeangrad[:,x,y,z]*epstildegrad[:,x,y,z])+bmeangrad[0,0,x,y,z]**2+bmeangrad[0,1,x,y,z]**2+bmeangrad[0,2,x,y,z]**2+bmeangrad[1,0,x,y,z]**2+bmeangrad[1,1,x,y,z]**2+bmeangrad[1,2,x,y,z]**2+bmeangrad[2,0,x,y,z]**2+bmeangrad[2,1,x,y,z]**2+bmeangrad[2,2,x,y,z]**2-dpdeps[x,y,z]/(rhomean[x,y,z])*(rhomean[x,y,z]*(vtildegrad[0,0,x,y,z]**2+vtildegrad[0,1,x,y,z]**2+vtildegrad[0,2,x,y,z]**2+vtildegrad[1,0,x,y,z]**2+vtildegrad[1,1,x,y,z]**2+vtildegrad[1,2,x,y,z]**2+vtildegrad[2,0,x,y,z]**2+vtildegrad[2,1,x,y,z]**2+vtildegrad[2,2,x,y,z]**2)+bmeangrad[0,0,x,y,z]**2+bmeangrad[0,1,x,y,z]**2+bmeangrad[0,2,x,y,z]**2+bmeangrad[1,0,x,y,z]**2+bmeangrad[1,1,x,y,z]**2+bmeangrad[1,2,x,y,z]**2+bmeangrad[2,0,x,y,z]**2+bmeangrad[2,1,x,y,z]**2+bmeangrad[2,2,x,y,z]**2))
    
    tau_T = np.zeros((3,3,len(rnew),len(phinew),len(znew)))
    tau_S = np.zeros((3,len(rnew),len(phinew),len(znew)))
    tau_M = tau_ind
    

    for x in range(0,len(rnew)):
        for y in range(0,len(phinew)): 
            for z in range(0,len(znew)):
                for k in range(0,3):
                    
                    tau_S[k,x,y,z] = tau_ener[k,x,y,z]+vtilde[k,x,y,z]*tau_pres[x,y,z]
                    
                    for i in range(0,3):
                        
                        tau_T[k,i,x,y,z] = tau_kin[k,i,x,y,z]-tau_mag[k,i,x,y,z]+delta[k,i]*tau_pres[x,y,z]
                        
                
    return [tau_T, tau_S, tau_M]

#===========================================================================================
    
def sfs_comp(filtersize,filtersize2,lent,difr,r,phi,z,rnew,phinew,znew,Sf,direc, B_old,filtering,res,path_files):
    #TIME EVOLUTION 
    lenr = len(r)
    lenphi = len(phi)
    lenz = len(z)
    lenrnew = len(rnew)
    lenphinew = len(phinew)
    lenznew = len(znew)
    
    tau_T_theo = np.zeros((lent,3,3,lenrnew,lenphinew,lenznew))
    tau_S_theo = np.zeros((lent,3,lenrnew,lenphinew,lenznew))
    tau_M_theo = np.zeros((lent,3,3,lenrnew,lenphinew,lenznew))
    tau_T_grad = np.zeros((lent,3,3,lenrnew,lenphinew,lenznew))
    tau_S_grad = np.zeros((lent,3,lenrnew,lenphinew,lenznew))
    tau_M_grad = np.zeros((lent,3,3,lenrnew,lenphinew,lenznew))
    
    
    delta = np.identity(3) #kronecker delta 
    kappacgs = 4.8974894*10**(14) 
    gamma1 = 1.31
    gammat = 1.5 #es lo que sale en la tesis (martin dijo 2.5)
    kappa = kappacgs*8.26110825*10**(-50)/((7.42471382*10**(-29))**(gamma1))

    t = np.linspace(0,0.03,lent);

    for x in range(0,lent):
        
        if direc == '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/DATA/MRI/A100_100_34/':
            arx = format(10*x, "03")
            listvar=read_data(arx)
        
        elif lenphi == 304 or B_old == False:
            arx = format(25*x, "08")
            listvar = readh5files_notime(direc+'mri'+str(arx)+'.h5')
            print('Time : ['+str(t[x])+'] s')

        else :
            arx = format(25*x, "04")
            listvar = readh5files(direc+'mri-'+str(arx)+'.h5')

        B = [listvar[4],listvar[5],listvar[6]]
        B = np.array(B)*2.874214371*10**(-25)/((4*3.141592653589793)**(0.5))     
        v = [listvar[7],listvar[8],listvar[9]] 
        v = np.array(v)*3.33564095*10**(-11)
        rho = listvar[10]*7.42471382*10**(-29)
        #print('rho old : ', rho)
        rho = np.asarray(rho, order='C')
        #print('rho C : ' , rho)
        N = np.zeros((3,len(r),len(phi),len(z)))
        press = listvar[11]*8.26110825*10**(-50)
        
#let's determine the specific internal energy

        pressp = kappa*rho**(gamma1)
        ep = kappa/(gamma1-1)*rho**(gamma1-1) 
        presst = press-pressp
        et = presst/(rho*(gammat-1)) 
        eps = (ep+et)
    
    
        U = rho*eps+rho*0.5*(v[0]**2+v[1]**2+v[2]**2)+0.5*(B[0]**2+B[1]**2+B[2]**2) #energy density
        
        for j in range(0,3):
            N[j] = rho*v[j]
        
        #flux terms
        T = np.zeros((3,3,len(r),len(phi),len(z)))
        S = np.zeros((3,len(r), len(phi), len(z)))
        M = np.zeros((3,3,len(r), len(phi), len(z)))
    
        for i in range(0,3):
            S[i,:,:,:] = (U[:,:,:]+press[:,:,:]+0.5*(B[0,:,:,:]**2+B[1,:,:,:]**2+B[2,:,:,:]**2))*v[i,:,:,:]-(v[0,:,:,:]*B[0,:,:,:]+v[1,:,:,:]*B[1,:,:,:]+v[2,:,:,:]*B[2,:,:,:])*B[i,:,:,:]
            for j in range(0,3):
                T[j,i,:,:,:] = rho[:,:,:]*v[i,:,:,:]*v[j,:,:,:]-B[i,:,:,:]*B[j,:,:,:]+delta[j,i]*(press[:,:,:]+0.5*(B[0,:,:,:]**2+B[1,:,:,:]**2+B[2,:,:,:]**2))
                M[j,i,:,:,:] = -(v[i,:,:,:]*B[j,:,:,:]-B[i,:,:,:]*v[j,:,:,:])
        
        #filtered variables
        
        Nmean, rhomean, vtilde, bmean, Umean = vars_filter(B,N,rho,U,filtersize,difr,r,phi,z,lenr,lenphi,lenz,rnew,phinew,znew,lenrnew,lenphinew,lenznew,filtering)
        
        print('Calculation of the theoretical SFS tensors !!')
        tau_T_theo[x],tau_S_theo[x], tau_M_theo[x] = sfs_tensors_theo(difr,r,phi,z,lenr,lenphi,lenz,rnew,phinew,znew,lenrnew,lenphinew,lenznew,press,delta,Nmean, rhomean, vtilde, bmean, Umean,gammat,kappa,gamma1,filtersize,T,S,M,filtering)
        
        print('Calculation of the gradient SFS tensors !!')
        tau_T_grad[x],tau_S_grad[x],tau_M_grad[x] = sfs_tensors_gradient(filtersize,filtersize2,r,phi,z,lenr,lenphi,lenz,rnew,phinew,znew,lenrnew,lenphinew,lenznew,Nmean, rhomean, vtilde, bmean, Umean,delta,gammat,kappa,gamma1)
        
         
        hf = h5py.File(path_files+'/sfs_tensors_teo_res_'+res+'-'+str(arx)+'.h5', 'w')
        hf.create_dataset('time', data=t[x])
        hf.create_dataset('S_f', data = Sf)
        hf.create_dataset('r_f', data=rnew)
        hf.create_dataset('phi_f', data=phinew)
        hf.create_dataset('z_f', data=znew)
        hf.create_dataset('Trr', data=tau_T_theo[x,0,0])
        hf.create_dataset('Trphi', data=tau_T_theo[x,0,1])
        hf.create_dataset('Trz', data=tau_T_theo[x,0,2])
        hf.create_dataset('Tphiphi', data=tau_T_theo[x,1,1])
        hf.create_dataset('Tphiz', data=tau_T_theo[x,1,2])
        hf.create_dataset('Tzz', data=tau_T_theo[x,2,2])
        hf.create_dataset('Sr', data=tau_S_theo[x,0,])
        hf.create_dataset('Sphi', data=tau_S_theo[x,1])
        hf.create_dataset('Sz', data=tau_S_theo[x,2])
        hf.create_dataset('Mrphi', data=tau_M_theo[x,0,1])
        hf.create_dataset('Mrz', data=tau_M_theo[x,0,2])
        hf.create_dataset('Mphiz', data=tau_M_theo[x,1,2])
        hf.close()       

        hf2 = h5py.File(path_files+'/sfs_tensors_grad_res_'+res+'-'+str(arx)+'.h5', 'w')
        hf2.create_dataset('time', data=t[x])
        hf2.create_dataset('S_f', data = Sf)
        hf2.create_dataset('r_f', data=rnew)
        hf2.create_dataset('phi_f', data=phinew)
        hf2.create_dataset('z_f', data=znew)
        hf2.create_dataset('Trr', data=tau_T_grad[x,0,0])
        hf2.create_dataset('Trphi', data=tau_T_grad[x,0,1])
        hf2.create_dataset('Trz', data=tau_T_grad[x,0,2])
        hf2.create_dataset('Tphiphi', data=tau_T_grad[x,1,1])
        hf2.create_dataset('Tphiz', data=tau_T_grad[x,1,2])
        hf2.create_dataset('Tzz', data=tau_T_grad[x,2,2])
        hf2.create_dataset('Sr', data=tau_S_grad[x,0,])
        hf2.create_dataset('Sphi', data=tau_S_grad[x,1])
        hf2.create_dataset('Sz', data=tau_S_grad[x,2])
        hf2.create_dataset('Mrphi', data=tau_M_grad[x,0,1])
        hf2.create_dataset('Mrz', data=tau_M_grad[x,0,2])
        hf2.create_dataset('Mphiz', data=tau_M_grad[x,1,2])
        hf2.close()


    return [tau_T_theo, tau_S_theo, tau_M_theo, tau_T_grad, tau_S_grad, tau_M_grad]
               
#===========================================================================================
#%%     

def main():
    
    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/DATA/MRI/"
    
    filtering = sys.argv[1]
    B_old = True
    Bnew = 3
    Sf = int(sys.argv[2])

    lenphi = 304

    if lenphi == 400 :
        
        if B_old == True :
            direc = path_data+'A100_400_100_B_4.6e13_v_4.45d8_r_4.45d8_Lz_1.0_INTEL_stencil/'
        elif Bnew == 2: 
            direc = path_data+'A100_400_100_B_2.76/outp-mri/'
        elif Bnew == 3:
            direc = path_data+'A100_400_100_B_3.45/outp-mri/'
        lenr = 100
        lenz = 100
        t_turb = 12.5
        lent = 61
        t_final = 30

    elif lenphi == 800 :

        direc = path_data+'A200_800_200_B_4.6e13_v_4.45d8_r_4.45d8_Lz_1.0_INTEL_stencil/'

        lenr = 200
        lenz = 200
        t_turb = 11
        lent = 26
        t_final = 12.5

    elif lenphi == 240 :

        direc = path_data+'A60_240_60_B_4.6e13_v_4.45d8_r_4.45d8_Lz_1.0_INTEL_stencil_flex/'

        lenr = 60
        lenz = 60
        t_turb = 12.5
        lent = 61
        t_final = 30

    elif lenphi == 100 :

        direc = path_data+'A100_100_34/'

        lenr = 100
        lenz = 34
        t_turb = 4.8
        lent = 61
        t_final = 6


    elif lenphi == 304 :

        direc = path_data+'A76_304_76/outp-mri/'
        
        lenr = 76
        lenz = 76
        lent = 61
        t_turb = 12.5
        t_final = 30

    elif lenphi == 1600 : 
        
        direc = path_data+'A400_1600_134'
        
        lenr = 400
        lenz = 134
        lent = 4
        
    if lenphi == 100 :
        var = read_data('000')
        
    elif lenphi == 304 or B_old == False:
        var = readh5files_notime(direc+'mri00000000.h5')
    else : 
        var = readh5files(direc+'mri-0000.h5')
    
    res = str(lenr)+'_'+str(lenphi)+'_'+str(lenz)
    r = var[1].astype(np.float64)
    print('r TYPE : ', r.dtype)
    phi = var[2].astype(np.float64)
    z = var[3].astype(np.float64)
    
    rhalf = (r[1]-r[0])/2

    r_front = np.zeros(len(r)+1)

    r_front[0] = r[0]-rhalf
    r_front[-1] = r[-1]+rhalf

    for i in range(1,len(r)):
        r_front[i] = (r[i]+r[i-1])/2

    difr = np.zeros(len(r))
    for j in range(0,len(r)):
        difr[j] = r_front[j+1]**2-r_front[j]**2

    
    if filtering == 'gaussian':
    	cellsize = (r[1]-r[0]+(r[-1]+r[0])*0.5*(phi[1]-phi[0])+z[1]-z[0])/3
    else : 
    	cellsize = np.array([r[1]-r[0],phi[1]-phi[0],z[1]-z[0]])

    filtersize = Sf*cellsize
    filtersize2 = Sf*(r[1]-r[0]+(r[-1]+r[0])*0.5*(phi[1]-phi[0])+z[1]-z[0])/3
    
    entries = os.listdir(direc)
    entries = sorted(entries)
       
    a_rz = int(len(r)/2)
    a_phi = int(len(phi)/2)
    rzmin = a_rz-5
    rzmax = a_rz+5
    phimin = a_phi-5
    phimax = a_phi+5
    
    rfilter= r[rzmin:rzmax]
    phifilter = phi[phimin:phimax]
    zfilter = z[rzmin:rzmax]
    lenrfilter = len(rfilter)
    lenphifilter = len(phifilter)
    lenzfilter = len(zfilter)
    print(lenrfilter)
    print(lenphifilter)
    print(lenzfilter)
    if filtering == 'gaussian':
    	print('Filter size: ', filtersize,' cm')
    else : 
    	print('Filter size: ', filtersize[0],' cm')

    if B_old == True: 
        
        if filtering == 'gaussian':

            pathsfs = 'results/gaussian-filter/Sf_'+str(Sf)+'/'+res+'/sfs_tensors/files/'+str(lenrfilter)+'_points'
            pathpearson = 'results/gaussian-filter/Sf_'+str(Sf)+'/'+res+'/pearson_coeff/'+str(lenrfilter)+'_points'
    
        elif filtering == 'box':

            pathsfs = 'results/box-filter/Sf_'+str(Sf)+'/'+res+'/sfs_tensors/files/'+str(lenrfilter)+'_points'
            pathpearson = 'results/box-filter/Sf_'+res+'/pearson_coeff/'+str(lenrfilter)+'_points'
    
    elif B_old == False: 
    
        if filtering == 'gaussian':

            pathsfs = 'results/gaussian-filter/Sf_'+str(Sf)+'/'+res+'/B_'+str(Bnew)+'/sfs_tensors/files/'+str(lenrfilter)+'_points'
            pathpearson = 'results/gaussian-filter/Sf_'+str(Sf)+'/'+res+'/B_'+str(Bnew)+'/pearson_coeff/'+str(lenrfilter)+'_points'

        elif filtering == 'box':

            pathsfs = 'results/box-filter/Sf_'+str(Sf)+'/'+res+'/B_'+str(Bnew)+'/sfs_tensors/files/'+str(lenrfilter)+'_points'
            pathpearson = 'results/box-filter/Sf_'+str(Sf)+'/'+res+'/B_'+str(Bnew)+'/pearson_coeff/'+str(lenrfilter)+'_points'

    try:
        os.makedirs(pathsfs)
    except OSError as error:
        print(error)
        
    try:
        os.makedirs(pathpearson)
    except OSError as error:
        print(error)
        
    tau_T_theo, tau_S_theo, tau_M_theo, tau_T_grad, tau_S_grad, tau_M_grad = sfs_comp(filtersize,filtersize2,lent,difr,r,phi,z,rfilter,phifilter,zfilter,Sf,direc, B_old,filtering,res,pathsfs)

    pearson(Sf,lenrfilter,lenphifilter,lenzfilter,lent,tau_T_theo,tau_T_grad,tau_S_theo,tau_S_grad,tau_M_theo,tau_M_grad,t_turb,t_final,pathpearson)

    return()
    
    
#%%
if __name__ == "__main__":

    main()
