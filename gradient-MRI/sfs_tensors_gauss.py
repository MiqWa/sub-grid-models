#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 16:41:11 2020

@author: miquelmiravet
"""
import os 
import numpy as np
import h5py
import csv 
from scipy.io import FortranFile
from scipy.stats import pearsonr
import numpy.ctypeslib as npct
from numpy.ctypeslib import ndpointer
from ctypes import c_int, c_float, c_double 
#------------------------------------------------------------
# input type for the function
# must be a double array, with single dimension that is contiguous
array_3d_float = npct.ndpointer(dtype=np.float64, ndim=3, flags='CONTIGUOUS')
array_1d_float = npct.ndpointer(dtype=np.float64, ndim=1, flags='CONTIGUOUS')

libgkernel = npct.load_library("libgkernel", ".")
libgkernel.gaussian_kernel.restype = None
libgkernel.gaussian_kernel.argtypes = [c_int, c_int, c_int, array_3d_float, c_double, array_1d_float, array_1d_float, array_1d_float, array_3d_float]

#===========================================================================================

def readh5files(file):

    print('DATA FROM FILE:      ',file)

    with h5py.File(file, "r") as f:
    # List all groups
        print("Keys: %s" % f.keys())
        a_group_key = list(f.keys())
        for i in a_group_key:
            print(i)

        Pgas = f[a_group_key[0]][...]
        print('Pgas shape    :', Pgas.shape)
        bphi = f[a_group_key[1]][...]
        print('Bphi shape    :', bphi.shape)
        br = f[a_group_key[2]][...]
        print('Br shape    :', br.shape)
        bz = f[a_group_key[3]][...]
        print('Bz shape    :', bz.shape)
        gravpot = f[a_group_key[4]][...]
        print('gravpot shape    :', gravpot.shape)
        phi = f[a_group_key[5]][...]
        print('Phi shape    :', phi.shape)
        r = f[a_group_key[6]][...]
        print('r shape    :', r.shape)
        rho = f[a_group_key[7]][...]
        print('Rho shape    :', rho.shape)
        time = f[a_group_key[8]][...]
        print('Time shape    :', time.shape)
        vphi = f[a_group_key[9]][...]
        print('vphi shape    :', vphi.shape)
        vr = f[a_group_key[10]][...]
        print('vr shape    :', vr.shape)
        vz = f[a_group_key[11]][...]
        print('vz shape    :', vz.shape)
        z = f[a_group_key[12]][...]
        print('z shape    :', z.shape)
        #time = file.replace('.h5','')
        #time = float(time[-4:])/250
        
        
        f.close()

    print('Dimensions of the grid     :',len(r)," x ",len(phi)," x ",len(z))
    print('Time           :',time,' s')
    
    return [time, r, phi, z, br, bphi, bz, vr, vphi, vz, rho, Pgas, gravpot]

#===========================================================================================
    
def read_data(arxiu):
    
    """
    This function reads the data from
    the files, by choosing the file we 
    are interested to study.
    -Arguments: 
        arxiu: number of the file.
        
    """
    
    print('DATA FROM FILE:      ',arxiu)
    str1 = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/DATA/A100_100_34/mriquel-0file.dat" 
    ubi=str1.replace('file',arxiu)
    with FortranFile(ubi,'r','>u4') as f: 
        # read time
        time=f.read_reals(dtype='>f8')
        print ("time                     = ", time)
        # read dimensions of the grid
        nr = f.read_reals(dtype='>i4')[0]
        nphi = f.read_reals(dtype='>i4')[0]
        nz = f.read_reals(dtype='>i4')[0]
        print ("nr x nphi x nz           = ",nr," x ",nphi," x ",nz)
        # read grid arrays (1D)
        r = f.read_reals(dtype='>f8')
        phi = f.read_reals(dtype='>f8')
        z = f.read_reals(dtype='>f8')
        print ("len(r), len(phi), len(z) = ",len(r), len(phi), len(z))

        # read variables on the grid (3D arrays)
        Br = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("Br                       : ",Br.shape)
        Bphi = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("Bphi                     : ",Bphi.shape)
        Bz = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("Bz                       : ",Bz.shape)
        vr = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("vr                       : ",vr.shape)
        vphi = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("vphi                     : ",vphi.shape)
        vz = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("vz                       : ",vz.shape)
        rho = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("rho                      : ",rho.shape)
        Pgas = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("Pgas                     : ",Pgas.shape)
        phi2 = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("phi                      : ",phi2.shape)
         
        
    return [time, r, phi, z, Br, Bphi, Bz, vr, vphi, vz, rho, Pgas, phi2]

#===========================================================================================
    
def gaussian_filter(var,filtersize,r,phi,z):

    lenr = int(len(r))
    lenphi = int(len(phi))
    lenz = int(len(z))
    varmean = np.zeros((lenr,lenphi,lenz))

    print('Gaussian filtering !!')  
    
    libgkernel.gaussian_kernel(lenr, lenphi, lenz, var,filtersize, r, phi, z, varmean)
                
    varmean = np.delete(varmean, np.where((r -r[0] <= filtersize) & (r[-1]-r <= filtersize)), axis = 0)
    
    varmean = np.delete(varmean, np.where(((r[-1]+r[0])*0.5*(phi-phi[0]) <= filtersize) & ((r[-1]+r[0])*0.5*(phi[-1]-phi) <= filtersize)), axis = 1)

    varmean = np.delete(varmean, np.where((z -z[0] <= filtersize) & (z[-1]-z <= filtersize)), axis = 2)
 
    print('Shape of filtered variable : ',varmean.shape)
    return varmean
         
 #===========================================================================================                               

def splitting(arr,n):
    """
    Function that splits the array in order to 
    have a number of components proportional to n
    """
    frac = len(arr)/n
    if frac.is_integer() == False: 
        while True: 
            arr = np.delete(arr,len(arr)-1)
            frac_new = len(arr)/n
            if frac_new.is_integer() == True:
                break
    return arr

#===========================================================================================
    
def averaging_subboxes_bis(var,n,m,l,r,phi,z): 
    """
    This function averages over (m,l,n) subboxes, 
    in order to determine the partial derivatives. 
    """
    
    #reshaping of the coordinate arrays into m/n subarrays of equal length
    r_split = r.reshape(m,int(len(r)/m))
    phi_split = phi.reshape(l,int(len(phi)/l))
    z_split = z.reshape(n,int(len(z)/n))
    
    var = np.array(var)
    varnew = np.zeros((len(r),len(phi),len(z)))
    
    #splitting of the variable arrays
    for i in range(0,len(r)):
        for j in range(0,len(phi)):
            varnew[i,j,:] = splitting(var[i,j,:],n)
        for k in range(0,len(z)):
            varnew[i,:,k] = splitting(var[i,:,k],l)
    for j in range(0,len(phi)):
        for k in range(0,len(z)):
            varnew[:,j,k] = splitting(var[:,j,k],m)
    
    #reshaping of the variable arrays into subarrays of equal length, corresponding to each sub-box.
    var_split = varnew.reshape(m,int(len(r)/m),l,int(len(phi)/l),n,int(len(z)/n))
    
    #AVERAGING PROCEDURE
    
    #differentials
    dr = r_split[0,1]-r_split[0,0]
    dphi = phi_split[0,1]-phi_split[0,0]
    dz = z_split[0,1]-z_split[0,0]
    
    varmean = np.zeros((m,l,n))
    
    #construction of the matrix that represents the integral differentials. It will be different for each sub-box
    dVmatrix = np.ones((m,int(len(r)/m),l,int(len(phi)/l),n,int(len(z)/n)))
    dVmatrixdef = np.zeros((m,int(len(r)/m),l,int(len(phi)/l),n,int(len(z)/n)))   
    for i in range(0,m):
        dV = r_split[i,:]*dr*dphi*dz
        for j in range(0,l):
            for k in range(0,n):
                for x in range(0,int(len(r)/m)):
                    dVmatrixdef[i,x,j,:,k,:] = dV[x]*dVmatrix[i,x,j,:,k,:]
    
    #volume of each sub-box                
    V = np.zeros((m,l,n))
    for i in range(0,m):
        for j in range(0,l):
            for k in range(0,n):
                V[i,j,k] = np.sum(dVmatrixdef[i,:,j,:,k,:])
    
    #averaging
    for i in range(0,m):
        for j in range(0,l):
            for k in range(0,n):
                varmean[i,j,k] = np.sum(dVmatrixdef[i,:,j,:,k,:]*var_split[i,:,j,:,k,:])/V[i,j,k] 
    return varmean

#===========================================================================================
    
def averaging(var,n,m,l,r,phi,z): 
    """
    This function averages over the whole box.  
    """
    
    #reshaping of the coordinate arrays into m/l/n subarrays of equal length
    r_split = r.reshape(m,int(len(r)/m))
    phi_split = phi.reshape(l,int(len(phi)/l))
    z_split = z.reshape(n,int(len(z)/n))
    r_subbox = np.zeros(m)
    for i in range(0,m):
        r_subbox[i] = (r_split[i,-1]+r_split[i,0])/2
    

    #AVERAGING PROCEDURE
    
    #differentials
    dr = r_split[1,0]-r_split[0,0]
    dphi = phi_split[1,0]-phi_split[0,0]
    dz = z_split[1,0]-z_split[0,0]
    
    
    #construction of the matrix that represents the integral differentials. It will be different for each sub-box
    dVmatrix = np.ones((m,l,n))
    dVmatrixdef = np.zeros((m,l,n))   
    
    dV = r_subbox[:]*dr*dphi*dz
    for x in range(0,m):
        dVmatrixdef[x,:,:] = dV[x]*dVmatrix[x,:,:]
    
    #volume                
    V = np.sum(dVmatrixdef)
    
    #averaging
    varmean = np.sum(dVmatrixdef*var)/V 
    
    return varmean

#===========================================================================================
    
def sfs_tensors_theo_gaussian(r,phi,z,rnew,phinew,znew,v,B,press,U,rho,delta,N,gammat,kappa,gamma1,filtersize):
    """
    Construction of the flux terms. Use of the averaging 
    function to get the SFS tensors. 
    """

    #flux terms
    T = np.zeros((3,3,len(r),len(phi),len(z)))
    S = np.zeros((3,len(r), len(phi), len(z)))
    M = np.zeros((3,3,len(r), len(phi), len(z)))
    
    for i in range(0,3):
        S[i,:,:,:] = (U[:,:,:]+press[:,:,:]+0.5*(B[0,:,:,:]**2+B[1,:,:,:]**2+B[2,:,:,:]**2))*v[i,:,:,:]-(v[0,:,:,:]*B[0,:,:,:]+v[1,:,:,:]*B[1,:,:,:]+v[2,:,:,:]*B[2,:,:,:])*B[i,:,:,:]
        for j in range(0,3):
            T[j,i,:,:,:] = rho[:,:,:]*v[i,:,:,:]*v[j,:,:,:]-B[i,:,:,:]*B[j,:,:,:]+delta[j,i]*(press[:,:,:]+0.5*(B[0,:,:,:]**2+B[1,:,:,:]**2+B[2,:,:,:]**2))
            M[j,i,:,:,:] = -(v[i,:,:,:]*B[j,:,:,:]-B[i,:,:,:]*v[j,:,:,:])
    
    #determine averages:
    Nrmean = gaussian_filter(N[0,:,:,:],filtersize,r,phi,z)
    Nphimean = gaussian_filter(N[1,:,:,:],filtersize,r,phi,z)
    Nzmean = gaussian_filter(N[2,:,:,:],filtersize,r,phi,z)
    Nmean = np.array([Nrmean,Nphimean,Nzmean])
    rhomean = gaussian_filter(rho,filtersize,r,phi,z)
    vtilde = np.zeros((3,len(rnew),len(phinew),len(znew)))
    for i in range(0,3):
        vtilde[i,:,:,:] = Nmean[i,:,:,:]/rhomean[:,:,:] 
    bmean = np.array([gaussian_filter(B[0,:,:,:],filtersize,r,phi,z),gaussian_filter(B[1,:,:,:],filtersize,r,phi,z),gaussian_filter(B[2,:,:,:],filtersize,r,phi,z)])
    Umean = gaussian_filter(U,filtersize,r,phi,z)
    #pmean = averaging_subboxes_bis(press,n,m,l,r,phi,z)
    epstilde = Umean/rhomean-0.5*(vtilde[0]**2+vtilde[1]**2+vtilde[2]**2)-0.5*(bmean[0]**2+bmean[1]**2+bmean[2]**2)/rhomean
    pmean = kappa*rhomean**(gamma1)+(gammat-1)*rhomean*(epstilde-rhomean**(gamma1-1)*kappa/(gamma1-1))
    
    
    #flux terms in terms of averaged/tilde variables: 
    Ttilde = np.zeros((3,3,len(rnew),len(phinew),len(znew)))
    Stilde = np.zeros((3,len(rnew),len(phinew),len(znew)))
    Mtilde = np.zeros((3,3,len(rnew),len(phinew),len(znew)))
    for i in range(0,3):
        Stilde[i,:,:,:] = (Umean[:,:,:]+pmean[:,:,:]+0.5*(bmean[0,:,:,:]**2+bmean[1,:,:,:]**2+bmean[2,:,:,:]**2))*vtilde[i,:,:,:]-(vtilde[0,:,:,:]*bmean[0,:,:,:]+vtilde[1,:,:,:]*bmean[1,:,:,:]+vtilde[2,:,:,:]*bmean[2,:,:,:])*bmean[i,:,:,:]
        for j in range(0,3):
            Ttilde[j,i,:,:,:] = rhomean[:,:,:]*vtilde[i,:,:,:]*vtilde[j,:,:,:]-bmean[i,:,:,:]*bmean[j,:,:,:]+delta[j,i]*(pmean[:,:,:]+0.5*(bmean[0,:,:,:]**2+bmean[1,:,:,:]**2+bmean[2,:,:,:]**2))
            Mtilde[j,i,:,:,:] = -(bmean[j,:,:,:]*vtilde[i,:,:,:]-vtilde[j,:,:,:]*bmean[i,:,:,:])
    
    #averaged flux terms
    
    Tmean = np.zeros((3,3,len(rnew),len(phinew),len(znew)))
    Smean = np.zeros((3,len(rnew),len(phinew),len(znew)))
    Mmean = np.zeros((3,3,len(rnew),len(phinew),len(znew)))
    for i in range(0,3):
        Smean[i,:,:,:] = gaussian_filter(S[i],filtersize,r,phi,z)
        for j in range(0,3):
            Tmean[i,j,:,:,:] = gaussian_filter(T[i,j],filtersize,r,phi,z)
            Mmean[i,j,:,:,:] = gaussian_filter(M[i,j],filtersize,r,phi,z)
    
    #SFS tensors
    
    tau_T = Ttilde-Tmean
    tau_S = Stilde-Smean
    tau_M = Mtilde-Mmean

    return [tau_T, tau_S, tau_M] 

#===========================================================================================
    
def sfs_tensors_theo(r,phi,z,n,m,l,v,B,press,U,rho,delta,N,gammat,kappa,gamma1):
    """
    Construction of the flux terms. Use of the averaging 
    function to get the SFS tensors. 
    """

    #flux terms
    T = np.zeros((3,3,len(r),len(phi),len(z)))
    S = np.zeros((3,len(r), len(phi), len(z)))
    M = np.zeros((3,3,len(r), len(phi), len(z)))
    
    for i in range(0,3):
        S[i,:,:,:] = (U[:,:,:]+press[:,:,:]+0.5*(B[0,:,:,:]**2+B[1,:,:,:]**2+B[2,:,:,:]**2))*v[i,:,:,:]-(v[0,:,:,:]*B[0,:,:,:]+v[1,:,:,:]*B[1,:,:,:]+v[2,:,:,:]*B[2,:,:,:])*B[i,:,:,:]
        for j in range(0,3):
            T[j,i,:,:,:] = rho[:,:,:]*v[i,:,:,:]*v[j,:,:,:]-B[i,:,:,:]*B[j,:,:,:]+delta[j,i]*(press[:,:,:]+0.5*(B[0,:,:,:]**2+B[1,:,:,:]**2+B[2,:,:,:]**2))
            M[j,i,:,:,:] = -(v[i,:,:,:]*B[j,:,:,:]-B[i,:,:,:]*v[j,:,:,:])
    
    #splitting of the coordinate arrays to divide the box
    r = splitting(r,m)
    r = np.array(r)
    phi = splitting(phi,l)
    phi = np.array(phi)
    z = splitting(z,n)
    z = np.array(z)
    #determine averages:
    Nrmean = averaging_subboxes_bis(N[0,:,:,:],n,m,l,r,phi,z)
    Nphimean = averaging_subboxes_bis(N[1,:,:,:],n,m,l,r,phi,z)
    Nzmean = averaging_subboxes_bis(N[2,:,:,:],n,m,l,r,phi,z)
    Nmean = np.array([Nrmean,Nphimean,Nzmean])
    rhomean = averaging_subboxes_bis(rho,n,m,l,r,phi,z)
    vtilde = np.zeros((3,m,l,n))
    for i in range(0,3):
        vtilde[i,:,:,:] = Nmean[i,:,:,:]/rhomean[:,:,:] 
    bmean = np.array([averaging_subboxes_bis(B[0,:,:,:],n,m,l,r,phi,z),averaging_subboxes_bis(B[1,:,:,:],n,m,l,r,phi,z),averaging_subboxes_bis(B[2,:,:,:],n,m,l,r,phi,z)])
    Umean = averaging_subboxes_bis(U,n,m,l,r,phi,z)
    #pmean = averaging_subboxes_bis(press,n,m,l,r,phi,z)
    epstilde = Umean/rhomean-0.5*(vtilde[0]**2+vtilde[1]**2+vtilde[2]**2)-0.5*(bmean[0]**2+bmean[1]**2+bmean[2]**2)/rhomean
    pmean = kappa*rhomean**(gamma1)+(gammat-1)*rhomean*(epstilde-rhomean**(gamma1-1)*kappa/(gamma1-1))
    
    
    #flux terms in terms of averaged/tilde variables: 
    Ttilde = np.zeros((3,3,m,l,n))
    Stilde = np.zeros((3,m,l,n))
    Mtilde = np.zeros((3,3,m,l,n))
    for i in range(0,3):
        Stilde[i,:,:,:] = (Umean[:,:,:]+pmean[:,:,:]+0.5*(bmean[0,:,:,:]**2+bmean[1,:,:,:]**2+bmean[2,:,:,:]**2))*vtilde[i,:,:,:]-(vtilde[0,:,:,:]*bmean[0,:,:,:]+vtilde[1,:,:,:]*bmean[1,:,:,:]+vtilde[2,:,:,:]*bmean[2,:,:,:])*bmean[i,:,:,:]
        for j in range(0,3):
            Ttilde[j,i,:,:,:] = rhomean[:,:,:]*vtilde[i,:,:,:]*vtilde[j,:,:,:]-bmean[i,:,:,:]*bmean[j,:,:,:]+delta[j,i]*(pmean[:,:,:]+0.5*(bmean[0,:,:,:]**2+bmean[1,:,:,:]**2+bmean[2,:,:,:]**2))
            Mtilde[j,i,:,:,:] = -(bmean[j,:,:,:]*vtilde[i,:,:,:]-vtilde[j,:,:,:]*bmean[i,:,:,:])
    
    #averaged flux terms
    
    Tmean = np.zeros((3,3,m,l,n))
    Smean = np.zeros((3,m,l,n))
    Mmean = np.zeros((3,3,m,l,n))
    for i in range(0,3):
        Smean[i,:,:,:] = averaging_subboxes_bis(S[i],n,m,l,r,phi,z)
        for j in range(0,3):
            Tmean[i,j,:,:,:] = averaging_subboxes_bis(T[i,j],n,m,l,r,phi,z)
            Mmean[i,j,:,:,:] = averaging_subboxes_bis(M[i,j],n,m,l,r,phi,z)
    
    #SFS tensors
    
    tau_T = Ttilde-Tmean
    tau_S = Stilde-Smean
    tau_M = Mtilde-Mmean

    return [tau_T, tau_S, tau_M]

#===========================================================================================
    
def gradients_bis(varmean,n,m,l,r,phi,z,gaussian):
    """
    Determination of the gradient by using the finite difference method. 
    In order to do it, we need to average the variables over different sub-boxes.
    At the end, we average the gradients over the whole box in order to get a single value.
    """
     
    r_split = r.reshape(m,int(len(r)/m))
    phi_split = phi.reshape(l,int(len(phi)/l))
    z_split = z.reshape(n,int(len(z)/n))
    #print('r filter:', r_split[1,0]-r_split[0,0])
    #print('phi filter:', phi_split[1,0]-phi_split[0,0])
    #print('z filter:', z_split[1,0]-z_split[0,0])
     
    #differentials for the whole box averaging
    
    if gaussian ==False:
        dr = r_split[1,0]-r_split[0,0]
        dphi = phi_split[1,0]-phi_split[0,0]
        dz = z_split[1,0]-z_split[0,0]

    #finite difference method
        dvardr = np.zeros((m,l,n))
        dvardphi = np.zeros((m,l,n))
        dvardz = np.zeros((m,l,n))
        
        lenr = m
        lenphi = l
        lenz = n
    
    elif gaussian == True:
        dr = r[1]-r[0]
        dphi = phi[1]-phi[0]
        dz = z[1]-z[0]
        
        dvardr = np.zeros((len(r),len(phi),len(z)))
        dvardphi = np.zeros((len(r),len(phi),len(z)))
        dvardz = np.zeros((len(r),len(phi),len(z)))
        
        lenr = len(r)
        lenphi = len(phi)
        lenz = len(z)
        
    for i in range(0,lenr):
        for j in range(0,lenphi):
            for k in range(0,lenz-2):
                dvardz[i,j,k+1] = (varmean[i,j,k+2]-varmean[i,j,k])/(2*dz)
        for j in range(0,lenz):
            for k in range(0,lenphi-2):
                dvardphi[i,k+1,j] = (varmean[i,k+2,j]-varmean[i,k,j])/(2*dphi)
    for i in range(0,lenphi):
        for j in range(0,lenz):
            for k in range(0,lenr-2):
                dvardr[k+1,i,j] = (varmean[k+2,i,j]-varmean[k,i,j])/(2*dr)
                
    for i in range(0,lenr):
        for j in range(0,lenphi):
            dvardz[i,j,0] = (varmean[i,j,1]-varmean[i,j,0])/dz
            dvardz[i,j,lenz-1] = (varmean[i,j,lenz-1]-varmean[i,j,lenz-2])/dz    
        for j in range(0,n):
            dvardphi[i,0,j] = (varmean[i,1,j]-varmean[i,0,j])/dphi
            dvardphi[i,lenphi-1,j] = (varmean[i,lenphi-1,j]-varmean[i,lenphi-2,j])/dphi
    for i in range(0,lenphi):
        for j in range(0,lenz):
            dvardr[0,i,j] = (varmean[1,i,j]-varmean[0,i,j])/dr
            dvardr[lenr-1,i,j] = (varmean[lenr-1,i,j]-varmean[lenr-2,i,j])/dr
 
    #GRADIENT: 
    
    #second component of the gradient : we have to divide by r (medium value in each sub-box)
    
    if gaussian == False:
        dvardphir = np.zeros((m,l,n))
        for i in range(0,m):
            dvardphir[i,:,:] = dvardphi[i,:,:]/((r_split[i,-1]+r_split[i,0])*0.5)
    
        grad = np.zeros((3,m,l,n))
        
    elif gaussian == True: 
        dvardphir = np.zeros((len(r),len(phi),len(z)))
        for i in range(0,len(r)):
            dvardphir[i,:,:] = dvardphi[i,:,:]/(r[i])
    
        grad = np.zeros((3,len(r),len(phi),len(z)))
    
    grad[0,:,:,:] = dvardr
    grad[1,:,:,:] = dvardphir
    grad[2,:,:,:] = dvardz
        
    return grad

#===========================================================================================
    
def sfs_tensors_gradient_gaussian(filtersize,r,phi,z,rnew,phinew,znew,n,m,l,B,v,rho,U,N,delta,gammat,kappa,gamma1,gaussian):
    """
    This function constructs the different SFS dictated
    by the gradient model. 
    """
    
    # chi = filter_size^2/24
    chi = (filtersize**2)/24
    
    #mean and tilde variables on which gradient will be applied
    
    Nrmean = gaussian_filter(N[0,:,:,:],filtersize,r,phi,z)
    Nphimean = gaussian_filter(N[1,:,:,:],filtersize,r,phi,z)
    Nzmean = gaussian_filter(N[2,:,:,:],filtersize,r,phi,z)
    Nmean = np.array([Nrmean,Nphimean,Nzmean])
    rhomean = gaussian_filter(rho,filtersize,r,phi,z)
    vtilde = np.zeros((3,len(rnew),len(phinew),len(znew)))
    for i in range(0,3):
        vtilde[i,:,:,:] = Nmean[i,:,:,:]/rhomean[:,:,:] 
    bmean = np.array([gaussian_filter(B[0,:,:,:],filtersize,r,phi,z),gaussian_filter(B[1,:,:,:],filtersize,r,phi,z),gaussian_filter(B[2,:,:,:],filtersize,r,phi,z)])
    Umean = gaussian_filter(U,n,m,l,r,phi,z)
    
    epstilde = Umean/rhomean-0.5*(vtilde[0]**2+vtilde[1]**2+vtilde[2]**2)-0.5*(bmean[0]**2+bmean[1]**2+bmean[2]**2)/rhomean
    pmean = kappa*rhomean**(gamma1)+(gammat-1)*rhomean*(epstilde-rhomean**(gamma1-1)*kappa/(gamma1-1))
    phitilde = Umean+pmean+0.5*(bmean[0]**2+bmean[1]**2+bmean[2]**2)
    dpdeps = (gammat-1)*rhomean
    dpdrho = kappa*gamma1*rhomean**(gamma1-1)+(gammat-1)*(epstilde-kappa*gamma1/(gamma1-1)*rhomean**(gamma1-1))
    

    #gradients
    vtildegrad = np.zeros((3,3,len(rnew),len(phinew),len(znew)))
    for i in range(0,3):
        vtildegrad[i,:,:,:,:] = gradients_bis(vtilde[i,:,:,:],n,m,l,r,phi,z,gaussian)
        
    bmeangrad = np.zeros((3,3,len(rnew),len(phinew),len(znew)))
    for i in range(0,3):        
        bmeangrad[i,:,:,:,:] = gradients_bis(bmean[i,:,:,:],n,m,l,r,phi,z,gaussian)
    
    dpdrhograd = gradients_bis(dpdrho,n,m,l,rnew,phinew,znew,gaussian)
    dpdepsgrad = gradients_bis(dpdeps,n,m,l,rnew,phinew,znew,gaussian)
    rhomeangrad = gradients_bis(rhomean,n,m,l,rnew,phinew,znew,gaussian)
    epstildegrad = gradients_bis(epstilde,n,m,l,rnew,phinew,znew,gaussian)
    phitildegrad = gradients_bis(phitilde,n,m,l,rnew,phinew,znew,gaussian)
    vbgrad = gradients_bis(vtilde[0]*bmean[0]+vtilde[1]*bmean[1]+vtilde[2]*bmean[2],n,m,l,rnew,phinew,znew,gaussian) #gradient of the scalar product
              
    #SFS tensors                  
                      
    tau_kin = np.zeros((3,3,len(rnew),len(phinew),len(znew)))
    tau_mag = np.zeros((3,3,len(rnew),len(phinew),len(znew)))
    tau_ener = np.zeros((3,len(rnew),len(phinew),len(znew)))
    tau_ind = np.zeros((3,3,len(rnew),len(phinew),len(znew)))
    tau_pres = np.zeros((len(rnew),len(phinew),len(znew)))
    

    for x in range(0,len(rnew)):
        for y in range(0,len(phinew)):
            for z in range(0,len(znew)):
                for i in range(0,3):
                    tau_ener[i,x,y,z] = -2*chi*(np.sum(phitildegrad[:,x,y,z]*vtildegrad[i,:,x,y,z])+(np.sum((bmean[i,x,y,z]*(bmean[0,x,y,z]*vtildegrad[0,:,x,y,z]+bmean[1,x,y,z]*vtildegrad[1,:,x,y,z]+bmean[2,x,y,z]*vtildegrad[2,:,x,y,z]))*rhomeangrad[:,x,y,z])-np.sum((phitilde[x,y,z]*vtildegrad[i,:,x,y,z])*rhomeangrad[:,x,y,z]))/rhomean[x,y,z]-bmean[i,x,y,z]*(bmeangrad[0,0,x,y,z]*vtildegrad[0,0,x,y,z]+bmeangrad[0,1,x,y,z]*vtildegrad[0,1,x,y,z]+bmeangrad[0,2,x,y,z]*vtildegrad[0,2,x,y,z]+bmeangrad[1,0,x,y,z]*vtildegrad[1,0,x,y,z]+bmeangrad[1,1,x,y,z]*vtildegrad[1,1,x,y,z]+bmeangrad[1,2,x,y,z]*vtildegrad[1,2,x,y,z]+bmeangrad[2,0,x,y,z]*vtildegrad[2,0,x,y,z]+bmeangrad[2,1,x,y,z]*vtildegrad[2,1,x,y,z]+bmeangrad[2,2,x,y,z]*vtildegrad[2,2,x,y,z])-np.sum(vbgrad[:,x,y,z]*bmeangrad[i,:,x,y,z]))
                    for j in range(0,3):
                        tau_kin[j,i,x,y,z] = -2*chi*rhomean[x,y,z]*np.sum(vtildegrad[i,:,x,y,z]*vtildegrad[j,:,x,y,z])
                        tau_mag[j,i,x,y,z] = -2*chi*np.sum(bmeangrad[i,:,x,y,z]*bmeangrad[j,:,x,y,z])
                        tau_ind[j,i,x,y,z] = 2*chi*(np.sum(vtildegrad[i,:,x,y,z]*bmeangrad[j,:,x,y,z])-np.sum(vtildegrad[j,:,x,y,z]*bmeangrad[i,:,x,y,z])+(bmean[i,x,y,z]*np.sum(vtildegrad[j,:,x,y,z]*rhomeangrad[:,x,y,z])-bmean[j,x,y,z]*np.sum(vtildegrad[i,:,x,y,z]*rhomeangrad[:,x,y,z]))/rhomean[x,y,z])
    
                tau_pres[x,y,z] = -chi*(np.sum(dpdrhograd[:,x,y,z]*rhomeangrad[:,x,y,z])+np.sum(dpdepsgrad[:,x,y,z]*epstildegrad[:,x,y,z])-2/(rhomean[x,y,z])*dpdeps[x,y,z]*np.sum(rhomeangrad[:,x,y,z]*epstildegrad[:,x,y,z])+bmeangrad[0,0,x,y,z]**2+bmeangrad[0,1,x,y,z]**2+bmeangrad[0,2,x,y,z]**2+bmeangrad[1,0,x,y,z]**2+bmeangrad[1,1,x,y,z]**2+bmeangrad[1,2,x,y,z]**2+bmeangrad[2,0,x,y,z]**2+bmeangrad[2,1,x,y,z]**2+bmeangrad[2,2,x,y,z]**2-dpdeps[x,y,z]/(rhomean[x,y,z])*(rhomean[x,y,z]*(vtildegrad[0,0,x,y,z]**2+vtildegrad[0,1,x,y,z]**2+vtildegrad[0,2,x,y,z]**2+vtildegrad[1,0,x,y,z]**2+vtildegrad[1,1,x,y,z]**2+vtildegrad[1,2,x,y,z]**2+vtildegrad[2,0,x,y,z]**2+vtildegrad[2,1,x,y,z]**2+vtildegrad[2,2,x,y,z]**2)+bmeangrad[0,0,x,y,z]**2+bmeangrad[0,1,x,y,z]**2+bmeangrad[0,2,x,y,z]**2+bmeangrad[1,0,x,y,z]**2+bmeangrad[1,1,x,y,z]**2+bmeangrad[1,2,x,y,z]**2+bmeangrad[2,0,x,y,z]**2+bmeangrad[2,1,x,y,z]**2+bmeangrad[2,2,x,y,z]**2))
    
    tau_T = np.zeros((3,3,len(rnew),len(phinew),len(znew)))
    tau_S = np.zeros((3,len(rnew),len(phinew),len(znew)))
    tau_M = tau_ind
    

    for x in range(0,len(rnew)):
        for y in range(0,len(phinew)): 
            for z in range(0,len(znew)):
                for k in range(0,3):
                    
                    tau_S[k,x,y,z] = tau_ener[k,x,y,z]+vtilde[k,x,y,z]*tau_pres[x,y,z]
                    
                    for i in range(0,3):
                        
                        tau_T[k,i,x,y,z] = tau_kin[k,i,x,y,z]-tau_mag[k,i,x,y,z]+delta[k,i]*tau_pres[x,y,z]
                        
                
    return [tau_T, tau_S, tau_M]

#===========================================================================================
    
def sfs_tensors_gradient(Sf,r,phi,z,n,m,l,B,v,rho,U,N,delta,gammat,kappa,gamma1):
    """
    This function constructs the different SFS dictated
    by the gradient model. 
    """
    
    gaussian = False
    
    # chi = filter_size^2/24
    dr = r[1]-r[0]
    chi = ((Sf*dr)**2)/24
    
    #splitting of the coordinate arrays to divide the box
    r = splitting(r,m)
    r = np.array(r)
    phi = splitting(phi,l)
    phi = np.array(phi)
    z = splitting(z,n)
    z = np.array(z)
    
    #mean and tilde variables on which gradient will be applied
    
    Nrmean = averaging_subboxes_bis(N[0,:,:,:],n,m,l,r,phi,z)
    Nphimean = averaging_subboxes_bis(N[1,:,:,:],n,m,l,r,phi,z)
    Nzmean = averaging_subboxes_bis(N[2,:,:,:],n,m,l,r,phi,z)
    Nmean = np.array([Nrmean,Nphimean,Nzmean])
    rhomean = averaging_subboxes_bis(rho,n,m,l,r,phi,z)
    vtilde = np.zeros((3,m,l,n))
    for i in range(0,3):
        vtilde[i,:,:,:] = Nmean[i,:,:,:]/rhomean[:,:,:] 
    bmean = np.array([averaging_subboxes_bis(B[0,:,:,:],n,m,l,r,phi,z),averaging_subboxes_bis(B[1,:,:,:],n,m,l,r,phi,z),averaging_subboxes_bis(B[2,:,:,:],n,m,l,r,phi,z)])
    Umean = averaging_subboxes_bis(U,n,m,l,r,phi,z)
    #pmean = averaging_subboxes_bis(press,n,m,l,r,phi,z)
    epstilde = Umean/rhomean-0.5*(vtilde[0]**2+vtilde[1]**2+vtilde[2]**2)-0.5*(bmean[0]**2+bmean[1]**2+bmean[2]**2)/rhomean
    pmean = kappa*rhomean**(gamma1)+(gammat-1)*rhomean*(epstilde-rhomean**(gamma1-1)*kappa/(gamma1-1))
    phitilde = Umean+pmean+0.5*(bmean[0]**2+bmean[1]**2+bmean[2]**2)
    dpdeps = (gammat-1)*rhomean
    dpdrho = kappa*gamma1*rhomean**(gamma1-1)+(gammat-1)*(epstilde-kappa*gamma1/(gamma1-1)*rhomean**(gamma1-1))
    

    #gradients
    vtildegrad = np.zeros((3,3,m,l,n))
    for i in range(0,3):
        vtildegrad[i,:,:,:,:] = gradients_bis(vtilde[i,:,:,:],n,m,l,r,phi,z,gaussian)
        
    bmeangrad = np.zeros((3,3,m,l,n))
    for i in range(0,3):        
        bmeangrad[i,:,:,:,:] = gradients_bis(bmean[i,:,:,:],n,m,l,r,phi,z,gaussian)
    
    dpdrhograd = gradients_bis(dpdrho,n,m,l,r,phi,z,gaussian)
    dpdepsgrad = gradients_bis(dpdeps,n,m,l,r,phi,z,gaussian)
    rhomeangrad = gradients_bis(rhomean,n,m,l,r,phi,z,gaussian)
    epstildegrad = gradients_bis(epstilde,n,m,l,r,phi,z,gaussian)
    phitildegrad = gradients_bis(phitilde,n,m,l,r,phi,z,gaussian)
    vbgrad = gradients_bis(vtilde[0]*bmean[0]+vtilde[1]*bmean[1]+vtilde[2]*bmean[2],n,m,l,r,phi,z,gaussian) #gradient of the scalar product
              
    #SFS tensors                  
                      
    tau_kin = np.zeros((3,3,m,l,n))
    tau_mag = np.zeros((3,3,m,l,n))
    tau_ener = np.zeros((3,m,l,n))
    tau_ind = np.zeros((3,3,m,l,n))
    tau_pres = np.zeros((m,l,n))
    

    for x in range(0,m):
        for y in range(0,l):
            for z in range(0,n):
                for i in range(0,3):
                    tau_ener[i,x,y,z] = -2*chi*(np.sum(phitildegrad[:,x,y,z]*vtildegrad[i,:,x,y,z])+(np.sum((bmean[i,x,y,z]*(bmean[0,x,y,z]*vtildegrad[0,:,x,y,z]+bmean[1,x,y,z]*vtildegrad[1,:,x,y,z]+bmean[2,x,y,z]*vtildegrad[2,:,x,y,z]))*rhomeangrad[:,x,y,z])-np.sum((phitilde[x,y,z]*vtildegrad[i,:,x,y,z])*rhomeangrad[:,x,y,z]))/rhomean[x,y,z]-bmean[i,x,y,z]*(bmeangrad[0,0,x,y,z]*vtildegrad[0,0,x,y,z]+bmeangrad[0,1,x,y,z]*vtildegrad[0,1,x,y,z]+bmeangrad[0,2,x,y,z]*vtildegrad[0,2,x,y,z]+bmeangrad[1,0,x,y,z]*vtildegrad[1,0,x,y,z]+bmeangrad[1,1,x,y,z]*vtildegrad[1,1,x,y,z]+bmeangrad[1,2,x,y,z]*vtildegrad[1,2,x,y,z]+bmeangrad[2,0,x,y,z]*vtildegrad[2,0,x,y,z]+bmeangrad[2,1,x,y,z]*vtildegrad[2,1,x,y,z]+bmeangrad[2,2,x,y,z]*vtildegrad[2,2,x,y,z])-np.sum(vbgrad[:,x,y,z]*bmeangrad[i,:,x,y,z]))
                    for j in range(0,3):
                        tau_kin[j,i,x,y,z] = -2*chi*rhomean[x,y,z]*np.sum(vtildegrad[i,:,x,y,z]*vtildegrad[j,:,x,y,z])
                        tau_mag[j,i,x,y,z] = -2*chi*np.sum(bmeangrad[i,:,x,y,z]*bmeangrad[j,:,x,y,z])
                        tau_ind[j,i,x,y,z] = 2*chi*(np.sum(vtildegrad[i,:,x,y,z]*bmeangrad[j,:,x,y,z])-np.sum(vtildegrad[j,:,x,y,z]*bmeangrad[i,:,x,y,z])+(bmean[i,x,y,z]*np.sum(vtildegrad[j,:,x,y,z]*rhomeangrad[:,x,y,z])-bmean[j,x,y,z]*np.sum(vtildegrad[i,:,x,y,z]*rhomeangrad[:,x,y,z]))/rhomean[x,y,z])
    
                tau_pres[x,y,z] = -chi*(np.sum(dpdrhograd[:,x,y,z]*rhomeangrad[:,x,y,z])+np.sum(dpdepsgrad[:,x,y,z]*epstildegrad[:,x,y,z])-2/(rhomean[x,y,z])*dpdeps[x,y,z]*np.sum(rhomeangrad[:,x,y,z]*epstildegrad[:,x,y,z])+bmeangrad[0,0,x,y,z]**2+bmeangrad[0,1,x,y,z]**2+bmeangrad[0,2,x,y,z]**2+bmeangrad[1,0,x,y,z]**2+bmeangrad[1,1,x,y,z]**2+bmeangrad[1,2,x,y,z]**2+bmeangrad[2,0,x,y,z]**2+bmeangrad[2,1,x,y,z]**2+bmeangrad[2,2,x,y,z]**2-dpdeps[x,y,z]/(rhomean[x,y,z])*(rhomean[x,y,z]*(vtildegrad[0,0,x,y,z]**2+vtildegrad[0,1,x,y,z]**2+vtildegrad[0,2,x,y,z]**2+vtildegrad[1,0,x,y,z]**2+vtildegrad[1,1,x,y,z]**2+vtildegrad[1,2,x,y,z]**2+vtildegrad[2,0,x,y,z]**2+vtildegrad[2,1,x,y,z]**2+vtildegrad[2,2,x,y,z]**2)+bmeangrad[0,0,x,y,z]**2+bmeangrad[0,1,x,y,z]**2+bmeangrad[0,2,x,y,z]**2+bmeangrad[1,0,x,y,z]**2+bmeangrad[1,1,x,y,z]**2+bmeangrad[1,2,x,y,z]**2+bmeangrad[2,0,x,y,z]**2+bmeangrad[2,1,x,y,z]**2+bmeangrad[2,2,x,y,z]**2))
    
    tau_T = np.zeros((3,3,m,l,n))
    tau_S = np.zeros((3,m,l,n))
    tau_M = tau_ind
    

    for x in range(0,m):
        for y in range(0,l): 
            for z in range(0,n):
                for k in range(0,3):
                    
                    tau_S[k,x,y,z] = tau_ener[k,x,y,z]+vtilde[k,x,y,z]*tau_pres[x,y,z]
                    
                    for i in range(0,3):
                        
                        tau_T[k,i,x,y,z] = tau_kin[k,i,x,y,z]-tau_mag[k,i,x,y,z]+delta[k,i]*tau_pres[x,y,z]
                        
                
    return [tau_T, tau_S, tau_M]

#===========================================================================================
    
def sfs_comp(m,l,n,filtersize,lent,r,phi,z,rnew,phinew,znew,Sf,direc,gaussian):
    #TIME EVOLUTION 

    if gaussian == False :
        lenr = m
        lenphi = l
        lenz = n
    elif gaussian == True:
        lenr = len(r)
        lenphi = len(phi)
        lenz = len(z)
        
    tau_T_theo = np.zeros((lent,3,3,lenr,lenphi,lenz))
    tau_S_theo = np.zeros((lent,3,lenr,lenphi,lenz))
    tau_M_theo = np.zeros((lent,3,3,lenr,lenphi,lenz))
    tau_T_grad = np.zeros((lent,3,3,lenr,lenphi,lenz))
    tau_S_grad = np.zeros((lent,3,lenr,lenphi,lenz))
    tau_M_grad = np.zeros((lent,3,3,lenr,lenphi,lenz))
    
    
    delta = np.identity(3) #kronecker delta 
    kappacgs = 4.8974894*10**(14) 
    gamma1 = 1.31
    gammat = 1.5 #es lo que sale en la tesis (martin dijo 2.5)
    kappa = kappacgs*8.26110825*10**(-50)/((7.42471382*10**(-29))**(gamma1))

    
    for x in range(0,lent):
        
        if direc == '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/DATA/A100_100_34/':
            arx = format(10*x, "03")
            listvar=read_data(arx)
        else :
            arx = format(25*x, "04")
            listvar = readh5files(direc+'mri-'+str(arx)+'.h5')

        B = [listvar[4],listvar[5],listvar[6]]
        B = np.array(B)*2.874214371*10**(-25)/((4*3.141592653589793)**(0.5))     
        v = [listvar[7],listvar[8],listvar[9]] 
        v = np.array(v)*3.33564095*10**(-11)
        rho = listvar[10]*7.42471382*10**(-29)
        N = np.zeros((3,len(r),len(phi),len(z)))
        press = listvar[11]*8.26110825*10**(-50)
        
        
#let's determine the specific internal energy

        pressp = kappa*rho**(gamma1)
        ep = kappa/(gamma1-1)*rho**(gamma1-1) 
        presst = press-pressp
        et = presst/(rho*(gammat-1)) 
        eps = (ep+et)
    
    
        U = rho*eps+rho*0.5*(v[0]**2+v[1]**2+v[2]**2)+0.5*(B[0]**2+B[1]**2+B[2]**2) #energy density
        
        for j in range(0,3):
            N[j] = rho*v[j]
        
        if gaussian == False:
            
            taustheo = sfs_tensors_theo(r,phi,z,n,m,l,v,B,press,U,rho,delta,N,gammat,kappa,gamma1)
            tausgrad = sfs_tensors_gradient(Sf,r,phi,z,n,m,l,B,v,rho,U,N,delta,gammat,kappa,gamma1)
            
        elif gaussian == True :
            
            taustheo = sfs_tensors_theo_gaussian(r,phi,z,rnew,phinew,znew,v,B,press,U,rho,delta,N,gammat,kappa,gamma1,filtersize)
            tausgrad = sfs_tensors_gradient_gaussian(filtersize,r,phi,z,rnew,phinew,znew,n,m,l,B,v,rho,U,N,delta,gammat,kappa,gamma1,gaussian)
    
        tau_T_theo[x] = taustheo[0]
        tau_S_theo[x] = taustheo[1]
        
        tau_M_theo[x] = taustheo[2]
        
        tau_T_grad[x] = tausgrad[0]
        tau_S_grad[x] = tausgrad[1]
        tau_M_grad[x] = tausgrad[2]
        
       
    return [tau_T_theo, tau_S_theo, tau_M_theo, tau_T_grad, tau_S_grad, tau_M_grad]

#===========================================================================================
    
    #FILE CREATION
def file_sfs(taus,m,l,n,Sf,r,phi,z): 
    
    tau_T_theo = taus[0]
    tau_T_grad = taus[3]
    tau_S_theo = taus[1]
    tau_M_theo = taus[2]
    tau_S_grad = taus[4]
    tau_M_grad = taus[5]

     

    with open('tauTteo.csv', 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        for a in range(0,m):
            for b in range(0,l):
                for c in range(0,n):
                    col1 = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59','60']
                    col2 = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17']
                    col3 = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25']
                    cols = zip(col1,tau_T_theo[:,0,0,a,b,c],tau_T_theo[:,0,1,a,b,c],tau_T_theo[:,0,2,a,b,c],tau_T_theo[:,1,1,a,b,c],tau_T_theo[:,1,2,a,b,c],tau_T_theo[:,2,2,a,b,c])
                    writer.writerows(cols)
        f.close()

    with open('tauTgrad.csv', 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        for a in range(0,m):
            for b in range(0,l):
                for c in range(0,n):
                    col1 = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59','60']
                    col2 = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17']
                    col3 = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25']
                    cols = zip(col1,tau_T_grad[:,0,0,a,b,c],tau_T_grad[:,0,1,a,b,c],tau_T_grad[:,0,2,a,b,c],tau_T_grad[:,1,1,a,b,c],tau_T_grad[:,1,2,a,b,c],tau_T_grad[:,2,2,a,b,c])
                    writer.writerows(cols)
        f.close()
              
    with open('tauSteo.csv', 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        for a in range(0,m):
            for b in range(0,l):
                for c in range(0,n):
                    col1 = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59','60']
                    col2 = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17']
                    col3 = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25']
                    cols = zip(col1,tau_S_theo[:,0,a,b,c],tau_S_theo[:,1,a,b,c],tau_S_theo[:,2,a,b,c])
                    writer.writerows(cols)
        f.close()
        
    with open('tauSgrad.csv', 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        for a in range(0,m):
            for b in range(0,l):
                for c in range(0,n):
                    col1 = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59','60']
                    col2 = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17']
                    col3 = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25']
                    cols = zip(col1,tau_S_grad[:,0,a,b,c],tau_S_grad[:,1,a,b,c],tau_S_grad[:,2,a,b,c])
                    writer.writerows(cols)
        f.close()
              
    with open('tauMteo.csv', 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        for a in range(0,m):
            for b in range(0,l):
                for c in range(0,n):
                    col1 = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59','60']
                    col2 = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17']
                    col3 = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25']
                    cols = zip(col1,tau_M_theo[:,0,1,a,b,c],tau_M_theo[:,0,2,a,b,c],tau_M_theo[:,1,2,a,b,c])
                    writer.writerows(cols)
        f.close() 
    
    with open('tauMgrad.csv', 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        for a in range(0,m):
            for b in range(0,l):
                for c in range(0,n):
                    col1 = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59','60']
                    col2 = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17']
                    col3 = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25']
                    cols = zip(col1,tau_M_grad[:,0,1,a,b,c],tau_M_grad[:,0,2,a,b,c],tau_M_grad[:,1,2,a,b,c])
                    writer.writerows(cols)
        f.close()      
    
    return()   
    
#===========================================================================================
 #%%   
def pearson(Sf,lenr,lenphi,lenz,lent,tau_T_theo,tau_T_grad,tau_S_theo,tau_S_grad,tau_M_theo,tau_M_grad,t_turb,t_final):
 
    print('PEARSON COEFF')
    corr_tauT = np.zeros((lent,3,3))
    corr_tauS = np.zeros((lent,3))
    corr_tauM = np.zeros((lent,3,3))
    
    tau_S_theo_corr = np.zeros((lent,lenr*lenphi*lenz,3))
    tau_S_grad_corr = np.zeros((lent,lenr*lenphi*lenz,3))        
    tau_T_theo_corr = np.zeros((lent,lenr*lenphi*lenz,3,3))
    tau_T_grad_corr = np.zeros((lent,lenr*lenphi*lenz,3,3))
    tau_M_theo_corr = np.zeros((lent,lenr*lenphi*lenz,3,3))
    tau_M_grad_corr = np.zeros((lent,lenr*lenphi*lenz,3,3))
     
    
    for x in range(0,lent):
        for i in range(0,3):
            tau_S_theo_corr[x,:,i] = tau_S_theo[x,i,:,:,:].reshape(lenr*lenphi*lenz)
            tau_S_grad_corr[x,:,i] = tau_S_grad[x,i,:,:,:,].reshape(lenr*lenphi*lenz)

            for j in range(0,3):
                tau_T_theo_corr[x,:,i,j] = tau_T_theo[x,i,j,:,:,:].reshape(lenr*lenphi*lenz)
                tau_T_grad_corr[x,:,i,j] = tau_T_grad[x,i,j,:,:,:].reshape(lenr*lenphi*lenz)
                tau_M_theo_corr[x,:,i,j] = tau_M_theo[x,i,j,:,:,:].reshape(lenr*lenphi*lenz)
                tau_M_grad_corr[x,:,i,j] = tau_M_grad[x,i,j,:,:,:].reshape(lenr*lenphi*lenz)

                
                

    corr_tauT_av = np.zeros(lent)
    corr_tauS_av = np.zeros(lent)
    corr_tauM_av = np.zeros(lent)

    for x in range(0,lent):
        for i in range(0,3):
            corr_tauS[x,i], ps = pearsonr(tau_S_theo_corr[x,:,i],tau_S_grad_corr[x,:,i])
            for j in range(0,3):
                corr_tauT[x,i,j], pt = pearsonr(tau_T_theo_corr[x,:,i,j],tau_T_grad_corr[x,:,i,j])
                corr_tauM[x,i,j], pm = pearsonr(tau_M_theo_corr[x,:,i,j],tau_M_grad_corr[x,:,i,j])
                
        corr_tauT_av[x] = (corr_tauT[x,0,0]+corr_tauT[x,1,1]+corr_tauT[x,2,2]+corr_tauT[x,0,1]+corr_tauT[x,0,2]+corr_tauT[x,1,2])/6
        corr_tauM_av[x] = (corr_tauM[x,0,1]+corr_tauM[x,0,2]+corr_tauM[x,1,2])/3
        corr_tauS_av[x] = np.sum(corr_tauS[x,:])/3
 
    #let's divide now between turbulent regime and the growth of the turbulence
    
    t = np.linspace(0,t_final*10,lent)
    print(t_final)
    index = np.where(t == t_turb*10)
    print('INDEX : ', index[0])
    index = int(index[0])
    
    tau_T_theo_turb = np.zeros((lent-index,lenr*lenphi*lenz,3,3))
    tau_T_grad_turb = np.zeros((lent-index,lenr*lenphi*lenz,3,3))
    tau_S_theo_turb = np.zeros((lent-index,lenr*lenphi*lenz,3))
    tau_S_grad_turb = np.zeros((lent-index,lenr*lenphi*lenz,3))
    tau_M_theo_turb = np.zeros((lent-index,lenr*lenphi*lenz,3,3))
    tau_M_grad_turb = np.zeros((lent-index,lenr*lenphi*lenz,3,3))
    
    tau_T_theo_nonturb = np.zeros((index,lenr*lenphi*lenz,3,3))
    tau_T_grad_nonturb = np.zeros((index,lenr*lenphi*lenz,3,3))
    tau_S_theo_nonturb = np.zeros((index,lenr*lenphi*lenz,3))
    tau_S_grad_nonturb = np.zeros((index,lenr*lenphi*lenz,3))
    tau_M_theo_nonturb = np.zeros((index,lenr*lenphi*lenz,3,3))
    tau_M_grad_nonturb = np.zeros((index,lenr*lenphi*lenz,3,3))
    
    for x in range(0,lent-index):
        tau_S_theo_turb[x,:,:] = tau_S_theo_corr[x+index,:,:]
        tau_S_grad_turb[x,:,:] = tau_S_grad_corr[x+index,:,:]
        tau_T_theo_turb[x,:,:,:] = tau_T_theo_corr[x+index,:,:,:]
        tau_T_grad_turb[x,:,:,:] = tau_T_grad_corr[x+index,:,:,:]
        tau_M_theo_turb[x,:,:,:] = tau_M_theo_corr[x+index,:,:,:]
        tau_M_grad_turb[x,:,:,:] = tau_M_grad_corr[x+index,:,:,:]
                
    for x in range(0,index):
        tau_S_theo_nonturb[x,:,:] = tau_S_theo_corr[x,:,:]
        tau_S_grad_nonturb[x,:,:] = tau_S_grad_corr[x,:,:]
        tau_T_theo_nonturb[x,:,:,:] = tau_T_theo_corr[x,:,:,:]
        tau_T_grad_nonturb[x,:,:,:] = tau_T_grad_corr[x,:,:,:]
        tau_M_theo_nonturb[x,:,:,:] = tau_M_theo_corr[x,:,:,:]
        tau_M_grad_nonturb[x,:,:,:] = tau_M_grad_corr[x,:,:,:]


    corr_tauT3 = np.zeros((3,3))
    corr_tauS3 = np.zeros(3)
    corr_tauM3 = np.zeros((3,3))
    
    tau_S_theo_turb = tau_S_theo_turb.reshape((lent-index)*lenr*lenphi*lenz,3)    
    tau_S_grad_turb = tau_S_grad_turb.reshape((lent-index)*lenr*lenphi*lenz,3) 
    tau_T_theo_turb = tau_T_theo_turb.reshape((lent-index)*lenr*lenphi*lenz,3,3) 
    tau_T_grad_turb = tau_T_grad_turb.reshape((lent-index)*lenr*lenphi*lenz,3,3) 
    tau_M_theo_turb = tau_M_theo_turb.reshape((lent-index)*lenr*lenphi*lenz,3,3) 
    tau_M_grad_turb = tau_M_grad_turb.reshape((lent-index)*lenr*lenphi*lenz,3,3)
    
    tau_S_theo_nonturb = tau_S_theo_nonturb.reshape((index)*lenr*lenphi*lenz,3)    
    tau_S_grad_nonturb = tau_S_grad_nonturb.reshape((index)*lenr*lenphi*lenz,3) 
    tau_T_theo_nonturb = tau_T_theo_nonturb.reshape((index)*lenr*lenphi*lenz,3,3) 
    tau_T_grad_nonturb = tau_T_grad_nonturb.reshape((index)*lenr*lenphi*lenz,3,3) 
    tau_M_theo_nonturb = tau_M_theo_nonturb.reshape((index)*lenr*lenphi*lenz,3,3) 
    tau_M_grad_nonturb = tau_M_grad_nonturb.reshape((index)*lenr*lenphi*lenz,3,3)
                                               
    for i in range(0,3): 
        corr_tauS3[i], ps3 = pearsonr(tau_S_theo_turb[:,i],tau_S_grad_turb[:,i])
        for j in range(0,3):
            corr_tauT3[i,j], pt3 = pearsonr(tau_T_theo_turb[:,i,j],tau_T_grad_turb[:,i,j])
            corr_tauM3[i,j], pm3 = pearsonr(tau_M_theo_turb[:,i,j],tau_M_grad_turb[:,i,j])
            
    
    corr_tauT_av3 = (corr_tauT3[0,0]+corr_tauT3[1,1]+corr_tauT3[2,2]+corr_tauT3[0,1]+corr_tauT3[0,2]+corr_tauT3[1,2])/6
    corr_tauM_av3 = (corr_tauM3[0,1]+corr_tauM3[0,2]+corr_tauM3[1,2])/3
    corr_tauS_av3 = np.sum(corr_tauS3[:])/3
    
    
    corr_tauT4 = np.zeros((3,3))
    corr_tauS4 = np.zeros(3)
    corr_tauM4 = np.zeros((3,3))
    
        
    for i in range(0,3): 
        corr_tauS4[i], ps4 = pearsonr(tau_S_theo_nonturb[:,i],tau_S_grad_nonturb[:,i])
        for j in range(0,3):
            corr_tauT4[i,j], pt4 = pearsonr(tau_T_theo_nonturb[:,i,j],tau_T_grad_nonturb[:,i,j])
            corr_tauM4[i,j], pm4 = pearsonr(tau_M_theo_nonturb[:,i,j],tau_M_grad_nonturb[:,i,j])
            
    
    corr_tauT_av4 = (corr_tauT4[0,0]+corr_tauT4[1,1]+corr_tauT4[2,2]+corr_tauT4[0,1]+corr_tauT4[0,2]+corr_tauT4[1,2])/6
    corr_tauM_av4 = (corr_tauM4[0,1]+corr_tauM4[0,2]+corr_tauM4[1,2])/3
    corr_tauS_av4 = np.sum(corr_tauS4[:])/3

    return corr_tauT_av, corr_tauS_av, corr_tauM_av, corr_tauT_av3, corr_tauS_av3, corr_tauM_av3, corr_tauT_av4, corr_tauS_av4, corr_tauM_av4

def pearson_file(corrs,Sf,lenr,lenphi,lenz,lent):
    
    corr_tauT_av = corrs[0]
    corr_tauS_av = corrs[1]
    corr_tauM_av = corrs[2]
    corr_tauT_av3 = corrs[3]
    corr_tauS_av3 = corrs[4]
    corr_tauM_av3 = corrs[5]
    corr_tauT_av4 = corrs[6]
    corr_tauS_av4 = corrs[7]
    corr_tauM_av4 = corrs[8]

    
    
    with open('pearson.txt', 'w') as f:
        for t in range(0,lent):
            f.write('%f\t%f\t%f\n'%(corr_tauT_av[t],corr_tauS_av[t],corr_tauM_av[t]))
            
            
    
    
    with open('pearson_split.txt','w') as f3:
        f3.write('%f\t%f\t%f\n'%(corr_tauT_av3,corr_tauS_av3,corr_tauM_av3))
        f3.write('%f\t%f\t%f\n'%(corr_tauT_av4,corr_tauS_av4,corr_tauM_av4))
        
    
    
    return()
#%%     

def main():
    
    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/DATA/"
    gaussian = True
    
    Sf = 16
            
    lenphi = 100
    
    if lenphi == 400 :
        
        direc = path_data+'A100_400_100_B_4.6e13_v_4.45d8_r_4.45d8_Lz_1.0_INTEL_stencil/'
    
        lenr = 100
        lenz = 100
        t_turb = 8.5
        lent = 17
        t_final = 8.5
    
    elif lenphi == 800 : 
        
        direc = path_data+'A200_800_200_B_4.6e13_v_4.45d8_r_4.45d8_Lz_1.0_INTEL_stencil/'
        
        lenr = 200
        lenz = 200
        t_turb = 11
        lent = 26
        t_final = 12.5
    
    elif lenphi == 240 :
        
        direc = path_data+'A60_240_60_B_4.6e13_v_4.45d8_r_4.45d8_Lz_1.0_INTEL_stencil_flex/'
        
        lenr = 60
        lenz = 60
        t_turb = 11.5
        lent = 61
        t_final = 30
    
    elif lenphi == 100 :
        
        direc = path_data+'A100_100_34/'
        
        lenr = 100
        lenz = 34
        t_turb = 4.8
        lent = 61
        t_final = 6
     
        
    m = int(lenr/Sf)
    l = int(lenphi/Sf)
    n = int(lenz/Sf)
    
    if lenphi == 100 :
        var = read_data('000')
        
    else :
        var = readh5files(direc+'mri-0000.h5')
    
    r = var[1].astype(np.float64)
    print('r TYPE : ', r.dtype)
    phi = var[2].astype(np.float64)
    z = var[3].astype(np.float64)
    
    cellsize = (r[1]-r[0]+(r[-1]+r[0])*0.5*(phi[1]-phi[0])+z[1]-z[0])/3
    filtersize = Sf*cellsize
    
    entries = os.listdir(direc)
    print(entries)
    entries = sorted(entries)
    print(entries)
    

    if gaussian == False: 
        
        print('BOX FILTER')
    
        
        print('m,l,n : ',m,l,n)
    
        taus = sfs_comp(m,l,n,filtersize,lent,r,phi,z,Sf,direc,gaussian)
            
        file_sfs(taus,m,l,n,Sf,r,phi,z)
    
        path = 'Sf_'+str(Sf)+'/'+str(lenr)+'_'+str(lenphi)+'_'+str(lenz)+'/sfs_tensors'
        
        pathfiles = path+'/text_files'
    
        try:
            os.makedirs(pathfiles)
        except OSError as error:
            print(error)
    
        os.system('mv tau* '+pathfiles+'/.')
        
        
    else : 
             
        rsplit = np.delete(r, np.where((r -r[0] <= filtersize) & (r[-1]-r <= filtersize)))
        phisplit = np.delete(phi, np.where(((r[-1]+r[0])*0.5*(phi-phi[0]) <= filtersize) & ((r[-1]+r[0])*0.5*(phi[-1]-phi) <= filtersize)))
        zsplit = np.delete(z, np.where((z -z[0] <= filtersize) & (z[-1]-z <= filtersize)))        
        
        lenrnew = len(rsplit)
        lenphinew = len(phisplit)
        lenznew = len(zsplit)
        
        print('Filter size: ', filtersize,' cm')
        
        sigma = filtersize/(12)**(0.5)
        taus = sfs_comp(m,l,n,filtersize,lent,r,phi,z,rsplit,phisplit,zsplit,Sf,direc,gaussian)
        
        tau_T_theo = taus[0]
        tau_T_grad = taus[3]
        tau_S_theo = taus[1]
        tau_M_theo = taus[2]
        tau_S_grad = taus[4]
        tau_M_grad = taus[5]
        
        corrs = pearson(Sf,lenrnew,lenphinew,lenznew,lent,tau_T_theo,tau_T_grad,tau_S_theo,tau_S_grad,tau_M_theo,tau_M_grad,t_turb,t_final)
        pearson_file(corrs,Sf,lenrnew,lenphinew,lenznew,lent)
        
        path = 'Sf_'+str(Sf)+'/'+str(lenr)+'_'+str(lenphi)+'_'+str(lenz)+'/pearson_coeff/gaussian-filter'
    
        try:
            os.makedirs(path)
        except OSError as error:
            print(error)
        
        os.system('mv pearson.txt '+path+'/.')
        os.system('mv pearson_split.txt '+path+'/.')
    
    
    return()
    
    
#%%
if __name__ == "__main__":
    main()
