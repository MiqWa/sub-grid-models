#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 10 10:18:26 2020

@author: miquelmiravet
"""

## We are going to copy the text files in the local machine to do the plots
import os 

Sf = input('Sf :  ')
lenr = input('lenr :  ')
lenphi = input('lenphi :  ')
lenz = input('lenz :  ')

path = 'Sf_'+Sf+'/'+lenr+'_'+lenphi+'_'+lenz+'/sfs_tensors'
pathfiles = path+'/text_files'
    
try:
    os.makedirs(pathfiles)
except OSError as error:
    print(error)
        
os.system('scp miquel@tyrion24:/scr/miquel/MRI/Miquel/'+pathfiles+'/tau* '+pathfiles+'/.')
        
