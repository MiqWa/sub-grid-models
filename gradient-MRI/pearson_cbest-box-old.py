#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  1 16:26:50 2020

@author: miquelmiravet
"""

"""

Now we can compute the Pearson correlation coefficient as done in the paper to 
check the correlation between the SFS and SGS tensors. We can do it for each component.

We also calculate de C best fit parameter. 

"""

import os 
import numpy as np
from scipy.stats import pearsonr
import h5py
from scipy.io import FortranFile

def read_file(arx,path,m,l,n,lent):
        
    if arx == 'tauTteo' or arx == 'tauTgrad':   
        var = np.loadtxt(path+arx+'.csv', usecols=(1,2,3,4,5,6))
        
        var = np.swapaxes(var,0,1)
        
        var = var.reshape(6,m,l,n,lent)
        
        out = np.zeros((lent,3,3,m,l,n))
        for x in range(0,lent):
            out[x,0,0,:,:,:] = var[0,:,:,:,x]
            out[x,0,1,:,:,:] = var[1,:,:,:,x]
            out[x,0,2,:,:,:] = var[2,:,:,:,x]
            out[x,1,1,:,:,:] = var[3,:,:,:,x]
            out[x,1,2,:,:,:] = var[4,:,:,:,x]
            out[x,2,2,:,:,:] = var[5,:,:,:,x]
            out[x,1,0,:,:,:] = var[1,:,:,:,x]
            out[x,2,0,:,:,:] = var[2,:,:,:,x]
            out[x,2,1,:,:,:] = var[4,:,:,:,x]
        
    elif arx == 'tauSteo' or arx == 'tauSgrad':
        var = np.loadtxt(path+arx+'.csv', usecols=(1,2,3))
        
        var = np.swapaxes(var,0,1)
        
        var = var.reshape(3,m,l,n,lent)
        
        out = np.zeros((lent,3,m,l,n))
        
        for x in range(0,lent):
            out[x,:,:,:,:] = var[:,:,:,:,x]
    
    elif arx == 'tauMteo' or arx == 'tauMgrad': 
        var = np.loadtxt(path+arx+'.csv', usecols=(1,2,3))
        
        var = np.swapaxes(var,0,1)
        
        var = var.reshape(3,m,l,n,lent)
        
        out = np.zeros((lent,3,3,m,l,n))
        
        for x in range(0,lent):
            
            out[x,0,1,:,:,:] = var[0,:,:,:,x]
            out[x,0,2,:,:,:] = var[1,:,:,:,x]
            out[x,1,2,:,:,:] = var[2,:,:,:,x]
            out[x,1,0,:,:,:] = -var[0,:,:,:,x]
            out[x,2,0,:,:,:] = -var[1,:,:,:,x]
            out[x,2,1,:,:,:] = -var[2,:,:,:,x]
        
    return out

def pearson(Sf,m,l,n,lent,tau_T_theo,tau_T_grad,tau_S_theo,tau_S_grad,tau_M_theo,tau_M_grad,t_turb,t_final):
 
    corr_tauT = np.zeros((lent,3,3))
    corr_tauS = np.zeros((lent,3))
    corr_tauM = np.zeros((lent,3,3))
    
    tau_S_theo_corr = np.zeros((lent,3,m*l*n))
    tau_S_grad_corr = np.zeros((lent,3,m*l*n))        
    tau_T_theo_corr = np.zeros((lent,3,3,m*l*n))
    tau_T_grad_corr = np.zeros((lent,3,3,m*l*n))
    tau_M_theo_corr = np.zeros((lent,3,3,m*l*n))
    tau_M_grad_corr = np.zeros((lent,3,3,m*l*n))
     
    
    for x in range(0,lent):
        for i in range(0,3):
            tau_S_theo_corr[x,i,:] = tau_S_theo[x,i,:,:,:].reshape(m*l*n)
            tau_S_grad_corr[x,i,:] = tau_S_grad[x,i,:,:,:].reshape(m*l*n)

            for j in range(0,3):
                tau_T_theo_corr[x,i,j,:] = tau_T_theo[x,i,j,:,:,:].reshape(m*l*n)
                tau_T_grad_corr[x,i,j,:] = tau_T_grad[x,i,j,:,:,:].reshape(m*l*n)
                tau_M_theo_corr[x,i,j,:] = tau_M_theo[x,i,j,:,:,:].reshape(m*l*n)
                tau_M_grad_corr[x,i,j,:] = tau_M_grad[x,i,j,:,:,:].reshape(m*l*n)
  

    corr_tauT_av = np.zeros(lent)
    corr_tauS_av = np.zeros(lent)
    corr_tauM_av = np.zeros(lent)

    for x in range(0,lent):
        for i in range(0,3):
            corr_tauS[x,i], ps = pearsonr(tau_S_theo_corr[x,i,:],tau_S_grad_corr[x,i,:])
            for j in range(0,3):
                corr_tauT[x,i,j], pt = pearsonr(tau_T_theo_corr[x,i,j,:],tau_T_grad_corr[x,i,j,:])
                corr_tauM[x,i,j], pm = pearsonr(tau_M_theo_corr[x,i,j,:],tau_M_grad_corr[x,i,j,:])
                
        corr_tauT_av[x] = (corr_tauT[x,0,0]+corr_tauT[x,1,1]+corr_tauT[x,2,2]+corr_tauT[x,0,1]+corr_tauT[x,0,2]+corr_tauT[x,1,2])/6
        corr_tauM_av[x] = (corr_tauM[x,0,1]+corr_tauM[x,0,2]+corr_tauM[x,1,2])/3
        corr_tauS_av[x] = np.sum(corr_tauS[x,:])/3
 

#let's divide now between turbulent regime and the growth of the turbulence
    
    t = np.linspace(0,t_final*10,lent)
    print(t_final)
    index = np.where(t == t_turb*10)
    print('INDEX : ', index[0])
    index = int(index[0])
    
    tau_T_theo_turb = np.zeros((lent-index,m*l*n,3,3))
    tau_T_grad_turb = np.zeros((lent-index,m*l*n,3,3))
    tau_S_theo_turb = np.zeros((lent-index,m*l*n,3))
    tau_S_grad_turb = np.zeros((lent-index,m*l*n,3))
    tau_M_theo_turb = np.zeros((lent-index,m*l*n,3,3))
    tau_M_grad_turb = np.zeros((lent-index,m*l*n,3,3))
    
    tau_T_theo_nonturb = np.zeros((index,m*l*n,3,3))
    tau_T_grad_nonturb = np.zeros((index,m*l*n,3,3))
    tau_S_theo_nonturb = np.zeros((index,m*l*n,3))
    tau_S_grad_nonturb = np.zeros((index,m*l*n,3))
    tau_M_theo_nonturb = np.zeros((index,m*l*n,3,3))
    tau_M_grad_nonturb = np.zeros((index,m*l*n,3,3))
    

    for x in range(0,lent-index):
        for i in range(0,3):
            tau_S_theo_turb[x,:,i] = tau_S_theo_corr[x+index,i,:]
            tau_S_grad_turb[x,:,i] = tau_S_grad_corr[x+index,i,:]
            for j in range(0,3):
                tau_T_theo_turb[x,:,i,j] = tau_T_theo_corr[x+index,i,j,:]
                tau_T_grad_turb[x,:,i,j] = tau_T_grad_corr[x+index,i,j,:]
                tau_M_theo_turb[x,:,i,j] = tau_M_theo_corr[x+index,i,j,:]
                tau_M_grad_turb[x,:,i,j] = tau_M_grad_corr[x+index,i,j,:]
                
    for x in range(0,index):
        for i in range(0,3):         
            tau_S_theo_nonturb[x,:,i] = tau_S_theo_corr[x,i,:]
            tau_S_grad_nonturb[x,:,i] = tau_S_grad_corr[x,i,:]
            for j in range(0,3):      
                tau_T_theo_nonturb[x,:,i,j] = tau_T_theo_corr[x,i,j,:]
                tau_T_grad_nonturb[x,:,i,j] = tau_T_grad_corr[x,i,j,:]
                tau_M_theo_nonturb[x,:,i,j] = tau_M_theo_corr[x,i,j,:]
                tau_M_grad_nonturb[x,:,i,j] = tau_M_grad_corr[x,i,j,:]


    corr_tauT3 = np.zeros((3,3))
    corr_tauS3 = np.zeros(3)
    corr_tauM3 = np.zeros((3,3))
    
    tau_S_theo_turb = tau_S_theo_turb.reshape((lent-index)*m*l*n,3)    
    tau_S_grad_turb = tau_S_grad_turb.reshape((lent-index)*m*l*n,3) 
    tau_T_theo_turb = tau_T_theo_turb.reshape((lent-index)*m*l*n,3,3) 
    tau_T_grad_turb = tau_T_grad_turb.reshape((lent-index)*m*l*n,3,3) 
    tau_M_theo_turb = tau_M_theo_turb.reshape((lent-index)*m*l*n,3,3) 
    tau_M_grad_turb = tau_M_grad_turb.reshape((lent-index)*m*l*n,3,3)
    
    tau_S_theo_nonturb = tau_S_theo_nonturb.reshape((index)*m*l*n,3)    
    tau_S_grad_nonturb = tau_S_grad_nonturb.reshape((index)*m*l*n,3) 
    tau_T_theo_nonturb = tau_T_theo_nonturb.reshape((index)*m*l*n,3,3) 
    tau_T_grad_nonturb = tau_T_grad_nonturb.reshape((index)*m*l*n,3,3) 
    tau_M_theo_nonturb = tau_M_theo_nonturb.reshape((index)*m*l*n,3,3) 
    tau_M_grad_nonturb = tau_M_grad_nonturb.reshape((index)*m*l*n,3,3)
                                               
    for i in range(0,3): 
        corr_tauS3[i], ps3 = pearsonr(tau_S_theo_turb[:,i],tau_S_grad_turb[:,i])
        for j in range(0,3):
            corr_tauT3[i,j], pt3 = pearsonr(tau_T_theo_turb[:,i,j],tau_T_grad_turb[:,i,j])
            corr_tauM3[i,j], pm3 = pearsonr(tau_M_theo_turb[:,i,j],tau_M_grad_turb[:,i,j])
            
    
    corr_tauT_av3 = (corr_tauT3[0,0]+corr_tauT3[1,1]+corr_tauT3[2,2]+corr_tauT3[0,1]+corr_tauT3[0,2]+corr_tauT3[1,2])/6
    corr_tauM_av3 = (corr_tauM3[0,1]+corr_tauM3[0,2]+corr_tauM3[1,2])/3
    corr_tauS_av3 = np.sum(corr_tauS3[:])/3
    
    
    corr_tauT4 = np.zeros((3,3))
    corr_tauS4 = np.zeros(3)
    corr_tauM4 = np.zeros((3,3))
    
        
    for i in range(0,3): 
        corr_tauS4[i], ps4 = pearsonr(tau_S_theo_nonturb[:,i],tau_S_grad_nonturb[:,i])
        for j in range(0,3):
            corr_tauT4[i,j], pt4 = pearsonr(tau_T_theo_nonturb[:,i,j],tau_T_grad_nonturb[:,i,j])
            corr_tauM4[i,j], pm4 = pearsonr(tau_M_theo_nonturb[:,i,j],tau_M_grad_nonturb[:,i,j])
            
    
    corr_tauT_av4 = (corr_tauT4[0,0]+corr_tauT4[1,1]+corr_tauT4[2,2]+corr_tauT4[0,1]+corr_tauT4[0,2]+corr_tauT4[1,2])/6
    corr_tauM_av4 = (corr_tauM4[0,1]+corr_tauM4[0,2]+corr_tauM4[1,2])/3
    corr_tauS_av4 = np.sum(corr_tauS4[:])/3

    return corr_tauT_av, corr_tauS_av, corr_tauM_av, corr_tauT_av3, corr_tauS_av3, corr_tauM_av3, corr_tauT_av4, corr_tauS_av4, corr_tauM_av4

#write files
def pearson_file(corrs,Sf,lenr,lenphi,lenz,lent):
    
    corr_tauT_av = corrs[0]
    corr_tauS_av = corrs[1]
    corr_tauM_av = corrs[2]
   
    corr_tauT_av3 = corrs[3]
    corr_tauS_av3 = corrs[4]
    corr_tauM_av3 = corrs[5]

    corr_tauT_av4 = corrs[6]
    corr_tauS_av4 = corrs[7]
    corr_tauM_av4 = corrs[8]
    
    with open('pearson.txt', 'w') as f:
        for t in range(0,lent):
            f.write('%f\t%f\t%f\n'%(corr_tauT_av[t],corr_tauS_av[t],corr_tauM_av[t]))
            
            
    
    
    with open('pearson_split.txt','w') as f3:
        #f3.write('%f\t%f\t%f\n'%(corr_tauT_av2,corr_tauS_av2,corr_tauM_av2))
        f3.write('%f\t%f\t%f\n'%(corr_tauT_av3,corr_tauS_av3,corr_tauM_av3))
        f3.write('%f\t%f\t%f\n'%(corr_tauT_av4,corr_tauS_av4,corr_tauM_av4))
        
    
    return()
    
def cbest(Sf,lenr,lenphi,lenz,lent,tau_T_theo,tau_T_grad,tau_S_theo,tau_S_grad,tau_M_theo,tau_M_grad):
    
    c_tauT = np.zeros((lent,3,3))
    c_tauS = np.zeros((lent,3))
    c_tauM = np.zeros((lent,3,3))
    c_tauT_av = np.zeros(lent)
    c_tauM_av = np.zeros(lent)
    c_tauS_av = np.zeros(lent)

    for x in range(0,lent):
        for i in range(0,3):
            c_tauS[x,i] = np.sum(tau_S_theo[i,:,:,:,x]*tau_S_grad[i,:,:,:,x])/np.sum(tau_S_grad[i,:,:,:,x]**2)
            for j in range(0,3):
                c_tauT[x,i,j] = np.sum(tau_T_theo[i,j,:,:,:,x]*tau_T_grad[i,j,:,:,:,x])/np.sum(tau_T_grad[i,j,:,:,:,x]**2)
                c_tauM[x,i,j] = np.sum(tau_M_theo[i,j,:,:,:,x]*tau_M_grad[i,j,:,:,:,x])/np.sum(tau_M_grad[i,j,:,:,:,x]**2)
    
        c_tauT_av[x] = (c_tauT[x,0,0]+c_tauT[x,1,1]+c_tauT[x,2,2]+c_tauT[x,0,1]+c_tauT[x,0,2]+c_tauT[x,1,2])/6
        c_tauM_av[x] = (c_tauM[x,0,2]+c_tauM[x,0,1]+c_tauM[x,1,2])/3
        c_tauS_av[x] = np.sum(c_tauS[x,:])/3

    return c_tauT_av, c_tauS_av, c_tauM_av

def cbest_files(cparams,Sf,lenr,lenphi,lenz,lent):
     
    c_tauT_av = cparams[0]
    c_tauS_av = cparams[1]
    c_tauM_av = cparams[2]
    
    with open('c_best.txt', 'w') as f:
        for t in range(0,lent):
            f.write('%f\t%f\t%f\n'%(c_tauT_av[t],c_tauS_av[t],c_tauM_av[t]))
            
      
    
    return()
 
#%%
def main():
    
    gaussian = False
    Sfs = [2,4,8,16]
    lenphis = [304,400,240]
    i = 1
    j = 0
    if i == 1: 
        if j == 0 : 
    #for i in range(0,2): 
       # for j in range(0,3): 

            lenphi = lenphis[i]
        
            Sf = Sfs[j]    
    
            if lenphi == 800 :
            
                t_turb = 11
                lent = 26
                t_final = 12.5
                lenr = 200
                lenz = 200
                    
            elif lenphi == 100 :
        
                t_turb = 4.8
                lent = 61
                t_final = 6
                lenr = 100
                lenz = 34
    
            elif lenphi == 240 : 
        
                t_turb = 14
                lent = 61
                t_final = 30
                lenr = 60
                lenz = 60
                   
            elif lenphi == 400 :
                
                t_turb = 13.5
                lent = 61
                t_final = 30
                lenr = 100
                lenz = 100
            
            elif lenphi == 304 :
                 
                t_turb = 13.5
                lent = 61
                t_final = 30
                lenr = 76
                lenz = 76

            m = int(lenr/Sf)
            l = int(lenphi/Sf)
            n = int(lenz/Sf)
                
            print('m = ',m)
            print('l = ',l)
            print('n = ',n)

            pathtensors = 'RESULTS/Sf_'+str(Sf)+'/'+str(lenr)+'_'+str(lenphi)+'_'+str(lenz)+'/sfs_tensors/text_files/'

            pathfile = pathtensors
            tau_T_theo = read_file('tauTteo',pathfile,m,l,n,lent)
            tau_S_theo = read_file('tauSteo',pathfile,m,l,n,lent)
            tau_M_theo = read_file('tauMteo',pathfile,m,l,n,lent)
            tau_T_grad = read_file('tauTgrad',pathfile,m,l,n,lent)
            tau_S_grad = read_file('tauSgrad',pathfile,m,l,n,lent)
            tau_M_grad = read_file('tauMgrad',pathfile,m,l,n,lent)
    
            corrs = pearson(Sf,m,l,n,lent,tau_T_theo,tau_T_grad,tau_S_theo,tau_S_grad,tau_M_theo,tau_M_grad,t_turb,t_final)
            pearson_file(corrs,Sf,m,l,n,lent)
        
            pathpearson = 'RESULTS/Sf_'+str(Sf)+'/'+str(lenr)+'_'+str(lenphi)+'_'+str(lenz)+'/pearson_coeff'
    
            try:
                os.makedirs(pathpearson)
            except OSError as error:
                print(error)
        
            os.system('mv pearson.txt '+pathpearson+'/.')
            os.system('mv pearson_split.txt '+pathpearson+'/.')
    

#cbest parameter

   # path = 'Sf_'+str(Sf)+'/'+str(lenr)+'_'+str(lenphi)+'_'+str(lenz)+'/new/c_best'
    
   # try:
  #      os.makedirs(path)
  #  except OSError as error:
   #     print(error)
        
    #os.system('scp miquel@tyrion24:/scr/miquel/MRI/Miquel/c_best.txt '+path+'/.')
        
    #os.system('mv c_best.txt '+path+'/.')
                
    return()
    
    
#%%
    
if __name__ == "__main__":

    main()


    
    
    
