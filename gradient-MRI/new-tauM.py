#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 25 12:26:23 2021

@author: miquelmiravet
"""

"""
CORRECTION OF tau_M SFS tensor

"""

import os 
import numpy as np
import h5py
import csv 
#plt.style.use('seaborn-white')
from scipy.io import FortranFile

def readh5files(file):

    print('DATA FROM FILE:      ',file)

    with h5py.File(file, "r") as f:
    # List all groups
        print("Keys: %s" % f.keys())
        a_group_key = list(f.keys())
        for i in a_group_key:
            print(i)

        Pgas = f[a_group_key[0]][...]
        print('Pgas shape    :', Pgas.shape)
        bphi = f[a_group_key[1]][...]
        print('Bphi shape    :', bphi.shape)
        br = f[a_group_key[2]][...]
        print('Br shape    :', br.shape)
        bz = f[a_group_key[3]][...]
        print('Bz shape    :', bz.shape)
        gravpot = f[a_group_key[4]][...]
        print('gravpot shape    :', gravpot.shape)
        phi = f[a_group_key[5]][...]
        print('Phi shape    :', phi.shape)
        r = f[a_group_key[6]][...]
        print('r shape    :', r.shape)
        rho = f[a_group_key[7]][...]
        print('Rho shape    :', rho.shape)
        time = f[a_group_key[8]][...]
        print('Time shape    :', time.shape)
        vphi = f[a_group_key[9]][...]
        print('vphi shape    :', vphi.shape)
        vr = f[a_group_key[10]][...]
        print('vr shape    :', vr.shape)
        vz = f[a_group_key[11]][...]
        print('vz shape    :', vz.shape)
        z = f[a_group_key[12]][...]
        print('z shape    :', z.shape)
        #time = file.replace('.h5','')
        #time = float(time[-4:])/250
        
        
        f.close()

    print('Dimensions of the grid     :',len(r)," x ",len(phi)," x ",len(z))
    print('Time           :',time,' s')
    
    return [time, r, phi, z, br, bphi, bz, vr, vphi, vz, rho, Pgas, gravpot]


def read_data(arxiu):
    
    """
    This function reads the data from
    the files, by choosing the file we 
    are interested to study.
    -Arguments: 
        arxiu: number of the file.
        
    """
    
    print('DATA FROM FILE:      ',arxiu)
    str1 = "A100_100_34/mriquel-0file.dat" 
    ubi=str1.replace('file',arxiu)
    with FortranFile(ubi,'r','>u4') as f: 
        # read time
        time=f.read_reals(dtype='>f8')
        print ("time                     = ", time)
        # read dimensions of the grid
        nr = f.read_reals(dtype='>i4')[0]
        nphi = f.read_reals(dtype='>i4')[0]
        nz = f.read_reals(dtype='>i4')[0]
        print ("nr x nphi x nz           = ",nr," x ",nphi," x ",nz)
        # read grid arrays (1D)
        r = f.read_reals(dtype='>f8')
        phi = f.read_reals(dtype='>f8')
        z = f.read_reals(dtype='>f8')
        print ("len(r), len(phi), len(z) = ",len(r), len(phi), len(z))

        # read variables on the grid (3D arrays)
        Br = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("Br                       : ",Br.shape)
        Bphi = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("Bphi                     : ",Bphi.shape)
        Bz = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("Bz                       : ",Bz.shape)
        vr = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("vr                       : ",vr.shape)
        vphi = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("vphi                     : ",vphi.shape)
        vz = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("vz                       : ",vz.shape)
        rho = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("rho                      : ",rho.shape)
        Pgas = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("Pgas                     : ",Pgas.shape)
        phi2 = np.reshape(f.read_reals(dtype='>f8'),(nr,nphi,nz),'F')
        print ("phi                      : ",phi2.shape)
         
        
    return [time, r, phi, z, Br, Bphi, Bz, vr, vphi, vz, rho, Pgas, phi2]
         
def splitting(arr,n):
    """
    Function that splits the array in order to 
    have a number of components proportional to n
    """
    frac = len(arr)/n
    if frac.is_integer() == False: 
        while True: 
            arr = np.delete(arr,len(arr)-1)
            frac_new = len(arr)/n
            if frac_new.is_integer() == True:
                break
    return arr

def averaging_subboxes_bis(var,n,m,l,r,phi,z): 
    """
    This function averages over (m,l,n) subboxes, 
    in order to determine the partial derivatives. 
    """
    
    #reshaping of the coordinate arrays into m/n subarrays of equal length
    r_split = r.reshape(m,int(len(r)/m))
    phi_split = phi.reshape(l,int(len(phi)/l))
    z_split = z.reshape(n,int(len(z)/n))
    
    var = np.array(var)
    varnew = np.zeros((len(r),len(phi),len(z)))
    
    #splitting of the variable arrays
    for i in range(0,len(r)):
        for j in range(0,len(phi)):
            varnew[i,j,:] = splitting(var[i,j,:],n)
        for k in range(0,len(z)):
            varnew[i,:,k] = splitting(var[i,:,k],l)
    for j in range(0,len(phi)):
        for k in range(0,len(z)):
            varnew[:,j,k] = splitting(var[:,j,k],m)
    
    #reshaping of the variable arrays into subarrays of equal length, corresponding to each sub-box.
    var_split = varnew.reshape(m,int(len(r)/m),l,int(len(phi)/l),n,int(len(z)/n))
    
    #AVERAGING PROCEDURE
    
    #differentials
    dr = r_split[0,1]-r_split[0,0]
    dphi = phi_split[0,1]-phi_split[0,0]
    dz = z_split[0,1]-z_split[0,0]
    
    varmean = np.zeros((m,l,n))
    
    #construction of the matrix that represents the integral differentials. It will be different for each sub-box
    dVmatrix = np.ones((m,int(len(r)/m),l,int(len(phi)/l),n,int(len(z)/n)))
    dVmatrixdef = np.zeros((m,int(len(r)/m),l,int(len(phi)/l),n,int(len(z)/n)))   
    for i in range(0,m):
        dV = r_split[i,:]*dr*dphi*dz
        for j in range(0,l):
            for k in range(0,n):
                for x in range(0,int(len(r)/m)):
                    dVmatrixdef[i,x,j,:,k,:] = dV[x]*dVmatrix[i,x,j,:,k,:]
    
    #volume of each sub-box                
    V = np.zeros((m,l,n))
    for i in range(0,m):
        for j in range(0,l):
            for k in range(0,n):
                V[i,j,k] = np.sum(dVmatrixdef[i,:,j,:,k,:])
    
    #averaging
    for i in range(0,m):
        for j in range(0,l):
            for k in range(0,n):
                varmean[i,j,k] = np.sum(dVmatrixdef[i,:,j,:,k,:]*var_split[i,:,j,:,k,:])/V[i,j,k] 
    return varmean

def averaging(var,n,m,l,r,phi,z): 
    """
    This function averages over the whole box.  
    """
    
    #reshaping of the coordinate arrays into m/l/n subarrays of equal length
    r_split = r.reshape(m,int(len(r)/m))
    phi_split = phi.reshape(l,int(len(phi)/l))
    z_split = z.reshape(n,int(len(z)/n))
    r_subbox = np.zeros(m)
    for i in range(0,m):
        r_subbox[i] = (r_split[i,-1]+r_split[i,0])/2
    

    #AVERAGING PROCEDURE
    
    #differentials
    dr = r_split[1,0]-r_split[0,0]
    dphi = phi_split[1,0]-phi_split[0,0]
    dz = z_split[1,0]-z_split[0,0]
    
    
    #construction of the matrix that represents the integral differentials. It will be different for each sub-box
    dVmatrix = np.ones((m,l,n))
    dVmatrixdef = np.zeros((m,l,n))   
    
    dV = r_subbox[:]*dr*dphi*dz
    for x in range(0,m):
        dVmatrixdef[x,:,:] = dV[x]*dVmatrix[x,:,:]
    
    #volume                
    V = np.sum(dVmatrixdef)
    
    #averaging
    varmean = np.sum(dVmatrixdef*var)/V 
    
    return varmean

def sfs_tensors_theo(r,phi,z,n,m,l,v,B,rho,delta,N):
    """
    Construction of the flux terms. Use of the averaging 
    function to get the SFS tensors. 
    """

    #flux terms
    M = np.zeros((3,3,len(r), len(phi), len(z)))
    
    for i in range(0,3):
        for j in range(0,3):
            M[i,j,:,:,:] = v[i,:,:,:]*B[j,:,:,:]-B[i,:,:,:]*v[j,:,:,:]
    
    #splitting of the coordinate arrays to divide the box
    r = splitting(r,m)
    r = np.array(r)
    phi = splitting(phi,l)
    phi = np.array(phi)
    z = splitting(z,n)
    z = np.array(z)
    #determine averages:
    Nrmean = averaging_subboxes_bis(N[0,:,:,:],n,m,l,r,phi,z)
    Nphimean = averaging_subboxes_bis(N[1,:,:,:],n,m,l,r,phi,z)
    Nzmean = averaging_subboxes_bis(N[2,:,:,:],n,m,l,r,phi,z)
    Nmean = np.array([Nrmean,Nphimean,Nzmean])
    rhomean = averaging_subboxes_bis(rho,n,m,l,r,phi,z)

    vtilde = np.zeros((3,m,l,n))
    for i in range(0,3):
        vtilde[i,:,:,:] = Nmean[i,:,:,:]/rhomean[:,:,:] 
    bmean = np.array([averaging_subboxes_bis(B[0,:,:,:],n,m,l,r,phi,z),averaging_subboxes_bis(B[1,:,:,:],n,m,l,r,phi,z),averaging_subboxes_bis(B[2,:,:,:],n,m,l,r,phi,z)])
    #pmean = averaging_subboxes_bis(press,n,m,l,r,phi,z)
    
    
    #flux terms in terms of averaged/tilde variables: 
    Mtilde = np.zeros((3,3,m,l,n))
    for i in range(0,3):
        for j in range(0,3):
            Mtilde[j,i,:,:,:] = bmean[j,:,:,:]*vtilde[i,:,:,:]-vtilde[j,:,:,:]*bmean[i,:,:,:]
    
    #averaged flux terms

    Mmean = np.zeros((3,3,m,l,n))
    for i in range(0,3):
        for j in range(0,3):
            Mmean[i,j,:,:,:] = averaging_subboxes_bis(M[i,j],n,m,l,r,phi,z)
    
    #SFS tensors
    
    tau_M = Mtilde-Mmean

    return [tau_M]    

def gradients_bis(varmean,n,m,l,r,phi,z):
    """
    Determination of the gradient by using the finite difference method. 
    In order to do it, we need to average the variables over different sub-boxes.
    At the end, we average the gradients over the whole box in order to get a single value.
    """
     
    r_split = r.reshape(m,int(len(r)/m))
    phi_split = phi.reshape(l,int(len(phi)/l))
    z_split = z.reshape(n,int(len(z)/n))
    #print('r filter:', r_split[1,0]-r_split[0,0])
    #print('phi filter:', phi_split[1,0]-phi_split[0,0])
    #print('z filter:', z_split[1,0]-z_split[0,0])
     
    #differentials for the whole box averaging
    dr = r_split[1,0]-r_split[0,0]
    dphi = phi_split[1,0]-phi_split[0,0]
    dz = z_split[1,0]-z_split[0,0]

    #finite difference method
    dvardr = np.zeros((m,l,n))
    dvardphi = np.zeros((m,l,n))
    dvardz = np.zeros((m,l,n))
    
    
    for i in range(0,m):
        for j in range(0,l):
            for k in range(0,n-2):
                dvardz[i,j,k+1] = (varmean[i,j,k+2]-varmean[i,j,k])/(2*dz)
        for j in range(0,n):
            for k in range(0,l-2):
                dvardphi[i,k+1,j] = (varmean[i,k+2,j]-varmean[i,k,j])/(2*dphi)
    for i in range(0,l):
        for j in range(0,n):
            for k in range(0,m-2):
                dvardr[k+1,i,j] = (varmean[k+2,i,j]-varmean[k,i,j])/(2*dr)
                
    for i in range(0,m):
        for j in range(0,l):
            dvardz[i,j,0] = (varmean[i,j,1]-varmean[i,j,0])/dz
            dvardz[i,j,n-1] = (varmean[i,j,n-1]-varmean[i,j,n-2])/dz    
        for j in range(0,n):
            dvardphi[i,0,j] = (varmean[i,1,j]-varmean[i,0,j])/dphi
            dvardphi[i,m-1,j] = (varmean[i,m-1,j]-varmean[i,m-2,j])/dphi
    for i in range(0,l):
        for j in range(0,n):
            dvardr[0,i,j] = (varmean[1,i,j]-varmean[0,i,j])/dr
            dvardr[m-1,i,j] = (varmean[m-1,i,j]-varmean[m-2,i,j])/dr
 
    #GRADIENT: 
    
    #second component of the gradient : we have to divide by r (medium value in each sub-box)
    dvardphir = np.zeros((m,l,n))
    for i in range(0,m):
        dvardphir[i,:,:] = dvardphi[i,:,:]/((r_split[i,-1]+r_split[i,0])*0.5)
    
    grad = np.zeros((3,m,l,n)) 
    grad[0,:,:,:] = dvardr
    grad[1,:,:,:] = dvardphir
    grad[2,:,:,:] = dvardz
        
    return grad

def sfs_tensors_gradient(Sf,r,phi,z,n,m,l,B,v,rho,N,delta):
    """
    This function constructs the different SFS dictated
    by the gradient model. 
    """
    
    # chi = filter_size^2/24
    dr = r[1]-r[0]
    chi = ((Sf*dr)**2)/24
    
    #splitting of the coordinate arrays to divide the box
    r = splitting(r,m)
    r = np.array(r)
    phi = splitting(phi,l)
    phi = np.array(phi)
    z = splitting(z,n)
    z = np.array(z)
    
    #mean and tilde variables on which gradient will be applied
    
    Nrmean = averaging_subboxes_bis(N[0,:,:,:],n,m,l,r,phi,z)
    Nphimean = averaging_subboxes_bis(N[1,:,:,:],n,m,l,r,phi,z)
    Nzmean = averaging_subboxes_bis(N[2,:,:,:],n,m,l,r,phi,z)
    Nmean = np.array([Nrmean,Nphimean,Nzmean])
    rhomean = averaging_subboxes_bis(rho,n,m,l,r,phi,z)
   
    vtilde = np.zeros((3,m,l,n))
    for i in range(0,3):
        vtilde[i,:,:,:] = Nmean[i,:,:,:]/rhomean[:,:,:] 
    bmean = np.array([averaging_subboxes_bis(B[0,:,:,:],n,m,l,r,phi,z),averaging_subboxes_bis(B[1,:,:,:],n,m,l,r,phi,z),averaging_subboxes_bis(B[2,:,:,:],n,m,l,r,phi,z)])
    #pmean = averaging_subboxes_bis(press,n,m,l,r,phi,z)  
    #gradients
    vtildegrad = np.zeros((3,3,m,l,n))
    for i in range(0,3):
        vtildegrad[i,:,:,:,:] = gradients_bis(vtilde[i,:,:,:],n,m,l,r,phi,z)
        
    bmeangrad = np.zeros((3,3,m,l,n))
    for i in range(0,3):        
        bmeangrad[i,:,:,:,:] = gradients_bis(bmean[i,:,:,:],n,m,l,r,phi,z)
    
    rhomeangrad = gradients_bis(rhomean,n,m,l,r,phi,z)
              
    #SFS tensors                  
                      

    tau_ind = np.zeros((3,3,m,l,n))

    

    for x in range(0,m):
        for y in range(0,l):
            for z in range(0,n):
                for i in range(0,3):
                    for j in range(0,3):
                        tau_ind[j,i,x,y,z] = -2*chi*(np.sum(vtildegrad[j,:,x,y,z]*bmeangrad[i,:,x,y,z])-np.sum(vtildegrad[i,:,x,y,z]*bmeangrad[j,:,x,y,z])+(bmean[j,x,y,z]*np.sum(vtildegrad[i,:,x,y,z]*rhomeangrad[:,x,y,z])-bmean[i,x,y,z]*np.sum(vtildegrad[j,:,x,y,z]*rhomeangrad[:,x,y,z]))/rhomean[x,y,z])
    
    tau_M = tau_ind                    
                
    return [tau_M]




#%%
def sfs_comp(m,l,n,lent,r,phi,z,Sf,direc):
    #TIME EVOLUTION 
    
    tau_M_theo = np.zeros((lent,3,3,m,l,n))
    tau_M_grad = np.zeros((lent,3,3,m,l,n))
    
    delta = np.identity(3) #kronecker delta 
    

    
    for x in range(0,lent):
        #arx = format(25*x, "04")
        #listvar = readh5files(direc+'mri-'+str(arx)+'.h5')
        arx = format(10*x, "03")
        listvar = read_data(arx)
        B = [listvar[4],listvar[5],listvar[6]]
        B = np.array(B)*2.874214371*10**(-25)/((4*3.141592653589793)**(0.5))     
        v = [listvar[7],listvar[8],listvar[9]] 
        v = np.array(v)*3.33564095*10**(-11)
        rho = listvar[10]*7.42471382*10**(-29)
        N = np.zeros((3,len(r),len(phi),len(z)))
       
      

        for j in range(0,3):
            N[j] = rho*v[j]

        taustheo = sfs_tensors_theo(r,phi,z,n,m,l,v,B,rho,delta,N)

        tau_M_theo[x] = taustheo[0]
        

        tausgrad = sfs_tensors_gradient(Sf,r,phi,z,n,m,l,B,v,rho,N,delta)
        
        tau_M_grad[x] = tausgrad[0]
        
        
    return [tau_M_theo, tau_M_grad]


  
  # %%  
    #FILE CREATION
def file_sfs(taus,m,l,n,Sf,r,phi,z,lent): 
    
    tau_M_theo = taus[2]
    tau_M_grad = taus[5]
              
    with open('tauMteo-new.csv', 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        for a in range(0,m):
            for b in range(0,l):
                for c in range(0,n):
                    if lent == 61:
                        col = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59','60']
                    elif lent == 26:
                        col = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25']
                    elif lent == 18:
                        col = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17']
                        
                    cols = zip(col,tau_M_theo[:,0,1,a,b,c],tau_M_theo[:,0,2,a,b,c],tau_M_theo[:,1,2,a,b,c])
                    writer.writerows(cols)
        f.close() 
    
    with open('tauMgrad-new.csv', 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        for a in range(0,m):
            for b in range(0,l):
                for c in range(0,n):
                    if lent == 61:
                        col = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59','60']
                    elif lent == 26:
                        col = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25']
                    elif lent == 18:
                        col = [str(a)+'/'+str(b)+'/'+str(c),'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17']
                        
                    cols = zip(col,tau_M_grad[:,0,1,a,b,c],tau_M_grad[:,0,2,a,b,c],tau_M_grad[:,1,2,a,b,c])
                    writer.writerows(cols)
        f.close()      

 
    return()    
    
#%%     

def main():
    
    Sf = 16
    lenr = 100
    lenphi = 100
    lenz = 34
    lent = 61

    m = int(lenr/Sf)
    l = int(lenphi/Sf)
    n = int(lenz/Sf)
    
    if lenphi == 400 :
        
        direc = 'A100_400_100_B_4.6e13_v_4.45d8_r_4.45d8_Lz_1.0_INTEL_stencil/'
    
    elif lenphi == 800 : 
        
        direc = 'A200_800_200_B_4.6e13_v_4.45d8_r_4.45d8_Lz_1.0_INTEL_stencil/'
    
    elif lenphi == 240 :
        
        direc = 'A60_240_60_B_4.6e13_v_4.45d8_r_4.45d8_Lz_1.0_INTEL_stencil_flex/'
    
    elif lenphi == 100 :
        
        direc = 'A100_100_34/'
        
    entries = os.listdir(direc)
    print(entries)
    entries = sorted(entries)
    print(entries)
    #sub-boxes

    if lenphi == 100 :
        var = read_data('000')
        
    else :
        var = readh5files(direc+'mri-0000.h5')
    
    r = var[1]
    phi = var[2]
    z = var[3]

    
    print('m,l,n : ',m,l,n)
    
    taus = sfs_comp(m,l,n,lent,r,phi,z,Sf,direc)
            
    file_sfs(taus,m,l,n,Sf,r,phi,z,lent)
    
    path = 'Sf_'+str(Sf)+'/'+str(lenr)+'_'+str(lenphi)+'_'+str(lenz)+'/sfs_tensors'
    pathfiles = path+'/text_files'
    
    try:
        os.makedirs(pathfiles)
    except OSError as error:
        print(error)
    
    os.system('mv tauM* '+pathfiles+'/.')
    
    return()
    
    
#%%
main()

