#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  1 16:26:50 2020

@author: miquelmiravet
"""

"""

Now we can compute the Pearson correlation coefficient as done in the paper to 
check the correlation between the SFS and SGS tensors. We can do it for each component.

We also calculate de C best fit parameter. 

"""

import os 
import numpy as np
from scipy.stats import pearsonr
import h5py
from scipy.io import FortranFile
from statistics import stdev
from matplotlib import pyplot as plt
import sys

def readh5files(arx):

    print('DATA FROM FILE:      ',arx)

    with h5py.File(arx, "r") as f:
    # List all groups
        print("Keys: %s" % f.keys())
        a_group_key = list(f.keys())
        for i in a_group_key:
            print(i)

        Trr = np.array(f.get('Trr'))
        Trphi = np.array(f.get('Trphi'))
        Trz = np.array(f.get('Trz'))
        Tphiphi = np.array(f.get('Tphiphi'))
        Tphiz = np.array(f.get('Tphiz'))
        Tzz = np.array(f.get('Tzz'))
        Sr = np.array(f.get('Sr'))
        Sphi = np.array(f.get('Sphi'))
        Sz = np.array(f.get('Sz'))
        Drz = np.array(f.get('Drz'))
        Drphi = np.array(f.get('Drphi'))
        Dphiz = np.array(f.get('Dphiz'))
        time = np.array(f.get('time'))

        T = np.array([Trr,Trphi,Trz,Tphiphi,Tphiz,Tzz])
        S = np.array([Sr,Sphi,Sz])
        D = np.array([Drphi,Drz,Dphiz])
        f.close()

        print('Time           :',time,' ms')

    return T,S,D

def read_file(arx,path,m,l,n,lent):
        
    with open(path+'/'+arx+'.csv', 'r') as inp, open(path+'/'+arx+'.txt', 'w') as out:
        for line in inp:
            line = line.replace(',', ':')
            out.write(line)
        
    with open(path+'/'+arx+'.txt','r') as f:
        f2 = f.readlines()
        f.close()
     
    if arx == 'tauTteo' or arx == 'tauTgrad':   
        var = np.loadtxt(f2, usecols=(1,2,3,4,5,6))
        
        var = np.swapaxes(var,0,1)
        
        var = var.reshape(6,m,l,n,lent)
        
        out = np.zeros((3,3,m,l,n,lent))
        out[0,0,:,:,:,:] = var[0,:,:,:,:]
        out[0,1,:,:,:,:] = var[1,:,:,:,:]
        out[0,2,:,:,:,:] = var[2,:,:,:,:]
        out[1,1,:,:,:,:] = var[3,:,:,:,:]
        out[1,2,:,:,:,:] = var[4,:,:,:,:]
        out[2,2,:,:,:,:] = var[5,:,:,:,:]
        out[1,0,:,:,:,:] = var[1,:,:,:,:]
        out[2,0,:,:,:,:] = var[2,:,:,:,:]
        out[2,1,:,:,:,:] = var[4,:,:,:,:]
        
    elif arx == 'tauSteo' or arx == 'tauSgrad':
        var = np.loadtxt(f2, usecols=(1,2,3))
        
        var = np.swapaxes(var,0,1)
        
        var = var.reshape(3,m,l,n,lent)
        
        out = var
    
    elif arx == 'tauMteo' or arx == 'tauMgrad': 
        var = np.loadtxt(f2, usecols=(1,2,3))
        
        var = np.swapaxes(var,0,1)
        
        var = var.reshape(3,m,l,n,lent)
        
        out = np.zeros((3,3,m,l,n,lent))
        out[0,1,:,:,:,:] = var[0,:,:,:,:]
        out[0,2,:,:,:,:] = var[1,:,:,:,:]
        out[1,2,:,:,:,:] = var[2,:,:,:,:]
        out[1,0,:,:,:,:] = -var[0,:,:,:,:]
        out[2,0,:,:,:,:] = -var[1,:,:,:,:]
        out[2,1,:,:,:,:] = -var[2,:,:,:,:]
        
    return out

def pearson(Sf,m,l,n,lent,tau_T_theo,tau_T_grad,tau_S_theo,tau_S_grad,tau_M_theo,tau_M_grad,t_turb,t_final,path):

    corr_tauT = np.zeros((lent,3,3))
    corr_tauS = np.zeros((lent,3))
    corr_tauM = np.zeros((lent,3,3))
    
    tau_S_theo_corr = np.zeros((lent,3,m*l*n))
    tau_S_grad_corr = np.zeros((lent,3,m*l*n))        
    tau_T_theo_corr = np.zeros((lent,3,3,m*l*n))
    tau_T_grad_corr = np.zeros((lent,3,3,m*l*n))
    tau_M_theo_corr = np.zeros((lent,3,3,m*l*n))
    tau_M_grad_corr = np.zeros((lent,3,3,m*l*n))
     
    
    for x in range(0,lent):
        for i in range(0,3):
            tau_S_theo_corr[x,i,:] = tau_S_theo[x,i,:,:,:].reshape(m*l*n)
            tau_S_grad_corr[x,i,:] = tau_S_grad[x,i,:,:,:].reshape(m*l*n)

            for j in range(0,3):
                tau_T_theo_corr[x,i,j,:] = tau_T_theo[x,i,j,:,:,:].reshape(m*l*n)
                tau_T_grad_corr[x,i,j,:] = tau_T_grad[x,i,j,:,:,:].reshape(m*l*n)
                tau_M_theo_corr[x,i,j,:] = tau_M_theo[x,i,j,:,:,:].reshape(m*l*n)
                tau_M_grad_corr[x,i,j,:] = tau_M_grad[x,i,j,:,:,:].reshape(m*l*n)
  

    corr_tauT_av = np.zeros(lent)
    corr_tauS_av = np.zeros(lent)
    corr_tauM_av = np.zeros(lent)

    for x in range(0,lent):
        for i in range(0,3):
            corr_tauS[x,i], ps = pearsonr(tau_S_theo_corr[x,i,:],tau_S_grad_corr[x,i,:])
            for j in range(0,3):
                corr_tauT[x,i,j], pt = pearsonr(tau_T_theo_corr[x,i,j,:],tau_T_grad_corr[x,i,j,:])
                corr_tauM[x,i,j], pm = pearsonr(tau_M_theo_corr[x,i,j,:],tau_M_grad_corr[x,i,j,:])
                
        corr_tauT_av[x] = (corr_tauT[x,0,0]+corr_tauT[x,1,1]+corr_tauT[x,2,2]+corr_tauT[x,0,1]+corr_tauT[x,0,2]+corr_tauT[x,1,2])/6
        corr_tauM_av[x] = (corr_tauM[x,0,1]+corr_tauM[x,0,2]+corr_tauM[x,1,2])/3
        corr_tauS_av[x] = np.sum(corr_tauS[x,:])/3
 
 #now, consider correlations over time as well

#    corr_tauT2 = np.zeros((3,3))
#    corr_tauS2 = np.zeros((3))
#    corr_tauM2 = np.zeros((3,3))

#    tau_S_theo_corr2 = np.zeros((lent,m*l*n,3))
#    tau_S_grad_corr2 = np.zeros((lent,m*l*n,3))
#    tau_T_theo_corr2 = np.zeros((lent,m*l*n,3,3))
#    tau_T_grad_corr2 = np.zeros((lent,m*l*n,3,3))
#    tau_M_theo_corr2 = np.zeros((lent,m*l*n,3,3))
#    tau_M_grad_corr2 = np.zeros((lent,m*l*n,3,3))
 
   
#    for x in range(0,lent):
#        for i in range(0,3):
#            tau_S_theo_corr2[x,:,i] = tau_S_theo_corr[x,i,:]
#            tau_S_grad_corr2[x,:,i] = tau_S_grad_corr[x,i,:]
#            for j in range(0,3):
#                tau_T_theo_corr2[x,:,i,j] = tau_T_theo_corr[x,i,j,:]
#                tau_T_grad_corr2[x,:,i,j] = tau_T_grad_corr[x,i,j,:]
#                tau_M_theo_corr2[x,:,i,j] = tau_M_theo_corr[x,i,j,:]
#                tau_M_grad_corr2[x,:,i,j] = tau_M_grad_corr[x,i,j,:]


#    tau_S_theo_corr2 = tau_S_theo_corr2.reshape(lent*m*l*n,3)
#    tau_S_grad_corr2 = tau_S_grad_corr2.reshape(lent*m*l*n,3)
#    tau_T_theo_corr2 = tau_T_theo_corr2.reshape(lent*m*l*n,3,3)
#    tau_T_grad_corr2 = tau_T_grad_corr2.reshape(lent*m*l*n,3,3)
#    tau_M_theo_corr2 = tau_M_theo_corr2.reshape(lent*m*l*n,3,3)
#    tau_M_grad_corr2 = tau_M_grad_corr2.reshape(lent*m*l*n,3,3)


#    for i in range(0,3):
#        corr_tauS2[i], ps2 = pearsonr(tau_S_theo_corr2[:,i],tau_S_grad_corr2[:,i])
#        for j in range(0,3):
#            corr_tauT2[i,j], pt2 = pearsonr(tau_T_theo_corr2[:,i,j],tau_T_grad_corr2[:,i,j])
#            corr_tauM2[i,j], pm2 = pearsonr(tau_M_theo_corr2[:,i,j],tau_M_grad_corr2[:,i,j])


#    corr_tauT_av2 = (corr_tauT2[0,0]+corr_tauT2[1,1]+corr_tauT2[2,2]+corr_tauT2[0,1]+corr_tauT2[0,2]+corr_tauT2[1,2])/6
#    corr_tauM_av2 = (corr_tauM2[0,1]+corr_tauM2[0,2]+corr_tauM2[1,2])/3
#    corr_tauS_av2 = np.sum(corr_tauS2[:])/3

#let's divide now between turbulent regime and the growth of the turbulence
    
    t = np.linspace(0,t_final*10,lent)
    print(t_final)
    index = np.where(t == t_turb*10)
    print('INDEX : ', index[0])
    index = int(index[0])
   
# average over time

    corr_tauT_avt = np.sum(corr_tauT_av[index:])/(lent-index)
    corr_tauS_avt = np.sum(corr_tauS_av[index:])/(lent-index)
    corr_tauM_avt = np.sum(corr_tauM_av[index:])/(lent-index)
    
    with open(path+'/pearson.txt', 'w') as f:
        for t in range(0,lent):
            f.write('%f\t%f\t%f\n'%(corr_tauT_av[t],corr_tauS_av[t],corr_tauM_av[t]))
            
    with open(path+'/pearson_av_t.txt','w') as f3:
        f3.write('%f\t%f\t%f\n'%(corr_tauT_avt,corr_tauS_avt,corr_tauM_avt))

    return()


#================================================================

def pearson_split(Sf,m,l,n,lent,tau_T_theo,tau_T_grad,tau_S_theo,tau_S_grad,tau_M_theo,tau_M_grad,t_turb,t_final):
    
    tau_T_theo_turb = np.zeros((lent-index,m*l*n,3,3))
    tau_T_grad_turb = np.zeros((lent-index,m*l*n,3,3))
    tau_S_theo_turb = np.zeros((lent-index,m*l*n,3))
    tau_S_grad_turb = np.zeros((lent-index,m*l*n,3))
    tau_M_theo_turb = np.zeros((lent-index,m*l*n,3,3))
    tau_M_grad_turb = np.zeros((lent-index,m*l*n,3,3))
    
    tau_T_theo_nonturb = np.zeros((index,m*l*n,3,3))
    tau_T_grad_nonturb = np.zeros((index,m*l*n,3,3))
    tau_S_theo_nonturb = np.zeros((index,m*l*n,3))
    tau_S_grad_nonturb = np.zeros((index,m*l*n,3))
    tau_M_theo_nonturb = np.zeros((index,m*l*n,3,3))
    tau_M_grad_nonturb = np.zeros((index,m*l*n,3,3))
    

    for x in range(0,lent-index):
        for i in range(0,3):
            tau_S_theo_turb[x,:,i] = tau_S_theo_corr[x+index,i,:]
            tau_S_grad_turb[x,:,i] = tau_S_grad_corr[x+index,i,:]
            for j in range(0,3):
                tau_T_theo_turb[x,:,i,j] = tau_T_theo_corr[x+index,i,j,:]
                tau_T_grad_turb[x,:,i,j] = tau_T_grad_corr[x+index,i,j,:]
                tau_M_theo_turb[x,:,i,j] = tau_M_theo_corr[x+index,i,j,:]
                tau_M_grad_turb[x,:,i,j] = tau_M_grad_corr[x+index,i,j,:]
                
    for x in range(0,index):
        for i in range(0,3):         
            tau_S_theo_nonturb[x,:,i] = tau_S_theo_corr[x,i,:]
            tau_S_grad_nonturb[x,:,i] = tau_S_grad_corr[x,i,:]
            for j in range(0,3):      
                tau_T_theo_nonturb[x,:,i,j] = tau_T_theo_corr[x,i,j,:]
                tau_T_grad_nonturb[x,:,i,j] = tau_T_grad_corr[x,i,j,:]
                tau_M_theo_nonturb[x,:,i,j] = tau_M_theo_corr[x,i,j,:]
                tau_M_grad_nonturb[x,:,i,j] = tau_M_grad_corr[x,i,j,:]


    corr_tauT3 = np.zeros((3,3))
    corr_tauS3 = np.zeros(3)
    corr_tauM3 = np.zeros((3,3))
    
    tau_S_theo_turb = tau_S_theo_turb.reshape((lent-index)*m*l*n,3)    
    tau_S_grad_turb = tau_S_grad_turb.reshape((lent-index)*m*l*n,3) 
    tau_T_theo_turb = tau_T_theo_turb.reshape((lent-index)*m*l*n,3,3) 
    tau_T_grad_turb = tau_T_grad_turb.reshape((lent-index)*m*l*n,3,3) 
    tau_M_theo_turb = tau_M_theo_turb.reshape((lent-index)*m*l*n,3,3) 
    tau_M_grad_turb = tau_M_grad_turb.reshape((lent-index)*m*l*n,3,3)
    
    tau_S_theo_nonturb = tau_S_theo_nonturb.reshape((index)*m*l*n,3)    
    tau_S_grad_nonturb = tau_S_grad_nonturb.reshape((index)*m*l*n,3) 
    tau_T_theo_nonturb = tau_T_theo_nonturb.reshape((index)*m*l*n,3,3) 
    tau_T_grad_nonturb = tau_T_grad_nonturb.reshape((index)*m*l*n,3,3) 
    tau_M_theo_nonturb = tau_M_theo_nonturb.reshape((index)*m*l*n,3,3) 
    tau_M_grad_nonturb = tau_M_grad_nonturb.reshape((index)*m*l*n,3,3)
                                               
    for i in range(0,3): 
        corr_tauS3[i], ps3 = pearsonr(tau_S_theo_turb[:,i],tau_S_grad_turb[:,i])
        for j in range(0,3):
            corr_tauT3[i,j], pt3 = pearsonr(tau_T_theo_turb[:,i,j],tau_T_grad_turb[:,i,j])
            corr_tauM3[i,j], pm3 = pearsonr(tau_M_theo_turb[:,i,j],tau_M_grad_turb[:,i,j])
            
    print('CORR TAU_M   = ',corr_tauM3) 
    print('CORR TAU_T   = ', corr_tauT3)
    print('CORR TAU_S   = ', corr_tauS3)
    corr_tauT_av3 = (corr_tauT3[0,0]+corr_tauT3[1,1]+corr_tauT3[2,2]+corr_tauT3[0,1]+corr_tauT3[0,2]+corr_tauT3[1,2])/6
    corr_tauM_av3 = (corr_tauM3[0,1]+corr_tauM3[0,2]+corr_tauM3[1,2])/3
    corr_tauS_av3 = np.sum(corr_tauS3[:])/3
    
    
    corr_tauT4 = np.zeros((3,3))
    corr_tauS4 = np.zeros(3)
    corr_tauM4 = np.zeros((3,3))
    
        
    for i in range(0,3): 
        corr_tauS4[i], ps4 = pearsonr(tau_S_theo_nonturb[:,i],tau_S_grad_nonturb[:,i])
        for j in range(0,3):
            corr_tauT4[i,j], pt4 = pearsonr(tau_T_theo_nonturb[:,i,j],tau_T_grad_nonturb[:,i,j])
            corr_tauM4[i,j], pm4 = pearsonr(tau_M_theo_nonturb[:,i,j],tau_M_grad_nonturb[:,i,j])
            
    
    corr_tauT_av4 = (corr_tauT4[0,0]+corr_tauT4[1,1]+corr_tauT4[2,2]+corr_tauT4[0,1]+corr_tauT4[0,2]+corr_tauT4[1,2])/6
    corr_tauM_av4 = (corr_tauM4[0,1]+corr_tauM4[0,2]+corr_tauM4[1,2])/3
    corr_tauS_av4 = np.sum(corr_tauS4[:])/3

    return corr_tauT_av3, corr_tauS_av3, corr_tauM_av3, corr_tauT_av4, corr_tauS_av4, corr_tauM_av4

#write files
def pearson_file(corrs,Sf,lenr,lenphi,lenz,lent):
    
    corr_tauT_av = corrs[0]
    corr_tauS_av = corrs[1]
    corr_tauM_av = corrs[2]
   
    corr_tauT_avt = corrs[3]
    corr_tauS_avt = corrs[4]
    corr_tauM_avt = corrs[5]
    
    with open('pearson.txt', 'w') as f:
        for t in range(0,lent):
            f.write('%f\t%f\t%f\n'%(corr_tauT_av[t],corr_tauS_av[t],corr_tauM_av[t]))
            
    with open('pearson_av_t.txt','w') as f3:
        f3.write('%f\t%f\t%f\n'%(corr_tauT_avt,corr_tauS_avt,corr_tauM_avt))
    
    return()
#===========================================================================================

def l2_log(Msim, Rsim , Fsim, Mmod, Rmod, Fmod,t, lenrf,lenphif,lenzf,res,Sf,path):
    ind = 33
    ind0 = 11
    ind_prob = 27

    dif = ind-ind0
    t2 = t[ind0:]
    t = t[ind:]
    Msimt = np.log10(abs(Msim[ind0:]))
    Mmodt = np.log10(abs(Mmod[ind0:]))
    Rsimt = np.log10(abs(Rsim[ind0:]))
    Rmodt = np.log10(abs(Rmod[ind0:]))
    Fsimt = np.log10(abs(Fsim[ind0:]))
    Fmodt = np.log10(abs(Fmod[ind0:]))
    Msimflat_t_comp = np.zeros((len(t2),6,lenrf*lenphif*lenzf))
    Mmodflat_t_comp = np.zeros((len(t2),6,lenrf*lenphif*lenzf))
    Rsimflat_t_comp = np.zeros((len(t2),3,lenrf*lenphif*lenzf))
    Rmodflat_t_comp = np.zeros((len(t2),3,lenrf*lenphif*lenzf))
    Fsimflat_t_comp = np.zeros((len(t2),3,lenrf*lenphif*lenzf))
    Fmodflat_t_comp = np.zeros((len(t2),3,lenrf*lenphif*lenzf))

    l2_M_t_comp = np.zeros((len(t2),6))
    l2_R_t_comp = np.zeros((len(t2),3))
    l2_F_t_comp = np.zeros((len(t2),3))

    for tt in range(0,len(t2)):
        for cc in range(0,6):
            Msimflat_t_comp[tt,cc,:] = Msimt[tt,cc,:,:,:].flatten()
            Mmodflat_t_comp[tt,cc,:] = Mmodt[tt,cc,:,:,:].flatten()
            l2_M_t_comp[tt,cc] = (np.sum((Msimflat_t_comp[tt,cc,:]-Mmodflat_t_comp[tt,cc,:])**2)/np.sum(Msimflat_t_comp[tt,cc,:]**2))**(0.5)
        for cc in range(0,3):
            Rsimflat_t_comp[tt,cc,:] = Rsimt[tt,cc,:,:,:].flatten()
            Rmodflat_t_comp[tt,cc,:] = Rmodt[tt,cc,:,:,:].flatten()
            l2_R_t_comp[tt,cc] = (np.sum((Rsimflat_t_comp[tt,cc,:]-Rmodflat_t_comp[tt,cc,:])**2)/np.sum(Rsimflat_t_comp[tt,cc,:]**2))**(0.5)

            Fsimflat_t_comp[tt,cc,:] = Fsimt[tt,cc,:,:,:].flatten()
            Fmodflat_t_comp[tt,cc,:] = Fmodt[tt,cc,:,:,:].flatten()
            l2_F_t_comp[tt,cc] = (np.sum((Fsimflat_t_comp[tt,cc,:]-Fmodflat_t_comp[tt,cc,:])**2)/np.sum(Fsimflat_t_comp[tt,cc,:]**2))**(0.5)

    l2M_t_mean = np.mean(l2_M_t_comp, axis = 0)
    l2R_t_mean = np.mean(l2_R_t_comp, axis = 0)
    l2F_t_mean = np.mean(l2_F_t_comp, axis = 0)
    
    l2Mrms = (np.sum(l2M_t_mean**2)/len(l2M_t_mean))**(0.5)
    l2Rrms = (np.sum(l2R_t_mean**2)/len(l2R_t_mean))**(0.5)
    l2Frms = (np.sum(l2F_t_mean**2)/len(l2F_t_mean))**(0.5)

    l2Mrms_t = (np.sum(l2_M_t_comp**2, axis = 1)/6)**(0.5)
    l2Rrms_t = (np.sum(l2_R_t_comp**2, axis = 1)/3)**(0.5)
    l2Frms_t = (np.sum(l2_F_t_comp**2, axis = 1)/3)**(0.5)
    
    with open(path+'/l2log_time_ev.txt', 'w') as f:
        f.write('# T \t S \t D #\n')
        for x in range(0,len(t2)):
            f.write('%f\t%f\t%f\t%f\n'%(t2[x],l2Mrms_t[x], l2Rrms_t[x], l2Frms_t[x]))

    with open(path+'/l2Tlog_comp_time_ev.txt', 'w') as f:
        f.write('# T_rr \t T_phiphi \t T_zz \t T_rphi \t T_rz |t T_phiz #\n')
        for x in range(0,len(t2)):
            f.write('%f\t%f\t%f\t%f\t%f\t%f\t%f\n'%(t2[x],l2_M_t_comp[x,0], l2_M_t_comp[x,3], l2_M_t_comp[x,5],l2_M_t_comp[x,1], l2_M_t_comp[x,2], l2_M_t_comp[x,4])) #FINISH THIS!!!!

    with open(path+'/l2Slog_comp_time_ev.txt', 'w') as f:
        f.write('# S_r \t S_phi \t S_z  #\n')
        for x in range(0,len(t2)):
            f.write('%f\t%f\t%f\t%f\n'%(t2[x],l2_R_t_comp[x,0], l2_R_t_comp[x,1], l2_R_t_comp[x,2]))

    with open(path+'/l2Dlog_comp_time_ev.txt', 'w') as f:
        f.write('# D_rphi \t D_rz \t D_phiz  #\n')
        for x in range(0,len(t2)):
            f.write('%f\t%f\t%f\t%f\n'%(t2[x],l2_F_t_comp[x,0], l2_F_t_comp[x,1], l2_F_t_comp[x,2]))

    with open(path+'/l2_rms.txt', 'w') as f:
        f.write('# T \t S \t D #\n')
        f.write('%f\t%f\t%f\n'%(l2Mrms,l2Rrms,l2Frms))

    return()


def c_best(Msim, Rsim , Fsim, Mmod, Rmod, Fmod,t, lenrf,lenphif,lenzf,res,Sf,path):

    ind = 33
    ind0 = 11

    dif = ind-ind0
#    Msim = np.delete(Msim, 36, axis = 0)
#    Mmod = np.delete(Mmod, 36, axis = 0)
#    Rsim = np.delete(Rsim, 36, axis = 0)
#    Rmod = np.delete(Rmod, 36, axis = 0)
#    Fsim = np.delete(Fsim, 36, axis = 0)
#    Fmod = np.delete(Fmod, 36, axis = 0)
#    t = np.delete(t, 36)

    t2 = t[ind0:]
    t = t[ind:]
    Msimt = Msim[ind0:]
    Mmodt = Mmod[ind0:]
    Rsimt = Rsim[ind0:]
    Rmodt = Rmod[ind0:]
    Fsimt = Fsim[ind0:]
    Fmodt = Fmod[ind0:]

    Msim = Msim[ind:]
    Mmod = Mmod[ind:]
    Rsim = Rsim[ind:]
    Rmod = Rmod[ind:]
    Fsim = Fsim[ind:]
    Fmod = Fmod[ind:]

#    Rsimdiag = np.delete(Rsim,[1,2,4], axis = 1)
#    Rmoddiag = np.delete(Rmod,[1,2,4], axis = 1)
#    Msimdiag = np.delete(Msim, [1,2,4], axis = 1)
#    Mmoddiag = np.delete(Mmod, [1,2,4], axis = 1)

#    Rsimdiagt = np.delete(Rsimt,[1,2,4], axis = 1)
#    Rmoddiagt = np.delete(Rmodt,[1,2,4], axis = 1)
#    Msimdiagt = np.delete(Msimt, [1,2,4], axis = 1)
#    Mmoddiagt = np.delete(Mmodt, [1,2,4], axis = 1)
#    Rsim = np.log10(abs(Rsim))
#    Rmod = np.log10(abs(Rmod))
#    Msim = np.log10(abs(Msim))
#    Mmod = np.log10(abs(Mmod))
#    Fsim = abs(Fsim)
#    Fmod = abs(Fmod)

    cbest_M_sp = np.zeros((lenrf,lenphif,lenzf))
    cbest_R_sp = np.zeros((lenrf,lenphif,lenzf))
#    cbest_Mdiag_sp = np.zeros((lenrf,lenphif,lenzf))
#    cbest_Rdiag_sp = np.zeros((lenrf,lenphif,lenzf))
    cbest_F_sp = np.zeros((lenrf,lenphif,lenzf))
    cbest_all_sp = np.zeros((lenrf,lenphif,lenzf))
#    cbest_alldiag_sp = np.zeros((lenrf,lenphif,lenzf))

    cbest_M_sp_comp = np.zeros((6,lenrf,lenphif,lenzf))
    cbest_R_sp_comp = np.zeros((3,lenrf,lenphif,lenzf))
#    cbest_Mdiag_sp_comp = np.zeros((3,lenrf,lenphif,lenzf))
#    cbest_Rdiag_sp_comp = np.zeros((3,lenrf,lenphif,lenzf))
    cbest_F_sp_comp = np.zeros((3,lenrf,lenphif,lenzf))

    cbest_M_t = np.zeros(len(t2))
    cbest_R_t = np.zeros(len(t2))
#    cbest_Mdiag_t = np.zeros(len(t2))
#    cbest_Rdiag_t = np.zeros(len(t2))
    cbest_F_t = np.zeros(len(t2))
    cbest_all_t = np.zeros(len(t2))
#    cbest_alldiag_t = np.zeros(len(t2))

    cbest_M_t_comp = np.zeros((len(t2),6))
    cbest_R_t_comp = np.zeros((len(t2),3))
#    cbest_Mdiag_t_comp = np.zeros((len(t2),3))
#    cbest_Rdiag_t_comp = np.zeros((len(t2),3))
    cbest_F_t_comp = np.zeros((len(t2),3))

    l2_M_sp = np.zeros((lenrf,lenphif,lenzf))
    l2_R_sp = np.zeros((lenrf,lenphif,lenzf))
#    l2_Mdiag_sp = np.zeros((lenrf,lenphif,lenzf))
#    l2_Rdiag_sp = np.zeros((lenrf,lenphif,lenzf))
    l2_F_sp = np.zeros((lenrf,lenphif,lenzf))
    l2_all_sp = np.zeros((lenrf,lenphif,lenzf))
#    l2_alldiag_sp = np.zeros((lenrf,lenphif,lenzf))

    l2_M_sp_comp = np.zeros((6,lenrf,lenphif,lenzf))
    l2_R_sp_comp = np.zeros((3,lenrf,lenphif,lenzf))
#    l2_Mdiag_sp_comp = np.zeros((3,lenrf,lenphif,lenzf))
#    l2_Rdiag_sp_comp = np.zeros((3,lenrf,lenphif,lenzf))
    l2_F_sp_comp = np.zeros((3,lenrf,lenphif,lenzf))

    l2_M_t = np.zeros(len(t2))
    l2_R_t = np.zeros(len(t2))
#    l2_Mdiag_t = np.zeros(len(t2))
#    l2_Rdiag_t = np.zeros(len(t2))
    l2_F_t = np.zeros(len(t2))
    l2_all_t = np.zeros(len(t2))
#    l2_alldiag_t = np.zeros(len(t2))

    l2_M_t_comp = np.zeros((len(t2),6))
    l2_R_t_comp = np.zeros((len(t2),3))
#    l2_Mdiag_t_comp = np.zeros((len(t2),3))
#    l2_Rdiag_t_comp = np.zeros((len(t2),3))
    l2_F_t_comp = np.zeros((len(t2),3))

    Msimflat_t_comp = np.zeros((len(t2),6,lenrf*lenphif*lenzf))
    Mmodflat_t_comp = np.zeros((len(t2),6,lenrf*lenphif*lenzf))
    Rsimflat_t_comp = np.zeros((len(t2),3,lenrf*lenphif*lenzf))
    Rmodflat_t_comp = np.zeros((len(t2),3,lenrf*lenphif*lenzf))
#    Msimdiagflat_t_comp = np.zeros((len(t2),3,lenrf*lenphif*lenzf))
#    Mmoddiagflat_t_comp = np.zeros((len(t2),3,lenrf*lenphif*lenzf))
#    Rsimdiagflat_t_comp = np.zeros((len(t2),3,lenrf*lenphif*lenzf))
#    Rmoddiagflat_t_comp = np.zeros((len(t2),3,lenrf*lenphif*lenzf))
    Fsimflat_t_comp = np.zeros((len(t2),3,lenrf*lenphif*lenzf))
    Fmodflat_t_comp = np.zeros((len(t2),3,lenrf*lenphif*lenzf))

    for i in range(0,lenrf):
        for j in range(0,lenphif):
            for k in range(0,lenzf):
                Msimflat_sp = Msim[:,:,i,j,k].flatten()
                Mmodflat_sp = Mmod[:,:,i,j,k].flatten()
                Rsimflat_sp = Rsim[:,:,i,j,k].flatten()
                Rmodflat_sp = Rmod[:,:,i,j,k].flatten()
#                Msimdiagflat_sp = Msimdiag[:,:,i,j,k].flatten()
#                Mmoddiagflat_sp = Mmoddiag[:,:,i,j,k].flatten()
#                Rsimdiagflat_sp = Rsimdiag[:,:,i,j,k].flatten()
#                Rmoddiagflat_sp = Rmoddiag[:,:,i,j,k].flatten()
                Fsimflat_sp = Fsim[:,:,i,j,k].flatten()
                Fmodflat_sp = Fmod[:,:,i,j,k].flatten()

                a = np.concatenate((Msimflat_sp[:],Rsimflat_sp[:],Fsimflat_sp[:]), axis = None)
                b = np.concatenate((Mmodflat_sp[:],Rmodflat_sp[:],Fmodflat_sp[:]), axis = None)
#                adiag = np.concatenate((Msimdiagflat_sp[:],Rsimdiagflat_sp[:]), axis = None)
#                bdiag = np.concatenate((Mmoddiagflat_sp[:],Rmoddiagflat_sp[:]), axis = None)

                cbest_all_sp[i,j,k] = np.sum(a*b)/np.sum(b*b)
#                cbest_alldiag_sp[i,j,k] = np.sum(adiag*bdiag)/np.sum(bdiag*bdiag)

                cbest_M_sp[i,j,k] = np.sum(Msimflat_sp[:]*Mmodflat_sp[:])/np.sum(Mmodflat_sp[:]*Mmodflat_sp[:])
                cbest_R_sp[i,j,k] = np.sum(Rsimflat_sp[:]*Rmodflat_sp[:])/np.sum(Rmodflat_sp[:]*Rmodflat_sp[:])
#                cbest_Mdiag_sp[i,j,k] = np.sum(Msimdiagflat_sp[:]*Mmoddiagflat_sp[:])/np.sum(Mmoddiagflat_sp[:]*Mmoddiagflat_sp[:])
#                cbest_Rdiag_sp[i,j,k] = np.sum(Rsimdiagflat_sp[:]*Rmoddiagflat_sp[:])/np.sum(Rmoddiagflat_sp[:]*Rmoddiagflat_sp[:])
                cbest_F_sp[i,j,k] = np.sum(Fsimflat_sp[:]*Fmodflat_sp[:])/np.sum(Fmodflat_sp[:]*Fmodflat_sp[:])

                l2_all_sp[i,j,k] = (np.sum((a-b)**2)/np.sum(a**2))**(0.5)
                l2_M_sp[i,j,k] = (np.sum((Msimflat_sp[:]-Mmodflat_sp[:])**2)/np.sum(Msimflat_sp[:]**2))**(0.5)
                l2_R_sp[i,j,k] = (np.sum((Rsimflat_sp[:]-Rmodflat_sp[:])**2)/np.sum(Rsimflat_sp[:]**2))**(0.5)
                l2_F_sp[i,j,k] = (np.sum((Fsimflat_sp[:]-Fmodflat_sp[:])**2)/np.sum(Fsimflat_sp[:]**2))**(0.5)
#                l2_alldiag_sp[i,j,k] = (np.sum((adiag-bdiag)**2)/np.sum(adiag**2))**(0.5)
#                l2_Mdiag_sp[i,j,k] = (np.sum((Msimdiagflat_sp[:]-Mmoddiagflat_sp[:])**2)/np.sum(Msimdiagflat_sp[:]**2))**(0.5)
#                l2_Rdiag_sp[i,j,k] = (np.sum((Rsimdiagflat_sp[:]-Rmoddiagflat_sp[:])**2)/np.sum(Rsimdiagflat_sp[:]**2))**(0.5)
#                l2_all_sp[i,j,k] = (np.sum(((a-b)/a)**2)*1/len(a))**(0.5)
#                l2_M_sp[i,j,k] = (np.sum(((Msimflat_sp[:]-Mmodflat_sp[:])/Msimflat_sp[:])**2)*1/len(Msimflat_sp[:]))**(0.5)
#                l2_R_sp[i,j,k] = (np.sum(((Rsimflat_sp[:]-Rmodflat_sp[:])/Rsimflat_sp[:])**2)*1/len(Rsimflat_sp[:]))**(0.5)
#                l2_alldiag_sp[i,j,k] = (np.sum(((adiag-bdiag)/adiag)**2)*1/len(adiag))**(0.5)
#                l2_Mdiag_sp[i,j,k] = (np.sum(((Msimdiagflat_sp[:]-Mmoddiagflat_sp[:])/Msimdiagflat_sp[:])**2)*1/len(Msimdiagflat_sp[:]))**(0.5)
#                l2_Rdiag_sp[i,j,k] = (np.sum(((Rsimdiagflat_sp[:]-Rmoddiagflat_sp[:])/Rsimdiagflat_sp[:])**2)*1/len(Rsimdiagflat_sp[:]))**(0.5)
#                l2_F_sp[i,j,k] = (np.sum(((Fsimflat_sp[:]-Fmodflat_sp[:])/Fsimflat_sp[:])**2)*1/len(Fsimflat_sp[:]))**(0.5)  

                for cc in range(0,6):
                    cbest_M_sp_comp[cc,i,j,k] = np.sum(Msim[:,cc,i,j,k]*Mmod[:,cc,i,j,k])/np.sum(Mmod[:,cc,i,j,k]*Mmod[:,cc,i,j,k])
                    l2_M_sp_comp[cc,i,j,k] = (np.sum((Msim[:,cc,i,j,k]-Mmod[:,cc,i,j,k])**2)/np.sum(Msim[:,cc,i,j,k]**2))**(0.5)
#                    l2_M_sp_comp[cc,i,j,k] = (np.sum(((Msim[:,cc,i,j,k]-Mmod[:,cc,i,j,k])/Msim[:,cc,i,j,k])**2)*1/np.size(Msim, axis = 0))**(0.5)
#                    l2_R_sp_comp[cc,i,j,k] = (np.sum(((Rsim[:,cc,i,j,k]-Rmod[:,cc,i,j,k])/Rsim[:,cc,i,j,k])**2)*1/np.size(Rsim, axis = 0))**(0.5)                
                for cc in range(0,3):
                    cbest_F_sp_comp[cc,i,j,k] = np.sum(Fsim[:,cc,i,j,k]*Fmod[:,cc,i,j,k])/np.sum(Fmod[:,cc,i,j,k]*Fmod[:,cc,i,j,k])
                    cbest_R_sp_comp[cc,i,j,k] = np.sum(Rsim[:,cc,i,j,k]*Rmod[:,cc,i,j,k])/np.sum(Rmod[:,cc,i,j,k]*Rmod[:,cc,i,j,k])
#                    l2_F_sp_comp[cc,i,j,k] = (np.sum(((Fsim[:,cc,i,j,k]-Fmod[:,cc,i,j,k])/Fsim[:,cc,i,j,k])**2)*1/np.size(Fsim, axis = 0))**(0.5)
#                    cbest_Mdiag_sp_comp[cc,i,j,k] = np.sum(Msimdiag[:,cc,i,j,k]*Mmoddiag[:,cc,i,j,k])/np.sum(Mmoddiag[:,cc,i,j,k]*Mmoddiag[:,cc,i,j,k])
#                    cbest_Rdiag_sp_comp[cc,i,j,k] = np.sum(Rsimdiag[:,cc,i,j,k]*Rmoddiag[:,cc,i,j,k])/np.sum(Rmoddiag[:,cc,i,j,k]*Rmoddiag[:,cc,i,j,k])
#                    l2_Mdiag_sp_comp[cc,i,j,k] = (np.sum((Msimdiag[:,cc,i,j,k]-Mmoddiag[:,cc,i,j,k])**2)/np.sum(Msimdiag[:,cc,i,j,k]**2))**(0.5)
#                    l2_Rdiag_sp_comp[cc,i,j,k] = (np.sum((Rsimdiag[:,cc,i,j,k]-Rmoddiag[:,cc,i,j,k])**2)/np.sum(Rsimdiag[:,cc,i,j,k]**2))**(0.5)
#                    l2_Mdiag_sp_comp[cc,i,j,k] = (np.sum(((Msimdiag[:,cc,i,j,k]-Mmoddiag[:,cc,i,j,k])/Msimdiag[:,cc,i,j,k])**2)*1/np.size(Msimdiag, axis = 0))**(0.5)
#                    l2_Rdiag_sp_comp[cc,i,j,k] = (np.sum(((Rsimdiag[:,cc,i,j,k]-Rmoddiag[:,cc,i,j,k])/Rsimdiag[:,cc,i,j,k])**2)*1/np.size(Rsimdiag, axis = 0))**(0.5)                                      
                    l2_R_sp_comp[cc,i,j,k] = (np.sum((Rsim[:,cc,i,j,k]-Rmod[:,cc,i,j,k])**2)/np.sum(Rsim[:,cc,i,j,k]**2))**(0.5)                   
                    l2_F_sp_comp[cc,i,j,k] = (np.sum((Fsim[:,cc,i,j,k]-Fmod[:,cc,i,j,k])**2)/np.sum(Fsim[:,cc,i,j,k]**2))**(0.5)

    for tt in range(0,len(t2)):
        Msimflat_t = Msimt[tt,:,:,:,:].flatten()
        Mmodflat_t = Mmodt[tt,:,:,:,:].flatten()
        Rsimflat_t = Rsimt[tt,:,:,:,:].flatten()
        Rmodflat_t = Rmodt[tt,:,:,:,:].flatten()
#        Msimdiagflat_t = Msimdiagt[tt,:,:,:,:].flatten()
#        Mmoddiagflat_t = Mmoddiagt[tt,:,:,:,:].flatten()
#        Rsimdiagflat_t = Rsimdiagt[tt,:,:,:,:].flatten()
#        Rmoddiagflat_t = Rmoddiagt[tt,:,:,:,:].flatten()
        Fsimflat_t = Fsimt[tt,:,:,:,:].flatten()
        Fmodflat_t = Fmodt[tt,:,:,:,:].flatten()

        a = np.concatenate((Msimflat_t[:],Rsimflat_t[:],Fsimflat_t[:]), axis = None)
        b = np.concatenate((Mmodflat_t[:],Rmodflat_t[:],Fmodflat_t[:]), axis = None)
#        adiag = np.concatenate((Msimdiagflat_t[:],Rsimdiagflat_t[:]), axis = None)
#        bdiag = np.concatenate((Mmoddiagflat_t[:],Rmoddiagflat_t[:]), axis = None)

        cbest_all_t[tt] = np.sum(a*b)/np.sum(b*b)
        cbest_M_t[tt] = np.sum(Msimflat_t[:]*Mmodflat_t[:])/np.sum(Mmodflat_t[:]*Mmodflat_t[:])
        cbest_R_t[tt] = np.sum(Rsimflat_t[:]*Rmodflat_t[:])/np.sum(Rmodflat_t[:]*Rmodflat_t[:])
#        cbest_alldiag_t[tt] = np.sum(adiag*bdiag)/np.sum(bdiag*bdiag)
#        cbest_Mdiag_t[tt] = np.sum(Msimdiagflat_t[:]*Mmoddiagflat_t[:])/np.sum(Mmoddiagflat_t[:]*Mmoddiagflat_t[:])
#        cbest_Rdiag_t[tt] = np.sum(Rsimdiagflat_t[:]*Rmoddiagflat_t[:])/np.sum(Rmoddiagflat_t[:]*Rmoddiagflat_t[:])
        cbest_F_t[tt] = np.sum(Fsimflat_t[:]*Fmodflat_t[:])/np.sum(Fmodflat_t[:]*Fmodflat_t[:])

        l2_all_t[tt] = (np.sum((a-b)**2)/np.sum(a**2))**(0.5)
        l2_M_t[tt] = (np.sum((Msimflat_t[:]-Mmodflat_t[:])**2)/np.sum(Msimflat_t[:]**2))**(0.5)
        l2_R_t[tt] = (np.sum((Rsimflat_t[:]-Rmodflat_t[:])**2)/np.sum(Rsimflat_t[:]**2))**(0.5)
        l2_F_t[tt] = (np.sum((Fsimflat_t[:]-Fmodflat_t[:])**2)/np.sum(Fsimflat_t[:]**2))**(0.5)
#        l2_alldiag_t[tt] = (np.sum((adiag-bdiag)**2)/np.sum(adiag**2))**(0.5)
#        l2_Mdiag_t[tt] = (np.sum((Msimdiagflat_t[:]-Mmoddiagflat_t[:])**2)/np.sum(Msimdiagflat_t[:]**2))**(0.5)
#        l2_Rdiag_t[tt] = (np.sum((Rsimdiagflat_t[:]-Rmoddiagflat_t[:])**2)/np.sum(Rsimdiagflat_t[:]**2))**(0.5)
#        l2_all_t[tt] = (np.sum(((a-b)/a)**2)*1/len(a)**2)**(0.5)
#        l2_M_t[tt] = (np.sum(((Msimflat_t[:]-Mmodflat_t[:])/Msimflat_t[:])**2)*1/len(Msimflat_t[:]))**(0.5)
#        l2_R_t[tt] = (np.sum(((Rsimflat_t[:]-Rmodflat_t[:])/Rsimflat_t[:])**2)*1/len(Rsimflat_t[:]))**(0.5)
#        l2_alldiag_t[tt] = (np.sum(((a-b)/a)**2)*1/len(a))**(0.5)
#        l2_Mdiag_t[tt] = (np.sum(((Msimdiagflat_t[:]-Mmoddiagflat_t[:])/Msimdiagflat_t[:])**2)*1/len(Msimdiagflat_t[:]))**(0.5)
#        l2_Rdiag_t[tt] = (np.sum(((Rsimdiagflat_t[:]-Rmoddiagflat_t[:])/Rsimdiagflat_t[:])**2)*1/len(Rsimdiagflat_t[:]))**(0.5)        
#        l2_F_t[tt] = (np.sum(((Fsimflat_t[:]-Fmodflat_t[:])/Fsimflat_t[:])**2)*1/len(Fsimflat_t[:]))**(0.5)
        for cc in range(0,6):
            Msimflat_t_comp[tt,cc,:] = Msimt[tt,cc,:,:,:].flatten()
            Mmodflat_t_comp[tt,cc,:] = Mmodt[tt,cc,:,:,:].flatten()
        
            cbest_M_t_comp[tt,cc] = np.sum(Msimflat_t_comp[tt,cc,:]*Mmodflat_t_comp[tt,cc,:])/np.sum(Mmodflat_t_comp[tt,cc,:]*Mmodflat_t_comp[tt,cc,:])
            l2_M_t_comp[tt,cc] = (np.sum((Msimflat_t_comp[tt,cc,:]-Mmodflat_t_comp[tt,cc,:])**2)/np.sum(Msimflat_t_comp[tt,cc,:]**2))**(0.5)
        
#            l2_M_t_comp[tt,cc] = (np.sum(((Msimflat_t_comp[tt,cc,:]-Mmodflat_t_comp[tt,cc,:])/Msimflat_t_comp[tt,cc,:])**2)*1/np.size(Msimflat_t_comp, axis = 2))**(0.5)
#            l2_R_t_comp[tt,cc] = (np.sum(((Rsimflat_t_comp[tt,cc,:]-Rmodflat_t_comp[tt,cc,:])/Rsimflat_t_comp[tt,cc,:])**2)*1/np.size(Rsimflat_t_comp, axis = 2))**(0.5)        

        for cc in range(0,3):
#            Msimdiagflat_t_comp[tt,cc,:] = Msimdiagt[tt,cc,:,:,:].flatten()
#            Mmoddiagflat_t_comp[tt,cc,:] = Mmoddiagt[tt,cc,:,:,:].flatten()
#            Rsimdiagflat_t_comp[tt,cc,:] = Rsimdiagt[tt,cc,:,:,:].flatten()
#            Rmoddiagflat_t_comp[tt,cc,:] = Rmoddiagt[tt,cc,:,:,:].flatten()

#            cbest_Mdiag_t_comp[tt,cc] = np.sum(Msimflat_t_comp[tt,cc,:]*Mmodflat_t_comp[tt,cc,:])/np.sum(Mmodflat_t_comp[tt,cc,:]*Mmodflat_t_comp[tt,cc,:])
#            cbest_Rdiag_t_comp[tt,cc] = np.sum(Rsimflat_t_comp[tt,cc,:]*Rmodflat_t_comp[tt,cc,:])/np.sum(Rmodflat_t_comp[tt,cc,:]*Rmodflat_t_comp[tt,cc,:])
#            l2_Mdiag_t_comp[tt,cc] = (np.sum((Msimdiagflat_t_comp[tt,cc,:]-Mmoddiagflat_t_comp[tt,cc,:])**2)/np.sum(Msimdiagflat_t_comp[tt,cc,:]**2))**(0.5)
#            l2_Rdiag_t_comp[tt,cc] = (np.sum((Rsimdiagflat_t_comp[tt,cc,:]-Rmoddiagflat_t_comp[tt,cc,:])**2)/np.sum(Rsimdiagflat_t_comp[tt,cc,:]**2))**(0.5)
#            l2_Mdiag_t_comp[tt,cc] = (np.sum(((Msimdiagflat_t_comp[tt,cc,:]-Mmoddiagflat_t_comp[tt,cc,:])/Msimdiagflat_t_comp[tt,cc,:])**2)*1/np.size(Msimdiagflat_t_comp, axis = 2))**(0.5)
#            l2_Rdiag_t_comp[tt,cc] = (np.sum(((Rsimdiagflat_t_comp[tt,cc,:]-Rmoddiagflat_t_comp[tt,cc,:])/Rsimdiagflat_t_comp[tt,cc,:])**2)*1/np.size(Rsimdiagflat_t_comp, axis = 2))**(0.5)
            Rsimflat_t_comp[tt,cc,:] = Rsimt[tt,cc,:,:,:].flatten()
            Rmodflat_t_comp[tt,cc,:] = Rmodt[tt,cc,:,:,:].flatten()
            Fsimflat_t_comp[tt,cc,:] = Fsimt[tt,cc,:,:,:].flatten()
            Fmodflat_t_comp[tt,cc,:] = Fmodt[tt,cc,:,:,:].flatten()
            
            cbest_R_t_comp[tt,cc] = np.sum(Rsimflat_t_comp[tt,cc,:]*Rmodflat_t_comp[tt,cc,:])/np.sum(Rmodflat_t_comp[tt,cc,:]*Rmodflat_t_comp[tt,cc,:])
            l2_R_t_comp[tt,cc] = (np.sum((Rsimflat_t_comp[tt,cc,:]-Rmodflat_t_comp[tt,cc,:])**2)/np.sum(Rsimflat_t_comp[tt,cc,:]**2))**(0.5)
            cbest_F_t_comp[tt,cc] = np.sum(Fsimflat_t_comp[tt,cc,:]*Fmodflat_t_comp[tt,cc,:])/np.sum(Fmodflat_t_comp[tt,cc,:]*Fmodflat_t_comp[tt,cc,:])
            l2_F_t_comp[tt,cc] = (np.sum((Fsimflat_t_comp[tt,cc,:]-Fmodflat_t_comp[tt,cc,:])**2)/np.sum(Fsimflat_t_comp[tt,cc,:]**2))**(0.5)
#            l2_F_t_comp[tt,cc] = (np.sum(((Fsimflat_t_comp[tt,cc,:]-Fmodflat_t_comp[tt,cc,:])/Fsimflat_t_comp[tt,cc,:])**2)*1/np.size(Fsimflat_t_comp, axis = 2))**(0.5)
    
    cbest_M_comp = np.zeros(6)
    cbest_R_comp = np.zeros(3)
    cbest_F_comp = np.zeros(3)
    l2_M_comp = np.zeros(6)
    l2_R_comp = np.zeros(3)
    l2_F_comp = np.zeros(3)

    for cc in range(0,6):
        Msimflatcomp = Msim[:,cc,:,:,:].flatten()
        Mmodflatcomp = Mmod[:,cc,:,:,:].flatten()
    
        cbest_M_comp[cc] =  np.sum(Msimflatcomp[:]*Mmodflatcomp[:])/np.sum(Mmodflatcomp[:]*Mmodflatcomp[:])
        l2_M_comp[cc] = (np.sum((Msimflatcomp[:]-Mmodflatcomp[:])**2)/np.sum(Msimflatcomp[:]**2))**(0.5)
    
    for cc in range(0,3):
        Rsimflatcomp = Rsim[:,cc,:,:,:].flatten()
        Rmodflatcomp = Rmod[:,cc,:,:,:].flatten()

        cbest_R_comp[cc] =  np.sum(Rsimflatcomp[:]*Rmodflatcomp[:])/np.sum(Rmodflatcomp[:]*Rmodflatcomp[:])
        l2_R_comp[cc] = (np.sum((Rsimflatcomp[:]-Rmodflatcomp[:])**2)/np.sum(Rsimflatcomp[:]**2))**(0.5)

        Fsimflatcomp = Fsim[:,cc,:,:,:].flatten()
        Fmodflatcomp = Fmod[:,cc,:,:,:].flatten()

        cbest_F_comp[cc] =  np.sum(Fsimflatcomp[:]*Fmodflatcomp[:])/np.sum(Fmodflatcomp[:]*Fmodflatcomp[:])
        l2_F_comp[cc] = (np.sum((Fsimflatcomp[:]-Fmodflatcomp[:])**2)/np.sum(Fsimflatcomp[:]**2))**(0.5)

    cbest_M_comp_mean = np.mean(cbest_M_comp)
    cbest_R_comp_mean = np.mean(cbest_R_comp)
    cbest_F_comp_mean = np.mean(cbest_F_comp)

    l2_M_comp_mean = np.mean(l2_M_comp)
    l2_R_comp_mean = np.mean(l2_R_comp)
    l2_F_comp_mean = np.mean(l2_F_comp)

#    print('cbest T mean comp    = ', cbest_M_comp_mean)
#    print('cbest S mean comp    = ', cbest_R_comp_mean)
#    print('cbest D mean comp    = ', cbest_F_comp_mean)

    print('L2 T mean comp    = ', l2_M_comp_mean)
    print('L2 S mean comp    = ', l2_R_comp_mean)
    print('L2 D mean comp    = ', l2_F_comp_mean)

    Msimflat = Msim.flatten()
    Mmodflat = Mmod.flatten()
    Rsimflat = Rsim.flatten()
    Rmodflat = Rmod.flatten()
#    Msimdiagflat = Msimdiag.flatten()
#    Mmoddiagflat = Mmoddiag.flatten()
#    Rsimdiagflat = Rsimdiag.flatten()
#    Rmoddiagflat = Rmoddiag.flatten()
    Fsimflat = Fsim.flatten()
    Fmodflat = Fmod.flatten()

    a = np.concatenate((Msimflat,Rsimflat,Fsimflat),axis=None)
    b = np.concatenate((Mmodflat,Rmodflat,Fmodflat),axis=None)
#    adiag = np.concatenate((Msimdiagflat,Rsimdiagflat),axis=None)
#    bdiag = np.concatenate((Mmoddiagflat,Rmoddiagflat),axis=None)

    cbest_all = np.sum(a*b)/np.sum(b*b)
#    cbest_alldiag = np.sum(adiag*bdiag)/np.sum(bdiag*bdiag)
    l2_all = (np.sum((a-b)**2)/np.sum(a**2))**(0.5)
#    l2_alldiag = (np.sum((adiag-bdiag)**2)/np.sum(adiag**2))**(0.5)
#    l2_all = (np.sum(((a-b)/a)**2)*1/len(a))**(0.5)
#    l2_alldiag = (np.sum(((adiag-bdiag)/adiag)**2)*1/len(adiag))**(0.5)

    cbest_M = np.sum(Msimflat[:]*Mmodflat[:])/np.sum(Mmodflat[:]*Mmodflat[:])
    cbest_R = np.sum(Rsimflat[:]*Rmodflat[:])/np.sum(Rmodflat[:]*Rmodflat[:])
    cbest_F = np.sum(Fsimflat[:]*Fmodflat[:])/np.sum(Fmodflat[:]*Fmodflat[:])
#    cbest_Mdiag = np.sum(Msimdiagflat[:]*Mmoddiagflat[:])/np.sum(Mmoddiagflat[:]*Mmoddiagflat[:])
#    cbest_Rdiag = np.sum(Rsimdiagflat[:]*Rmoddiagflat[:])/np.sum(Rmoddiagflat[:]*Rmoddiagflat[:])

    l2_M = (np.sum((Msimflat[:]-Mmodflat[:])**2)/np.sum(Msimflat[:]**2))**(0.5)
    l2_R = (np.sum((Rsimflat[:]-Rmodflat[:])**2)/np.sum(Rsimflat[:]**2))**(0.5)
    l2_F = (np.sum((Fsimflat[:]-Fmodflat[:])**2)/np.sum(Fsimflat[:]**2))**(0.5)
#    l2_Mdiag = (np.sum((Msimdiagflat[:]-Mmoddiagflat[:])**2)/np.sum(Msimdiagflat[:]**2))**(0.5)
#    l2_Rdiag = (np.sum((Rsimdiagflat[:]-Rmoddiagflat[:])**2)/np.sum(Rsimdiagflat[:]**2))**(0.5)

#    l2_M = (np.sum(((Msimflat[:]-Mmodflat[:])/Msimflat[:])**2)*1/len(Msimflat[:]))**(0.5)
#    l2_R = (np.sum(((Rsimflat[:]-Rmodflat[:])/Rsimflat[:])**2)*1/len(Rsimflat[:]))**(0.5)
#    l2_Mdiag = (np.sum(((Msimdiagflat[:]-Mmoddiagflat[:])/Msimdiagflat[:])**2)*1/len(Msimdiagflat[:]))**(0.5)
#    l2_Rdiag = (np.sum(((Rsimdiagflat[:]-Rmoddiagflat[:])/Rsimdiagflat[:])**2)*1/len(Rsimdiagflat[:]))**(0.5)
#    l2_F = (np.sum(((Fsimflat[:]-Fmodflat[:])/Fsimflat[:])**2)*1/len(Fsimflat[:]))**(0.5)

    cbest_all_av_sp = np.mean(cbest_all_sp)
    cbest_M_av_sp = np.mean(cbest_M_sp)
    cbest_R_av_sp = np.mean(cbest_R_sp)
#    cbest_alldiag_av_sp = np.mean(cbest_alldiag_sp)
#    cbest_Mdiag_av_sp = np.mean(cbest_Mdiag_sp)
#    cbest_Rdiag_av_sp = np.mean(cbest_Rdiag_sp)
    cbest_F_av_sp = np.mean(cbest_F_sp)

    std_all_sp = stdev(cbest_all_sp.flatten())
    std_M_sp = stdev(cbest_M_sp.flatten())
    std_R_sp = stdev(cbest_R_sp.flatten())
#    std_alldiag_sp = stdev(cbest_alldiag_sp.flatten())
#    std_Mdiag_sp = stdev(cbest_Mdiag_sp.flatten())
#    std_Rdiag_sp = stdev(cbest_Rdiag_sp.flatten())
    std_F_sp = stdev(cbest_F_sp.flatten())

#    print('c best all av space    = ', cbest_all_av_sp,'+-',std_all_sp)
#    print('c best T av space    = ', cbest_M_av_sp,'+-',std_M_sp)
#    print('c best S av space    = ', cbest_R_av_sp,'+-',std_R_sp)
#    print('c best all diag av space    = ', cbest_alldiag_av_sp,'+-',std_alldiag_sp)
#    print('c best M diag av space    = ', cbest_Mdiag_av_sp,'+-',std_Mdiag_sp)
#    print('c best R diag av space    = ', cbest_Rdiag_av_sp,'+-',std_Rdiag_sp)
#    print('c best D av space    = ', cbest_F_av_sp,'+-',std_F_sp)

    cbest_M_av_sp_comp = np.mean(cbest_M_sp_comp, axis = 0)
    cbest_R_av_sp_comp = np.mean(cbest_R_sp_comp, axis = 0)
#    cbest_Mdiag_av_sp_comp = np.mean(cbest_Mdiag_sp_comp, axis = 0)
#    cbest_Rdiag_av_sp_comp = np.mean(cbest_Rdiag_sp_comp, axis = 0)
    cbest_F_av_sp_comp = np.mean(cbest_F_sp_comp, axis = 0)

    cbest_M_av_sp_comp2 = np.mean(cbest_M_av_sp_comp)
    cbest_R_av_sp_comp2 = np.mean(cbest_R_av_sp_comp)
#    cbest_Mdiag_av_sp_comp2 = np.mean(cbest_Mdiag_av_sp_comp)
#    cbest_Rdiag_av_sp_comp2 = np.mean(cbest_Rdiag_av_sp_comp)
    cbest_F_av_sp_comp2 = np.mean(cbest_F_av_sp_comp)

    std_M_sp_comp = stdev(cbest_M_av_sp_comp.flatten())
    std_R_sp_comp = stdev(cbest_R_av_sp_comp.flatten())
#    std_Mdiag_sp_comp = stdev(cbest_Mdiag_av_sp_comp.flatten())
#    std_Rdiag_sp_comp = stdev(cbest_Rdiag_av_sp_comp.flatten())
    std_F_sp_comp = stdev(cbest_F_av_sp_comp.flatten())

#    print('c best T av space comp    = ', cbest_M_av_sp_comp2,'+-',std_M_sp_comp)
#    print('c best S av space comp   = ', cbest_R_av_sp_comp2,'+-',std_R_sp_comp)
#    print('c best M diag av space comp    = ', cbest_Mdiag_av_sp_comp2,'+-',std_Mdiag_sp_comp)
#    print('c best R diag av space comp   = ', cbest_Rdiag_av_sp_comp2,'+-',std_Rdiag_sp_comp)
#    print('c best D av space comp   = ', cbest_F_av_sp_comp2,'+-',std_F_sp_comp)

    cbest_all_av_t = np.mean(cbest_all_t[dif:])
    cbest_M_av_t = np.mean(cbest_M_t[dif:])
    cbest_R_av_t = np.mean(cbest_R_t[dif:])
#    cbest_alldiag_av_t = np.mean(cbest_alldiag_t[dif:])
#    cbest_Mdiag_av_t = np.mean(cbest_Mdiag_t[dif:])
#    cbest_Rdiag_av_t = np.mean(cbest_Rdiag_t[dif:])
    cbest_F_av_t = np.mean(cbest_F_t[dif:])

    std_all_t = stdev(cbest_all_t[dif:])
    std_M_t = stdev(cbest_M_t[dif:])
    std_R_t = stdev(cbest_R_t[dif:])
#    std_alldiag_t = stdev(cbest_alldiag_t[dif:])
#    std_Mdiag_t = stdev(cbest_Mdiag_t[dif:])
#    std_Rdiag_t = stdev(cbest_Rdiag_t[dif:])
    std_F_t = stdev(cbest_F_t[dif:])

#    print('c best all av time    = ', cbest_all_av_t,'+-',std_all_t)
#    print('c best T av time    = ', cbest_M_av_t,'+-',std_M_t)
#    print('c best S av time    = ', cbest_R_av_t,'+-',std_R_t)
#    print('c best all diag av time    = ', cbest_alldiag_av_t,'+-',std_alldiag_t)
#    print('c best M diag av time    = ', cbest_Mdiag_av_t,'+-',std_Mdiag_t)
#    print('c best R diag av time    = ', cbest_Rdiag_av_t,'+-',std_Rdiag_t)
#    print('c best D av time    = ', cbest_F_av_t,'+-',std_F_t)

    cbest_M_av_t_comp = np.mean(cbest_M_t_comp, axis = 1)
    cbest_R_av_t_comp = np.mean(cbest_R_t_comp, axis = 1)
#    cbest_Mdiag_av_t_comp = np.mean(cbest_Mdiag_t_comp, axis = 1)
#    cbest_Rdiag_av_t_comp = np.mean(cbest_Rdiag_t_comp, axis = 1)
    cbest_F_av_t_comp = np.mean(cbest_F_t_comp, axis = 1)

    cbest_M_av_t_comp2 = np.mean(cbest_M_av_t_comp[dif:])
    cbest_R_av_t_comp2 = np.mean(cbest_R_av_t_comp[dif:])
#    cbest_Mdiag_av_t_comp2 = np.mean(cbest_Mdiag_av_t_comp[dif:])
#    cbest_Rdiag_av_t_comp2 = np.mean(cbest_Rdiag_av_t_comp[dif:])
    cbest_F_av_t_comp2 = np.mean(cbest_F_av_t_comp[dif:])

    std_M_t_comp = stdev(cbest_M_av_t_comp[dif:])
    std_R_t_comp = stdev(cbest_R_av_t_comp[dif:])
#    std_Mdiag_t_comp = stdev(cbest_Mdiag_av_t_comp[dif:])
#    std_Rdiag_t_comp = stdev(cbest_Rdiag_av_t_comp[dif:])
    std_F_t_comp = stdev(cbest_F_av_t_comp[dif:])

#    print('c best T av time comp   = ', cbest_M_av_t_comp2,'+-',std_M_t_comp)
#    print('c best S av time comp   = ', cbest_R_av_t_comp2,'+-',std_R_t_comp)
#    print('c best M diag av time comp   = ', cbest_Mdiag_av_t_comp2,'+-',std_Mdiag_t_comp)
#    print('c best R diag av time comp   = ', cbest_Rdiag_av_t_comp2,'+-',std_Rdiag_t_comp)
#    print('c best D av time comp    = ', cbest_F_av_t_comp2,'+-',std_F_t_comp)

#    print('c best all   = ', cbest_all)
#    print('c best T     = ', cbest_M)
#    print('c best S     = ', cbest_R)
#    print('c best all diag  = ', cbest_alldiag)
#    print('c best M diag    = ', cbest_Mdiag)
#    print('c best R diag    = ', cbest_Rdiag)
#    print('c best D     = ', cbest_F)


    l2_all_av_sp = np.mean(l2_all_sp)
    l2_M_av_sp = np.mean(l2_M_sp)
    l2_R_av_sp = np.mean(l2_R_sp)
#    l2_alldiag_av_sp = np.mean(l2_alldiag_sp)
#    l2_Mdiag_av_sp = np.mean(l2_Mdiag_sp)
#    l2_Rdiag_av_sp = np.mean(l2_Rdiag_sp)
    l2_F_av_sp = np.mean(l2_F_sp)

    l2std_all_sp = stdev(l2_all_sp.flatten())
    l2std_M_sp = stdev(l2_M_sp.flatten())
    l2std_R_sp = stdev(l2_R_sp.flatten())
#    l2std_alldiag_sp = stdev(l2_alldiag_sp.flatten())
#    l2std_Mdiag_sp = stdev(l2_Mdiag_sp.flatten())
#    l2std_Rdiag_sp = stdev(l2_Rdiag_sp.flatten())
    l2std_F_sp = stdev(l2_F_sp.flatten())

#    print('L2 all av space    = ', l2_all_av_sp,'+-',l2std_all_sp)
#    print('L2 T av space    = ', l2_M_av_sp,'+-',l2std_M_sp)
#    print('L2 S av space    = ', l2_R_av_sp,'+-',l2std_R_sp)
#    print('L2 all diag av space    = ', l2_alldiag_av_sp,'+-',l2std_alldiag_sp)
#    print('L2 M diag av space    = ', l2_Mdiag_av_sp,'+-',l2std_Mdiag_sp)
#    print('L2 R diag av space    = ', l2_Rdiag_av_sp,'+-',l2std_Rdiag_sp)
#    print('L2 D av space    = ', l2_F_av_sp,'+-',l2std_F_sp)

#    print('L2 R av space    = ', l2_R_av_sp,'+-',l2std_R_sp)
#    print('L2 all diag av space    = ', l2_alldiag_av_sp,'+-',l2std_alldiag_sp)
#    print('L2 M diag av space    = ', l2_Mdiag_av_sp,'+-',l2std_Mdiag_sp)
#    print('L2 R diag av space    = ', l2_Rdiag_av_sp,'+-',l2std_Rdiag_sp)
#    print('L2 F av space    = ', l2_F_av_sp,'+-',l2std_F_sp)

    l2_M_av_sp_comp = np.mean(l2_M_sp_comp, axis = 0)
    l2_R_av_sp_comp = np.mean(l2_R_sp_comp, axis = 0)
#    l2_Mdiag_av_sp_comp = np.mean(l2_Mdiag_sp_comp, axis = 0)
#    l2_Rdiag_av_sp_comp = np.mean(l2_Rdiag_sp_comp, axis = 0)
    l2_F_av_sp_comp = np.mean(l2_F_sp_comp, axis = 0)

    l2_M_av_sp_comp2 = np.mean(l2_M_av_sp_comp)
    l2_R_av_sp_comp2 = np.mean(l2_R_av_sp_comp)
#    l2_Mdiag_av_sp_comp2 = np.mean(l2_Mdiag_av_sp_comp)
#    l2_Rdiag_av_sp_comp2 = np.mean(l2_Rdiag_av_sp_comp)
    l2_F_av_sp_comp2 = np.mean(l2_F_av_sp_comp)

    l2std_M_sp_comp = stdev(l2_M_av_sp_comp.flatten())
    l2std_R_sp_comp = stdev(l2_R_av_sp_comp.flatten())
#    l2std_Mdiag_sp_comp = stdev(l2_Mdiag_av_sp_comp.flatten())
#    l2std_Rdiag_sp_comp = stdev(l2_Rdiag_av_sp_comp.flatten())
    l2std_F_sp_comp = stdev(l2_F_av_sp_comp.flatten())

#    print('L2 T av space comp    = ', l2_M_av_sp_comp2,'+-',l2std_M_sp_comp)
#    print('L2 S av space comp   = ', l2_R_av_sp_comp2,'+-',l2std_R_sp_comp)
#    print('L2 M diag av space comp    = ', l2_Mdiag_av_sp_comp2,'+-',l2std_Mdiag_sp_comp)
#    print('L2 R diag av space comp   = ', l2_Rdiag_av_sp_comp2,'+-',l2std_Rdiag_sp_comp)
#    print('L2 D av space comp   = ', l2_F_av_sp_comp2,'+-',l2std_F_sp_comp)

    l2_all_av_t = np.mean(l2_all_t[dif:])
    l2_M_av_t = np.mean(l2_M_t[dif:])
    l2_R_av_t = np.mean(l2_R_t[dif:])
#    l2_alldiag_av_t = np.mean(l2_alldiag_t[dif:])
#    l2_Mdiag_av_t = np.mean(l2_Mdiag_t[dif:])
#    l2_Rdiag_av_t = np.mean(l2_Rdiag_t[dif:])
    l2_F_av_t = np.mean(l2_F_t[dif:])

    l2std_all_t = stdev(l2_all_t[dif:])
    l2std_M_t = stdev(l2_M_t[dif:])
    l2std_R_t = stdev(l2_R_t[dif:])
#    l2std_alldiag_t = stdev(l2_alldiag_t[dif:])
#    l2std_Mdiag_t = stdev(l2_Mdiag_t[dif:])
#    l2std_Rdiag_t = stdev(l2_Rdiag_t[dif:])
    l2std_F_t = stdev(l2_F_t[dif:])

    print('L2 all av time    = ', l2_all_av_t,'+-',l2std_all_t)
    print('L2 T av time    = ', l2_M_av_t,'+-',l2std_M_t)
    print('L2 S av time    = ', l2_R_av_t,'+-',l2std_R_t)
#    print('L2 all diag av time    = ', l2_alldiag_av_t,'+-',l2std_alldiag_t)
#    print('L2 M diag av time    = ', l2_Mdiag_av_t,'+-',l2std_Mdiag_t)
#    print('L2 R diag av time    = ', l2_Rdiag_av_t,'+-',l2std_Rdiag_t)
    print('L2 D av time    = ', l2_F_av_t,'+-',l2std_F_t)

    l2_M_av_t_comp = np.mean(l2_M_t_comp, axis = 1)
    l2_R_av_t_comp = np.mean(l2_R_t_comp, axis = 1)
#    l2_Mdiag_av_t_comp = np.mean(l2_Mdiag_t_comp, axis = 1)
#    l2_Rdiag_av_t_comp = np.mean(l2_Rdiag_t_comp, axis = 1)
    l2_F_av_t_comp = np.mean(l2_F_t_comp, axis = 1)

    l2_M_av_t_comp2 = np.mean(l2_M_av_t_comp[dif:])
    l2_R_av_t_comp2 = np.mean(l2_R_av_t_comp[dif:])
#    l2_Mdiag_av_t_comp2 = np.mean(l2_Mdiag_av_t_comp[dif:])
#    l2_Rdiag_av_t_comp2 = np.mean(l2_Rdiag_av_t_comp[dif:])
    l2_F_av_t_comp2 = np.mean(l2_F_av_t_comp[dif:])

    l2std_M_t_comp = stdev(l2_M_av_t_comp[dif:])
    l2std_R_t_comp = stdev(l2_R_av_t_comp[dif:])
#    l2std_Mdiag_t_comp = stdev(l2_Mdiag_av_t_comp[dif:])
#    l2std_Rdiag_t_comp = stdev(l2_Rdiag_av_t_comp[dif:])
    l2std_F_t_comp = stdev(l2_F_av_t_comp[dif:])

    print('L2 T av time comp   = ', l2_M_av_t_comp2,'+-',l2std_M_t_comp)
    print('L2 S av time comp   = ', l2_R_av_t_comp2,'+-',l2std_R_t_comp)
#    print('L2 M diag av time comp   = ', l2_Mdiag_av_t_comp2,'+-',l2std_Mdiag_t_comp)
#    print('L2 R diag av time comp   = ', l2_Rdiag_av_t_comp2,'+-',l2std_Rdiag_t_comp)
    print('L2 D av time comp    = ', l2_F_av_t_comp2,'+-',l2std_F_t_comp)


    print('L2 all   = ', l2_all)
    print('L2 T     = ', l2_M)
    print('L2 S     = ', l2_R)
#    print('L2 all diag  = ', l2_alldiag)
#    print('L2 M diag    = ', l2_Mdiag)
#    print('L2 R diag    = ', l2_Rdiag)
    print('L2 D     = ', l2_F)


    with open(path+'/cbest_time_ev.txt', 'w') as f:
        f.write('# ALL  \t  T \t S \t D #\n')
        for x in range(0,len(t2)):
            f.write('%f\t%f\t%f\t%f\t%f\n'%(t2[x],cbest_all_t[x], cbest_M_t[x], cbest_R_t[x], cbest_F_t[x]))

    with open(path+'/l2_time_ev.txt', 'w') as f:
        f.write('# ALL  \t T \t S \t D #\n')
        for x in range(0,len(t2)):
            f.write('%f\t%f\t%f\t%f\t%f\n'%(t2[x],l2_all_t[x], l2_M_t[x], l2_R_t[x], l2_F_t[x]))

    with open(path+'/cbest_l2.txt','w') as f2:
        f2.write('# ALL  \t T \t S \t D #\n')
        f2.write('%f\t%f\t%f\t%f\n'%(cbest_all,cbest_M,cbest_R,cbest_F))
        f2.write('%f\t%f\t%f\t%f\n'%(l2_all,l2_M, l2_R, l2_F))

    with open(path+'/cbest_l2_mean_comp.txt','w') as f2:
        f2.write('# T \t S \t D #\n')
        f2.write('%f\t%f\t%f\n'%(cbest_M_comp_mean,cbest_R_comp_mean,cbest_F_comp_mean))
        f2.write('%f\t%f\t%f\n'%(l2_M_comp_mean, l2_R_comp_mean, l2_F_comp_mean))

    with open(path+'/cbest_l2_space_av.txt','w') as f:
        f.write('# C-ALL \t C-T \t C-S \t C-D \t L2-ALL \t L2-T \t L2-S \t L2-D #\n')
        f.write('%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n'%(cbest_all_av_sp,cbest_M_av_sp,cbest_R_av_sp,cbest_F_av_sp,l2_all_av_sp,l2_M_av_sp,l2_R_av_sp,l2_F_av_sp))
        f.write('%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n'%(std_all_sp,std_M_sp,std_R_sp,std_F_sp,l2std_all_sp,l2std_M_sp,l2std_R_sp,l2std_F_sp))

    with open(path+'/cbest_l2_space_av_comp.txt','w') as f:
        f.write('# C-T \t C-S \t C-D \t L2-T \t L2-S \t L2-D #\n')
        f.write('%f\t%f\t%f\t%f\t%f\t%f\n'%(cbest_M_av_sp_comp2,cbest_R_av_sp_comp2,cbest_F_av_sp_comp2,l2_M_av_sp_comp2,l2_R_av_sp_comp2,l2_F_av_sp_comp2))
        f.write('%f\t%f\t%f\t%f\t%f\t%f\n'%(std_M_sp_comp,std_R_sp_comp,std_F_sp_comp,l2std_M_sp_comp,l2std_R_sp_comp,l2std_F_sp_comp))

    with open(path+'/cbest_l2_time_av.txt','w') as f:
        f.write('# C-ALL \t C-T \t C-S \t C-D \t L2-ALL \t L2-T \t L2-S \t L2-D #\n')
        f.write('%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n'%(cbest_all_av_t,cbest_M_av_t,cbest_R_av_t,cbest_F_av_t,l2_all_av_t,l2_M_av_t,l2_R_av_t,l2_F_av_t))
        f.write('%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n'%(std_all_t,std_M_t,std_R_t,std_F_t,l2std_all_t,l2std_M_t,l2std_R_t,l2std_F_t))

    with open(path+'/cbest_l2_space_time_comp.txt','w') as f:
        f.write('# C-T \t C-S \t C-D \t L2-T \t L2-S \t L2-D #\n')
        f.write('%f\t%f\t%f\t%f\t%f\t%f\n'%(cbest_M_av_t_comp2,cbest_R_av_t_comp2,cbest_F_av_t_comp2,l2_M_av_t_comp2,l2_R_av_t_comp2,l2_F_av_t_comp2))
        f.write('%f\t%f\t%f\t%f\t%f\t%f\n'%(std_M_t_comp,std_R_t_comp,std_F_t_comp,l2std_M_t_comp,l2std_R_t_comp,l2std_F_t_comp))


    Sf = str(Sf)

    fig, ax = plt.subplots(1,3, sharex = 'all', figsize=(20,5))
    ax[0].plot(t2,cbest_M_av_t_comp, color = 'red')
    ax[0].set_xlabel('t [ms]')
    ax[0].set_ylabel(r'$C_{\rm best, T}$')

    ax[1].plot(t2,cbest_R_av_t_comp, color = 'blue')
    ax[1].set_xlabel('t [ms]')
    ax[1].set_ylabel(r'$C_{\rm best, S}$')

    ax[2].plot(t2,cbest_F_av_t_comp, color = 'green')
    ax[2].set_xlabel('t [ms]')
    ax[2].set_ylabel(r'$C_{\rm best, D}$')
    fig.suptitle(r'$S_f = $'+Sf+', component av', fontsize= 20)
    fig.tight_layout()
    plt.savefig(path+'/c_best_time_ev_comp_res'+res+'-sf_'+Sf+'.pdf')

    fig, ax2 = plt.subplots(1,3,sharex='all', figsize=(20,5))
    ax2[0].plot(t2,l2_M_av_t_comp, color = 'red')
    ax2[0].set_xlabel('t [ms]')
    ax2[0].set_ylabel(r'$|L2|_{\rm T}$')

    ax2[1].plot(t2,l2_R_av_t_comp, color = 'blue')
    ax2[1].set_xlabel('t [ms]')
    ax2[1].set_ylabel(r'$|L2|_{\rm S}$')

    ax2[2].plot(t2,l2_F_av_t_comp, color = 'green')
    ax2[2].set_xlabel('t [ms]')
    ax2[2].set_ylabel(r'$|L2|_{\rm D}$')
    fig.suptitle(r'$S_f = $'+Sf+', component av', fontsize= 20)
    fig.tight_layout()
    plt.savefig(path+'/l2_time_ev_comp_res'+res+'-sf_'+Sf+'.pdf')


    fig, ax3 = plt.subplots(1,4,sharex='all', figsize=(25,5))
    ax3[0].plot(t2,cbest_M_t,color='red')
    ax3[0].set_xlabel('t [ms]')
    ax3[0].set_ylabel(r'$C_{\rm best, T}$')

    ax3[1].plot(t2,cbest_R_t, color = 'blue')
    ax3[1].set_xlabel('t [ms]')
    ax3[1].set_ylabel(r'$C_{\rm best, S}$')

    ax3[2].plot(t2,cbest_F_t, color = 'green')
    ax3[2].set_xlabel('t [ms]')
    ax3[2].set_ylabel(r'$C_{\rm best, D}$')

    ax3[3].plot(t2,cbest_all_t, color = 'magenta')
    ax3[3].set_xlabel('t [ms]')
    ax3[3].set_ylabel(r'$C_{\rm best, all}$')
    fig.suptitle(r'$S_f = $'+Sf, fontsize= 20)
    fig.tight_layout()
    plt.savefig(path+'/c_best_time_ev_res'+res+'-sf_'+Sf+'.pdf')

    fig, ax4 = plt.subplots(1,4,sharex='all', figsize=(25,5))
    ax4[0].plot(t2,l2_M_t, color = 'red')
    ax4[0].set_xlabel('t [ms]')
    ax4[0].set_ylabel(r'$|L2|_{\rm T}$')

    ax4[1].plot(t2,l2_R_t, color = 'blue')
    ax4[1].set_xlabel('t [ms]')
    ax4[1].set_ylabel(r'$|L2|_{\rm S}$')

    ax4[2].plot(t2,l2_F_t, color = 'green')
    ax4[2].set_xlabel('t [ms]')
    ax4[2].set_ylabel(r'$|L2|_{\rm D}$')

    ax4[3].plot(t2,l2_all_t, color = 'magenta')
    ax4[3].set_xlabel('t [ms]')
    ax4[3].set_ylabel(r'$|L2|_{\rm all}$')

    fig.suptitle(r'$S_f = $'+Sf, fontsize= 20)

    fig.tight_layout()
    plt.savefig(path+'/l2_time_ev_res'+res+'-sf_'+Sf+'.pdf')

    return()
    
def cbest_old(Sf,lenr,lenphi,lenz,lent,tau_T_theo,tau_T_grad,tau_S_theo,tau_S_grad,tau_M_theo,tau_M_grad):
   
    lent = lent-1
#    tauTteoflat = np.zeros((lent,6*lenr*lenphi*lenz))
#    tauTgradflat = np.zeros((lent,6*lenr*lenphi*lenz))
#    tauSteoflat = np.zeros((lent,3*lenr*lenphi*lenz))
#    tauSgradflat = np.zeros((lent,3*lenr*lenphi*lenz))
#    tauMteoflat = np.zeros((lent,3*lenr*lenphi*lenz))
#    tauMgradflat = np.zeros((lent,3*lenr*lenphi*lenz))
    c_tauS = np.zeros((lent,3))
    c_tauT = np.zeros((lent,6))
    c_tauM = np.zeros((lent,3))

    tau_S_theo = tau_S_theo[1:]
    tau_S_grad = tau_S_grad[1:]
    
    tauTteo = np.array([tau_T_theo[1:,0,0,:,:,:], tau_T_theo[1:,0,1,:,:,:], tau_T_theo[1:,0,2,:,:,:], tau_T_theo[1:,1,1,:,:,:], tau_T_theo[1:,1,2,:,:,:], tau_T_theo[1:,2,2,:,:,:]])
    tauTgrad = np.array([tau_T_grad[1:,0,0,:,:,:], tau_T_grad[1:,0,1,:,:,:], tau_T_grad[1:,0,2,:,:,:], tau_T_grad[1:,1,1,:,:,:], tau_T_grad[1:,1,2,:,:,:], tau_T_grad[1:,2,2,:,:,:]])
    
    tauMteo = np.array([tau_M_theo[1:,0,1,:,:,:], tau_M_theo[1:,0,2,:,:,:], tau_M_theo[1:,1,2,:,:,:]])
    tauMgrad = np.array([tau_M_grad[1:,0,1,:,:,:], tau_M_grad[1:,0,2,:,:,:], tau_M_grad[1:,1,2,:,:,:]])

    for x in range(0,lent):
        for cc in range(0,6):
            tauTteoflat = tauTteo[cc,x,:,:,:].flatten()
            tauTgradflat = tauTgrad[cc,x,:,:,:].flatten()

            c_tauT[x,cc] = np.sum(tauTteoflat[:]*tauTgradflat[:])/np.sum(tauTgradflat[:]*tauTgradflat[:])
        
        for cc in range(0,3):
            tauSteoflat = tau_S_theo[x,cc,:,:,:].flatten()
            tauSgradflat = tau_S_grad[x,cc,:,:,:].flatten()            
            tauMteoflat = tauMteo[cc,x,:,:,:].flatten()
            tauMgradflat = tauMgrad[cc,x,:,:,:].flatten()        
        
            c_tauS[x,cc] = np.sum(tauSteoflat[:]*tauSgradflat[:])/np.sum(tauSgradflat[:]*tauSgradflat[:])
            c_tauM[x,cc] = np.sum(tauMteoflat[:]*tauMgradflat[:])/np.sum(tauMgradflat[:]*tauMgradflat[:])
    
    c_tauT_av = np.mean(c_tauT, axis = 1)
    c_tauM_av = np.mean(c_tauM, axis = 1)
    c_tauS_av = np.mean(c_tauS, axis = 1)

    print('cbest T time = ', c_tauT_av)
    print('cbest S time = ', c_tauS_av)
    print('cbest M time = ', c_tauM_av)
    
    c_tauS_av_t = np.mean(c_tauS_av)
    c_tauT_av_t = np.mean(c_tauT_av)
    c_tauM_av_t = np.mean(c_tauM_av) 
    
    stdS = stdev(c_tauS_av)
    stdT = stdev(c_tauT_av)
    stdM = stdev(c_tauM_av)
    
    print('cbest T  = ', c_tauT_av_t,'+-',stdT)
    print('cbest S  = ', c_tauS_av_t,'+-',stdS)
    print('cbest M  = ', c_tauM_av_t,'+-',stdM)

    return c_tauT_av, c_tauS_av, c_tauM_av

def cbest_files(cparams,Sf,lenr,lenphi,lenz,lent):
     
    c_tauT_av = cparams[0]
    c_tauS_av = cparams[1]
    c_tauM_av = cparams[2]
    
    with open('c_best.txt', 'w') as f:
        for t in range(0,lent):
            f.write('%f\t%f\t%f\n'%(c_tauT_av[t],c_tauS_av[t],c_tauM_av[t]))
              
    
    return()
 
#%%
def main():

    lenphi = int(sys.argv[2])
    
    Sf = int(sys.argv[1])
    
    if lenphi == 240 : 
        
        lenr = 60
        lenz = 60
        
    elif lenphi == 400 :
                
        lenr = 100
        lenz = 100

    elif lenphi == 304:

        lenr = 76
        lenz = 76
         
    m = 10
    l = 10
    n = 10
    
    res = str(lenr)+'_'+str(lenphi)+'_'+str(lenz)
    pathtensors = 'results/box-filter/Sf_'+str(Sf)+'/'+res+'/sfs_tensors/files/'+str(m)+'_points'

    t = np.linspace(0,30,61)

    tauTteo = np.zeros((len(t),6,m,l,n))
    tauTgrad = np.zeros((len(t),6,m,l,n))
    tauSteo = np.zeros((len(t),3,m,l,n))
    tauSgrad = np.zeros((len(t),3,m,l,n))
    tauDteo = np.zeros((len(t),3,m,l,n))
    tauDgrad = np.zeros((len(t),3,m,l,n))

    for x in range(0,len(t)):
        
        if lenphi == 304:
            arx = format(25*x, "08")
        else : 
            arx = format(25*x, "04")
        
        arxteo = pathtensors+'/sfs_tensors_teo_res_'+res+'-'+str(arx)+'.h5' 
        arxgrad = pathtensors+'/sfs_tensors_grad_res_'+res+'-'+str(arx)+'.h5'
    
        tauTteo[x], tauSteo[x], tauDteo[x] = readh5files(arxteo)
        tauTgrad[x], tauSgrad[x], tauDgrad[x] = readh5files(arxgrad)

    pathpearson = 'results/box-filter/cbest_l2norm/res_'+res+'/Sf_'+str(Sf)+'/'+str(m)+'_points'
    
    try:
        os.makedirs(pathpearson)
    except OSError as error:
        print(error)
        
    l2_log(tauTteo, tauSteo , tauDteo, tauTgrad, tauSgrad, tauDgrad,t, m,l,n,res,str(Sf),pathpearson)

    return()
    
    
#%%
    
if __name__ == "__main__":

    main()


    
    
    
