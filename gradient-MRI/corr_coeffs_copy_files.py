#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 10 10:39:54 2020

@author: miquelmiravet
"""

import os

Sf = input('Sf : ')
lenr = input('lenr : ')
lenphi = input('lenphi : ')
lenz = input('lenz : ')

#pearson corr coeff

path = 'Sf_'+Sf+'/'+lenr+'_'+lenphi+'_'+lenz+'/pearson_coeff'
    
try:
    os.mkdir(path)
except OSError as error:
    print(error)
        
os.system('scp miquel@tyrion24:/scr/miquel/MRI/Miquel/'+path+'/pearson.txt '+path+'/.')
#os.system('scp miquel@tyrion24:/scr/miquel/MRI/Miquel/'+path+'/pearson_bis.txt '+path+'/.')
os.system('scp miquel@tyrion24:/scr/miquel/MRI/Miquel/'+path+'/pearson_split.txt '+path+'/.')
#cbest parameter

path ='Sf_'+Sf+'/'+lenr+'_'+lenphi+'_'+lenz+'/c_best'
    
try:
    os.mkdir(path)
except OSError as error:
    print(error)
        
#os.system('scp miquel@tyrion24:/scr/miquel/MRI/Miquel/'+path+'/c_best.txt '+path+'/.')
