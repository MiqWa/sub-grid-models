#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 12 17:09:16 2022

@author: miquelmiravet
"""

#####################################
#                                   #
#            MODEL TEST             #
#                                   #
#####################################

"""
We're going to test the performance of our model by computing the energy of the
stresses and comparing it to the one from our model.

"""
import os
import numpy as np
from scipy.optimize import minimize
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy.ctypeslib as npct
from ctypes import c_int, c_double
import h5py
from scipy.stats import pearsonr
from scipy.linalg import norm

#=================================================================================
array_3d_float = npct.ndpointer(dtype=np.float64, ndim=3, flags='CONTIGUOUS')
array_1d_float = npct.ndpointer(dtype=np.float64, ndim=1, flags='CONTIGUOUS')

libenergyev = npct.load_library("/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/energy_dens_ev/libenergyev", ".")
libenergyev.energy_ev.restype = None
libenergyev.energy_ev.argtypes = [c_double, c_int,c_double,c_double, c_double,array_1d_float, array_1d_float]
#=================================================================================

def readh5files(arx):

    print('DATA FROM FILE:      ',arx)

    with h5py.File(arx, "r") as f:
    # List all groups
        print("Keys: %s" % f.keys())
        a_group_key = list(f.keys())
        for i in a_group_key:
            print(i)

        Sf = f[a_group_key[0]][...]
        print('Sf    =', Sf)
        phi = f[a_group_key[2]][...]
        print('phi shape    :', phi.shape)
        phif = f[a_group_key[3]][...]
        print('phi filter shape    :', phif.shape)
        r = f[a_group_key[4]][...]
        print('r shape    :', r.shape)
        rf = f[a_group_key[5]][...]
        print('r filter shape    :', rf.shape)
        rho = f[a_group_key[6]][...]
        print('rho mean shape    :', rho.shape)
        Fphiz = f[a_group_key[7]][...]
        print('F_phi,z shape    :', Fphiz.shape)
        Frphi = f[a_group_key[8]][...]
        print('F_r,phi shape    :', Frphi.shape)
        Frz = f[a_group_key[9]][...]
        print('F_r,z shape    :', Frz.shape)
        Mphiphi = f[a_group_key[10]][...]
        print('M_phi,phi shape    :', Mphiphi.shape)
        Mphiz = f[a_group_key[11]][...]
        print('M_phi,z shape    :', Mphiz.shape)
        Mrphi = f[a_group_key[12]][...]
        print('M_r,phi shape    :', Mrphi.shape)
        Mrr = f[a_group_key[13]][...]
        print('M_r,r shape    :', Mrr.shape)
        Mrz = f[a_group_key[14]][...]
        print('M_r,z shape    :', Mrz.shape)
        Mzz = f[a_group_key[15]][...]
        print('M_z,z shape    :', Mzz.shape)
        Rphiphi = f[a_group_key[16]][...]
        print('R_phi,phi shape    :', Rphiphi.shape)
        Rphiz = f[a_group_key[17]][...]
        print('R_phi,z shape    :', Rphiz.shape)
        Rrphi = f[a_group_key[18]][...]
        print('R_r,phi shape    :', Rrphi.shape)
        Rrr = f[a_group_key[19]][...]
        print('R_r,r shape    :', Rrr.shape)
        Rrz = f[a_group_key[20]][...]
        print('R_r,z shape    :', Rrz.shape)
        Rzz = f[a_group_key[21]][...]
        print('R_z,z shape    :', Rzz.shape)
        time = f[a_group_key[22]][...]
        print('time shape', time.shape)
        z = f[a_group_key[23]][...]
        print('z shape    :', z.shape)
        zf = f[a_group_key[24]][...]
        print('z filter shape    :', zf.shape)

        f.close()

    M = np.array([Mrr,Mrphi,Mrz,Mphiphi,Mphiz,Mzz])
    R = np.array([Rrr,Rrphi,Rrz,Rphiphi,Rphiz,Rzz])
    F = np.array([Frphi,Frz,Fphiz])

    print('Dimensions of the grid     :',len(r)," x ",len(phi)," x ",len(z))
    print('Time           :',time,' ms')

    return [M, R, F, rho, r, phi, z, rf, phif, zf]

#===========================================================================================

def read_coeffs(res):

    path = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/coefficients_r0/coeffs-MRI-final_v2/'

    alpha = np.loadtxt(path+'alphaR_r0_av.txt')

    beta = np.loadtxt(path+'betaR_r0_av.txt')

    gamma = np.loadtxt(path+'gammaR_r0_av.txt')

    return alpha, beta, gamma

#===========================================================================================

def energy_ev(t, scale,q,omega, factor_c, energy_ini, factor_en,rho):

    sigma = 0.27

    b0 = 3.45e13*2.874214371e-25

    gamma_pi_fac = 2*np.sqrt(2)*sigma*(1-(2-q)**2/4)**(0.5)*omega/b0 #CHECK THIS EXPRESSION

    std_fact= factor_c/(rho**(0.5)*scale)

    timestep= 5e-5

    timestep = 0.001*2.99792458e10*timestep
    tfinal = t[-1]*0.001*2.99792458e10
    lent = int(round((tfinal)/timestep))+1

    energy_t = np.zeros(lent)
    energy_t[0] = energy_ini

    energy_pi = np.zeros(lent)
    energy_pi[0] =energy_ini*factor_en

    gamma_pi = np.zeros(lent)
    gamma_pi[0] = gamma_pi_fac*energy_ini**(0.5)

    gamma_mri = q*omega

    libenergyev.energy_ev(std_fact,lent,timestep,gamma_mri, gamma_pi_fac,energy_t,energy_pi)

    ratio = int((lent-1)/(len(t)-1))
    energy_model = np.zeros(len(t))
    energy_pi2 = np.zeros(len(t))

    for i in range(0,len(t)-1):
        energy_model[i] = energy_t[ratio*i]
        energy_pi2[i] = energy_pi[ratio*i]

    energy_model[-1] = energy_t[-1]
    energy_pi2[-1] = energy_pi[-1]

    return energy_model, energy_pi2

#===========================================================================================

def energy_mod_space(rf,lenphif,lenzf,factor_c,t, scale, energy_ini,res, factor_en, rho):

    r0tilde = 15.5e5
    omega0tilde = 1824*3.33564095e-11
    q = 1.25

    energy_mod = np.zeros((len(t), len(rf), lenphif, lenzf))
    energy_pi = np.zeros((len(t), len(rf), lenphif, lenzf))

    for i in range(0,len(rf)):

        omega = omega0tilde*(rf[i]/r0tilde)**(-q)

        for j in range(0,lenphif):

            for k in range(0,lenzf):

                energy_mod[:,i,j,k], energy_pi[:,i,j,k] = energy_ev(t, scale, q, omega, factor_c, energy_ini, factor_en,rho[i,j,k])

    return energy_mod, energy_pi

#===========================================================================================

def comparison_stress(sim,mod,t, comp):

    ind = 28
    compdel = comp

    t = t[ind:]
    sim = sim[ind:]
    mod = mod[ind:]

    l2norm = np.zeros(len(t))

    for tt in range(0,len(t)):
        integrand = (sim[tt,:]-mod[tt,:])**2
        mean_har = 0.5*(1/np.sum(mod[tt,:]**2)+1/np.sum(sim[tt,:]**2))
        l2norm[tt] = np.sqrt(np.sum(integrand)*mean_har)

    l2norm_tmean = (np.sum(l2norm**2)/len(t))**(0.5)

    return l2norm_tmean

#===========================================================================================

def comparison_stress_diag(sim,mod,t, comp):

    ind = 28

    if comp == 6:
        sim = np.delete(sim, [2,4,5], axis = 1)
        mod = np.delete(mod, [2,4,5], axis = 1)

    t = t[ind:]
    sim = sim[ind:]
    mod = mod[ind:]

    l2norm = np.zeros(len(t))

    for tt in range(0,len(t)):
        integrand = (sim[tt,:]-mod[tt,:])**2
        mean_har = 0.5*(1/np.sum(mod[tt,:]**2)+1/np.sum(sim[tt,:]**2))
        l2norm[tt] = np.sqrt(np.sum(integrand)*mean_har)

    l2norm_tmean = (np.sum(l2norm**2)/len(t))**(0.5)

    return l2norm_tmean

#===========================================================================================

def fun_minimize_M(x):

    factor_c = x
    energy_ini = 7e-31
    factor_en = 1e3
    lambda_mri = 25000

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_sim_test/"

    Sf = int(sys.argv[1])

    lenphi = 400

    if lenphi == 400 :

        lenr = 100
        lenz = 100


    elif lenphi == 240 :

        lenr = 60
        lenz = 60

    elif lenphi == 304 :

        lenr = 76
        lenz = 76

    res = str(lenr)+'_'+str(lenphi)+'_'+str(lenz)
    direc = path_data+'res_'+res+'/b0z-3.45e13/Sf_'+str(Sf)+'/'
    t_final = 30
    lent = 61
    t = np.linspace(0,t_final,lent)
    inivars = readh5files(direc+'stresses_sim_res_'+res+'-0000.h5')

    rfilter = inivars[7]
    phifilter = inivars[8]
    zfilter = inivars[9]

    Msim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Rsim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Fsim = np.zeros((lent,3,len(rfilter),len(phifilter),len(zfilter)))
    rhomean = np.zeros((lent,len(rfilter),len(phifilter),len(zfilter)))

    for tt in range(0, lent):

        arx = format(25*tt, "04")
        Msim[tt], Rsim[tt], Fsim[tt], rhomean[tt], r, phi, z, rfilter, phifilter, zfilter = readh5files(direc+'stresses_sim_res_'+res+'-'+arx+'.h5')

    cellsize = (r[1]-r[0]+(r[-1]+r[0])*0.5*(phi[1]-phi[0])+z[1]-z[0])/3

    filtersize = Sf*cellsize
    scale = min(filtersize,lambda_mri)

    alphaPI, betaPI, gammaPI = read_coeffs(res)

    q = 1.25
    alphaMRI = np.zeros(6)
    alphaMRI[0] = (4-q)/q
    alphaMRI[3] = alphaMRI[0]
    alphaMRI[1] = -alphaMRI[0]

    energyMRI, energyPI = energy_mod_space(rfilter,len(phifilter),len(zfilter),factor_c,t, scale, energy_ini,res, factor_en,rhomean[0,:,:,:])

    Mmod = np.zeros((lent, 6, len(rfilter), len(phifilter), len(zfilter)))

    for i in range(0,6):
        Mmod[:,i,:,:,:] = alphaMRI[i]*energyMRI[:,:,:,:]+alphaPI[i]*energyPI[:,:,:,:]

    l2norm = comparison_stress(Msim,Mmod, t, 6)

    return l2norm

#===========================================================================================

def fun_minimize_M_diag(x):

    factor_c = x
    energy_ini = 7e-31
    factor_en = 1e3
    lambda_mri = 25000

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_sim_test/"

    Sf = int(sys.argv[1])

    lenphi = 400

    if lenphi == 400 :

        lenr = 100
        lenz = 100


    elif lenphi == 240 :

        lenr = 60
        lenz = 60

    elif lenphi == 304 :

        lenr = 76
        lenz = 76

    res = str(lenr)+'_'+str(lenphi)+'_'+str(lenz)
    direc = path_data+'res_'+res+'/b0z-3.45e13/Sf_'+str(Sf)+'/'
    t_final = 30
    lent = 61
    t = np.linspace(0,t_final,lent)
    inivars = readh5files(direc+'stresses_sim_res_'+res+'-0000.h5')

    rfilter = inivars[7]
    phifilter = inivars[8]
    zfilter = inivars[9]

    Msim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Rsim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Fsim = np.zeros((lent,3,len(rfilter),len(phifilter),len(zfilter)))
    rhomean = np.zeros((lent,len(rfilter),len(phifilter),len(zfilter)))

    for tt in range(0, lent):

        arx = format(25*tt, "04")
        Msim[tt], Rsim[tt], Fsim[tt], rhomean[tt], r, phi, z, rfilter, phifilter, zfilter = readh5files(direc+'stresses_sim_res_'+res+'-'+arx+'.h5')

    cellsize = (r[1]-r[0]+(r[-1]+r[0])*0.5*(phi[1]-phi[0])+z[1]-z[0])/3

    filtersize = Sf*cellsize
    scale = min(filtersize,lambda_mri)

    alphaPI, betaPI, gammaPI = read_coeffs(res)

    q = 1.25
    alphaMRI = np.zeros(6)
    alphaMRI[0] = (4-q)/q
    alphaMRI[3] = alphaMRI[0]
    alphaMRI[1] = -alphaMRI[0]

    energyMRI, energyPI = energy_mod_space(rfilter,len(phifilter),len(zfilter),factor_c,t, scale, energy_ini,res, factor_en,rhomean[0,:,:,:])

    Mmod = np.zeros((lent, 6, len(rfilter), len(phifilter), len(zfilter)))

    for i in range(0,6):
        Mmod[:,i,:,:,:] = alphaMRI[i]*energyMRI[:,:,:,:]+alphaPI[i]*energyPI[:,:,:,:]

    l2norm = comparison_stress_diag(Msim,Mmod, t, 6)

    return l2norm

#===========================================================================================

def fun_minimize_R(x):

    factor_c = x
    energy_ini = 7e-31
    factor_en = 1e3
    lambda_mri = 25000

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_sim_test/"

    Sf = int(sys.argv[1])

    lenphi = 400

    if lenphi == 400 :

        lenr = 100
        lenz = 100


    elif lenphi == 240 :

        lenr = 60
        lenz = 60

    elif lenphi == 304 :

        lenr = 76
        lenz = 76

    res = str(lenr)+'_'+str(lenphi)+'_'+str(lenz)
    direc = path_data+'res_'+res+'/b0z-3.45e13/Sf_'+str(Sf)+'/'
    t_final = 30
    lent = 61
    t = np.linspace(0,t_final,lent)
    inivars = readh5files(direc+'stresses_sim_res_'+res+'-0000.h5')

    rfilter = inivars[7]
    phifilter = inivars[8]
    zfilter = inivars[9]

    Msim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Rsim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Fsim = np.zeros((lent,3,len(rfilter),len(phifilter),len(zfilter)))
    rhomean = np.zeros((lent,len(rfilter),len(phifilter),len(zfilter)))

    for tt in range(0, lent):

        arx = format(25*tt, "04")
        Msim[tt], Rsim[tt], Fsim[tt], rhomean[tt], r, phi, z, rfilter, phifilter, zfilter = readh5files(direc+'stresses_sim_res_'+res+'-'+arx+'.h5')

    cellsize = (r[1]-r[0]+(r[-1]+r[0])*0.5*(phi[1]-phi[0])+z[1]-z[0])/3

    filtersize = Sf*cellsize

    scale =min(filtersize,lambda_mri)

    alphaPI, betaPI, gammaPI = read_coeffs(res)

    betaMRI = np.zeros(6)
    betaMRI[0] = 1
    betaMRI[3] = 1
    betaMRI[1] = 1

    energyMRI, energyPI = energy_mod_space(rfilter,len(phifilter),len(zfilter),factor_c,t, scale, energy_ini,res, factor_en,rhomean[0,:,:,:])

    Rmod = np.zeros((lent, 6, len(rfilter), len(phifilter), len(zfilter)))

    for i in range(0,6):
        Rmod[:,i,:,:,:] = (betaMRI[i]*energyMRI[:,:,:,:]+betaPI[i]*energyPI[:,:,:,:])/rhomean[:,:,:,:]

    l2norm = comparison_stress(Rsim,Rmod,t,6)

    return l2norm

#===========================================================================================

def fun_minimize_R_diag(x):

    factor_c = x
    energy_ini = 7e-31
    factor_en = 1e3
    lambda_mri = 25000

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_sim_test/"

    Sf = int(sys.argv[1])

    lenphi = 400

    if lenphi == 400 :

        lenr = 100
        lenz = 100


    elif lenphi == 240 :

        lenr = 60
        lenz = 60

    elif lenphi == 304 :

        lenr = 76
        lenz = 76

    res = str(lenr)+'_'+str(lenphi)+'_'+str(lenz)
    direc = path_data+'res_'+res+'/b0z-3.45e13/Sf_'+str(Sf)+'/'
    t_final = 30
    lent = 61
    t = np.linspace(0,t_final,lent)
    inivars = readh5files(direc+'stresses_sim_res_'+res+'-0000.h5')

    rfilter = inivars[7]
    phifilter = inivars[8]
    zfilter = inivars[9]

    Msim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Rsim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Fsim = np.zeros((lent,3,len(rfilter),len(phifilter),len(zfilter)))
    rhomean = np.zeros((lent,len(rfilter),len(phifilter),len(zfilter)))

    for tt in range(0, lent):

        arx = format(25*tt, "04")
        Msim[tt], Rsim[tt], Fsim[tt], rhomean[tt], r, phi, z, rfilter, phifilter, zfilter = readh5files(direc+'stresses_sim_res_'+res+'-'+arx+'.h5')

    cellsize = (r[1]-r[0]+(r[-1]+r[0])*0.5*(phi[1]-phi[0])+z[1]-z[0])/3

    filtersize = Sf*cellsize

    scale =min(filtersize,lambda_mri)

    alphaPI, betaPI, gammaPI = read_coeffs(res)

    betaMRI = np.zeros(6)
    betaMRI[0] = 1
    betaMRI[3] = 1
    betaMRI[1] = 1

    energyMRI, energyPI = energy_mod_space(rfilter,len(phifilter),len(zfilter),factor_c,t, scale, energy_ini,res, factor_en,rhomean[0,:,:,:])

    Rmod = np.zeros((lent, 6, len(rfilter), len(phifilter), len(zfilter)))

    for i in range(0,6):
        Rmod[:,i,:,:,:] = (betaMRI[i]*energyMRI[:,:,:,:]+betaPI[i]*energyPI[:,:,:,:])/rhomean[:,:,:,:]

    l2norm = comparison_stress_diag(Rsim,Rmod,t,6)

    return l2norm

#===========================================================================================

def fun_minimize_F(x):

    factor_c = x
    energy_ini = 7e-31
    factor_en = 1e3
    lambda_mri = 25000

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_sim_test/"

    Sf = int(sys.argv[1])

    lenphi = 400

    if lenphi == 400 :

        lenr = 100
        lenz = 100


    elif lenphi == 240 :

        lenr = 60
        lenz = 60

    elif lenphi == 304 :

        lenr = 76
        lenz = 76

    res = str(lenr)+'_'+str(lenphi)+'_'+str(lenz)
    direc = path_data+'res_'+res+'/b0z-3.45e13/Sf_'+str(Sf)+'/'
    t_final = 30
    lent = 61
    t = np.linspace(0,t_final,lent)
    inivars = readh5files(direc+'stresses_sim_res_'+res+'-0000.h5')

    rfilter = inivars[7]
    phifilter = inivars[8]
    zfilter = inivars[9]

    Msim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Rsim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Fsim = np.zeros((lent,3,len(rfilter),len(phifilter),len(zfilter)))
    rhomean = np.zeros((lent,len(rfilter),len(phifilter),len(zfilter)))

    for tt in range(0, lent):

        arx = format(25*tt, "04")
        Msim[tt], Rsim[tt], Fsim[tt], rhomean[tt], r, phi, z, rfilter, phifilter, zfilter = readh5files(direc+'stresses_sim_res_'+res+'-'+arx+'.h5')

    cellsize = (r[1]-r[0]+(r[-1]+r[0])*0.5*(phi[1]-phi[0])+z[1]-z[0])/3

    filtersize = Sf*cellsize

    scale = min(filtersize,lambda_mri)

    alphaPI, betaPI, gammaPI = read_coeffs(res)

    energyMRI, energyPI = energy_mod_space(rfilter,len(phifilter),len(zfilter),factor_c,t, scale, energy_ini,res, factor_en,rhomean[0,:,:,:])

    Fmod = np.zeros((lent, 3, len(rfilter), len(phifilter), len(zfilter)))

    for j in range(0,3):
        Fmod[:,j,:,:,:] = gammaPI[j]*energyPI[:,:,:,:]/rhomean[:,:,:,:]**(0.5)

    l2norm = comparison_stress(Fsim,Fmod,t,3)

    return l2norm

#===========================================================================================

def plot_norml2(factor_carr,Sf, path):

    l2cont = np.zeros((3,len(factor_carr)))

    for k in range(0,len(factor_carr)):
        l2cont[0,k] = fun_minimize_M(factor_carr[k])
        l2cont[1,k] = fun_minimize_R(factor_carr[k])
        l2cont[2,k] = fun_minimize_F(factor_carr[k])

    fig, ax = plt.subplots(1,3, sharex = 'all', sharey = 'all', figsize = (15,4))

    for i in range(0,3):
        ax[i].plot(factor_carr,l2cont[i,:], color = 'blue')
        ax[i].set_xlabel('C')

    ax[0].set_ylabel('L2')
    ax[1].set_title('R')
    ax[2].set_title('F')
    ax[0].set_title('M')
#    ax[0].set_yscale('log')
#    ax[1].set_yscale('log')
#    ax[2].set_yscale('log')

    plt.savefig(path+'/nologv2l2norm_Sf_'+str(Sf)+'early.pdf')

    l2diag = np.zeros((2,len(factor_carr)))

    for k in range(0,len(factor_carr)):
        l2diag[0,k] = fun_minimize_M_diag(factor_carr[k])
        l2diag[1,k] = fun_minimize_R_diag(factor_carr[k])

    fig, ax = plt.subplots(1,2, sharex = 'all', sharey = 'all', figsize = (8,4))

    for i in range(0,2):
        ax[i].plot(factor_carr,l2diag[i,:], color = 'blue')
        ax[i].set_xlabel('C')

    ax[0].set_ylabel('L2')
    ax[1].set_title('R')
    ax[0].set_title('M')
#    ax[0].set_yscale('log')
#    ax[1].set_yscale('log')

    plt.savefig(path+'/nologv2l2norm_diag_Sf_'+str(Sf)+'early.pdf')

    minl2M = np.amin(l2cont[0])
    minl2R = np.amin(l2cont[1])

    indmin = np.where(l2cont[0] == minl2M)[0][0]
    copt = factor_carr[indmin]

    indexesM = []
    for i in range(0,len(l2cont[0])):
        if l2cont[0,i] > minl2M*(1.1):
            indexesM.append(i)

    cnomM = factor_carr[indexesM]

    cnomM = cnomM-copt
    cneg = cnomM[cnomM<0]
    cminM = np.amin(abs(cneg))
    cminM = -cminM+copt
    cpos = cnomM[cnomM>0]
    cmaxM = np.amin(cpos)
    cmaxM = cmaxM+copt

    with open(path+'/c_opt_interval'+str(Sf)+'_M_nologv2.txt', 'w') as f:
        f.write('# C OPT \t C+ \t C- # \n')
        f.write('%.10f\t%.10f\t%.10f\n'%(copt,cmaxM,cminM))

    indmin = np.where(l2cont[1] == minl2R)[0][0]
    copt = factor_carr[indmin]

    indexesR = [i for i in range(0,len(l2cont[1])) if l2cont[1,i] > minl2R*(1.1)]

    cnomR = factor_carr[indexesR]

    cnomR = cnomR-copt
    cneg = cnomR[cnomR<0]
    cminR = np.amin(abs(cneg))
    cminR = -cminR+copt
    cpos = cnomR[cnomR>0]
    cmaxR = np.amin(cpos)
    cmaxR = cmaxR+copt

    with open(path+'/c_opt_interval'+str(Sf)+'_R_nologv2.txt', 'w') as f:
        f.write('# C OPT \t C+ \t C- # \n')
        f.write('%.10f\t%.10f\t%.10f\n'%(copt,cmaxR,cminR))


    minl2Md = np.amin(l2diag[0])
    minl2Rd = np.amin(l2diag[1])

    indmind = np.where(l2diag[0] == minl2Md)[0][0]
    coptd = factor_carr[indmind]

    indexesMd = [i for i in range(0,len(l2diag[0])) if l2diag[0,i] > minl2Md*(1.1)]

    cnomM = factor_carr[indexesMd]

    cnomM = cnomM-coptd
    cneg = cnomM[cnomM<0]
    cminM = np.amin(abs(cneg))
    cminM = -cminM+coptd
    cpos = cnomM[cnomM>0]
    cmaxM = np.amin(cpos)
    cmaxM = cmaxM+coptd

    with open(path+'/c_opt_interval'+str(Sf)+'_diagM_nologv2.txt', 'w') as f:
        f.write('# C OPT \t C+ \t C- # \n')
        f.write('%.10f\t%.10f\t%.10f\n'%(coptd,cmaxM,cminM))

    indmind = np.where(l2diag[1] == minl2Rd)[0][0]
    coptd = factor_carr[indmind]

    indexesRd = [i for i in range(0,len(l2diag[1])) if l2diag[1,i] > minl2Rd*(1.1)]

    cnomR = factor_carr[indexesRd]

    cnomR = cnomR-coptd
    cneg = cnomR[cnomR<0]
    cminR = np.amin(abs(cneg))
    cminR = -cminR+coptd
    cpos = cnomR[cnomR>0]
    cmaxR = np.amin(cpos)
    cmaxR = cmaxR+coptd

    with open(path+'/c_opt_interval'+str(Sf)+'_diagR_nologv2.txt', 'w') as f:
        f.write('# C OPT \t C+ \t C- # \n')
        f.write('%.10f\t%.10f\t%.10f\n'%(coptd,cmaxR,cminR))

    return()

#===========================================================================================

def main():

    Sf = int(sys.argv[1])
    plot = True

    lenphi = 400

    if lenphi == 400 :

        lenr = 100
        lenz = 100

    elif lenphi == 240 :

        lenr = 60
        lenz = 60

    elif lenphi == 304 :

        lenr = 76
        lenz = 76

    res = str(lenr)+'_'+str(lenphi)+'_'+str(lenz)

    #Initial guesses for C:

    x0 = 10

    #bounds for the parameters:
    bounds = ((5,15),)

    test_path = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/test_l2norm/res_"+res+"/b0z-3.45e13/Sf_"+str(Sf)

    try:
        os.makedirs(test_path)
    except OSError as error:
        print(error)

    # plot :
    if plot:
        factor_c = np.linspace(7,14,25)

        plot_norml2(factor_c,Sf, test_path)

        exit

  #WITH STRESS M

    sol_M = minimize(fun_minimize_M, x0, bounds = bounds)

    l2norm_opt_M = fun_minimize_M(sol_M.x)

    with open(test_path+'/nologv2l2normMsatSf'+str(Sf)+'_facC.txt', 'w') as f:
        f.write('# L_2 NORM \t FACTOR C  # \n')
        f.write('%.10f\t%.10f\n'%(l2norm_opt_M,sol_M.x[0]))

    sol_Mdiag = minimize(fun_minimize_M_diag, x0, bounds = bounds)

    l2normdiag_opt_M = fun_minimize_M_diag(sol_Mdiag.x)

    with open(test_path+'/nologl2norm_diag_MsatSf'+str(Sf)+'_facC.txt', 'w') as f:
        f.write('# L_2 NORM \t FACTOR C  # \n')
        f.write('%.10f\t%.10f\n'%(l2norm_opt_M,sol_M.x[0]))


    #WITH STRESS R

    sol_R = minimize(fun_minimize_R, x0, bounds = bounds)

    l2norm_opt_R = fun_minimize_R(sol_R.x)

    with open(test_path+'/nologv2l2normRsatSf'+str(Sf)+'_facC.txt', 'w') as f:
        f.write('# L_2 NORM \t FACTOR C  # \n')
        f.write('%.10f\t%.10f\n'%(l2norm_opt_R,sol_R.x[0]))

    sol_Rdiag = minimize(fun_minimize_R_diag, x0, bounds = bounds)

    l2normdiag_opt_R = fun_minimize_R_diag(sol_Rdiag.x)

    with open(test_path+'/nologv2l2norm_diag_RsatSf'+str(Sf)+'_facC.txt', 'w') as f:
        f.write('# L_2 NORM \t FACTOR C  # \n')
        f.write('%.10f\t%.10f\n'%(l2norm_opt_R,sol_R.x[0]))


    #WITH STRESS F

    sol_F = minimize(fun_minimize_F, x0, bounds = bounds)

    l2norm_opt_F = fun_minimize_F(sol_F.x)

    with open(test_path+'/nologv2l2normFsatSf'+str(Sf)+'_facC.txt', 'w') as f:
        f.write('# L_2 NORM \t FACTOR C  # \n')
        f.write('%.10f\t%.10f\n'%(l2norm_opt_F,sol_F.x[0]))

    return()


main()
