#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 19 16:21:16 2022

@author: miquelmiravet
"""

#####################################
#                                   #
#               L2 NORM             #
#                                   #
#####################################

"""
We're going to perform the L2 norm test.

"""
import os
import numpy as np
from scipy.optimize import minimize
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy.ctypeslib as npct
from ctypes import c_int, c_double
import h5py
from scipy.stats import pearsonr
from scipy.linalg import norm
from statistics import stdev

#=================================================================================
array_3d_float = npct.ndpointer(dtype=np.float64, ndim=3, flags='CONTIGUOUS')
array_1d_float = npct.ndpointer(dtype=np.float64, ndim=1, flags='CONTIGUOUS')

libenergyev = npct.load_library("/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/energy_dens_ev/libenergyev", ".")
libenergyev.energy_ev.restype = None
libenergyev.energy_ev.argtypes = [c_double, c_int,c_double,c_double, c_double,array_1d_float, array_1d_float]
#=================================================================================
def readh5files(arx):

    print('DATA FROM FILE:      ',arx)

    with h5py.File(arx, "r") as f:
    # List all groups
        print("Keys: %s" % f.keys())
        a_group_key = list(f.keys())
        for i in a_group_key:
            print(i)

        Sf = f[a_group_key[0]][...]
        print('Sf    =', Sf)
        phi = f[a_group_key[2]][...]
        print('phi shape    :', phi.shape)
        phif = f[a_group_key[3]][...]
        print('phi filter shape    :', phif.shape)
        r = f[a_group_key[4]][...]
        print('r shape    :', r.shape)
        rf = f[a_group_key[5]][...]
        print('r filter shape    :', rf.shape)
        rho = f[a_group_key[6]][...]
        print('rho mean shape    :', rho.shape)
        Fphiz = f[a_group_key[7]][...]
        print('F_phi,z shape    :', Fphiz.shape)
        Frphi = f[a_group_key[8]][...]
        print('F_r,phi shape    :', Frphi.shape)
        Frz = f[a_group_key[9]][...]
        print('F_r,z shape    :', Frz.shape)
        Mphiphi = f[a_group_key[10]][...]
        print('M_phi,phi shape    :', Mphiphi.shape)
        Mphiz = f[a_group_key[11]][...]
        print('M_phi,z shape    :', Mphiz.shape)
        Mrphi = f[a_group_key[12]][...]
        print('M_r,phi shape    :', Mrphi.shape)
        Mrr = f[a_group_key[13]][...]
        print('M_r,r shape    :', Mrr.shape)
        Mrz = f[a_group_key[14]][...]
        print('M_r,z shape    :', Mrz.shape)
        Mzz = f[a_group_key[15]][...]
        print('M_z,z shape    :', Mzz.shape)
        Rphiphi = f[a_group_key[16]][...]
        print('R_phi,phi shape    :', Rphiphi.shape)
        Rphiz = f[a_group_key[17]][...]
        print('R_phi,z shape    :', Rphiz.shape)
        Rrphi = f[a_group_key[18]][...]
        print('R_r,phi shape    :', Rrphi.shape)
        Rrr = f[a_group_key[19]][...]
        print('R_r,r shape    :', Rrr.shape)
        Rrz = f[a_group_key[20]][...]
        print('R_r,z shape    :', Rrz.shape)
        Rzz = f[a_group_key[21]][...]
        print('R_z,z shape    :', Rzz.shape)
        time = f[a_group_key[22]][...]
        print('time shape', time.shape)
        z = f[a_group_key[23]][...]
        print('z shape    :', z.shape)
        zf = f[a_group_key[24]][...]
        print('z filter shape    :', zf.shape)

        f.close()

    M = np.array([Mrr,Mrphi,Mrz,Mphiphi,Mphiz,Mzz])
    R = np.array([Rrr,Rrphi,Rrz,Rphiphi,Rphiz,Rzz])
    F = np.array([Frphi,Frz,Fphiz])

    print('Dimensions of the grid     :',len(r)," x ",len(phi)," x ",len(z))
    print('Time           :',time,' ms')

    return [M, R, F, rho, r, phi, z, rf, phif, zf]
#=================================================================================

def readh5files_r0(arx):

    print('DATA FROM FILE:      ',arx)

    with h5py.File(arx, "r") as f:
    # List all groups
        print("Keys: %s" % f.keys())
        a_group_key = list(f.keys())
        for i in a_group_key:
            print(i)

        Fmean = np.array(f.get('Fmean'))
        F9mean = np.array(f.get('F9mean'))
        Rmean = np.array(f.get('Rmean'))
        Mmean = np.array(f.get('Mmean'))
        rhomean = np.array(f.get('rhomean'))
        r0 = np.array(f.get('r0'))
        time = np.array(f.get('time'))

        M = np.array([Mmean[0,0],Mmean[0,1],Mmean[0,2], Mmean[1,1], Mmean[1,2], Mmean[2,2]])
        R = np.array([Rmean[0,0],Rmean[0,1],Rmean[0,2], Rmean[1,1], Rmean[1,2], Rmean[2,2]])
        F = np.array([Fmean[0,1], Fmean[0,2], Fmean[1,2]])
        F9 = np.array([F9mean[0,0],F9mean[0,1],F9mean[0,2],F9mean[1,0],F9mean[1,1],F9mean[1,2],F9mean[2,0],F9mean[2,1],F9mean[2,2]])
        f.close()

        print('Time           :',time,' ms')

    return [M, R, F, rhomean, r0]

#===========================================================================================

def read_coeffs(res):

    res = 'coeffs-MRI-final_v2'

    path = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/coefficients_r0/'+res+'/'

    alpha = np.loadtxt(path+'alphaR_r0_av.txt')

    beta = np.loadtxt(path+'betaR_r0_av.txt')

    gamma = np.loadtxt(path+'gammaR_r0_av.txt')

    return alpha, beta, gamma

#===========================================================================================

def energy_ev(t, scale,q,omega, factor_c, energy_ini, factor_en,rho):

    sigma = 0.27

    b0 = 4.6e13*2.874214371e-25

    gamma_pi_fac = 2*np.sqrt(2)*sigma*(1-(2-q)**2/4)**(0.5)*omega/b0 #CHECK THIS EXPRESSION

    std_fact= factor_c/(rho**(0.5)*scale)

    timestep= 5e-5

    timestep = 0.001*2.99792458e10*timestep
    tfinal = t[-1]*0.001*2.99792458e10
    lent = int(round((tfinal)/timestep))+1

    energy_t = np.zeros(lent)
    energy_t[0] = energy_ini

    energy_pi = np.zeros(lent)
    energy_pi[0] =energy_ini*factor_en

    gamma_pi = np.zeros(lent)
    gamma_pi[0] = gamma_pi_fac*energy_ini**(0.5)

    gamma_mri = q*omega

    libenergyev.energy_ev(std_fact,lent,timestep,gamma_mri, gamma_pi_fac,energy_t,energy_pi)

    ratio = int((lent-1)/(len(t)-1))
    energy_model = np.zeros(len(t))
    energy_pi2 = np.zeros(len(t))

    for i in range(0,len(t)-1):
        energy_model[i] = energy_t[ratio*i]
        energy_pi2[i] = energy_pi[ratio*i]

    energy_model[-1] = energy_t[-1]
    energy_pi2[-1] = energy_pi[-1]

    return energy_model, energy_pi2

#===========================================================================================

def energy_mod_space(rf,lenphif,lenzf,factor_c,t, scale, energy_ini,res, factor_en, rho):

    r0tilde = 15.5e5
    omega0tilde = 1824*3.33564095e-11
    q = 1.25

    energy_mod = np.zeros((len(t), len(rf), lenphif, lenzf))
    energy_pi = np.zeros((len(t), len(rf), lenphif, lenzf))

    for i in range(0,len(rf)):

        omega = omega0tilde*(rf[i]/r0tilde)**(-q)

        for j in range(0,lenphif):

            for k in range(0,lenzf):

                energy_mod[:,i,j,k], energy_pi[:,i,j,k] = energy_ev(t, scale, q, omega, factor_c, energy_ini, factor_en,rho[i,j,k])

    return energy_mod, energy_pi

#===========================================================================================

def stresses_mod_r0(factor_c,energy_ini,res,t, factor_en):

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_r0/res_"+res

    Msim = np.zeros((len(t),6))
    Rsim = np.zeros((len(t),6))
    Fsim = np.zeros((len(t),3))
    F9sim = np.zeros((len(t),9))
    rhomean = np.zeros(len(t))

    for tt in range(0, len(t)):

        arx = format(25*tt, "04")
        Msim[tt], Rsim[tt], Fsim[tt], rhomean[tt], r0 = readh5files_r0(path_data+'/stresses_sigma_r0-'+arx+'.h5')

    alpha, beta, gamma = read_coeffs(res)

    alphatrace = alpha[0]+alpha[3]+alpha[5]

    omega = 1824*3.33564095e-11
    q = 1.25
    filtersize = 460765
    lambda_mri = 33300
    scale = min(filtersize,lambda_mri)
    energy_mod = energy_ev(alphatrace,t,scale,q,omega, factor_c, energy_ini, factor_en)

    energy_sim = 0.5*rhomean*(Rsim[:,0]+Rsim[:,3]+Rsim[:,5])

    Mmod = np.zeros((len(t), 6))
    Rmod = np.zeros((len(t), 6))
    Fmod = np.zeros((len(t), 3))

    for i in range(0,6):
        Mmod[:,i] = alpha[i]*energy_mod[:]
        Rmod[:,i] = beta[i]*energy_mod[:]/rhomean[:]

    for j in range(0,3):
        Fmod[:,j] = gamma[j]*energy_mod[:]/rhomean[:]**(0.5)

    return Msim, Rsim, Fsim, Mmod, Rmod, Fmod, energy_mod, energy_sim, rhomean

#===========================================================================================

def stresses(res,direc,factor_c,energy_ini,factor_en,t,Sf):

    lent = len(t)
    inivars = readh5files(direc+'stresses_sim_res_'+res+'-0000.h5')

    rfilter = inivars[7]
    phifilter = inivars[8]
    zfilter = inivars[9]

    Msim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Rsim = np.zeros((lent,6,len(rfilter),len(phifilter),len(zfilter)))
    Fsim = np.zeros((lent,3,len(rfilter),len(phifilter),len(zfilter)))
    rhomean = np.zeros((lent,len(rfilter),len(phifilter),len(zfilter)))

    for tt in range(0, len(t)):

        arx = format(25*tt, "04")
        Msim[tt], Rsim[tt], Fsim[tt], rhomean[tt], r, phi, z, rfilter, phifilter, zfilter = readh5files(direc+'stresses_sim_res_'+res+'-'+arx+'.h5')

    cellsize = (r[1]-r[0]+(r[-1]+r[0])*0.5*(phi[1]-phi[0])+z[1]-z[0])/3

    filtersize = Sf*cellsize
    lambda_mri = 33300

    scale = min(filtersize, lambda_mri)

    alphaPI, betaPI, gammaPI = read_coeffs(res)

    q = 1.25
    alphaMRI = np.zeros(6)
    alphaMRI[0] = (4-q)/q
    alphaMRI[3] = (4-q)/q
    alphaMRI[1] = -(4-q)/q

    betaMRI = np.zeros(6)
    betaMRI[0] = 1
    betaMRI[3] = 1
    betaMRI[1] = 1

    energy_sim = 0.5*rhomean*(Rsim[:,0]+Rsim[:,3]-2*Rsim[:,5])

    energyMRI, energyPI = energy_mod_space(rfilter,len(phifilter),len(zfilter),factor_c,t, scale, energy_ini,res, factor_en,rhomean[0,:,:,:])

    Mmod = np.zeros((lent, 6, len(rfilter), len(phifilter), len(zfilter)))
    Rmod = np.zeros((lent, 6, len(rfilter), len(phifilter), len(zfilter)))
    Fmod = np.zeros((lent, 3, len(rfilter), len(phifilter), len(zfilter)))

    for i in range(0,6):
        Mmod[:,i,:,:,:] = alphaMRI[i]*energyMRI[:,:,:,:]+alphaPI[i]*energyPI[:,:,:,:]
        Rmod[:,i,:,:,:] = (betaMRI[i]*energyMRI[:,:,:,:]+betaPI[i]*energyPI[:,:,:,:])/rhomean[:,:,:,:]

    for j in range(0,3):
        Fmod[:,j,:,:,:] = gammaPI[j]*energyPI[:,:,:,:]/rhomean[:,:,:,:]**(0.5)

    return Msim, Rsim, Fsim, Mmod, Rmod, Fmod, energyMRI, energy_sim, rfilter, phifilter, zfilter

#===========================================================================================

def l2_log(Msim, Rsim , Fsim, Mmod, Rmod, Fmod,t,path):


    ind0 = 10

    t = t[ind0:]

    Msimt = Msim[ind0:]
    Mmodt = Mmod[ind0:]
    Rsimt = Rsim[ind0:]
    Rmodt = Rmod[ind0:]
    Fsimt = Fsim[ind0:]
    Fmodt = Fmod[ind0:]

    l2_M_t_comp = np.zeros((len(t),6))
    l2_R_t_comp = np.zeros((len(t),6))
    l2_F_t_comp = np.zeros((len(t),3))

    l2_M_t = np.zeros(len(t))
    l2_R_t = np.zeros(len(t))
    l2_F_t = np.zeros(len(t))



    for tt in range(0,len(t)):
        integrandM = (Msimt[tt,:]-Mmodt[tt,:])**2
        integrandR = (Rsimt[tt,:]-Rmodt[tt,:])**2
        integrandF = (Fsimt[tt,:]-Fmodt[tt,:])**2
        mean_harF = 0.5*(1/np.sum(Fmodt[tt,:]**2)+1/np.sum(Fsimt[tt,:]**2))
        mean_harM = 0.5*(1/np.sum(Mmodt[tt,:]**2)+1/np.sum(Msimt[tt,:]**2))
        mean_harR = 0.5*(1/np.sum(Rmodt[tt,:]**2)+1/np.sum(Rsimt[tt,:]**2))

        l2_M_t[tt] = np.sqrt(np.sum(integrandM)*mean_harM)
        l2_R_t[tt] = np.sqrt(np.sum(integrandR)*mean_harR)
        l2_F_t[tt] = np.sqrt(np.sum(integrandF)*mean_harF)

    l2Mv2 = (np.sum(l2_M_t[:]**2)/len(t))**(0.5)
    l2Rv2 = (np.sum(l2_R_t[:]**2)/len(t))**(0.5)
    l2Fv2 = (np.sum(l2_F_t[:]**2)/len(t))**(0.5)

    for tt in range(0,len(t)):

        for cc in range(0,6):
            integrandM = (Msimt[tt,cc,:]-Mmodt[tt,cc,:])**2
            integrandR = (Rsimt[tt,cc,:]-Rmodt[tt,cc,:])**2
            mean_harM = 0.5*(1/np.sum(Mmodt[tt,cc,:]**2)+1/np.sum(Msimt[tt,cc,:]**2))
            mean_harR = 0.5*(1/np.sum(Rmodt[tt,cc,:]**2)+1/np.sum(Rsimt[tt,cc,:]**2))

            l2_M_t_comp[tt,cc] = np.sqrt(np.sum(integrandM)*mean_harM)
            #l2_M_t_comp[tt,cc] = (np.sum((np.log10(abs(Msimflat_t_comp[tt,cc,:]/Mmodflat_t_comp[tt,cc,:])))**2)/np.sum(np.log10(abs(Msimflat_t_comp[tt,cc,:]))**2))**(0.5)

            #Rsimflat_t_comp[tt,cc,:] = Rsimt[tt,cc,:,:,:].flatten()
            #Rmodflat_t_comp[tt,cc,:] = Rmodt[tt,cc,:,:,:].flatten()
            l2_R_t_comp[tt,cc] = np.sqrt(np.sum(integrandR)*mean_harR)
            #l2_R_t_comp[tt,cc] = (np.sum((np.log10(abs(Rsimflat_t_comp[tt,cc,:]/Rmodflat_t_comp[tt,cc,:])))**2)/np.sum(np.log10(abs(Rsimflat_t_comp[tt,cc,:]))**2))**(0.5)

        for cc in range(0,3):
            #Fsimflat_t_comp[tt,cc,:] = Fsimt[tt,cc,:,:,:].flatten()
            #Fmodflat_t_comp[tt,cc,:] = Fmodt[tt,cc,:,:,:].flatten()
            #for x in range(0,len(dr)):
            integrandF = (Fsimt[tt,cc,:]-Fmodt[tt,cc,:])**2
            mean_harF = 0.5*(1/np.sum(Fmodt[tt,cc,:]**2)+1/np.sum(Fsimt[tt,cc,:]**2))
            l2_F_t_comp[tt,cc] = np.sqrt(np.sum(integrandF)*mean_harF)
            #l2_F_t_comp[tt,cc] = (np.sum((np.log10(abs(Fsimflat_t_comp[tt,cc,:]/Fmodflat_t_comp[tt,cc,:])))**2)/np.sum(np.log10(abs(Fsimflat_t_comp[tt,cc,:]))**2))**(0.5)

    l2M_t_mean = (np.sum(l2_M_t_comp[:,:]**2, axis = 0)/len(t))
    l2R_t_mean = (np.sum(l2_R_t_comp[:,:]**2, axis = 0)/len(t))
    l2F_t_mean = (np.sum(l2_F_t_comp[:,:]**2, axis = 0)/len(t))

    l2Mrms = (np.sum(l2M_t_mean**2)/len(l2M_t_mean))**(0.5)
    l2Rrms = (np.sum(l2R_t_mean**2)/len(l2R_t_mean))**(0.5)
    l2Frms = (np.sum(l2F_t_mean**2)/len(l2F_t_mean))**(0.5)

    l2Mrms_t = (np.sum(l2_M_t_comp**2, axis = 1)/6)**(0.5)
    l2Rrms_t = (np.sum(l2_R_t_comp**2, axis = 1)/6)**(0.5)
    l2Frms_t = (np.sum(l2_F_t_comp**2, axis = 1)/3)**(0.5)

    with open(path+'/l2log_time_ev_facC.txt', 'w') as f:
        f.write('# MAXWELL \t REYNOLDS \t FARADAY #\n')
        for x in range(0,len(t)):
            f.write('%f\t%f\t%f\t%f\n'%(t[x],l2Mrms_t[x], l2Rrms_t[x], l2Frms_t[x]))

    with open(path+'/l2_time_ev_minit.txt', 'w') as f:
        f.write('# MAXWELL \t REYNOLDS \t FARADAY #\n')
        for x in range(0,len(t)):
            f.write('%f\t%f\t%f\t%f\n'%(t[x],l2_M_t[x], l2_R_t[x], l2_F_t[x]))

    with open(path+'/l2_minit.txt', 'w') as f:
        f.write('# MAXWELL \t REYNOLDS \t FARADAY #\n')
        f.write('%f\t%f\t%f\n'%(l2Mv2, l2Rv2, l2Fv2))

    with open(path+'/l2Mlog_comp_time_ev_facC.txt', 'w') as f:
        f.write('# M_rr \t M_rphi \t M_rz \t M_phiphi \t M_phiz \t M_zz #\n')
        for x in range(0,len(t)):
            f.write('%f\t%f\t%f\t%f\t%f\t%f\t%f\n'%(t[x],l2_M_t_comp[x,0], l2_M_t_comp[x,1], l2_M_t_comp[x,2], l2_M_t_comp[x,3], l2_M_t_comp[x,4], l2_M_t_comp[x,5]))

    with open(path+'/l2Rlog_comp_time_ev_facC.txt', 'w') as f:
        f.write('# R_rr \t R_rphi \t R_rz \t R_phiphi \t R_phiz \t R_zz  #\n')
        for x in range(0,len(t)):
            f.write('%f\t%f\t%f\t%f\t%f\t%f\t%f\n'%(t[x],l2_R_t_comp[x,0], l2_R_t_comp[x,1], l2_R_t_comp[x,2],l2_R_t_comp[x,3], l2_R_t_comp[x,4], l2_R_t_comp[x,5]))

    with open(path+'/l2Flog_comp_time_ev_facC.txt', 'w') as f:
        f.write('# F_rphi \t F_rz \t F_phiz #\n')
        for x in range(0,len(t)):
            f.write('%f\t%f\t%f\t%f\n'%(t[x],l2_F_t_comp[x,0], l2_F_t_comp[x,1], l2_F_t_comp[x,2]))

    with open(path+'/l2_rms_facC.txt', 'w') as f:
        f.write('# MAXWELL \t REYNOLDS \t FARADAY #\n')
        f.write('%f\t%f\t%f\n'%(l2Mrms,l2Rrms,l2Frms))

    return()

#===========================================================================================

def l2_log_phase(Msim, Rsim , Fsim, Mmod, Rmod, Fmod,t,path,phase):

    ind0 = 10
    indsat = 21
    difearly = int(indsat-ind0)
    diflate = int(difearly+1)
    t = t[ind0:]

    Msimt = Msim[ind0:]
    Mmodt = Mmod[ind0:]
    Rsimt = Rsim[ind0:]
    Rmodt = Rmod[ind0:]
    Fsimt = Fsim[ind0:]
    Fmodt = Fmod[ind0:]

    if phase == 'early':
        Msimt = Msimt[:difearly]
        Rsimt = Rsimt[:difearly]
        Fsimt = Fsimt[:difearly]
        Mmodt = Mmodt[:difearly]
        Rmodt = Rmodt[:difearly]
        Fmodt = Fmodt[:difearly]
        t = t[:difearly]

    elif phase == 'late':
        Msimt = Msimt[diflate:]
        Rsimt = Rsimt[diflate:]
        Fsimt = Fsimt[diflate:]
        Mmodt = Mmodt[diflate:]
        Rmodt = Rmodt[diflate:]
        Fmodt = Fmodt[diflate:]
        t = t[diflate:]

    l2_M_t_comp = np.zeros((len(t),6))
    l2_R_t_comp = np.zeros((len(t),6))
    l2_F_t_comp = np.zeros((len(t),3))

    l2_M_t = np.zeros(len(t))
    l2_R_t = np.zeros(len(t))
    l2_F_t = np.zeros(len(t))


    for tt in range(0,len(t)):
        integrandM = (Msimt[tt,:]-Mmodt[tt,:])**2
        integrandR = (Rsimt[tt,:]-Rmodt[tt,:])**2
        integrandF = (Fsimt[tt,:]-Fmodt[tt,:])**2
        mean_harF = 0.5*(1/np.sum(Fmodt[tt,:]**2)+1/np.sum(Fsimt[tt,:]**2))
        mean_harM = 0.5*(1/np.sum(Mmodt[tt,:]**2)+1/np.sum(Msimt[tt,:]**2))
        mean_harR = 0.5*(1/np.sum(Rmodt[tt,:]**2)+1/np.sum(Rsimt[tt,:]**2))

        l2_M_t[tt] = np.sqrt(np.sum(integrandM)*mean_harM)
        l2_R_t[tt] = np.sqrt(np.sum(integrandR)*mean_harR)
        l2_F_t[tt] = np.sqrt(np.sum(integrandF)*mean_harF)

    l2Mv2 = (np.sum(l2_M_t[:]**2)/len(t))**(0.5)
    l2Rv2 = (np.sum(l2_R_t[:]**2)/len(t))**(0.5)
    l2Fv2 = (np.sum(l2_F_t[:]**2)/len(t))**(0.5)


    if phase=='early':
        with open(path+'/l2_minit_early.txt', 'w') as f:
            f.write('# MAXWELL \t REYNOLDS \t FARADAY #\n')
            f.write('%f\t%f\t%f\n'%(l2Mv2, l2Rv2, l2Fv2))
    elif phase=='late':
        with open(path+'/l2_minit_late.txt', 'w') as f:
            f.write('# MAXWELL \t REYNOLDS \t FARADAY #\n')
            f.write('%f\t%f\t%f\n'%(l2Mv2, l2Rv2, l2Fv2))

    return()

#===========================================================================================

def l2_log_non0(Msim, Rsim , Fsim, Mmod, Rmod, Fmod,t, lenrf,lenphif,lenzf,dr,phi,z,path):

    ind = 11
    ind0 = 11
    ind_prob1 = 40-ind0
    ind_prob2 = 45-ind0

    dif = ind-ind0
    t2 = t[ind0:]
    t = t[ind:]

    Msimt = Msim[ind0:]
    Mmodt = Mmod[ind0:]
    Rsimt = Rsim[ind0:]
    Rmodt = Rmod[ind0:]
    Fsimt = Fsim[ind0:]
    Fmodt = Fmod[ind0:]

    l2_M_t_comp = np.zeros((len(t2),6))
    l2_R_t_comp = np.zeros((len(t2),6))
    l2_F_t_comp = np.zeros((len(t2),3))

    dphi = phi[1]-phi[0]
    dz = z[1]-z[0]
    dV = 0.5*dr*dphi*dz
    dVmatrix = dV*np.ones((len(dr),len(phi),len(z)))
    V = np.sum(dVmatrix)
    integrandM = np.zeros((len(dr),len(phi),len(z)))
    integrandR = np.zeros((len(dr),len(phi),len(z)))
    integrandF = np.zeros((len(dr),len(phi),len(z)))

    #Msimflat_t_comp = np.zeros((len(t2),6,lenrf*lenphif*lenzf))
    #Mmodflat_t_comp = np.zeros((len(t2),6,lenrf*lenphif*lenzf))
    #Rsimflat_t_comp = np.zeros((len(t2),6,lenrf*lenphif*lenzf))
    #Rmodflat_t_comp = np.zeros((len(t2),6,lenrf*lenphif*lenzf))
    #Fsimflat_t_comp = np.zeros((len(t2),3,lenrf*lenphif*lenzf))
    #Fmodflat_t_comp = np.zeros((len(t2),3,lenrf*lenphif*lenzf))

    for tt in range(0,len(t2)):
        for cc in range(0,6):
            #Msimflat_t_comp[tt,cc,:] = Msimt[tt,cc,:,:,:].flatten()
            #Mmodflat_t_comp[tt,cc,:] = Mmodt[tt,cc,:,:,:].flatten()
            for x in range(0,len(dr)):
                integrandM[x,:,:] = ((np.log10(abs(Msimt[tt,cc,x,:,:]))-np.log10(abs(Mmodt[tt,cc,x,:,:])))**2)*dV[x]
                integrandR[x,:,:] = ((np.log10(abs(Rsimt[tt,cc,x,:,:]))-np.log10(abs(Rmodt[tt,cc,x,:,:])))**2)*dV[x]

            l2_M_t_comp[tt,cc] = np.sqrt(np.sum(integrandM)/V)
            #l2_M_t_comp[tt,cc] = (np.sum((np.log10(abs(Msimflat_t_comp[tt,cc,:]/Mmodflat_t_comp[tt,cc,:])))**2)/np.sum(np.log10(abs(Msimflat_t_comp[tt,cc,:]))**2))**(0.5)

            #Rsimflat_t_comp[tt,cc,:] = Rsimt[tt,cc,:,:,:].flatten()
            #Rmodflat_t_comp[tt,cc,:] = Rmodt[tt,cc,:,:,:].flatten()
            l2_R_t_comp[tt,cc] = np.sqrt(np.sum(integrandR)/V)
            #l2_R_t_comp[tt,cc] = (np.sum((np.log10(abs(Rsimflat_t_comp[tt,cc,:]/Rmodflat_t_comp[tt,cc,:])))**2)/np.sum(np.log10(abs(Rsimflat_t_comp[tt,cc,:]))**2))**(0.5)

        for cc in range(0,3):
            #Fsimflat_t_comp[tt,cc,:] = Fsimt[tt,cc,:,:,:].flatten()
            #Fmodflat_t_comp[tt,cc,:] = Fmodt[tt,cc,:,:,:].flatten()
            for x in range(0,len(dr)):
                integrandF[x,:,:] = ((np.log10(abs(Fsimt[tt,cc,x,:,:]))-np.log10(abs(Fmodt[tt,cc,x,:,:])))**2)*dV[x]

            l2_F_t_comp[tt,cc] = np.sqrt(np.sum(integrandF)/V)
            #l2_F_t_comp[tt,cc] = (np.sum((np.log10(abs(Fsimflat_t_comp[tt,cc,:]/Fmodflat_t_comp[tt,cc,:])))**2)/np.sum(np.log10(abs(Fsimflat_t_comp[tt,cc,:]))**2))**(0.5)

#    l2_M_t_compd = np.delete(l2_M_t_comp,[int(ind_prob1),int(ind_prob1+1),int(ind_prob1+2),int(ind_prob1+3),int(ind_prob1+4),int(ind_prob1+5)], axis=0)
#    l2_R_t_compd = np.delete(l2_R_t_comp,[int(ind_prob1),int(ind_prob1+1),int(ind_prob1+2),int(ind_prob1+3),int(ind_prob1+4),int(ind_prob1+5)], axis=0)
#    l2_F_t_compd = np.delete(l2_F_t_comp,[int(ind_prob1),int(ind_prob1+1),int(ind_prob1+2),int(ind_prob1+3),int(ind_prob1+4),int(ind_prob1+5)], axis=0)

    l2M_t_mean = np.mean(l2_M_t_comp[dif:], axis = 0)
    l2R_t_mean = np.mean(l2_R_t_comp[dif:], axis = 0)
    l2F_t_mean = np.mean(l2_F_t_comp[dif:], axis = 0)
    #l2R_t_mean_nophi = np.delete(l2R_t_mean,[0,2,5])

    l2M_t_mean = np.delete(l2M_t_mean,[2,4])
    l2R_t_mean = np.delete(l2R_t_mean,[2,4])
    l2Mrms = (np.sum(l2M_t_mean**2)/len(l2M_t_mean))**(0.5)
    l2Rrms = (np.sum(l2R_t_mean**2)/len(l2R_t_mean))**(0.5)
    l2Frms = (np.sum(l2F_t_mean**2)/len(l2F_t_mean))**(0.5)

    l2Mnon0 = np.delete(l2_M_t_comp,[2,4],axis = 1)
    l2Mrms_t = (np.sum(l2Mnon0**2, axis = 1)/4)**(0.5)
    l2Rnon0 = np.delete(l2_R_t_comp,[2,4], axis =1)
    l2Rrms_t = (np.sum(l2Rnon0**2, axis = 1)/4)**(0.5)
    l2Frms_t = (np.sum(l2_F_t_comp**2, axis = 1)/3)**(0.5)


    with open(path+'/l2log_time_ev_facC_non0.txt', 'w') as f:
        f.write('# MAXWELL \t REYNOLDS \t FARADAY #\n')
        for x in range(0,len(t2)):
            f.write('%f\t%f\t%f\t%f\n'%(t2[x],l2Mrms_t[x], l2Rrms_t[x], l2Frms_t[x]))

    with open(path+'/l2_rms_facC_non0.txt', 'w') as f:
        f.write('# MAXWELL \t REYNOLDS \t FARADAY #\n')
        f.write('%f\t%f\t%f\n'%(l2Mrms,l2Rrms,l2Frms))

    with open(path+'/l2Mlog_comp_time_ev_facC_non0.txt', 'w') as f:
        f.write('# M_rr \t M_rphi \t M_rz \t M_phiphi \t M_phiz \t M_zz #\n')
        for x in range(0,len(t2)):
            f.write('%f\t%f\t%f\t%f\t%f\t%f\t%f\n'%(t2[x],l2_M_t_comp[x,0], l2_M_t_comp[x,1], l2_M_t_comp[x,2], l2_M_t_comp[x,3], l2_M_t_comp[x,4], l2_M_t_comp[x,5]))

    with open(path+'/l2Rlog_comp_time_ev_facC_non0.txt', 'w') as f:
        f.write('# R_rr \t R_rphi \t R_rz \t R_phiphi \t R_phiz \t R_zz  #\n')
        for x in range(0,len(t2)):
            f.write('%f\t%f\t%f\t%f\t%f\t%f\t%f\n'%(t2[x],l2_R_t_comp[x,0], l2_R_t_comp[x,1], l2_R_t_comp[x,2],l2_R_t_comp[x,3], l2_R_t_comp[x,4], l2_R_t_comp[x,5]))

    with open(path+'/l2Flog_comp_time_ev_facC_non0.txt', 'w') as f:
        f.write('# F_rphi \t F_rz \t F_phiz #\n')
        for x in range(0,len(t2)):
            f.write('%f\t%f\t%f\t%f\n'%(t2[x],l2_F_t_comp[x,0], l2_F_t_comp[x,1], l2_F_t_comp[x,2]))
    return()

#===========================================================================================

def main():

    factor_c = float(sys.argv[3])
    #factor_c_non0 = float(sys.argv[4])
    energy_ini = 7e-31
    factor_en = 1e3

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_sim_test/"

    Sf = int(sys.argv[1])

    lenphi = int(sys.argv[2])

    if lenphi == 400 :

        lenr = 100
        lenz = 100


    elif lenphi == 240 :

        lenr = 60
        lenz = 60

    elif lenphi == 304 :

        lenr = 76
        lenz = 76

    res = str(lenr)+'_'+str(lenphi)+'_'+str(lenz)
    direc = path_data+'res_'+res+'/Sf_'+str(Sf)+'/'

    t_final = 30
    lent = 61
    t = np.linspace(0,t_final,lent)

    Msim, Rsim, Fsim, Mmod, Rmod, Fmod, energy_mod, energy_sim, rfilter, phif, zf = stresses(res,direc,factor_c,energy_ini,factor_en,t,Sf)

    save_path = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/final_eval_minit/res_"+res+"/b0z-4.6e13/Sf_"+str(Sf)

    try:
        os.makedirs(save_path)
    except OSError as error:
        print(error)

    l2_log(Msim, Rsim , Fsim, Mmod, Rmod, Fmod,t,save_path)
    l2_log_phase(Msim, Rsim , Fsim, Mmod, Rmod, Fmod,t,save_path,'early')
    l2_log_phase(Msim, Rsim , Fsim, Mmod, Rmod, Fmod,t,save_path,'late')

#    Msim, Rsim, Fsim, Mmod, Rmod, Fmod, energy_mod, energy_sim, rfilter, phif, zf = stresses(res,direc,factor_c_non0,energy_ini,factor_en,t,Sf)

    #l2_log_non0(Msim, Rsim , Fsim, Mmod, Rmod, Fmod,t, len(rfilter),len(phif),len(zf),difr,phif,zf,save_path)


    return()

main()
