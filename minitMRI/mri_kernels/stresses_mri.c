//
//  box_kernel_cart.c
//
//
//  Created by Miquel Miravet on 23/06/2021.
//

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stdbool.h>
//#include "khi_kernel.c"

void box_kernel_cyl(int lenr, int lenphi, int lenz, double r[], double dr[], double phi[], double z[], int lenrfilter, int lenphifilter, int lenzfilter, double rfilter[], double phifilter[], double
zfilter[], double filtersize[], double var[][lenphi][lenz], double varmean[][lenphifilter][lenzfilter]){


  double difr, difphi, difz;
  int i, j, k, p, q, l;
  double dphi = phi[1]-phi[0];
  double dz = z[1]-z[0];
  double sum= 0;
  double sum2= 0;

  double deltar = filtersize[0]*0.5000001;
  double deltaphi = filtersize[1]*0.5000001;
  double deltaz = filtersize[2]*0.5000001;

 /* printf("filtersize: %lf", filtersize[0]);

  printf("deltar: %lf", deltar);
  printf("deltaphi: %lf", deltaphi);
  printf("deltaz: %lf", deltaz);
*/

  int G = 0;

  for (p = 0 ; p < lenrfilter ; p++){
    for (q = 0 ; q < lenphifilter ; q++){
      for (l = 0 ; l < lenzfilter ; l++){

        for (i = 0 ; i < lenr ; i++){
          difr = fabs(rfilter[p]-r[i]);
          for (j = 0; j < lenphi ; j++){
            difphi = fabs(phifilter[q]-phi[j]);
            for (k = 0; k < lenz ; k++){
              difz = fabs(zfilter[l]-z[k]);
              
              if((difr <= fabs(deltar)) && (difphi <= fabs(deltaphi)) && (difz <= fabs(deltaz))){
                G = 1;
            
              }
              else{
                G = 0;
              }

 /*               printf("difr: %lf", difr);
                printf("difphi: %lf", difphi);
                printf("difz: %lf", difz);
*/
              sum2 += G*0.5*dr[i]*dphi*dz;
              sum += var[i][j][k]*G*0.5*dr[i]*dphi*dz;

              G = 0;
            }
          }
        }
        varmean[p][q][l] = sum/sum2;
        sum = 0;
        sum2 = 0;

      }
    }
  }
}

void stresses_cyl(int lenr, int lenphi, int lenz, int lenrfilter, int lenphifilter, int lenzfilter, double v[][lenr][lenphi][lenz], double vm[][lenrfilter][lenphifilter][lenzfilter], double b[][lenr][lenphi][lenz],double bm[][lenrfilter][lenphifilter][lenzfilter],double rho[][lenphi][lenz], double filtersize[], double r[], double dr[], double phi[], double z[], double rfilter[], double phifilter[],
double zfilter[], double stressM[][lenrfilter][lenphifilter][lenzfilter], double stressR[][lenrfilter][lenphifilter][lenzfilter], double stressF[][lenrfilter][lenphifilter][lenzfilter], double
rhomean[][lenphifilter][lenzfilter], double aux[][lenphi][lenz], double aux2[][lenphifilter][lenzfilter]){


  int i,j,k,l,m,n;

  box_kernel_cyl(lenr, lenphi,lenz, r, dr, phi, z, lenrfilter, lenphifilter, lenzfilter, rfilter, phifilter, zfilter, filtersize, b[0], bm[0]);
  box_kernel_cyl(lenr, lenphi,lenz, r, dr, phi, z, lenrfilter, lenphifilter, lenzfilter, rfilter, phifilter, zfilter, filtersize, b[1], bm[1]);
  box_kernel_cyl(lenr, lenphi,lenz, r, dr, phi, z, lenrfilter, lenphifilter, lenzfilter, rfilter, phifilter, zfilter, filtersize, b[2], bm[2]);

  box_kernel_cyl(lenr, lenphi,lenz, r, dr, phi, z, lenrfilter, lenphifilter, lenzfilter, rfilter, phifilter, zfilter, filtersize, v[0], vm[0]);
  box_kernel_cyl(lenr, lenphi,lenz, r, dr, phi, z, lenrfilter, lenphifilter, lenzfilter, rfilter, phifilter, zfilter, filtersize, v[1], vm[1]);
  box_kernel_cyl(lenr, lenphi,lenz, r, dr, phi, z, lenrfilter, lenphifilter, lenzfilter, rfilter, phifilter, zfilter, filtersize, v[2], vm[2]);

  k = 0;
  for (i = 0; i < 3; i++){
    for (j = 0; j < 3; j++){

      for (m = 0; m < lenr; m++){
        for (n = 0; n < lenphi; n++){
          for (l = 0; l < lenz; l++){
            aux[m][n][l] = b[i][m][n][l]*b[j][m][n][l];
          }
        }
      }

      box_kernel_cyl(lenr, lenphi,lenz, r, dr, phi, z, lenrfilter, lenphifilter, lenzfilter, rfilter, phifilter, zfilter, filtersize, aux, aux2);

      for (m = 0; m < lenrfilter; m++){
        for (n = 0; n < lenphifilter; n++){
          for (l = 0; l < lenzfilter; l++){
            stressM[k][m][n][l] = aux2[m][n][l]-bm[i][m][n][l]*bm[j][m][n][l];
          }
        }
      }
      k++;
    }
  }

  k = 0;
  for (i = 0; i < 3; i++){
    for (j = 0; j < 3; j++){

      for (m = 0; m < lenr; m++){
        for (n = 0; n < lenphi; n++){
          for (l = 0; l < lenz; l++){
            aux[m][n][l] = v[i][m][n][l]*v[j][m][n][l];
          }
        }
      }

      box_kernel_cyl(lenr, lenphi,lenz, r, dr, phi, z, lenrfilter, lenphifilter, lenzfilter, rfilter, phifilter, zfilter, filtersize, aux, aux2);

      for (m = 0; m < lenrfilter; m++){
        for (n = 0; n < lenphifilter; n++){
          for (l = 0; l < lenzfilter; l++){
            stressR[k][m][n][l] = aux2[m][n][l]-vm[i][m][n][l]*vm[j][m][n][l];
          }
        }
      }
      k++;
    }
  }

  k = 0;
  for (i = 0; i < 3; i++){
    for (j = 0; j < 3; j++){

      for (m = 0; m < lenr; m++){
        for (n = 0; n < lenphi; n++){
          for (l = 0; l < lenz; l++){
            aux[m][n][l] = b[i][m][n][l]*v[j][m][n][l];
          }
        }
      }

      box_kernel_cyl(lenr, lenphi,lenz, r, dr, phi, z, lenrfilter, lenphifilter, lenzfilter, rfilter, phifilter, zfilter, filtersize, aux, aux2);

      for (m = 0; m < lenrfilter; m++){
        for (n = 0; n < lenphifilter; n++){
          for (l = 0; l < lenzfilter; l++){
            stressF[k][m][n][l] = aux2[m][n][l]-bm[i][m][n][l]*vm[j][m][n][l];
          }
        }
      }
      k++;
    }
  }


  box_kernel_cyl(lenr, lenphi,lenz, r, dr, phi, z, lenrfilter, lenphifilter, lenzfilter, rfilter, phifilter, zfilter, filtersize, rho, rhomean);


}
