#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 28 18:05:04 2021

@author: miquelmiravet

PABLO'S MODEL TESTING
"""
#############################################

# Let's model the stress tensors Mij, Rij and Tij as functions of the total energy density.
# First approach: Consider that the tensors are proportional to the energy density.

#############################################

import os
import numpy as np
import h5py
import csv
from statistics import stdev

def readh5files(arxiv):

    print('DATA FROM FILE:      ',arxiv)

    with h5py.File(arxiv, "r") as f:
    # List all groups
        print("Keys: %s" % f.keys())
        a_group_key = list(f.keys())
        for i in a_group_key:
            print(i)

        F9mean = f[a_group_key[0]][...]
        print('F9 stress shape    :', F9mean.shape)
        Fmean = f[a_group_key[1]][...]
        print('F stress shape    :', Fmean.shape)
        Fnew = np.array([Fmean[0,1],Fmean[0,2],Fmean[1,2]])
        Mmean = f[a_group_key[2]][...]
        print('M stress shape    :', Mmean.shape)
        Mnew = np.array([Mmean[0,0],Mmean[0,1], Mmean[0,2], Mmean[1,1], Mmean[1,2], Mmean[2,2]])
        Rmean = f[a_group_key[3]][...]
        print('R stress shape    :', Rmean.shape)
        Rnew = np.array([Rmean[0,0],Rmean[0,1], Rmean[0,2], Rmean[1,1], Rmean[1,2], Rmean[2,2]])
        r0 = f[a_group_key[4]][...]
        print('r0 shape    :', r0.shape)
        rhomean = f[a_group_key[5]][...]
        print('filtered rho shape    :', rhomean.shape)
        sigmaF = f[a_group_key[6]][...]
        print('sigma F shape    :', sigmaF.shape)
        sFnew = np.array([sigmaF[0,1], sigmaF[0,2], sigmaF[1,2]])
        sigmaF9 = f[a_group_key[7]][...]
        print('sigma F9 shape    :', sigmaF9.shape)
        sigmaM = f[a_group_key[8]][...]
        print('sigma M shape    :', sigmaM.shape)
        sMnew = np.array([sigmaM[0,0],sigmaM[0,1],sigmaM[0,2],sigmaM[1,1],sigmaM[1,2],sigmaM[2,2]])
        sigmaR = f[a_group_key[9]][...]
        print('sigma R shape    :', sigmaR.shape)
        sRnew = np.array([sigmaR[0,0],sigmaR[0,1],sigmaR[0,2],sigmaR[1,1],sigmaR[1,2],sigmaR[2,2]])
        sigma_rho= f[a_group_key[10]][...]
        print('sigma rho shape    :', sigma_rho.shape)
        time = f[a_group_key[11]][...]
        print('time shape    :', time.shape)
        f.close()
#    print('F9_rr    = ', sigmaF9[1,0])
    print('Evaluation of the filtered quantities at r0  =',r0)
    print('Time           =',time,' ms')

    return time, Mnew, Rnew, Fnew, sMnew, sRnew, sFnew, rhomean, sigma_rho, r0

def read_files_whole_box(arxiv):

    print('DATA FROM FILE:      ',arxiv)
    with h5py.File(arxiv, "r") as f:
        Mmean = np.array(f.get('Mmean'))
        Mnew = np.array([Mmean[0,0],Mmean[0,1], Mmean[0,2], Mmean[1,1], Mmean[1,2], Mmean[2,2]])
        Rmean = np.array(f.get('Rmean'))
        Rnew = np.array([Rmean[0,0],Rmean[0,1], Rmean[0,2], Rmean[1,1], Rmean[1,2], Rmean[2,2]])
        Fmean = np.array(f.get('Fmean'))
        Fnew = np.array([Fmean[0,1], Fmean[0,2], Fmean[1,2],])
        rhomean = np.array(f.get('rhomean'))
        sigmaM = np.array(f.get('sigma_M'))
        sMnew = np.array([sigmaM[0,0],sigmaM[0,1],sigmaM[0,2],sigmaM[1,1],sigmaM[1,2],sigmaM[2,2]])
        sigmaR = np.array(f.get('sigma_R'))
        sRnew = np.array([sigmaR[0,0],sigmaR[0,1],sigmaR[0,2],sigmaR[1,1],sigmaR[1,2],sigmaR[2,2]])
        sigmaF = np.array(f.get('sigma_F'))
        sFnew = np.array([sigmaF[0,1], sigmaF[0,2], sigmaF[1,2]])
        sigma_rho = np.array(f.get('sigma_rho'))
        time = np.array(f.get('time'))

    print('Time         = ',time,' ms')

    return time, Mnew, Rnew, Fnew, sMnew, sRnew, sFnew, rhomean, sigma_rho


def energy_dens(Rtensor,rho):

    #traceR = Rtensor[0]+Rtensor[3]+Rtensor[5]
    #energyR = 0.5*rho*traceR
    energyR = 0.5*rho*(Rtensor[0]+Rtensor[3]-Rtensor[5])

    return energyR

def error_propagation(rho,err_rho,M,err_M,R,err_R,F,err_F,lent):

    traceR = R[0]+R[3]+R[5]
    e_alphaR = np.zeros((6,lent))
    e_betaR = np.zeros((6,lent))
    e_gammaR = np.zeros((3,lent))

    for i in range(0,6):
        e_alphaR[i,:] = (err_M[i,:]/(0.5*rho[:]*traceR[:])**2+(M[i,:]/(0.5*traceR[:]*rho[:]**2))**2*err_rho[:]+(M[i,:]/(0.5*rho[:]*traceR[:]**2))**2*(err_R[0,:]+err_R[3,:]+err_R[5,:]))**(0.5)
        if i == 1 or i == 2 or i == 4:
            e_betaR[i,:] = (err_R[i,:]/(0.5*traceR[:])**2+(R[i,:]/(0.5*traceR[:]**2))**2*(err_R[0,:]+err_R[3,:]+err_R[5,:]))**(0.5)
        elif i == 0:
            e_betaR[i,:] = (err_R[i,:]*(1/0.5*(1/traceR[:]-R[i,:]/traceR[:]**2))**2+(R[i,:]/(0.5*traceR[:]))**2*(err_R[3,:]+err_R[5,:]))**(0.5)
        elif i == 3:
            e_betaR[i,:] = (err_R[i,:]*(1/0.5*(1/traceR[:]-R[i,:]/traceR[:]**2))**2+(R[i,:]/(0.5*traceR[:]))**2*(err_R[0,:]+err_R[5,:]))**(0.5)
        elif i == 5:
            e_betaR[i,:] = (err_R[i,:]*(1/0.5*(1/traceR[:]-R[i,:]/traceR[:]**2))**2+(R[i,:]/(0.5*traceR[:]))**2*(err_R[3,:]+err_R[0,:]))**(0.5)
    for j in range(0,3):
        e_gammaR[j] = (err_F[j,:]/(0.5*rho[:]**(0.5)*traceR[:])**2+(F[j,:]/(0.25*rho[:]**(-1.5)*traceR[:]))**2*err_rho[:]+(F[j,:]/(0.5*rho[:]**(0.5)*traceR[:]**2))**2*(err_R[0,:]+err_R[3,:]+err_R[5,:]))**(0.5)

    return e_alphaR, e_betaR, e_gammaR

def constants_files_6comp(t,constant,econstant,constant_av,econstant_av,arxiu):

    with open(arxiu+'_r0.csv', 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        cols = zip(t,constant[0,:],constant[1,:],constant[2,:],constant[3,:],constant[4,:],constant[5,:])
        writer.writerows(cols)

    with open('sigma_'+arxiu+'_r0.csv', 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        cols = zip(t,econstant[0,:],econstant[1,:],econstant[2,:],econstant[3,:],econstant[4,:],econstant[5,:])
        writer.writerows(cols)

    with open(arxiu+'_r0_av.txt','w') as f:
        f.write('%f\t%f\t%f\t%f\t%f\t%f\n'%(constant_av[0],constant_av[1],constant_av[2],constant_av[3],constant_av[4],constant_av[5]))

    with open('sigma_'+arxiu+'_r0_av.txt','w') as f:
        f.write('%f\t%f\t%f\t%f\t%f\t%f\n'%(econstant_av[0],econstant_av[1],econstant_av[2],econstant_av[3],econstant_av[4],econstant_av[5]))

    return()

def constants_files_3comp(t,constant,econstant,constant_av,econstant_av,arxiu):

    with open(arxiu+'_r0.csv', 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        cols = zip(t,constant[0,:],constant[1,:],constant[2,:])
        writer.writerows(cols)

    with open('sigma_'+arxiu+'_r0.csv', 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        cols = zip(t,econstant[0,:],econstant[1,:],econstant[2,:])
        writer.writerows(cols)

    with open(arxiu+'_r0_av.txt','w') as f:
        f.write('%f\t%f\t%f\n'%(constant_av[0],constant_av[1],constant_av[2]))

    with open('sigma_'+arxiu+'_r0_av.txt','w') as f:
        f.write('%f\t%f\t%f\n'%(econstant_av[0],econstant_av[1],econstant_av[2]))
    return()

def energy_files(t,energy_dens):

    with open('energy_sim_r0.csv', 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        cols = zip(t,energy_dens[:])
        writer.writerows(cols)

    return()

#%%

def main():

    resu = ['100_400_100','60_240_60','76_304_76']
    ind_ini = int(23-10)
    told = np.linspace(0,30,61)
    t = told[10:]
    volume = True

    for j in range(0,1):

        print('RESOLUTION : ',resu[j])

        #stress tensors:

        F = np.zeros((3,len(told)))
        M = np.zeros((6,len(told)))
        R = np.zeros((6,len(told)))
        F9 = np.zeros((3,3,len(told)))
        sigmaF = np.zeros((3,len(told)))
        sigmaM = np.zeros((6,len(told)))
        sigmaR = np.zeros((6,len(told)))
        sigmaF9 = np.zeros((3,3,len(told)))
        rho = np.zeros(len(told))
        sigmarho = np.zeros(len(told))

        for x in range(0,len(told)):
            arx = format(25*x, "04")
            if volume == True:
                path = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_r0/res_'+resu[j]+'/volume_av/b0z-4.6e13/'
                files = 'stresses_sigma_whole_box-'+str(arx)+'.h5'
                filesdir = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/coefficients_r0/res_'+resu[j]+'/volume_av/b0z-4.6e13'
                path_energy = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/energy_dens/res_'+resu[j]+'/volume_av/b0z-4.6e13'

                listvar = read_files_whole_box(path+files)

                M[:,x] = listvar[1]
                R[:,x] = listvar[2]
                F[:,x] = listvar[3]
                sigmaM[:,x] = listvar[4]
                sigmaR[:,x] = listvar[5]
                sigmaF[:,x] = listvar[6]
                rho[x] = listvar[7]
                sigmarho[x] = listvar[8]

            else:
                path = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_r0/res_'+resu[j]+'/surface_av/b0z-4.6e13/'
                files = 'stresses_sigma_r0-'+str(arx)+'.h5'
                filesdir = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/coefficients_r0/res_'+resu[j]+'/surface_av/b0z-4.6e13/early_t'
                path_energy = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/energy_dens/res_'+resu[j]+'/surface_av/b0z-4.6e13/early_t'

                listvar = readh5files(path+files)

                M[:,x] = listvar[1]
                R[:,x] = listvar[2]
                F[:,x] = listvar[3]
                sigmaM[:,x] = listvar[4]
                sigmaR[:,x] = listvar[5]
                sigmaF[:,x] = listvar[6]
                rho[x] = listvar[7]
                sigmarho[x] = listvar[8]

        F = F[:,10:]
        M = M[:,10:]
        R = R[:,10:]
        sigmaF = sigmaF[:,10:]
        sigmaM = sigmaM[:,10:]
        sigmaR = sigmaR[:,10:]
        rho = rho[10:]
        sigmarho = sigmarho[10:]

        energyR = energy_dens(R,rho)

        #constants:

        alphaR = M[:]/energyR

        betaR = rho*R[:]/energyR

        gammaR = (rho)**(0.5)*F[:]/energyR

        #errors:

        e_alphaR, e_betaR, e_gammaR = error_propagation(rho,sigmarho,M,sigmaM,R,sigmaR,F,sigmaF,len(t))

        #averages:

        alpha_av = np.sum(alphaR[:,:ind_ini]/e_alphaR[:,:ind_ini]**2, axis = 1)/np.sum(1/e_alphaR[:,:ind_ini]**2, axis = 1)
        beta_av = np.sum(betaR[:,:ind_ini]/e_betaR[:,:ind_ini]**2, axis = 1)/np.sum(1/e_betaR[:,:ind_ini]**2, axis = 1)
        gamma_av = np.sum(gammaR[:,:ind_ini]/e_gammaR[:,:ind_ini]**2, axis = 1)/np.sum(1/e_gammaR[:,:ind_ini]**2, axis = 1)

#        sist_err_alpha = np.mean(e_alphaR, axis = 1)
#        sist_err_beta = np.mean(e_betaR, axis = 1)
#        sist_err_gamma = np.mean(e_gammaR, axis = 1)

#        err_av_alpha = np.zeros(6)
#        err_av_beta = np.zeros(6)
#        err_av_gamma = np.zeros(3)

#        std_alpha = np.zeros(6)
#        std_beta = np.zeros(6)
#        std_gamma = np.zeros(3)

#        for i in range(0,6):
#            std_alpha[i] = stdev(alphaR[i,:])
#            std_beta[i] = stdev(betaR[i,:])
#            err_av_alpha[i] = (std_alpha[i]**2+sist_err_alpha[i]**2)**(0.5)
#            err_av_beta[i] = (std_beta[i]**2+sist_err_beta[i]**2)**(0.5)
#        for i in range(0,3):
#            std_gamma[i] = stdev(gammaR[i,:])
#            err_av_gamma[i] = (std_gamma[i]**2+sist_err_gamma[i]**2)**(0.5)


        err_av_alpha = (1/np.sum(1/e_alphaR[:,:ind_ini]**2,axis = 1))**(0.5)
        err_av_beta = (1/np.sum(1/e_betaR[:,:ind_ini]**2,axis = 1))**(0.5)
        err_av_gamma = (1/np.sum(1/e_gammaR[:,:ind_ini]**2,axis = 1))**(0.5)

        #files:

        constants_files_6comp(t,alphaR,e_alphaR,alpha_av,err_av_alpha,'alphaR')
        constants_files_6comp(t,betaR,e_betaR,beta_av,err_av_beta,'betaR')
        constants_files_3comp(t,gammaR,e_gammaR,gamma_av,err_av_gamma,'gammaR')

        try:
            os.makedirs(filesdir)
        except OSError as error:
            print(error)

        os.system('mv *.csv '+filesdir+'/.')
        os.system('mv *av.txt '+filesdir+'/.')

        energy_files(t,energyR)

        try:
            os.makedirs(path_energy)
        except OSError as error:
            print(error)

        os.system('mv energy_sim_r0* '+path_energy+'/.')

    return()




#%%

if __name__ == "__main__":
    main()
