import numpy as np
from scipy.fftpack import fftn
from scipy.fft import rfftn
import h5py
import sys
import os

#============================================================================

def readh5files(file):

    print('DATA FROM FILE:      ',file)

    with h5py.File(file, "r") as f:
      # List all groups
        print("Keys: %s" % f.keys())
        a_group_key = list(f.keys())
        for i in a_group_key:
            print(i)

        Pgas = f[a_group_key[0]][...]
        bphi = f[a_group_key[1]][...]
        br = f[a_group_key[2]][...]
        bz = f[a_group_key[3]][...]
        gravpot = f[a_group_key[4]][...]
        phi = f[a_group_key[5]][...]
        r = f[a_group_key[6]][...]
        rho = f[a_group_key[7]][...]
        time = f[a_group_key[8]][...]
        vphi = f[a_group_key[9]][...]
        vr = f[a_group_key[10]][...]
        vz = f[a_group_key[11]][...]
        z = f[a_group_key[12]][...]

        f.close()

    print('Dimensions of the grid     :',len(r)," x ",len(phi)," x ",len(z))
    print('Time           :',time)

    return [time, r, phi, z, br.T, bphi.T, bz.T, vr.T, vphi.T, vz.T, rho.T, Pgas.T, gravpot.T]

def readh5files_notime(file):

    print('DATA FROM FILE:      ',file)

    with h5py.File(file, "r") as f:
    # List all groups
        print("Keys: %s" % f.keys())
        a_group_key = list(f.keys())
        for i in a_group_key:
            print(i)

        Pgas = f[a_group_key[0]][...]
        print('Pgas shape    :', Pgas.shape)
        bphi = f[a_group_key[1]][...]
        print('Bphi shape    :', bphi.shape)
        br = f[a_group_key[2]][...]
        print('Br shape    :', br.shape)
        bz = f[a_group_key[3]][...]
        print('Bz shape    :', bz.shape)
        gravpot = f[a_group_key[4]][...]
        print('gravpot shape    :', gravpot.shape)
        phi = f[a_group_key[5]][...]
        print('Phi shape    :', phi.shape)
        r = f[a_group_key[6]][...]
        print('r shape    :', r.shape)
        rho = f[a_group_key[7]][...]
        print('Rho shape    :', rho.shape)
        vphi = f[a_group_key[8]][...]
        print('vphi shape    :', vphi.shape)
        vr = f[a_group_key[9]][...]
        print('vr shape    :', vr.shape)
        vz = f[a_group_key[10]][...]
        print('vz shape    :', vz.shape)
        z = f[a_group_key[11]][...]
        print('z shape    :', z.shape)
        #time = file.replace('.h5','')
        #time = float(time[-4:])/250


        f.close()

    print('Dimensions of the grid     :',len(r)," x ",len(phi)," x ",len(z))

    return [gravpot, r, phi, z, br.T, bphi.T, bz.T, vr.T, vphi.T, vz.T, rho.T, Pgas.T]

def fourier_transf(r,phi,z,lr,lphi,lz,f):

    Nr = len(r)
    Nphi = len(phi)

    Nz = len(z)

    kr = np.linspace(0,2*np.pi/lr*int(Nr/2),int(Nr/2+1))
    kphi = np.linspace(0,2*np.pi/lphi*int(Nphi/2),int(Nphi/2+1))
    kz = np.linspace(0,2*np.pi/lz*int(Nz/2),int(Nz/2+1))
    #kphi = np.linspace(-2*np.pi*Nphi/(2*lphi),2*np.pi*Nphi/(2*lphi), Nphi+1)
    #kz = np.linspace(-2*np.pi*Nz/(2*lz),2*np.pi*Nz/(2*lz), Nz+1)

    ftilde = np.abs(fftn(f))**2

    print('Fourier transform shape : ', ftilde.shape)
    print('kr shape : ', kr.shape)
    print('kphi shape : ', kphi.shape)
    print('kz shape : ', kz.shape)

    return kr,kphi,kz, ftilde

def averaging(kr,kphi,kz,ftilde,L,size):

    dk = 2*np.pi/L
    kmod = np.zeros((len(kr),len(kphi),len(kz)))

    for i in range(0,len(kr)):
        for j in range(0,len(kphi)):
            for k in range(0,len(kz)):

                kmod[i,j,k] = np.sqrt(kr[i]**2+kphi[j]**2+kz[k]**2)


    kmod = np.array(kmod)
    krad = np.linspace(0,size/2,int(size/2+1))*dk

    dV = (kr[1]-kr[0])*(kphi[1]-kphi[0])*(kz[1]-kz[0])

    fmean = np.zeros(len(krad)-1)

    print('Averaging over radial k-vector...')

    for x in range(0,len(krad)-1):

        integrand = 0
        V = 0
        for i in range(0,len(kr)):
            for j in range(0,len(kphi)):
                for l in range(0,len(kz)):
                    if kmod[i,j,l] >= krad[x] and kmod[i,j,l] < krad[x+1]:
                            integrand += ((kmod[i,j,l]**2)*ftilde[i,j,l])*dV
                            V += dV

        if V == 0:
            fmean[x] = 0
            print('0 found in iteration nº ', x+1)
        else :
            fmean[x] = integrand/V

        #spectra = L**3*4*np.pi/((2*np.pi)**3*size**6)*fmean

    krmid = np.zeros(len(fmean))
    for x in range(0,len(krmid)):
        krmid[x] = (krad[x]+krad[x+1])/2

    print('DONE')
    print('*'*60)

    return krmid, fmean

def main():

    lr = 1e5
    lphi = 4e5
    lz = 1e5

    b0z = '4_6e13'
    res = '100_400_100'
    t = np.array([2,5,10,11,12,15,17,20,30])
    direc = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/DATA/MRI/A100_400_100_B_4.6e13_v_4.45d8_r_4.45d8_Lz_1.0_INTEL_stencil/"
    save_path = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/DATA/MRI/energy_spectra/res_"+res

    try:
        os.makedirs(save_path)
    except OSError as error:
        print(error)

    for tt in t:
        print('Energy spectra for t = ', tt)
        print('*'*60)
        arx = format(50*tt, "04")
        listvar = readh5files(direc+'mri-'+str(arx)+'.h5')
        r = listvar[1]
        phi = listvar[2]
        z = listvar[3]
        br = np.array(listvar[4])
        Bener = np.array([listvar[4],listvar[5],listvar[6]])*2.874214371*10**(-25)/((4*3.141592653589793)**(0.5))
        vener = np.array([listvar[7],listvar[8],listvar[9]])*3.33564095*10**(-11)
        rho = np.array(listvar[10])*7.42471382*10**(-29)

        print('Fourier transform of magnetic energy')
        print('*'*60)
        kr,kphi,kz,brft = fourier_transf(r,phi,z,lr,lphi,lz,Bener[0])
        kr,kphi,kz,bphift = fourier_transf(r,phi,z,lr,lphi,lz,Bener[1])
        kr,kphi,kz,bzft = fourier_transf(r,phi,z,lr,lphi,lz,Bener[2])
        emag_ft = (brft+bphift+bzft)

        kmean,emag_mean = averaging(kr,kphi,kz,emag_ft,lr, len(phi))

        arxiu = format(tt,"02")
        hf = h5py.File(save_path+'/energy_spectra--b0z-'+b0z+'--'+str(res)+'t-'+str(arxiu)+'.h5','w')
        hf.create_dataset('time',data=tt)
        hf.create_dataset('emag', data=emag_mean)
        hf.create_dataset('k', data = kmean)

        print('Fourier transform of kinetic energy')
        kr,kphi,kz,ekinrft = fourier_transf(r,phi,z,lr,lphi,lz,np.sqrt(rho)*vener[0])
        kr,kphi,kz,ekinphift = fourier_transf(r,phi,z,lr,lphi,lz,np.sqrt(rho)*vener[1])
        kr,kphi,kz,ekinzft = fourier_transf(r,phi,z,lr,lphi,lz,np.sqrt(rho)*vener[2])
        ekin_ft = (ekinrft+ekinphift+ekinzft)

        kmean,ekin_mean = averaging(kr,kphi,kz,ekin_ft,lr,len(phi))
        hf.create_dataset('ekin', data = ekin_mean)

        hf.close()


    return()

main()
