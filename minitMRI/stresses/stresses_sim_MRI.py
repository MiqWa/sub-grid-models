#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 28 14:38:10 2021

@author: miquelmiravet
"""

#############################################
#                                           #
#           STRESS TENSORS EVOLUTION        #
#                                           #
#############################################

"""
Calculation of the energy density of the stresses directly from the data of the
numerical simulations
"""

import os
import numpy as np
import sys
import csv
import numpy.ctypeslib as npct
from ctypes import c_int
import h5py

#=================================================================================

array_3d_float = npct.ndpointer(dtype=np.float64, ndim=3, flags='CONTIGUOUS')
array_1d_float = npct.ndpointer(dtype=np.float64, ndim=1, flags='CONTIGUOUS')

libboxkernel = npct.load_library("/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/gradient-model-test/libboxkernel", ".")
libboxkernel.box_kernel.restype = None
libboxkernel.box_kernel.argtypes = [c_int, c_int, c_int, array_1d_float, array_1d_float, array_1d_float, array_1d_float, c_int, c_int, c_int, array_1d_float, array_1d_float, array_1d_float, array_1d_float, array_3d_float, array_3d_float]

#=================================================================================

def readh5files(file):

    print('DATA FROM FILE:      ',file)

    with h5py.File(file, "r") as f:
    # List all groups
        print("Keys: %s" % f.keys())
        a_group_key = list(f.keys())
        for i in a_group_key:
            print(i)

        Pgas = f[a_group_key[0]][...]
        print('Pgas shape    :', Pgas.shape)
        bphi = f[a_group_key[1]][...]
        print('Bphi shape    :', bphi.shape)
        br = f[a_group_key[2]][...]
        print('Br shape    :', br.shape)
        bz = f[a_group_key[3]][...]
        print('Bz shape    :', bz.shape)
        gravpot = f[a_group_key[4]][...]
        print('gravpot shape    :', gravpot.shape)
        phi = f[a_group_key[5]][...]
        print('Phi shape    :', phi.shape)
        r = f[a_group_key[6]][...]
        print('r shape    :', r.shape)
        rho = f[a_group_key[7]][...]
        print('Rho shape    :', rho.shape)
        time = f[a_group_key[8]][...]
        print('Time shape    :', time.shape)
        vphi = f[a_group_key[9]][...]
        print('vphi shape    :', vphi.shape)
        vr = f[a_group_key[10]][...]
        print('vr shape    :', vr.shape)
        vz = f[a_group_key[11]][...]
        print('vz shape    :', vz.shape)
        z = f[a_group_key[12]][...]
        print('z shape    :', z.shape)
        #time = file.replace('.h5','')
        #time = float(time[-4:])/250


        f.close()

    print('Dimensions of the grid     :',len(r)," x ",len(phi)," x ",len(z))
    print('Time           :',time,' s')

    return [time, r, phi, z, br.T, bphi.T, bz.T, vr.T, vphi.T, vz.T, rho.T, Pgas.T, gravpot.T]

#===========================================================================================

def readh5files_notime(file):

    print('DATA FROM FILE:      ',file)

    with h5py.File(file, "r") as f:
    # List all groups
        print("Keys: %s" % f.keys())
        a_group_key = list(f.keys())
        for i in a_group_key:
            print(i)

        Pgas = f[a_group_key[0]][...]
        print('Pgas shape    :', Pgas.shape)
        bphi = f[a_group_key[1]][...]
        print('Bphi shape    :', bphi.shape)
        br = f[a_group_key[2]][...]
        print('Br shape    :', br.shape)
        bz = f[a_group_key[3]][...]
        print('Bz shape    :', bz.shape)
        gravpot = f[a_group_key[4]][...]
        print('gravpot shape    :', gravpot.shape)
        phi = f[a_group_key[5]][...]
        print('Phi shape    :', phi.shape)
        r = f[a_group_key[6]][...]
        print('r shape    :', r.shape)
        rho = f[a_group_key[7]][...]
        print('Rho shape    :', rho.shape)
        vphi = f[a_group_key[8]][...]
        print('vphi shape    :', vphi.shape)
        vr = f[a_group_key[9]][...]
        print('vr shape    :', vr.shape)
        vz = f[a_group_key[10]][...]
        print('vz shape    :', vz.shape)
        z = f[a_group_key[11]][...]
        print('z shape    :', z.shape)
        #time = file.replace('.h5','')
        #time = float(time[-4:])/250


        f.close()

    print('Dimensions of the grid     :',len(r)," x ",len(phi)," x ",len(z))

    return [gravpot.T, r, phi, z, br.T, bphi.T, bz.T, vr.T, vphi.T, vz.T, rho.T, Pgas.T]

#===========================================================================================

def box_filter(var,filtersize,dr,r,phi,z,rfilter,phifilter,zfilter):

    varmean = np.zeros((len(rfilter),len(phifilter),len(zfilter)))

    libboxkernel.box_kernel(len(r), len(phi),len(z), r, phi, z, dr, len(rfilter), len(phifilter), len(zfilter), rfilter, phifilter, zfilter, filtersize, var, varmean)

    return varmean

#===========================================================================================

def stresses(v, b, rho, filtersize, dr, r,phi, z, rfilter, phifilter, zfilter):

    rhomean = box_filter(rho,filtersize,dr,r,phi,z,rfilter,phifilter,zfilter)
    vmean = np.array([box_filter(v[0],filtersize,dr,r,phi,z,rfilter,phifilter,zfilter),box_filter(v[1],filtersize,dr,r,phi,z,rfilter,phifilter,zfilter),box_filter(v[2],filtersize,dr,r,phi,z,rfilter,phifilter,zfilter)])

    vsqmean = np.zeros((3,3,len(rfilter),len(phifilter),len(zfilter)))

    Rmean = np.zeros((3,3,len(rfilter),len(phifilter),len(zfilter)))

    bmean = np.array([box_filter(b[0],filtersize,dr,r,phi,z,rfilter,phifilter,zfilter),box_filter(b[1],filtersize,dr,r,phi,z,rfilter,phifilter,zfilter),box_filter(b[2],filtersize,dr,r,phi,z,rfilter,phifilter,zfilter)])

    bsqmean = np.zeros((3,3,len(rfilter),len(phifilter),len(zfilter)))

    Mmean = np.zeros((3,3,len(rfilter),len(phifilter),len(zfilter)))

    bvmean = np.zeros((3,3,len(rfilter),len(phifilter),len(zfilter)))

    Fmean = np.zeros((3,3,len(rfilter),len(phifilter),len(zfilter)))

    for x in range(0,3):
        for y in range(0,3):

            bsqmean[x,y] = box_filter(b[x,:,:,:]*b[y,:,:,:],filtersize, dr, r,phi, z, rfilter, phifilter, zfilter)
            Mmean[x,y] = bsqmean[x,y]-bmean[x]*bmean[y]

            vsqmean[x,y] = box_filter(v[x,:,:,:]*v[y,:,:,:],filtersize, dr, r,phi, z, rfilter, phifilter, zfilter)
            Rmean[x,y] = vsqmean[x,y]-vmean[x]*vmean[y]

            bvmean[x,y] = box_filter(b[x,:,:,:]*v[y,:,:,:],filtersize, dr, r,phi, z, rfilter, phifilter, zfilter)


    for i in range(0,3):
    	for j in range(0,3):

            Fmean[i,j] = bvmean[j,i]-bmean[j]*vmean[i]-(bvmean[i,j]-bmean[i]*vmean[j])


    return Mmean, Rmean, Fmean, rhomean

#===========================================================================================

def stress_sim_files_6comp(stress,name,t,lenrf,lenphif,lenzf, Sf,res):

    with open('stress'+name+'.csv', 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        for a in range(0,lenrf):
            for b in range(0,lenphif):
                for c in range(0,lenzf):
                    cols = zip(t,stress[:,0,0,a,b,c],stress[:,0,1,a,b,c],stress[:,0,2,a,b,c],stress[:,1,1,a,b,c],stress[:,1,2,a,b,c],stress[:,2,2,a,b,c])
                    writer.writerows(cols)

    return()

#===========================================================================================

def stress_sim_files_3comp(stress,name,t,lenrf,lenphif,lenzf, Sf,res):

    with open('stress'+name+'.csv', 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        for a in range(0,lenrf):
            for b in range(0,lenphif):
                for c in range(0,lenzf):
                    cols = zip(t,stress[:,0,1,a,b,c],stress[:,0,2,a,b,c],stress[:,1,2,a,b,c])
                    writer.writerows(cols)

    return()

#===========================================================================================

def stresses_ev(res,direc,Sf,t,path_files, lenphi, B_old):

    if lenphi == 304 or B_old == False:
        var = readh5files_notime(direc+'mri00000000.h5')
    else :
        var = readh5files(direc+'mri-0000.h5')

    r = var[1].astype(np.float64)
    print('r TYPE : ', r.dtype)
    phi = var[2].astype(np.float64)
    z = var[3].astype(np.float64)

    B0 = np.array([var[4],var[5],var[6]])
    v0 = np.array([var[7],var[8],var[9]])

    rhalf = (r[1]-r[0])/2

    r_front = np.zeros(len(r)+1)

    r_front[0] = r[0]-rhalf
    r_front[-1] = r[-1]+rhalf

    for i in range(1,len(r)):
        r_front[i] = (r[i]+r[i-1])/2

    difr = np.zeros(len(r))
    for j in range(0,len(r)):
        difr[j] = r_front[j+1]**2-r_front[j]**2

    cellsize = np.array([r[1]-r[0],phi[1]-phi[0],z[1]-z[0]])
    avcellsize =(r[1]-r[0]+(r[-1]+r[0])*0.5*(phi[1]-phi[0])+z[1]-z[0])/3
    filtersize = Sf*cellsize
    avfiltersize = Sf*avcellsize
    print('FILTER SIZE = ',avfiltersize)
    a_rz = int(len(r)/2)
    a_phi = int(len(phi)/2)
    rzmin = a_rz-5
    rzmax = a_rz+5
    phimin = a_phi-5
    phimax = a_phi+5

    rfilter= r[rzmin:rzmax]
    phifilter = phi[phimin:phimax]
    zfilter = z[rzmin:rzmax]

    Mmean = np.zeros((len(t),3,3,len(rfilter),len(phifilter),len(zfilter)))
    Rmean = np.zeros((len(t),3,3,len(rfilter),len(phifilter),len(zfilter)))
    Fmean = np.zeros((len(t),3,3,len(rfilter),len(phifilter),len(zfilter)))
    rhomean = np.zeros((len(t),len(rfilter),len(phifilter),len(zfilter)))

    for x in range(0,len(t)):

        if lenphi == 304 or B_old == False:
            arx = format(25*x, "08")
            listvar = readh5files_notime(direc+'mri'+str(arx)+'.h5')
            print('Time : ['+str(t[x])+'] s')

        else :
            arx = format(25*x, "04")
            listvar = readh5files(direc+'mri-'+str(arx)+'.h5')

        B = [listvar[4], listvar[5], listvar[6]]-B0
        B = np.array(B)*2.874214371*10**(-25)
        v = [listvar[7],listvar[8],listvar[9]]-v0
        v = np.array(v)*3.33564095*10**(-11)
        rho = listvar[10]*7.42471382*10**(-29)
        rho = np.asarray(rho, order='C')

        Mmean[x], Rmean[x], Fmean[x], rhomean[x] = stresses(v, B, rho, filtersize, difr, r,phi, z, rfilter, phifilter, zfilter)

        arxiu = format(25*x, "04")
        hf = h5py.File(path_files+'/stresses_sim_res_'+res+'-'+str(arxiu)+'.h5', 'w')
        #g_sf = hf.create_group('Sf '+str(Sf))
        hf.create_dataset('time', data=t[x])
        hf.create_dataset('S_f', data = Sf)
        hf.create_dataset('r', data=r)
        hf.create_dataset('phi', data=phi)
        hf.create_dataset('z', data=z)
        hf.create_dataset('dr', data = difr)
        hf.create_dataset('r_f', data=rfilter)
        hf.create_dataset('phi_f', data=phifilter)
        hf.create_dataset('z_f', data=zfilter)
        hf.create_dataset('stress_Mrr', data=Mmean[x,0,0])
        hf.create_dataset('stress_Mrphi', data=Mmean[x,0,1])
        hf.create_dataset('stress_Mrz', data=Mmean[x,0,2])
        hf.create_dataset('stress_Mphiphi', data=Mmean[x,1,1])
        hf.create_dataset('stress_Mphiz', data=Mmean[x,1,2])
        hf.create_dataset('stress_Mzz', data=Mmean[x,2,2])
        hf.create_dataset('stress_Rrr', data=Rmean[x,0,0])
        hf.create_dataset('stress_Rrphi', data=Rmean[x,0,1])
        hf.create_dataset('stress_Rrz', data=Rmean[x,0,2])
        hf.create_dataset('stress_Rphiphi', data=Rmean[x,1,1])
        hf.create_dataset('stress_Rphiz', data=Rmean[x,1,2])
        hf.create_dataset('stress_Rzz', data=Rmean[x,2,2])
        hf.create_dataset('stress_Frphi', data=Fmean[x,0,1])
        hf.create_dataset('stress_Frz', data=Fmean[x,0,2])
        hf.create_dataset('stress_Fphiz', data=Fmean[x,1,2])
        hf.create_dataset('rho_mean', data=rhomean[x])
        hf.create_dataset('b_mean', data = )
        hf.create_dataset('v_mean',data = )
        hf.close()

    return()

#===========================================================================================

def main():

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/DATA/MRI/"

    lenphi = int(sys.argv[2])
    B_old = False
    Bnew = 2

    Sf = int(sys.argv[1])


    if lenphi == 400 :

        lenr = 100
        lenz = 100
        if B_old == True :
            direc = path_data+'A100_400_100_B_4.6e13_v_4.45d8_r_4.45d8_Lz_1.0_INTEL_stencil/'
        elif Bnew == 2:
            direc = path_data+'A100_400_100_B_2.76/outp-mri/'
        elif Bnew == 3:
            direc = path_data+'A100_400_100_B_3.45/outp-mri/'


    elif lenphi == 240 :

        lenr = 60
        lenz = 60
        direc = path_data+'A60_240_60_B_4.6e13_v_4.45d8_r_4.45d8_Lz_1.0_INTEL_stencil_flex/'

    elif lenphi == 304 :

        lenr = 76
        lenz = 76
        direc = path_data+'A76_304_76/outp-mri/'

    lent = 61
    t_final = 30
    t = np.linspace(0,t_final,lent)
    res = str(lenr)+'_'+str(lenphi)+'_'+str(lenz)

    path_files = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_sim_test/res_"+res+"/b0z-2.76e13/Sf_"+str(Sf)

    try:
        os.makedirs(path_files)
    except OSError as error:
        print(error)

    stresses_ev(res,direc,Sf,t,path_files, lenphi, B_old)

    return()

main()
