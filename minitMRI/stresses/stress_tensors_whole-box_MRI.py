#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 2 15:07:10 2022

@author: miquelmiravet
"""

"""

COMPUTATION OF THE STRESS TENSORS OVER THE WHOLE BOX FOR MRI SIMULATION

"""

import os
import numpy as np
import matplotlib.pyplot as plt
plt.style.use('seaborn-white')
import h5py

#READ DATA FUNCTIONS:


def readh5files(file):

    print('DATA FROM FILE:      ',file)

    with h5py.File(file, "r") as f:
    # List all groups
        print("Keys: %s" % f.keys())
        a_group_key = list(f.keys())
        for i in a_group_key:
            print(i)

        Pgas = f[a_group_key[0]][...]
        print('Pgas shape    :', Pgas.shape)
        bphi = f[a_group_key[1]][...]
        print('Bphi shape    :', bphi.shape)
        br = f[a_group_key[2]][...]
        print('Br shape    :', br.shape)
        bz = f[a_group_key[3]][...]
        print('Bz shape    :', bz.shape)
        gravpot = f[a_group_key[4]][...]
        print('gravpot shape    :', gravpot.shape)
        phi = f[a_group_key[5]][...]
        print('Phi shape    :', phi.shape)
        r = f[a_group_key[6]][...]
        print('r shape    :', r.shape)
        rho = f[a_group_key[7]][...]
        print('Rho shape    :', rho.shape)
        time = f[a_group_key[8]][...]
        print('Time shape    :', time.shape)
        vphi = f[a_group_key[9]][...]
        print('vphi shape    :', vphi.shape)
        vr = f[a_group_key[10]][...]
        print('vr shape    :', vr.shape)
        vz = f[a_group_key[11]][...]
        print('vz shape    :', vz.shape)
        z = f[a_group_key[12]][...]
        print('z shape    :', z.shape)
        #time = file.replace('.h5','')
        #time = float(time[-4:])/250


        f.close()

    print('Dimensions of the grid     :',len(r)," x ",len(phi)," x ",len(z))
    print('Time           :',time,' s')

    return [time, r, phi, z, br.T, bphi.T, bz.T, vr.T, vphi.T, vz.T, rho.T, Pgas.T, gravpot.T]

def readh5files_notime(file):

    print('DATA FROM FILE:      ',file)

    with h5py.File(file, "r") as f:
    # List all groups
        print("Keys: %s" % f.keys())
        a_group_key = list(f.keys())
        for i in a_group_key:
            print(i)

        Pgas = f[a_group_key[0]][...]
        print('Pgas shape    :', Pgas.shape)
        bphi = f[a_group_key[1]][...]
        print('Bphi shape    :', bphi.shape)
        br = f[a_group_key[2]][...]
        print('Br shape    :', br.shape)
        bz = f[a_group_key[3]][...]
        print('Bz shape    :', bz.shape)
        gravpot = f[a_group_key[4]][...]
        print('gravpot shape    :', gravpot.shape)
        phi = f[a_group_key[5]][...]
        print('Phi shape    :', phi.shape)
        r = f[a_group_key[6]][...]
        print('r shape    :', r.shape)
        rho = f[a_group_key[7]][...]
        print('Rho shape    :', rho.shape)
        vphi = f[a_group_key[8]][...]
        print('vphi shape    :', vphi.shape)
        vr = f[a_group_key[9]][...]
        print('vr shape    :', vr.shape)
        vz = f[a_group_key[10]][...]
        print('vz shape    :', vz.shape)
        z = f[a_group_key[11]][...]
        print('z shape    :', z.shape)
        #time = file.replace('.h5','')
        #time = float(time[-4:])/250


        f.close()

    print('Dimensions of the grid     :',len(r)," x ",len(phi)," x ",len(z))

    return [gravpot.T, r, phi, z, br.T, bphi.T, bz.T, vr.T, vphi.T, vz.T, rho.T, Pgas.T]


#AVERAGING AND STRESS TENSORS FUNCTION:

def averaging_whole_box(r,phi,z,B,v,rho):

    rhalf = (r[1]-r[0])/2

    r_front = np.zeros(len(r)+1)

    r_front[0] = r[0]-rhalf
    r_front[-1] = r[-1]+rhalf

    for i in range(1,len(r)):
        r_front[i] = (r[i]+r[i-1])/2

    difr = np.zeros(len(r))
    for j in range(0,len(r)):
        difr[j] = r_front[j+1]**2-r_front[j]**2

    dphi = phi[1]-phi[0]
    dz = z[1]-z[0]
    dV = 0.5*difr*dphi*dz

    dVmatrix = dV*np.ones((len(r),len(phi),len(z)))
    V = np.sum(dVmatrix)

    #averaging:

    var_dec = [B[0],B[1],B[2],v[0],v[1],v[2],rho]
    meanvar=np.zeros(7)

    for y in range(0,7):
        integrand = dV*var_dec[y]
        meanvar[y] = np.sum(integrand)/V

    bmean = np.array([meanvar[0],meanvar[1],meanvar[2]])
    vmean = np.array([meanvar[3],meanvar[4],meanvar[5]])
    rhomean = meanvar[6]

    #turbulent parts:
    turb = np.zeros((6,len(r),len(phi),len(z)))
    for x in range(0,6):
        turb[x] = var_dec[x]-meanvar[x]

    brturb = turb[0]
    bphiturb = turb[1]
    bzturb = turb[2]
    vrturb = turb[3]
    vphiturb = turb[4]
    vzturb = turb[5]

    #stress tensors:
    bturb = np.array([brturb, bphiturb, bzturb])
    vturb = np.array([vrturb, vphiturb, vzturb])
    M = np.zeros((3,3,len(r),len(phi),len(z)))
    R = np.zeros((3,3,len(r),len(phi),len(z)))
    F = np.zeros((3,3,len(r),len(phi),len(z)))

    for x in range(0,3):
        for y in range(0,3):
            M[x,y] = bturb[x]*bturb[y]
            R[x,y] = vturb[x]*vturb[y]
            F[x,y] = bturb[y]*vturb[x]-bturb[x]*vturb[y]

    Mmean = np.zeros((3,3))
    Rmean = np.zeros((3,3))
    Fmean = np.zeros((3,3))
    sigmaMsq = np.zeros((3,3))
    sigmaRsq = np.zeros((3,3))
    sigmaFsq = np.zeros((3,3))

    sigmarhosq = np.sum(dV*(rho[:,:]-rhomean)**2)/V

    #averaging of the turbulent stress tensors:
    for x in range(0,3):
        for y in range(0,3):
            Mmean[x,y] = np.sum(M[x,y]*dV)/V
            Rmean[x,y] = np.sum(R[x,y]*dV)/V
            Fmean[x,y] = np.sum(F[x,y]*dV)/V

            sigmaMsq[x,y] = np.sum(dV*(M[x,y,:,:]-Mmean[x,y])**2)/V
            sigmaRsq[x,y] = np.sum(dV*(R[x,y,:,:]-Rmean[x,y])**2)/V
            sigmaFsq[x,y] = np.sum(dV*(F[x,y,:,:]-Fmean[x,y])**2)/V

    return bmean, vmean, rhomean, sigmarhosq, Mmean, Rmean, Fmean, sigmaMsq, sigmaRsq, sigmaFsq


#Time evolution:


def time_evolution(direc,r, phi, z,B0,v0,t,path,j):

    lent = len(t)

    omega0 = 767*3.33564095*10**(-11)

    bmean_t = np.zeros(3)
    vmean_t = np.zeros(3)
    Mmean_t = np.zeros((3,3))
    Rmean_t = np.zeros((3,3))
    Fmean_t = np.zeros((3,3))

    sigmaMsq_t = np.zeros((3,3))
    sigmaRsq_t = np.zeros((3,3))
    sigmaFsq_t = np.zeros((3,3))

    for x in range(0,lent):

        if j == 1 or j == 2:
            arx = format(25*x, "08")
            listvar = readh5files_notime(direc+'/outp-mri/mri'+str(arx)+'.h5')
            print('Time : ['+str(t[x])+'] s')
        else :
            arx = format(25*x, "04")
            listvar = readh5files(direc+'mri-'+str(arx)+'.h5')

        B = (np.array([listvar[4],listvar[5],listvar[6]])-B0)*2.874214371*10**(-25)
        v = (np.array([listvar[7],listvar[8],listvar[9]])-v0)*3.33564095*10**(-11)
        rho = listvar[10]*7.42471382*10**(-29)

        #averaging:

        bmean_t, vmean_t, rhomean_t, sigmarhosq_t, Mmean_t, Rmean_t, Fmean_t, sigmaMsq_t, sigmaRsq_t,sigmaFsq_t = averaging_whole_box(r,phi,z,B,v,rho)

        arxiu = format(25*x,"04")
        hf = h5py.File(path+'/stresses_sigma_whole_box-'+str(arxiu)+'.h5', 'w')
        hf.create_dataset('time', data=t[x])
        hf.create_dataset('rhomean', data = rhomean_t)
        hf.create_dataset('sigma_rho', data = sigmarhosq_t)
        hf.create_dataset('Mmean', data = Mmean_t)
        hf.create_dataset('Rmean', data = Rmean_t)
        hf.create_dataset('Fmean', data = Fmean_t)
        hf.create_dataset('sigma_M', data = sigmaMsq_t)
        hf.create_dataset('sigma_R', data = sigmaRsq_t)
        hf.create_dataset('sigma_F', data = sigmaFsq_t)
        hf.close()

    return()

#%%

def main():


    lenphi = 400

    resu = '100_400_100'
    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/DATA/MRI/"
    direcv = [path_data+'A100_400_100_B_4.6e13_v_4.45d8_r_4.45d8_Lz_1.0_INTEL_stencil/',path_data+'A100_400_100_B_3.45/',path_data+'A100_400_100_B_2.76/']

    t = np.linspace(0,30,61)

    for j in range(0,3):

        print('RESOLUTION : ',resu)

        direc = direcv[j]

        if j == 1 or j == 2:
            arx = '00000000'
            listvar = readh5files_notime(direc+'/outp-mri/mri'+str(arx)+'.h5')
        else :
            arx = '0000'
            listvar = readh5files(direc+'mri-'+str(arx)+'.h5')

        r = listvar[1]
        phi = listvar[2]
        z = listvar[3]
        B0 = np.array([listvar[4],listvar[5],listvar[6]])
        v0 = np.array([listvar[7],listvar[8],listvar[9]])

        if j == 0:

            path = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_r0/res_'+resu+'/volume_av/b0z-4.6e13/'

        elif j == 1:

            path = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_r0/res_'+resu+'/volume_av/b0z-3.45e13/'

        elif j == 2:

            path = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_r0/res_'+resu+'/volume_av/b0z-2.76e13/'

        try:
            os.makedirs(path)
        except OSError as error:
            print(error)

        time_evolution(direc,r, phi, z,B0, v0,t,path,j)

    return()





  #%%

main()
