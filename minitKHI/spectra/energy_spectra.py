import numpy as np
from scipy.fftpack import fftn
from scipy.fft import rfftn
import h5py
import sys
import os

#============================================================================

def readh5files(file):

    print('DATA FROM FILE:      ',file)
    hf = h5py.File(file, 'r')

 #   t = np.array(hf.get('time'))
 #   t0 = np.array(hf.get('t0'))
 #   tf = np.array(hf.get('tf'))
 #   x = np.array(hf.get('x'))
 #   y = np.array(hf.get('y'))
 #   z = np.array(hf.get('z'))
    rho = np.array(hf.get('rho'))
    vx = np.array(hf.get('vx'))
    vy = np.array(hf.get('vy'))
    vz = np.array(hf.get('vz'))
    Bx = np.array(hf.get('Bx'))
    By = np.array(hf.get('By'))
    Bz = np.array(hf.get('Bz'))

 #   print('Dimensions of the grid     :',len(x)," x ",len(y)," x ",len(z))
 #   print('Time           : ',t)

    return [Bx, By, Bz, vx, vy, vz,rho]

def fourier_transf(x,y,z,f):

    L = 1
    N = len(x)

    kx = np.linspace(0,N/L, N)
    ky = np.linspace(0,N/L, N)
    kz = np.linspace(0,N/L, N)

    ftilde = np.abs(fftn(f))**2

    print('fftn : ', np.size(ftilde))

    return kx,ky,kz, ftilde

def fourier_transf_CVP(length,f):

    L = 1
    N = length

    nvec = np.linspace(0,int(N/2),int(N/2+1))
    dk = 2*np.pi/L
    kx = nvec*dk
    ky = nvec*dk
    kz = nvec*dk

    ftilde = np.abs(rfftn(f))**2

    print('Fourier transform shape : ', ftilde.shape)
    print('kx shape : ', kx.shape)
    print('ky shape : ', ky.shape)
    print('kz shape : ', kz.shape)

    return kx,ky,kz, ftilde

def averaging(kx,ky,kz,ftilde,size):

    kmod = np.zeros((len(kx),len(ky),len(kz)))

    for i in range(0,len(kx)):
        for j in range(0,len(ky)):
            for k in range(0,len(kz)):

                kmod[i,j,k] = np.sqrt(kx[i]**2+ky[j]**2+kz[k]**2)

    kmin = np.amin(kmod)
    kmax = np.amax(kmod)
#    print('kmin = ', kmin)
#    print('kmax = ', kmax)
    kmod = np.array(kmod)
    k = np.linspace(kmin,kmax,size+1)
    k = np.array(k)
#    print('kmod    = ',kmod)

    dV = (kx[1]-kx[0])*(ky[1]-ky[0])*(kz[1]-kz[0])

    fmean = np.zeros(size)
    kmean = np.zeros(size)

    for x in range(0,size):
        integrand = 0
        V = 0
        integrandk = 0
#        print('Volume ini (should be 0) :  ', V)
        for i in range(0,len(kx)):
            for j in range(0,len(ky)):
                for l in range(0,len(kz)):
                    if kmod[i,j,l] >= k[x] and kmod[i,j,l] < k[x+1]:
                            integrand += (np.sqrt(ftilde[i,j,l]**2))*dV
                            integrandk += kmod[i,j,l]*dV
                            V += dV
#        print('Volume   = ', V)
        fmean[x] = integrand/V
        kmean[x] = integrandk/V

    return kmean, fmean

def averaging_CVP_old(kx,ky,kz,ftilde,size):

    L = 1
    dk = 2*np.pi/L
    kmod = np.zeros((len(kx),len(ky),len(kz)))

    for i in range(0,len(kx)):
        for j in range(0,len(ky)):
            for k in range(0,len(kz)):

                kmod[i,j,k] = np.sqrt(kx[i]**2+ky[j]**2+kz[k]**2)

    kmin = np.amin(kmod)
    kmax = np.amax(kmod)

    kmod = np.array(kmod)
    kr = np.linspace(0,size/2,int(size/2+1))*dk

    dV = (kx[1]-kx[0])*(ky[1]-ky[0])*(kz[1]-kz[0])

    fmean = np.zeros(len(kr)-1)

    for x in range(0,len(kr)-1):
        integrand = 0
        V = 0
        integrandk = 0
#        print('Volume ini (should be 0) :  ', V)
        for i in range(0,len(kx)):
            for j in range(0,len(ky)):
                for l in range(0,len(kz)):
                    if kmod[i,j,l] >= kr[x] and kmod[i,j,l] < kr[x+1]:
                            integrand += ((kmod[i,j,l]**2)*ftilde[i,j,l])*dV
                            V += dV

#        print('Volume   = ', V)
        fmean[x] = integrand/V

        spectra = L**3*4*np.pi/((2*np.pi)**3*size**6)*fmean

    krmid = np.zeros(len(fmean))
    for x in range(0,len(krmid)):
        krmid[x] = (kr[x]+kr[x+1])/2

    return krmid, spectra

def averaging_CVP(kx,ky,kz,ftilde,size):

    L = 1
    dk = 2*np.pi/L
    kmod = np.zeros((len(kx),len(ky),len(kz)))

    for i in range(0,len(kx)):
        for j in range(0,len(ky)):
            for k in range(0,len(kz)):

                kmod[i,j,k] = np.sqrt(kx[i]**2+ky[j]**2+kz[k]**2)

    kmin = np.amin(kmod)
    kmax = np.amax(kmod)

    kr = np.linspace(0,size/2,int(size/2+1))*dk

    #dV = (kx[1]-kx[0])*(ky[1]-ky[0])*(kz[1]-kz[0])
    dV = dk**3
    krmid = np.zeros(len(kr)-1)

    for x in range(0,len(krmid)):
        krmid[x] = (kr[x]+kr[x+1])/2

    krmid0 = krmid[0]/2
    krmidfin = krmid[-1]+krmid0

    fmean = np.zeros(len(kr))

    for x in range(0,len(kr)):
        integrand = 0
        V = 0
        integrandk = 0
        print('Element radial array:  ', x)
        print('Volume_0 = ', V)
        for i in range(0,len(kx)):
            for j in range(0,len(ky)):
                for l in range(0,len(kz)):
                    if x == 0:
                        if kmod[i,j,l] < krmid[0]:
                           # dV = 4*np.pi*dk*krmid0**2
                           # integrand += ftilde[i,j,l]**krmid0**2*dV/2
                            integrand += ftilde[i,j,l]*dV
                            V += dV
                    elif x == (len(kr)-1):
                        if kmod[i,j,l] >= krmid[-1] and kmod[i,j,k] < kr[-1]:
                           # dV = 4*np.pi*dk*krmidfin**2
                           # integrand += ftilde[i,j,l]*krmidfin**2*dV/2
                            integrand += ftilde[i,j,l]*dV
                            V += dV
                    else:
                        if kmod[i,j,l] >= krmid[x-1] and kmod[i,j,l] < krmid[x]:
                           # dV = 4*np.pi*dk*kr[x]**2
                           # integrand += ftilde[i,j,l]*kr[x]**2*dV
                            integrand += ftilde[i,j,l]*dV
                            V += dV

        if x == 0:
            vtot = 4*np.pi/3*((krmid0+dk/4)**3-(krmid0-dk/4)**3)/8
        elif x == (len(kr)-1):
            vtot = 4*np.pi/3*((krmidfin+dk/4)**3-(krmidfin-dk/4)**3)/8
        else:
          vtot = 4*np.pi/3*((kr[x]+dk/2)**3-(kr[x]-dk/2)**3)/8  

        print('ratio   = ', vtot/V)
        print('Volume = ', V)
        if V == 0:
            fmean[x] = 0
        else:
            fmean[x] = integrand*vtot/V

        #spectra = L**3*4*np.pi/((2*np.pi)**3*size**6)*fmean
    spectra = fmean

    return kr, spectra


def main():
    b0x = sys.argv[1]
    length = 512
    L = 1

    t = np.array([4,10,15,20,30,40])

    path = "/storage/scratch/lv43/lv43072/minit/KHI/Carrasco--"+str(length)+"--Std--bx"+b0x+"--rndm-1/"
    direc = path+"data/"
    save_path = path+"results/energy_spectra"

    try:
        os.makedirs(save_path)
    except OSError as error:
        print(error)

    for tt in t:
        arx = format(tt*10, "08")
        listvar = readh5files(direc+'h'+str(arx)+'.h5')
        x = listvar[1]
        y = listvar[2]
        z = listvar[3]
        Bener = np.array([listvar[4],listvar[5],listvar[6]])
        vener = np.array([listvar[7],listvar[8],listvar[9]])
        rho = np.array(listvar[10])

        kx,ky,kz,bxft = fourier_transf(x,y,z,Bener[0])
        kx,ky,kz,byft = fourier_transf(x,y,z,Bener[1])
        kx,ky,kz,bzft = fourier_transf(x,y,z,Bener[2])
        emag_ft = 0.5*(bxft+byft+bzft)

        kmean,emag_mean = averaging(kx,ky,kz,emag_ft,length)

        arxiu = format(tt,"02")
        hf = h5py.File(save_path+'/energy_spectra--b0x'+b0x+'--'+str(length)+'t-'+str(arxiu)+'.h5','w')
        hf.create_dataset('time',data=tt)
        hf.create_dataset('emag', data=emag_mean)
        hf.create_dataset('k', data = kmean)

        kx,ky,kz,ekinxft = fourier_transf(x,y,z,np.sqrt(rho)*vener[0])
        kx,ky,kz,ekinyft = fourier_transf(x,y,z,np.sqrt(rho)*vener[1])
        kx,ky,kz,ekinzft = fourier_transf(x,y,z,np.sqrt(rho)*vener[2])
        ekin_ft = 0.5*(ekinxft+ekinyft+ekinzft)

        kmean,ekin_mean = averaging(kx,ky,kz,ekin_ft,length)
        hf.create_dataset('ekin', data = ekin_mean)

        hf.close()


    return()

def main_CVP():

    b0x = sys.argv[1]
    length = 256
    L = 1

    t = np.array([20])
    
    path = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/DATA/KHI/res_"+str(length)+"_"+str(length)+"_"+str(length)+"/CVP--bx"+b0x+"--"+str(length)+"--rndom/"
    direc = path+"data/"
    save_path = path+"/energy_spectra/new/"

    try:
        os.makedirs(save_path)
    except OSError as error:
        print(error)

    for tt in t:
        arx = format(tt*10, "08")
        listvar = readh5files(direc+'KHI-'+str(arx)+'.h5')
     #   x = listvar[1]
     #   y = listvar[2]
     #   z = listvar[3]
        Bener = np.array([listvar[0],listvar[1],listvar[2]])
        vener = np.array([listvar[3],listvar[4],listvar[5]])
        rho = np.array(listvar[6])

        kx,ky,kz,bxft = fourier_transf_CVP(length,Bener[0])
        kx,ky,kz,byft = fourier_transf_CVP(length,Bener[1])
        kx,ky,kz,bzft = fourier_transf_CVP(length,Bener[2])
        emag_ft = (bxft+byft+bzft)

        kr,spectra_mag = averaging_CVP(kx,ky,kz,emag_ft,length)

        arxiu = format(tt,"02")
        hf = h5py.File(save_path+'/CVP--energy_spectra--v2--b0x'+b0x+'--'+str(length)+'t-'+str(arxiu)+'.h5','w')
        hf.create_dataset('time',data=tt)
        hf.create_dataset('emag', data=spectra_mag)
        hf.create_dataset('k', data = kr)

        kx,ky,kz,ekinxft = fourier_transf_CVP(length,np.sqrt(rho)*vener[0])
        kx,ky,kz,ekinyft = fourier_transf_CVP(length,np.sqrt(rho)*vener[1])
        kx,ky,kz,ekinzft = fourier_transf_CVP(length,np.sqrt(rho)*vener[2])
        ekin_ft = (ekinxft+ekinyft+ekinzft)

        kr,spectra_kin = averaging_CVP(kx,ky,kz,ekin_ft,length)
        hf.create_dataset('ekin', data = spectra_kin)

        hf.close()


    return()

main_CVP()
