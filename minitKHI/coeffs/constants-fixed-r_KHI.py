#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 28 18:05:04 2021

@author: miquelmiravet

PABLO'S MODEL TESTING
"""
#############################################

# Let's model the stress tensors Mij, Rij and Tij as functions of the total energy density.
# First approach: Consider that the tensors are proportional to the energy density.

#############################################

import os
import numpy as np
import h5py
import csv
from statistics import stdev

def readh5files(arxiv):

    print('DATA FROM FILE:      ',arxiv)

    with h5py.File(arxiv, "r") as f:
    # List all groups
        print("Keys: %s" % f.keys())
        a_group_key = list(f.keys())
        for i in a_group_key:
            print(i)
        Fmean = f[a_group_key[0]][...]
        print('F stress shape    :', Fmean.shape)
        Fnew = np.array([Fmean[0,1],Fmean[0,2],Fmean[1,2]])
        Mmean = f[a_group_key[1]][...]
        print('M stress shape    :', Mmean.shape)
        Mnew = np.array([Mmean[0,0],Mmean[0,1], Mmean[0,2], Mmean[1,1], Mmean[1,2], Mmean[2,2]])
        Rmean = f[a_group_key[2]][...]
        print('R stress shape    :', Rmean.shape)
        Rnew = np.array([Rmean[0,0],Rmean[0,1], Rmean[0,2], Rmean[1,1], Rmean[1,2], Rmean[2,2]])
        rhomean = f[a_group_key[3]][...]
        print('filtered rho shape    :', rhomean.shape)
        sigmaF = f[a_group_key[4]][...]
        print('sigma F shape    :', sigmaF.shape)
        sFnew = np.array([sigmaF[0,1], sigmaF[0,2], sigmaF[1,2]])
        sigmaM = f[a_group_key[5]][...]
        print('sigma M shape    :', sigmaM.shape)
        sMnew = np.array([sigmaM[0,0],sigmaM[0,1],sigmaM[0,2],sigmaM[1,1],sigmaM[1,2],sigmaM[2,2]])
        sigmaR = f[a_group_key[6]][...]
        print('sigma R shape    :', sigmaR.shape)
        sRnew = np.array([sigmaR[0,0],sigmaR[0,1],sigmaR[0,2],sigmaR[1,1],sigmaR[1,2],sigmaR[2,2]])
        sigma_rho= f[a_group_key[7]][...]
        print('sigma rho shape    :', sigma_rho.shape)
        time = f[a_group_key[8]][...]
        print('time shape    :', time.shape)
        f.close()
    print('Time           =',time,' ms')

    return time, Mnew, Rnew, Fnew, sMnew, sRnew, sFnew, rhomean, sigma_rho

def energy_dens(Rtensor,rho):

    traceR = Rtensor[0]+Rtensor[3]+Rtensor[5]
    energyR = 0.5*rho*traceR

    return energyR

def error_propagation(rho,err_rho,M,err_M,R,err_R,F,err_F,lent):

    traceR = R[0]+R[3]+R[5]
    e_alphaR = np.zeros((6,lent))
    e_betaR = np.zeros((6,lent))
    e_gammaR = np.zeros((3,lent))

    for i in range(0,6):
        e_alphaR[i,:] = (err_M[i,:]/(0.5*rho[:]*traceR[:])**2+(M[i,:]/(0.5*traceR[:]*rho[:]**2))**2*err_rho[:]+(M[i,:]/(0.5*rho[:]*traceR[:]**2))**2*(err_R[0,:]+err_R[3,:]+err_R[5,:]))**(0.5)
        if i == 1 or i == 2 or i == 4:
            e_betaR[i,:] = (err_R[i,:]/(0.5*traceR[:])**2+(R[i,:]/(0.5*traceR[:]**2))**2*(err_R[0,:]+err_R[3,:]+err_R[5,:]))**(0.5)
        elif i == 0:
            e_betaR[i,:] = (err_R[i,:]*(1/0.5*(1/traceR[:]-R[i,:]/traceR[:]**2))**2+(R[i,:]/(0.5*traceR[:]))**2*(err_R[3,:]+err_R[5,:]))**(0.5)
        elif i == 3:
            e_betaR[i,:] = (err_R[i,:]*(1/0.5*(1/traceR[:]-R[i,:]/traceR[:]**2))**2+(R[i,:]/(0.5*traceR[:]))**2*(err_R[0,:]+err_R[5,:]))**(0.5)
        elif i == 5:
            e_betaR[i,:] = (err_R[i,:]*(1/0.5*(1/traceR[:]-R[i,:]/traceR[:]**2))**2+(R[i,:]/(0.5*traceR[:]))**2*(err_R[3,:]+err_R[0,:]))**(0.5)
    for j in range(0,3):
        e_gammaR[j] = (err_F[j,:]/(0.5*rho[:]**(0.5)*traceR[:])**2+(F[j,:]/(0.25*rho[:]**(-1.5)*traceR[:]))**2*err_rho[:]+(F[j,:]/(0.5*rho[:]**(0.5)*traceR[:]**2))**2*(err_R[0,:]+err_R[3,:]+err_R[5,:]))**(0.5)

    return e_alphaR, e_betaR, e_gammaR

def constants_files_6comp(t,constant,econstant,constant_av,econstant_av,arxiu):

    with open(arxiu+'_r0.csv', 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        cols = zip(t,constant[0,:],constant[1,:],constant[2,:],constant[3,:],constant[4,:],constant[5,:])
        writer.writerows(cols)

    with open('sigma_'+arxiu+'_r0.csv', 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        cols = zip(t,econstant[0,:],econstant[1,:],econstant[2,:],econstant[3,:],econstant[4,:],econstant[5,:])
        writer.writerows(cols)

    with open(arxiu+'_r0_av.txt','w') as f:
        f.write('%.10f\t%.10f\t%.10f\t%.10f\t%.10f\t%.10f\n'%(constant_av[0],constant_av[1],constant_av[2],constant_av[3],constant_av[4],constant_av[5]))

    with open('sigma_'+arxiu+'_r0_av.txt','w') as f:
        f.write('%.10f\t%.10f\t%.10f\t%.10f\t%.10f\t%.10f\n'%(econstant_av[0],econstant_av[1],econstant_av[2],econstant_av[3],econstant_av[4],econstant_av[5]))

    return()

def constants_files_F9(t,constant,econstant,constant_av,econstant_av,arxiu):

    with open(arxiu+'_r0.csv', 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        cols = zip(t,constant[0,0,:],constant[0,1,:],constant[0,2,:],constant[1,0,:],constant[1,1,:],constant[1,2,:],constant[2,0,:],constant[2,1,:],constant[2,2,:])
        writer.writerows(cols)

    with open('sigma_'+arxiu+'_r0.csv', 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        cols = zip(t,econstant[0,0,:],econstant[0,1,:],econstant[0,2,:],econstant[1,0,:],econstant[1,1,:],econstant[1,2,:],econstant[2,0,:],econstant[2,1,:],econstant[2,2,:])
        writer.writerows(cols)

    with open(arxiu+'_r0_av.txt','w') as f:
        f.write('%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n'%(constant_av[0,0],constant_av[0,1],constant_av[0,2],constant_av[1,0],constant_av[1,1],constant_av[1,2],constant_av[2,0],constant_av[2,1],constant_av[2,2]))

    with open('sigma_'+arxiu+'_r0_av.txt','w') as f:
        f.write('%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n'%(econstant_av[0,0],econstant_av[0,1],econstant_av[0,2],econstant_av[1,0],econstant_av[1,1],econstant_av[1,2],econstant_av[2,0],econstant_av[2,1],econstant_av[2,2]))

    return()

def constants_files_3comp(t,constant,econstant,constant_av,econstant_av,arxiu):

    with open(arxiu+'_r0.csv', 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        cols = zip(t,constant[0,:],constant[1,:],constant[2,:])
        writer.writerows(cols)

    with open('sigma_'+arxiu+'_r0.csv', 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        cols = zip(t,econstant[0,:],econstant[1,:],econstant[2,:])
        writer.writerows(cols)

    with open(arxiu+'_r0_av.txt','w') as f:
        f.write('%.10f\t%.10f\t%.10f\n'%(constant_av[0],constant_av[1],constant_av[2]))

    with open('sigma_'+arxiu+'_r0_av.txt','w') as f:
        f.write('%.10f\t%.10f\t%.10f\n'%(econstant_av[0],econstant_av[1],econstant_av[2]))
    return()

def energy_files(t,energy_dens,filename):

    with open(filename+'_KHI.csv', 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        cols = zip(t,energy_dens[:])
        writer.writerows(cols)

    return()

#%%

def main():

    ind_ini = 100
    told = np.linspace(0,40,201)
    t = told[ind_ini:]


    #stress tensors:
    path = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/DATA/KHI/res_512_512_512/CVP--bx1e-3--512--rndom/stress_tensors_global/'

    F = np.zeros((3,len(told)))
    M = np.zeros((6,len(told)))
    R = np.zeros((6,len(told)))
    sigmaF = np.zeros((3,len(told)))
    sigmaM = np.zeros((6,len(told)))
    sigmaR = np.zeros((6,len(told)))
    rho = np.zeros(len(told))
    sigmarho = np.zeros(len(told))

    for x,tt in enumerate(told):
        arx = format(int(10*tt), "04")
        files = 'stresses_sigma_r0_KHI-'+str(arx)+'.h5'

        listvar = readh5files(path+files)

        M[:,x] = listvar[1]
        R[:,x] = listvar[2]
        F[:,x] = listvar[3]
        sigmaM[:,x] = listvar[4]
        sigmaR[:,x] = listvar[5]
        sigmaF[:,x] = listvar[6]
        rho[x] = listvar[7]
        sigmarho[x] = listvar[8]

    Fsat = F[:,ind_ini:]
    Msat = M[:,ind_ini:]
    Rsat = R[:,ind_ini:]
    sigmaFsat = sigmaF[:,ind_ini:]
    sigmaMsat = sigmaM[:,ind_ini:]
    sigmaRsat = sigmaR[:,ind_ini:]
    rhosat = rho[ind_ini:]
    sigmarhosat = sigmarho[ind_ini:]

    energyR = energy_dens(R[:,1:],rho[1:])
    energyRsat = energy_dens(Rsat,rhosat)

    #constants:

    alphaR = M[:,1:]/energyR
    betaR = rho[1:]*R[:,1:]/energyR
    gammaR = (rho[1:])**(0.5)*F[:,1:]/energyR
    alphaRsat = Msat[:]/energyRsat
    betaRsat = rhosat*Rsat[:]/energyRsat
    gammaRsat = (rhosat)**(0.5)*Fsat[:]/energyRsat

    #errors:

    e_alphaR, e_betaR, e_gammaR = error_propagation(rho[1:],sigmarho[1:],M[:,1:],sigmaM[:,1:],R[:,1:],sigmaR[:,1:],F[:,1:],sigmaF[:,1:],len(told[1:]))
    e_alphaRsat, e_betaRsat, e_gammaRsat = error_propagation(rhosat,sigmarhosat,Msat,sigmaMsat,Rsat,sigmaRsat,Fsat,sigmaFsat,len(t))

    #averages:

    alpha_av = np.sum(alphaR/e_alphaR**2, axis = 1)/np.sum(1/e_alphaR**2, axis = 1)
    beta_av = np.sum(betaR/e_betaR**2, axis = 1)/np.sum(1/e_betaR**2, axis = 1)
    gamma_av = np.sum(gammaR/e_gammaR**2, axis = 1)/np.sum(1/e_gammaR**2, axis = 1)

    alpha_avsat = np.sum(alphaRsat/e_alphaRsat**2, axis = 1)/np.sum(1/e_alphaRsat**2, axis = 1)
    beta_avsat = np.sum(betaRsat/e_betaRsat**2, axis = 1)/np.sum(1/e_betaRsat**2, axis = 1)
    gamma_avsat = np.sum(gammaRsat/e_gammaRsat**2, axis = 1)/np.sum(1/e_gammaRsat**2, axis = 1)


    err_av_alpha = (1/np.sum(1/e_alphaR**2,axis = 1))**(0.5)
    err_av_beta = (1/np.sum(1/e_betaR**2,axis = 1))**(0.5)
    err_av_gamma = (1/np.sum(1/e_gammaR**2,axis = 1))**(0.5)

    err_av_alphasat = (1/np.sum(1/e_alphaRsat**2,axis = 1))**(0.5)
    err_av_betasat = (1/np.sum(1/e_betaRsat**2,axis = 1))**(0.5)
    err_av_gammasat = (1/np.sum(1/e_gammaRsat**2,axis = 1))**(0.5)

    #files:

    constants_files_6comp(told,alphaR,e_alphaR,alpha_av,err_av_alpha,'alphaR')
    constants_files_6comp(told,betaR,e_betaR,beta_av,err_av_beta,'betaR')
    constants_files_3comp(told,gammaR,e_gammaR,gamma_av,err_av_gamma,'gammaR')

    constants_files_6comp(t,alphaRsat,e_alphaRsat,alpha_avsat,err_av_alphasat,'alphaRsat')
    constants_files_6comp(t,betaRsat,e_betaRsat,beta_avsat,err_av_betasat,'betaRsat')
    constants_files_3comp(t,gammaRsat,e_gammaRsat,gamma_avsat,err_av_gammasat,'gammaRsat')

    filesdir = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/coefficients_r0/res_512_512_512/CVP--bx1e-3--512'

    try:
        os.makedirs(filesdir)
    except OSError as error:
        print(error)

    os.system('mv *.csv '+filesdir+'/.')
    os.system('mv *av.txt '+filesdir+'/.')

    energy_files(t,energyR,'energy_sim')
    energy_files(told,energyRsat,'energy_sim_sat')

    path_energy = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/energy_dens/res_512_512_512/CVP--bx1e-3--512'

    try:
        os.makedirs(path_energy)
    except OSError as error:
        print(error)

    os.system('mv energy_sim* '+path_energy+'/.')

    return()




#%%

if __name__ == "__main__":
    main()
