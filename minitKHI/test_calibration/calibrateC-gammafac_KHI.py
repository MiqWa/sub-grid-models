#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 22 14:35:16 2022

@author: miquelmiravet
"""

#####################################
#                                   #
#         MODEL TEST for KHI        #
#                                   #
#####################################

"""
We're going to test the performance of our model by computing the energy of the
stresses and comparing it to the one from our model.

"""
import os
import numpy as np
from scipy.optimize import minimize
from scipy import interpolate
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy.ctypeslib as npct
from ctypes import c_int, c_double, c_float
import h5py

#=================================================================================
array_3d_float = npct.ndpointer(dtype=np.float64, ndim=3, flags='CONTIGUOUS')
array_1d_float = npct.ndpointer(dtype=np.float64, ndim=1, flags='CONTIGUOUS')

libenergyev = npct.load_library("/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/energy_dens_ev/KHI/lib_new_gamma", ".")
libenergyev.energy_ev_khi.restype = None
libenergyev.energy_ev_khi.argtypes = [c_double, c_int,c_double,array_1d_float, array_1d_float]
#=================================================================================

def readh5files(arx):

    print('DATA FROM FILE:      ',arx)

    with h5py.File(arx, "r") as f:
    # List all groups
        print("Keys: %s" % f.keys())
        a_group_key = list(f.keys())

        Sf = np.array(f.get('S_f'))
        x = np.array(f.get('x'))
        y = np.array(f.get('y'))
        z = np.array(f.get('z'))
        xf = np.array(f.get('x_f'))
        yf = np.array(f.get('y_f'))
        zf = np.array(f.get('z_f'))
        rho = np.array(f.get('rho_mean'))
        Mxx = np.array(f.get('stress_Mxx'))
        Mxy = np.array(f.get('stress_Mxy'))
        Mxz = np.array(f.get('stress_Mxz'))
        Myy = np.array(f.get('stress_Myy'))
        Myz = np.array(f.get('stress_Myz'))
        Mzz = np.array(f.get('stress_Mzz'))
        Rxx = np.array(f.get('stress_Rxx'))
        Rxy = np.array(f.get('stress_Rxy'))
        Rxz = np.array(f.get('stress_Rxz'))
        Ryy = np.array(f.get('stress_Ryy'))
        Ryz = np.array(f.get('stress_Ryz'))
        Rzz = np.array(f.get('stress_Rzz'))
        Fxx = np.array(f.get('stress_Fxx'))
        Fxy = np.array(f.get('stress_Fxy'))
        Fxz = np.array(f.get('stress_Fxz'))
        Fyx = np.array(f.get('stress_Fyx'))
        Fyy = np.array(f.get('stress_Fyy'))
        Fyz = np.array(f.get('stress_Fyz'))
        Fzx = np.array(f.get('stress_Fzx'))
        Fzy = np.array(f.get('stress_Fzy'))
        Fzz = np.array(f.get('stress_Fzz'))

        time = np.array(f.get('time'))

        f.close()

    M = np.array([Mxx,Mxy,Mxz,Myy,Myz,Mzz])
    R = np.array([Rxx,Rxy,Rxz,Ryy,Ryz,Rzz])
    F = np.array([Fyx-Fxy,Fzx-Fxz,Fzy-Fyz])

    print('Dimensions of the grid     :',len(x)," x ",len(y)," x ",len(z))
    print('Time           :',time)

    return [M, R, F, rho, x, y, z, xf, yf, zf]

#===========================================================================================

def read_growth(arx):

    with h5py.File(arx, "r") as f:
    # List all groups
        g_khi = np.array(f.get('growth'))

        time = np.array(f.get('time'))

        f.close()

    return g_khi

#===========================================================================================

def read_rhomean(arx):

    with h5py.File(arx, "r") as f:
        rhomean = np.array(f.get('rho_mean'))
        time = np.array(f.get('time'))
        f.close()

    return rhomean

#===========================================================================================

def read_coeffs():

    path = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/coefficients_r0/coeffs-KHI-512--t-20/'

    alphablow = np.loadtxt(path+'alpha-bx--3e-4.txt')

    betablow = np.loadtxt(path+'beta-bx--3e-4.txt')

    gammablow = np.loadtxt(path+'gamma-bx--3e-4.txt')

    salphablow = np.loadtxt(path+'sigma-alpha-bx--3e-4.txt')

    sbetablow = np.loadtxt(path+'sigma-beta-bx--3e-4.txt')

    sgammablow = np.loadtxt(path+'sigma-gamma-bx--3e-4.txt')

    alphabmid = np.loadtxt(path+'alpha-bx--1e-3.txt')

    betabmid = np.loadtxt(path+'beta-bx--1e-3.txt')

    gammabmid = np.loadtxt(path+'gamma-bx--1e-3.txt')

    salphabmid = np.loadtxt(path+'sigma-alpha-bx--1e-3.txt')

    sbetabmid = np.loadtxt(path+'sigma-beta-bx--1e-3.txt')

    sgammabmid = np.loadtxt(path+'sigma-gamma-bx--1e-3.txt')


    alpha = (alphablow/salphablow+alphabmid/salphabmid)/(1/salphablow+1/salphabmid)
    beta = (betablow/sbetablow+betabmid/sbetabmid)/(1/sbetablow+1/sbetabmid)
    gamma = (gammablow/sgammablow+gammabmid/sgammabmid)/(1/sgammablow+1/sgammabmid)

    return alpha, beta, gamma

#===========================================================================================

def energy_ev(Sf,sc,t,factor_c, energy_ini,gamma,fac_gamma,rho):

    timestep = 5e-5
    tfinal = t[-1]
    lent = int(round((tfinal-t[0])/timestep))+1
    tnew = np.linspace(t[0],tfinal,lent)

    interpg = interpolate.interp1d(t, gamma*fac_gamma, kind = "cubic")
    gamma_khi = interpg(tnew)
    std_fact = factor_c/(rho**(0.5)*sc)
  #  std_fact = factor_c/(rho**(0.5)*sc)*Sf/40
 #   print(std_fact)
#    interpstd = interpolate.interp1d(t,std_fact,kind = "cubic")
#    stdev = interpstd(tnew)
#    print(stdev)
 #   print(np.amin(stdev, axis = 0))
 #   print(np.amax(stdev, axis = 0))
    
    energy_t = np.zeros(lent)
    energy_t[0] = energy_ini
    libenergyev.energy_ev_khi(std_fact,lent,timestep,gamma_khi, energy_t)

    ratio = int((lent-1)/(len(t)-1))
    energy_model = np.zeros(len(t))

    for i in range(0,len(t)-1):
        energy_model[i] = energy_t[ratio*i]

    energy_model[-1] = energy_t[-1]

    return energy_model

#===========================================================================================

def comparison_stress(sim,mod,t,ind):

    t = t[ind:]
    sim = sim[ind:]
    mod = mod[ind:]

    l2norm = np.zeros(len(t))

    for tt in range(0,len(t)):
        integrand = (sim[tt,:]-mod[tt,:])**2
        mean_har = 0.5*(1/np.sum(mod[tt,:]**2)+1/np.sum(sim[tt,:]**2))
        l2norm[tt] = np.sqrt(np.sum(integrand)*mean_har)

    l2norm_tmean = (np.sum(l2norm**2)/len(t))**(0.5)

    return l2norm_tmean

#===========================================================================================

def norml2(fac_c, fac_gamma,Sf,bx0,leng, path_res,path_data):

    t_final = 40
    lent = 201
    ind = 100
    indf = 202
    told = np.linspace(0,t_final,lent)
    t = told[ind:indf]
    lent = len(t)
    

    inivars = readh5files(path_data+'/stress_tensors_filt/Sf_'+str(Sf)+'/stresses--'+str(leng)+'--bx'+bx0+'--Sf-'+str(Sf)+'-0000.h5')

    xfilter = inivars[7]
    yfilter = inivars[8]
    zfilter = inivars[9]

    Msim = np.zeros((lent,6,len(xfilter),len(yfilter),len(zfilter)))
    Rsim = np.zeros((lent,6,len(xfilter),len(yfilter),len(zfilter)))
    Fsim = np.zeros((lent,3,len(xfilter),len(yfilter),len(zfilter)))
    rhomean = np.zeros((lent,len(xfilter),len(yfilter),len(zfilter)))
    g_khi = np.zeros((len(told),len(xfilter),len(yfilter),len(zfilter)))
    rhoevo = np.zeros((len(told),len(xfilter),len(yfilter),len(zfilter)))

    for tt in range(0, lent):

        arx = format(int(10*t[tt]), "04")
        Msim[tt], Rsim[tt], Fsim[tt], rhomean[tt], x, y, z, xfilter, yfilter, zfilter = readh5files(path_data+'/stress_tensors_filt/Sf_'+str(Sf)+'/stresses--'+str(leng)+'--bx'+bx0+'--Sf-'+str(Sf)+'-'+arx+'.h5')

    for tt in range(0,len(told)):
        arx = format(int(10*told[tt]), "04")
        g_khi[tt] = read_growth(path_data+"/grates/Sf_"+str(Sf)+'/growth--'+str(leng)+'--bx'+bx0+'--Sf-'+str(Sf)+'-'+arx+'.h5')
  #      rhoevo[tt] = read_rhomean(path_data+"/mean_vars/Sf_"+str(Sf)+'/meanvars--'+str(leng)+'--bx'+bx0+'--Sf-'+str(Sf)+'-'+arx+'.h5')

    var0 = readh5files(path_data+'/stress_tensors_filt/Sf_'+str(Sf)+'/stresses--'+str(leng)+'--bx'+bx0+'--Sf-'+str(Sf)+'-0000.h5')
    rho0 = var0[3]
    Rsim0 = var0[1]


    cellsize = x[1]-x[0]

    filtersize = Sf*cellsize
    scale = 15.8*filtersize 

    alphaKHI, betaKHI, gammaKHI = read_coeffs()

    ekhi0 = 0.5*rho0[:,:,:]*(Rsim0[0,:,:,:]+Rsim0[3,:,:,:]+Rsim0[5,:,:,:])
    ekhi  = 0.5*rhomean*(Rsim[:,0,:,:,:]+Rsim[:,3,:,:,:]+Rsim[:,5,:,:,:])

    energyKHI= np.zeros((len(told), len(xfilter), len(yfilter), len(zfilter)))

    for i in range(0,len(xfilter)):

        for j in range(0,len(yfilter)):

            for k in range(0,len(zfilter)):

                energyKHI[:,i,j,k]= energy_ev(Sf,scale,told, fac_c, ekhi0[i,j,k],g_khi[:,i,j,k],fac_gamma,rho0[i,j,k])


    Mmod = np.zeros((lent, 6, len(xfilter), len(yfilter), len(zfilter)))
    Rmod = np.zeros((lent, 6, len(xfilter), len(yfilter), len(zfilter)))
    Fmod = np.zeros((lent, 3, len(xfilter), len(yfilter), len(zfilter)))

    for i in range(0,6):
        Mmod[:,i,:,:,:] = alphaKHI[i]*energyKHI[ind:indf,:,:,:]
        Rmod[:,i,:,:,:] = betaKHI[i]*energyKHI[ind:indf,:,:,:]/rhomean[:,:,:,:]

    for i in range(0,3):
        Fmod[:,i,:,:,:] = gammaKHI[i]*energyKHI[ind:indf,:,:,:]/rhomean[:,:,:,:]**(0.5)


    l2normM = comparison_stress(Msim,Mmod,t,ind=0)
    l2normR = comparison_stress(Rsim,Rmod,t,ind=0)
    l2normF = comparison_stress(Fsim,Fmod,t,ind=0)
    l2norm_en = comparison_stress(ekhi,energyKHI[ind:indf],t,ind=0)

    print(l2norm_en)
    print(l2normF)
    print(l2normM)
    print(l2normR)

    return l2normM, l2normR, l2normF, l2norm_en

#===========================================================================================

def plot_norm(factor_carr,factor_gamma,Sf,bx0,leng, path_res,path_data):

    l2cont = np.zeros((3,len(factor_carr),len(factor_gamma)))

    for i in range(0,len(factor_carr)):
        for j in range(0,len(factor_gamma)):
            l2cont[0,i,j], l2cont[1,i,j], l2cont[2,i,j] = norml2(factor_carr[i],factor_gamma[j],Sf,bx0,leng, path_res,path_data)


    fig, ax = plt.subplots(1,3,figsize = (30,12),sharey='all')
    plt.subplots_adjust(hspace = 0.5)
    plt.subplots_adjust(wspace = 0.1)

    for i in range(0,2):
        ax[i].tick_params(axis='y',labelsize = 20)
        ax[i].tick_params(axis='x',labelsize = 20)


    l2min = np.amin(l2cont)
    l2max = np.amax(l2cont)

    colorm = ax[0].pcolor(factor_carr,factor_gamma,l2cont[0,:,:].T,vmin=l2min,vmax=l2max,cmap="viridis")
    ax[0].set_xlabel(r'$C$', fontsize = 20)
    ax[0].set_ylabel(r'$A_{\gamma}$', fontsize = 20)
    ax[0].set_title('Maxwell', fontsize = 22)

    cbar = fig.colorbar(colorm,ax = ax[0], orientation='vertical',shrink = 0.65)
    cbar.ax.tick_params(labelsize=20)
    cbar.ax.yaxis.get_offset_text().set(size=20)
    cbar.set_label(r'$L_2$', fontsize = 22)

    colorm = ax[1].pcolor(factor_carr,factor_gamma,l2cont[1,:,:].T,vmin =l2min,vmax=l2max,cmap="viridis")
    ax[1].set_xlabel(r'$C$', fontsize = 20)
    ax[1].set_ylabel(r'$A_{\gamma}$', fontsize = 20)
    ax[1].set_title('Reynolds', fontsize = 22)

    cbar = fig.colorbar(colorm,ax = ax[1], orientation='vertical',shrink = 0.65)
    cbar.ax.tick_params(labelsize=20)
    cbar.ax.yaxis.get_offset_text().set(size=20)
    cbar.set_label(r'$L_2$', fontsize = 22)

    colorm = ax[2].pcolor(factor_carr,factor_gamma,l2cont[2,:,:].T,vmin =l2min,vmax=l2max,cmap="viridis")
    ax[2].set_xlabel(r'$C$', fontsize = 20)
    ax[2].set_ylabel(r'$A_{\gamma}$', fontsize = 20)
    ax[2].set_title('Faraday', fontsize = 22)

    cbar = fig.colorbar(colorm,ax = ax[2], orientation='vertical',shrink = 0.65)
    cbar.ax.tick_params(labelsize=20)
    cbar.ax.yaxis.get_offset_text().set(size=20)
    cbar.set_label(r'$L_2$', fontsize = 22)

    plt.savefig(path_res+'/l2norm--CVP--'+str(leng)+'--bx0-'+bx0+'--Sf_'+str(Sf)+'--v2.pdf', bbox_inches = 'tight')

    return()

#===========================================================================================

def minl2(factor_carr,Sf,bx0,leng, path_res,path_data):

    l2cont = np.zeros((4,len(factor_carr)))

    for i in range(0,len(factor_carr)):
        l2cont[0,i], l2cont[1,i], l2cont[2,i], l2cont[3,i] = norml2(factor_carr[i],0.14,Sf,bx0,leng, path_res,path_data)

    l2mean = (l2cont[0]+l2cont[1])*0.5

    minl2M = np.amin(l2cont[0,:])
    minl2R = np.amin(l2cont[1,:])
    minl2en = np.amin(l2cont[3,:])

    indminc = np.where(l2cont[0,:] == minl2M)[0][0]

    copt = factor_carr[indminc]

    indexesM = [i for i in range(0,len(factor_carr)) if l2cont[0,i] > minl2M*(1.1)]

    cnomM = factor_carr[indexesM]

    cnomM = cnomM-copt
    cneg = cnomM[cnomM<0]
    cminM = np.amin(abs(cneg))
    cminM = -cminM+copt
    cpos = cnomM[cnomM>0]
    cmaxM = np.amin(cpos)
    cmaxM = cmaxM+copt

    with open(path_res+'/c_opt_interval'+str(Sf)+'_M_v2.txt', 'w') as f:
        f.write('# C OPT \t C+ \t C- \t L2min # \n')
        f.write('%.5f\t%.5f\t%.5f\t%.5f\n'%(copt,cmaxM,cminM,minl2M))

    indminc = np.where(l2cont[1,:] == minl2R)[0][0]

    copt = factor_carr[indminc]

    indexesR = [i for i in range(0,len(factor_carr)) if l2cont[1,i] > minl2R*(1.1)]

    cnomR = factor_carr[indexesR]

    cnomR = cnomR-copt
    cneg = cnomR[cnomR<0]
    cminR = np.amin(abs(cneg))
    cminR = -cminR+copt
    cpos = cnomR[cnomR>0]
    cmaxR = np.amin(cpos)
    cmaxR = cmaxR+copt

    with open(path_res+'/c_opt_interval'+str(Sf)+'_R_v2.txt', 'w') as f:
        f.write('# C OPT \t C+ \t C- \t L2min # \n')
        f.write('%.5f\t%.5f\t%.5f\t%.5f\n'%(copt,cmaxR,cminR,minl2R))


    minl2 = np.amin(l2mean[:])

    indmin = np.where(l2mean[:] == minl2)[0][0]

    copt = factor_carr[indmin]

    indexes = [i for i in range(0,len(factor_carr)) if l2mean[i] > minl2*(1.1)]

    cnom = factor_carr[indexes]

    cnom = cnom-copt
    cneg = cnom[cnom<0]
    cmin = np.amin(abs(cneg))
    cmin = -cmin+copt
    cpos = cnom[cnom>0]
    cmax = np.amin(cpos)
    cmax = cmax+copt

    with open(path_res+'/c_opt_interval'+str(Sf)+'_mean_v2.txt', 'w') as f:
        f.write('# C OPT \t C+ \t C- \t L2min # \n')
        f.write('%.5f\t%.5f\t%.5f\t%.5f\n'%(copt,cmax,cmin,minl2))

    indminc = np.where(l2cont[3,:] == minl2en)[0][0]

    copt = factor_carr[indminc]

    indexes_en = [i for i in range(0,len(factor_carr)) if l2cont[3,i] > minl2en*(1.1)]

    cnomen = factor_carr[indexes_en]

    cnomen = cnomen-copt
    cneg = cnomen[cnomen<0]
    cminen = np.amin(abs(cneg))
    cminen = -cminen+copt
    cpos = cnomen[cnomen>0]
    cmaxen = np.amin(cpos)
    cmaxen = cmaxen+copt

    with open(path_res+'/c_opt_interval'+str(Sf)+'_en_v2.txt', 'w') as f:
        f.write('# C OPT \t C+ \t C- \t L2min # \n')
        f.write('%.5f\t%.5f\t%.5f\t%.5f\n'%(copt,cmaxen,cminen,minl2en))

    return()


def plots_sf(factor_carr,bx0,leng, path_res,path_data):

    Sf = [40,48,56]
#    Sf = [70,80,90,105]

    fig, ax = plt.subplots(1,5,figsize=(40,6))

    for ss in Sf:

        l2cont = np.zeros((4,len(factor_carr)))

        for i in range(0,len(factor_carr)):
            l2cont[0,i], l2cont[1,i], l2cont[2,i], l2cont[3,i] = norml2(factor_carr[i],0.14,ss,bx0,leng, path_res,path_data)

            l2mean = (l2cont[0]+l2cont[1])*0.5

        for i in range(0,4):
            print(l2cont[i,:])

        ax[0].plot(factor_carr, l2cont[0], label= r'$S_f$ = '+str(ss))
        ax[1].plot(factor_carr, l2cont[1], label= r'$S_f$ = '+str(ss))
        ax[2].plot(factor_carr, l2cont[2], label= r'$S_f$ = '+str(ss))
        ax[3].plot(factor_carr,l2mean, label= r'$S_f$ = '+str(ss))
        ax[4].plot(factor_carr, l2cont[3], label= r'$S_f$ = '+str(ss))

    ax[0].set_xlabel('C')
    ax[1].set_xlabel('C')
    ax[2].set_xlabel('C')
    ax[3].set_xlabel('C')
    ax[4].set_xlabel('C')
    ax[0].set_ylabel(r'$L_2$')
    ax[0].set_title('MAXWELL')
    ax[1].set_title('REYNOLDS')
    ax[2].set_title('FARADAY')
    ax[3].set_title('MEAN')
    ax[4].set_title('ENERGY')
    ax[0].legend(loc='upper right')
    ax[1].legend(loc='upper right')
    ax[2].legend(loc='upper right')
    ax[3].legend(loc='upper right')
    ax[4].legend(loc='upper right')

    plt.savefig("/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/test_khi/res_512_512_512/CVP--bx"+bx0+"--"+str(leng)+"/l2vsC-"+bx0+".png")

    plt.close()

    return()

#===========================================================================================

def main():

    Sf = int(sys.argv[1])
    bx0 = sys.argv[2]

    leng = 512

    res = str(leng)+'_'+str(leng)+'_'+str(leng)

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/DATA/KHI/res_"+res+"/CVP--bx"+bx0+"--"+str(leng)+"--rndom"

    test_path = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/test_khi/res_"+res+"/CVP--bx"+bx0+"--"+str(leng)+"/Sf_"+str(Sf)

    try:
        os.makedirs(test_path)
    except OSError as error:
        print(error)

    # plot :

    c = np.linspace(1,20,40)
#    c2 = np.linspace(0.2,1,25)
    #plot_norm(e0,c2, fac, Sf,bx0,leng,test_path,path_data)

#    plots_sf(c,bx0,leng,test_path,path_data)
    minl2(c, Sf,bx0,leng,test_path,path_data)

    return()
#===========================================================================================

if __name__ == "__main__":
    main()
