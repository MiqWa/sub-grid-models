#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 19 16:21:16 2022

@author: miquelmiravet
"""

#####################################
#                                   #
#               L2 NORM             #
#                                   #
#####################################

"""
We're going to perform the L2 norm test.

"""
import os
import numpy as np
from scipy.optimize import minimize
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy.ctypeslib as npct
from ctypes import c_int, c_double
import h5py
from scipy.stats import pearsonr
from scipy.linalg import norm
from statistics import stdev
from scipy import interpolate

#=================================================================================
array_3d_float = npct.ndpointer(dtype=np.float64, ndim=3, flags='CONTIGUOUS')
array_1d_float = npct.ndpointer(dtype=np.float64, ndim=1, flags='CONTIGUOUS')

libenergyev = npct.load_library("/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/energy_dens_ev/KHI/lib_new_gamma", ".")
libenergyev.energy_ev_khi.restype = None
libenergyev.energy_ev_khi.argtypes = [c_double, c_int,c_double,array_1d_float, array_1d_float]
#=================================================================================
def readh5files(arx):

    print('DATA FROM FILE:      ',arx)

    with h5py.File(arx, "r") as f:
    # List all groups
        print("Keys: %s" % f.keys())
        a_group_key = list(f.keys())

        Sf = np.array(f.get('S_f'))
        x = np.array(f.get('x'))
        y = np.array(f.get('y'))
        z = np.array(f.get('z'))
        xf = np.array(f.get('x_f'))
        yf = np.array(f.get('y_f'))
        zf = np.array(f.get('z_f'))
        rho = np.array(f.get('rho_mean'))
        Mxx = np.array(f.get('stress_Mxx'))
        Mxy = np.array(f.get('stress_Mxy'))
        Mxz = np.array(f.get('stress_Mxz'))
        Myy = np.array(f.get('stress_Myy'))
        Myz = np.array(f.get('stress_Myz'))
        Mzz = np.array(f.get('stress_Mzz'))
        Rxx = np.array(f.get('stress_Rxx'))
        Rxy = np.array(f.get('stress_Rxy'))
        Rxz = np.array(f.get('stress_Rxz'))
        Ryy = np.array(f.get('stress_Ryy'))
        Ryz = np.array(f.get('stress_Ryz'))
        Rzz = np.array(f.get('stress_Rzz'))
        Fxx = np.array(f.get('stress_Fxx'))
        Fxy = np.array(f.get('stress_Fxy'))
        Fxz = np.array(f.get('stress_Fxz'))
        Fyx = np.array(f.get('stress_Fyx'))
        Fyy = np.array(f.get('stress_Fyy'))
        Fyz = np.array(f.get('stress_Fyz'))
        Fzx = np.array(f.get('stress_Fzx'))
        Fzy = np.array(f.get('stress_Fzy'))
        Fzz = np.array(f.get('stress_Fzz'))

        time = np.array(f.get('time'))

        f.close()

    M = np.array([Mxx,Mxy,Mxz,Myy,Myz,Mzz])
    R = np.array([Rxx,Rxy,Rxz,Ryy,Ryz,Rzz])
    F = np.array([Fyx-Fxy,Fzx-Fxz,Fzy-Fyz])

    print('Dimensions of the grid     :',len(x)," x ",len(y)," x ",len(z))
    print('Time           :',time)

    return [M, R, F, rho, x, y, z, xf, yf, zf]


#===========================================================================================

def read_coeffs():

    path = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/coefficients_r0/coeffs-KHI-512--t-20/'

    alphablow = np.loadtxt(path+'alpha-bx--3e-4.txt')

    betablow = np.loadtxt(path+'beta-bx--3e-4.txt')

    gammablow = np.loadtxt(path+'gamma-bx--3e-4.txt')

    salphablow = np.loadtxt(path+'sigma-alpha-bx--3e-4.txt')

    sbetablow = np.loadtxt(path+'sigma-beta-bx--3e-4.txt')

    sgammablow = np.loadtxt(path+'sigma-gamma-bx--3e-4.txt')

    alphabmid = np.loadtxt(path+'alpha-bx--1e-3.txt')

    betabmid = np.loadtxt(path+'beta-bx--1e-3.txt')

    gammabmid = np.loadtxt(path+'gamma-bx--1e-3.txt')

    salphabmid = np.loadtxt(path+'sigma-alpha-bx--1e-3.txt')

    sbetabmid = np.loadtxt(path+'sigma-beta-bx--1e-3.txt')

    sgammabmid = np.loadtxt(path+'sigma-gamma-bx--1e-3.txt')


    alpha = (alphablow/salphablow+alphabmid/salphabmid)/(1/salphablow+1/salphabmid)
    beta = (betablow/sbetablow+betabmid/sbetabmid)/(1/sbetablow+1/sbetabmid)
    gamma = (gammablow/sgammablow+gammabmid/sgammabmid)/(1/sgammablow+1/sgammabmid)

    return alpha, beta, gamma
#===========================================================================================

def read_growth(arx):

    with h5py.File(arx, "r") as f:
    # List all groups
        g_khi = np.array(f.get('growth'))

        time = np.array(f.get('time'))

        f.close()

    print('Time           :',time)

    return g_khi

#===========================================================================================

def energy_ev(sc,t,factor_c, energy_ini,gamma,fac_gamma,rho):

    timestep= 5e-5
    tfinal = t[-1]
    lent = int(round((tfinal-t[0])/timestep))+1
    tnew = np.linspace(t[0],tfinal,lent)

    interpg = interpolate.interp1d(t, gamma*fac_gamma, kind = "cubic")
    gamma_khi = interpg(tnew)
    std_fact = factor_c/(rho**(0.5)*sc)

    energy_t = np.zeros(lent)
    energy_t[0] = energy_ini
    libenergyev.energy_ev_khi(std_fact,lent,timestep,gamma_khi, energy_t)

    ratio = int((lent-1)/(len(t)-1))
    energy_model = np.zeros(len(t))

    for i in range(0,len(t)-1):
        energy_model[i] = energy_t[ratio*i]

    energy_model[-1] = energy_t[-1]

    return energy_model

#===========================================================================================

def stresses(fac_c, fac_gamma,Sf,bx0,leng, path_res,path_data,told):

    ind = 100
    t = told[ind:]

    lent = len(t)

    inivars = readh5files(path_data+'/stress_tensors_filt/Sf_'+str(Sf)+'/stresses--'+str(leng)+'--bx'+bx0+'--Sf-'+str(Sf)+'-0200.h5')

    xfilter = inivars[7]
    yfilter = inivars[8]
    zfilter = inivars[9]

    Msim = np.zeros((lent,6,len(xfilter),len(yfilter),len(zfilter)))
    Rsim = np.zeros((lent,6,len(xfilter),len(yfilter),len(zfilter)))
    Fsim = np.zeros((lent,3,len(xfilter),len(yfilter),len(zfilter)))
    rhomean = np.zeros((lent,len(xfilter),len(yfilter),len(zfilter)))
    g_khi = np.zeros((len(told),len(xfilter),len(yfilter),len(zfilter)))

    for tt in range(0, lent):

        arx = format(int(10*t[tt]), "04")
        Msim[tt], Rsim[tt], Fsim[tt], rhomean[tt], x, y, z, xfilter, yfilter, zfilter = readh5files(path_data+'/stress_tensors_filt/Sf_'+str(Sf)+'/stresses--'+str(leng)+'--bx'+bx0+'--Sf-'+str(Sf)+'-'+arx+'.h5')

    for tt in range(0,len(told)):
        arx = format(int(10*told[tt]), "04")
        g_khi[tt] = read_growth(path_data+"/grates/Sf_"+str(Sf)+'/growth--'+str(leng)+'--bx'+bx0+'--Sf-'+str(Sf)+'-'+arx+'.h5')

    var0 = readh5files(path_data+'/stress_tensors_filt/Sf_'+str(Sf)+'/stresses--'+str(leng)+'--bx'+bx0+'--Sf-'+str(Sf)+'-0000.h5')
    rho0 = var0[3]
    Rsim0 = var0[1]

    cellsize = x[1]-x[0]

    filtersize = Sf*cellsize
    lambda_khi = 0.17
    scale = 15.8*filtersize

    alphaKHI, betaKHI, gammaKHI = read_coeffs()

    ekhi0 = 0.5*rho0[:,:,:]*(Rsim0[0,:,:,:]+Rsim0[3,:,:,:]+Rsim0[5,:,:,:])

    energyKHI= np.zeros((len(told), len(xfilter), len(yfilter), len(zfilter)))
    print(len(told))

    for i in range(0,len(xfilter)):

        for j in range(0,len(yfilter)):

            for k in range(0,len(zfilter)):

                energyKHI[:,i,j,k]= energy_ev(scale,told, fac_c, ekhi0[i,j,k],g_khi[:,i,j,k],fac_gamma,rho0[i,j,k])


    Mmod = np.zeros((lent, 6, len(xfilter), len(yfilter), len(zfilter)))
    Rmod = np.zeros((lent, 6, len(xfilter), len(yfilter), len(zfilter)))
    Fmod = np.zeros((lent, 3, len(xfilter), len(yfilter), len(zfilter)))

    for i in range(0,6):
        Mmod[:,i,:,:,:] = alphaKHI[i]*energyKHI[ind:,:,:,:]
        Rmod[:,i,:,:,:] = betaKHI[i]*energyKHI[ind:,:,:,:]/rhomean[ind:,:,:,:]

    for j in range(0,3):
        Fmod[:,j,:,:,:] = gammaKHI[j]*energyKHI[ind:,:,:,:]/rhomean[ind:,:,:,:]**(0.5)

    return Msim, Rsim, Fsim, Mmod, Rmod, Fmod, energyKHI, xfilter, yfilter, zfilter, t

#===========================================================================================

def l2_log(Msim, Rsim , Fsim, Mmod, Rmod, Fmod,t,path,facg):

    ind0 = 0

    t = t[ind0:]

    Msimt = Msim[ind0:]
    Mmodt = Mmod[ind0:]
    Rsimt = Rsim[ind0:]
    Rmodt = Rmod[ind0:]
    Fsimt = Fsim[ind0:]
    Fmodt = Fmod[ind0:]

    l2_M_t_comp = np.zeros((len(t),6))
    l2_R_t_comp = np.zeros((len(t),6))
    l2_F_t_comp = np.zeros((len(t),3))

    l2_M_t = np.zeros(len(t))
    l2_R_t = np.zeros(len(t))
    l2_F_t = np.zeros(len(t))

    for tt in range(0,len(t)):
        integrandM = (Msimt[tt,:]-Mmodt[tt,:])**2
        integrandR = (Rsimt[tt,:]-Rmodt[tt,:])**2
        integrandF = (Fsimt[tt,:]-Fmodt[tt,:])**2
        mean_harF = 0.5*(1/np.sum(Fmodt[tt,:]**2)+1/np.sum(Fsimt[tt,:]**2))
        mean_harM = 0.5*(1/np.sum(Mmodt[tt,:]**2)+1/np.sum(Msimt[tt,:]**2))
        mean_harR = 0.5*(1/np.sum(Rmodt[tt,:]**2)+1/np.sum(Rsimt[tt,:]**2))

        l2_M_t[tt] = np.sqrt(np.sum(integrandM)*mean_harM)
        l2_R_t[tt] = np.sqrt(np.sum(integrandR)*mean_harR)
        l2_F_t[tt] = np.sqrt(np.sum(integrandF)*mean_harF)

    l2Mv2 = (np.sum(l2_M_t[:]**2)/len(t))**(0.5)
    l2Rv2 = (np.sum(l2_R_t[:]**2)/len(t))**(0.5)
    l2Fv2 = (np.sum(l2_F_t[:]**2)/len(t))**(0.5)

    for tt in range(0,len(t)):

        for cc in range(0,6):
            integrandM = (Msimt[tt,cc,:]-Mmodt[tt,cc,:])**2
            integrandR = (Rsimt[tt,cc,:]-Rmodt[tt,cc,:])**2
            mean_harM = 0.5*(1/np.sum(Mmodt[tt,cc,:]**2)+1/np.sum(Msimt[tt,cc,:]**2))
            mean_harR = 0.5*(1/np.sum(Rmodt[tt,cc,:]**2)+1/np.sum(Rsimt[tt,cc,:]**2))

            l2_M_t_comp[tt,cc] = np.sqrt(np.sum(integrandM)*mean_harM)
            l2_R_t_comp[tt,cc] = np.sqrt(np.sum(integrandR)*mean_harR)

        for cc in range(0,3):
            integrandF = (Fsimt[tt,cc,:]-Fmodt[tt,cc,:])**2
            mean_harF = 0.5*(1/np.sum(Fmodt[tt,cc,:]**2)+1/np.sum(Fsimt[tt,cc,:]**2))
            l2_F_t_comp[tt,cc] = np.sqrt(np.sum(integrandF)*mean_harF)

    l2M_t_mean = (np.sum(l2_M_t_comp[:,:]**2, axis = 0)/len(t))**(0.5)
    l2R_t_mean = (np.sum(l2_R_t_comp[:,:]**2, axis = 0)/len(t))**(0.5)
    l2F_t_mean = (np.sum(l2_F_t_comp[:,:]**2, axis = 0)/len(t))**(0.5)


    with open(path+'/l2_time_ev_minit.txt', 'w') as f:
        f.write('# MAXWELL \t REYNOLDS \t FARADAY #\n')
        for x in range(0,len(t)):
            f.write('%f\t%f\t%f\t%f\n'%(t[x],l2_M_t[x], l2_R_t[x], l2_F_t[x]))

    with open(path+'/l2_minit.txt', 'w') as f:
        f.write('# MAXWELL \t REYNOLDS \t FARADAY #\n')
        f.write('%f\t%f\t%f\n'%(l2Mv2, l2Rv2, l2Fv2))

    with open(path+'/l2M_comp_time_ev.txt', 'w') as f:
        f.write('# M_xx \t M_xy \t M_xz \t M_yy \t M_yz \t M_zz #\n')
        for x in range(0,len(t)):
            f.write('%f\t%f\t%f\t%f\t%f\t%f\t%f\n'%(t[x],l2_M_t_comp[x,0], l2_M_t_comp[x,1], l2_M_t_comp[x,2], l2_M_t_comp[x,3], l2_M_t_comp[x,4], l2_M_t_comp[x,5]))

    with open(path+'/l2R_comp_time_ev.txt', 'w') as f:
        f.write('# R_xx \t R_xy \t R_xz \t R_yy \t R_yz \t R_zz  #\n')
        for x in range(0,len(t)):
            f.write('%f\t%f\t%f\t%f\t%f\t%f\t%f\n'%(t[x],l2_R_t_comp[x,0], l2_R_t_comp[x,1], l2_R_t_comp[x,2],l2_R_t_comp[x,3], l2_R_t_comp[x,4], l2_R_t_comp[x,5]))

    with open(path+'/l2F_comp_time_ev.txt', 'w') as f:
        f.write('# F_xy \t F_xz \t F_yz #\n')
        for x in range(0,len(t)):
            f.write('%f\t%f\t%f\t%f\n'%(t[x],l2_F_t_comp[x,0], l2_F_t_comp[x,1], l2_F_t_comp[x,2]))

    return()

#===========================================================================================

def l2_log_phase(Msim, Rsim , Fsim, Mmod, Rmod, Fmod,t,path,phase):

    ind0 = 10
    indsat = 21
    difearly = int(indsat-ind0)
    diflate = int(difearly+1)
    t = t[ind0:]

    Msimt = Msim[ind0:]
    Mmodt = Mmod[ind0:]
    Rsimt = Rsim[ind0:]
    Rmodt = Rmod[ind0:]
    Fsimt = Fsim[ind0:]
    Fmodt = Fmod[ind0:]

    if phase == 'early':
        Msimt = Msimt[:difearly]
        Rsimt = Rsimt[:difearly]
        Fsimt = Fsimt[:difearly]
        Mmodt = Mmodt[:difearly]
        Rmodt = Rmodt[:difearly]
        Fmodt = Fmodt[:difearly]
        t = t[:difearly]

    elif phase == 'late':
        Msimt = Msimt[diflate:]
        Rsimt = Rsimt[diflate:]
        Fsimt = Fsimt[diflate:]
        Mmodt = Mmodt[diflate:]
        Rmodt = Rmodt[diflate:]
        Fmodt = Fmodt[diflate:]
        t = t[diflate:]

    l2_M_t_comp = np.zeros((len(t),6))
    l2_R_t_comp = np.zeros((len(t),6))
    l2_F_t_comp = np.zeros((len(t),3))

    l2_M_t = np.zeros(len(t))
    l2_R_t = np.zeros(len(t))
    l2_F_t = np.zeros(len(t))


    for tt in range(0,len(t)):
        integrandM = (Msimt[tt,:]-Mmodt[tt,:])**2
        integrandR = (Rsimt[tt,:]-Rmodt[tt,:])**2
        integrandF = (Fsimt[tt,:]-Fmodt[tt,:])**2
        mean_harF = 0.5*(1/np.sum(Fmodt[tt,:]**2)+1/np.sum(Fsimt[tt,:]**2))
        mean_harM = 0.5*(1/np.sum(Mmodt[tt,:]**2)+1/np.sum(Msimt[tt,:]**2))
        mean_harR = 0.5*(1/np.sum(Rmodt[tt,:]**2)+1/np.sum(Rsimt[tt,:]**2))

        l2_M_t[tt] = np.sqrt(np.sum(integrandM)*mean_harM)
        l2_R_t[tt] = np.sqrt(np.sum(integrandR)*mean_harR)
        l2_F_t[tt] = np.sqrt(np.sum(integrandF)*mean_harF)

    l2Mv2 = (np.sum(l2_M_t[:]**2)/len(t))**(0.5)
    l2Rv2 = (np.sum(l2_R_t[:]**2)/len(t))**(0.5)
    l2Fv2 = (np.sum(l2_F_t[:]**2)/len(t))**(0.5)


    if phase=='early':
        with open(path+'/l2_minit_early.txt', 'w') as f:
            f.write('# MAXWELL \t REYNOLDS \t FARADAY #\n')
            f.write('%f\t%f\t%f\n'%(l2Mv2, l2Rv2, l2Fv2))
    elif phase=='late':
        with open(path+'/l2_minit_late.txt', 'w') as f:
            f.write('# MAXWELL \t REYNOLDS \t FARADAY #\n')
            f.write('%f\t%f\t%f\n'%(l2Mv2, l2Rv2, l2Fv2))

    return()

#===========================================================================================

def main():

    Sf = int(sys.argv[1])
    bx0 = sys.argv[2]
    fac_c = float(sys.argv[3])
    fac_gamma = float(sys.argv[4])

    leng = 512
    res = str(leng)+'_'+str(leng)+'_'+str(leng)


    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/DATA/KHI/res_"+res+"/CVP--bx"+bx0+"--"+str(leng)+"--rndom"

    save_path = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/final_eval_minit_khi/res_"+res+"/CVP--bx"+bx0+"--"+str(leng)+"/Sf_"+str(Sf)

    try:
        os.makedirs(save_path)
    except OSError as error:
        print(error)


    t_final = 40
    lent = 201
    t = np.linspace(0,t_final,lent)

    Msim, Rsim, Fsim, Mmod, Rmod, Fmod, energy_mod, rfilter, phif, zf, tnew = stresses(fac_c, fac_gamma,Sf,bx0,leng, save_path,path_data,t)

    l2_log(Msim, Rsim , Fsim, Mmod, Rmod, Fmod,tnew,save_path,fac_gamma)
#    l2_log_phase(Msim, Rsim , Fsim, Mmod, Rmod, Fmod,t,save_path,'early')
#    l2_log_phase(Msim, Rsim , Fsim, Mmod, Rmod, Fmod,t,save_path,'late')


    return()

main()
