#!/bin/bash -e
# #############################################################

# User Input

if [ $# != 3 ]; then
        echo "incorrect # of args"
        exit 1
fi

BFIELD=$1
C=$2
GAMMA=$3

prog="l2_ev_KHI.py"

for i in 70 80
do
        sf=${i}
        echo "python3 ${prog} ${sf} ${BFIELD} ${C} ${GAMMA}"
        python3 ${prog} ${sf} ${BFIELD} ${C} ${GAMMA}

done

