#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 19 12:07:40 2020

@author: miquelmiravet
"""

"""

COMPUTATION OF THE STRESS TENSORS

"""

import os
import numpy as np
import matplotlib.pyplot as plt
plt.style.use('seaborn-white')
import h5py
import sys

#READ DATA FUNCTIONS:

def readh5files(file,leng):

    print('DATA FROM FILE:      ',file)
    hf = h5py.File(file, 'r')

   # t = np.array(hf.get('time'))
   # t0 = np.array(hf.get('t0'))
   # tf = np.array(hf.get('tf'))
    x = np.linspace(-0.5,0.5,int(leng))
    y = np.linspace(-0.5,0.5,int(leng))
    z = np.linspace(-0.5,0.5,int(leng))
    rho = np.array(hf.get('rho'))
    vx = np.array(hf.get('vx'))
    vy = np.array(hf.get('vy'))
    vz = np.array(hf.get('vz'))
    Bx = np.array(hf.get('Bx'))
    By = np.array(hf.get('By'))
    Bz = np.array(hf.get('Bz'))

    hf.close()

    if leng == 512:
        print('change variables')
        x = x[:192]
        y = y[:192]
        z = z[:192]
        Bx = Bx[64:192,64:192,64:192]
        print('bx changed')
        By = By[64:192,64:192,64:192]
        print('by changed')
        Bz = Bz[64:192,64:192,64:192]
        print('bz changed')
        vx = vx[64:192,64:192,64:192]
        print('vx changed')
        vy = vy[64:192,64:192,64:192]
        print('vy changed')
        vz = vz[64:192,64:192,64:192]
        print('vz changed')
        rho = rho[64:192,64:192,64:192]
        print('rho changed')

    print('Dimensions of the grid     :',len(x)," x ",len(y)," x ",len(z))
   # print('Time           : ',t)

    return [x,y,z,Bx, By, Bz, vx, vy, vz,rho]

#===========================================================================================

#AVERAGING AND STRESS TENSORS FUNCTION:

def averaging(x,y,z,B,v,Bener,vener,rho):

    leng = len(x)
    dx = x[1]-x[0]
    dy = y[1]-y[0]
    dz= z[1]-z[0]
    dV = dx*dy*dz
    dVmatrix = dV*np.ones((len(x),len(y),len(z)))
    V = np.sum(dVmatrix)

    #averaging

    var_dec = [B[0],B[1],B[2],v[0],v[1],v[2],rho]
    meanvar=np.zeros(7)

    print('Averaging of fields...')

    for i in range(0,7):
        integrand = dV*var_dec[i]
        meanvar[i] = np.sum(integrand)/V

    bmean = np.array([meanvar[0],meanvar[1],meanvar[2]])
    vmean = np.array([meanvar[3],meanvar[4],meanvar[5]])
    rhomean = meanvar[6]

    #energies

    print('Energies...')

    Bmodulus = np.sqrt(Bener[0]**2+Bener[1]**2+Bener[2]**2)
    magener = 0.5*Bmodulus**2
    magenerx = 0.5*Bener[0]**2
    magenery = 0.5*Bener[1]**2
    magenerz = 0.5*Bener[2]**2

    vmodulus = np.sqrt(vener[0]**2+vener[1]**2+vener[2]**2)
    kinener = 0.5*rho*vmodulus**2
    kinenerx = 0.5*rho*vener[0]**2
    kinenery = 0.5*rho*vener[1]**2
    kinenerz = 0.5*rho*vener[2]**2

    var_ener = [magener,magenerx,magenery,magenerz,kinener,kinenerx,kinenery,kinenerz]
    meanener=np.zeros(8)

    for i in range(0,8):
        integrand = dV*var_ener[i]
        meanener[i] = np.sum(integrand)/V

    #v_x cut

    dS = dx*dz
    dSmatrix = dS*np.ones((len(x),len(z)))
    S = np.sum(dSmatrix)
    print('Velocity cut...')

    ycut = y
    vcut = vener[0]
  #  vxcut = vcut
    vxcut = np.zeros(len(ycut))
    for i in range(0,len(ycut)):
        vxcut[i] = np.sum(dS*vcut[:,i,:])/S

    #turbulent parts:
    turb = np.zeros((6,len(x),len(y),len(z)))
    for i in range(0,6):
        turb[i] = var_dec[i]-meanvar[i]

    bxturb = turb[0]
    byturb = turb[1]
    bzturb = turb[2]
    vxturb = turb[3]
    vyturb = turb[4]
    vzturb = turb[5]

    #stress tensors:

    print('Stress tensors...')

    bturb = np.array([bxturb, byturb, bzturb])
    vturb = np.array([vxturb, vyturb, vzturb])
    M = np.zeros((3,3,len(x),len(y),len(z)))
    R = np.zeros((3,3,len(x),len(y),len(z)))
    F = np.zeros((3,3,len(x),len(y),len(z)))

    for i in range(0,3):
        for j in range(0,3):
            M[i,j] = bturb[i]*bturb[j]
            R[i,j] = vturb[i]*vturb[j]
            F[i,j] = bturb[j]*vturb[i]-bturb[i]*vturb[j]


    Mmean = np.zeros((3,3))
    Rmean = np.zeros((3,3))
    Fmean = np.zeros((3,3))
    sigmaMsq = np.zeros((3,3))
    sigmaRsq = np.zeros((3,3))
    sigmaFsq = np.zeros((3,3))

    sigmarhosq = np.sum(dV*(rho[:,:]-rhomean)**2)/V

    #averaging of the turbulent stress tensors:
    for i in range(0,3):
        for j in range(0,3):
            Mmean[i,j] = np.sum(M[i,j]*dV)/V
            Rmean[i,j] = np.sum(R[i,j]*dV)/V
            Fmean[i,j] = np.sum(F[i,j]*dV)/V

            sigmaMsq[i,j] = np.sum(dV*(M[i,j,:,:]-Mmean[i,j])**2)/V
            sigmaRsq[i,j] = np.sum(dV*(R[i,j,:,:]-Rmean[i,j])**2)/V
            sigmaFsq[i,j] = np.sum(dV*(F[i,j,:,:]-Fmean[i,j])**2)/V

    print('DONE')

    return bmean, vmean, rhomean, sigmarhosq, Mmean, Rmean, Fmean, sigmaMsq, sigmaRsq, sigmaFsq, meanener, vxcut, ycut

#    return bmean, vmean, rhomean, meanener, vxcut, ycut

#Time evolution:


def time_evolution(direc,x,y, z, t,path,path_energies,leng, b0x,b0y,b0z,v0x,v0y,v0z):

    lent = len(t)

    print('TIME EVOLUTION')

    for i in range(0,lent):

        arx = format(i, "08")
        if leng == 512:
            listvar = readh5files(direc+'KHI-h'+str(arx)+'.h5',leng)
        elif leng == 128 or leng == 256:
            listvar = readh5files(direc+'KHI-'+str(arx)+'.h5',leng)

        B = np.array([listvar[3]-b0x,listvar[4]-b0y,listvar[5]-b0z])
        v = np.array([listvar[6]-v0x,listvar[7]-v0y,listvar[8]-v0z])
        rho = np.array(listvar[9])

        Bener = np.array([listvar[3],listvar[4],listvar[5]])
        vener = np.array([listvar[6],listvar[7],listvar[8]])


        #averaging:
        print('AVERAGING')

        bmean_t, vmean_t, rhomean_t, sigmarhosq_t, Mmean_t, Rmean_t, Fmean_t, sigmaMsq_t, sigmaRsq_t,sigmaFsq_t, energies, vxcut, ycut = averaging(x,y,z,B,v,Bener,vener,rho)
#        bmean_t, vmean_t, rhomean_t,energies, vxcut, ycut = averaging(x,y,z,B,v,Bener,vener,rho)
        arxiu = format(10*i,"04")
        hf = h5py.File(path+'/stresses_sigma_r0_KHI-'+str(arxiu)+'.h5', 'w')
        hf.create_dataset('time', data=t[i])
        hf.create_dataset('rhomean', data = rhomean_t)
        hf.create_dataset('sigma_rho', data = sigmarhosq_t)
        hf.create_dataset('Mmean', data = Mmean_t)
        hf.create_dataset('Rmean', data = Rmean_t)
        hf.create_dataset('Fmean', data = Fmean_t)
        hf.create_dataset('sigma_M', data = sigmaMsq_t)
        hf.create_dataset('sigma_R', data = sigmaRsq_t)
        hf.create_dataset('sigma_F', data = sigmaFsq_t)
        hf.close()

        hf2 = h5py.File(path_energies+'/energies_vxcut_KHI-'+str(arxiu)+'.h5','w')
        hf2.create_dataset('time', data=t[i])
        hf2.create_dataset('Emag', data=energies[0])
        hf2.create_dataset('Emagx', data=energies[1])
        hf2.create_dataset('Emagy', data=energies[2])
        hf2.create_dataset('Emagz', data=energies[3])
        hf2.create_dataset('Ekin', data=energies[4])
        hf2.create_dataset('Ekinx', data=energies[5])
        hf2.create_dataset('Ekiny', data=energies[6])
        hf2.create_dataset('Ekinz', data=energies[7])
        hf2.create_dataset('vxcut', data=vxcut)
        hf2.create_dataset('ycut', data=ycut)
        hf2.create_dataset('Bmean', data=bmean_t)
        hf2.create_dataset('vmean', data=vmean_t)
        hf2.create_dataset('rhomean',data=rhomean_t)
        hf2.close()
    return()

#%%

def main():


    length= int(sys.argv[1])
    b0x = '3e-2'

    if length == 128:
        resu = '128_128_128'
    elif length == 256:
        resu = '256_256_256'
    elif length == 512:
        resu = '512_512_512'

    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/DATA/KHI/"
    direc = path_data+'CVP--bx'+b0x+'--'+str(length)+'--rndom/'
    t = np.linspace(0,40,401)


    print('RESOLUTION : ',resu)

    if length == 512:
        var = readh5files(direc+'KHI-h00000000.h5',length)
    elif length == 128 or length == 256:
        var = readh5files(direc+'KHI-00000000.h5',length)

    x = var[0].astype(np.float64)
    y = var[1].astype(np.float64)
    z = var[2].astype(np.float64)

    b0x = var[3]
    b0y = var[4]
    b0z = var[5]
    v0x = var[6]
    v0y = var[7]
    v0z = var[8]

    print('INITIAL VALUES OBTAINED')

#    path = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_r0/res_'+resu+'/CVP--bx3e-2--'+str(length)+'--rndom'
#    path_energies = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/mean_quants/res_'+resu+'/CVP--bx3e-2--'+str(length)+'--rndom'

    path = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/stress_tensors_r0/res_'+resu+'/TEST--bx3e-2--'+str(length)
    path_energies = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/results/mean_quants/res_'+resu+'/TEST--bx3e-2--'+str(length)

    try:
        os.makedirs(path)
    except OSError as error:
        print(error)

    try:
         os.makedirs(path_energies)
    except OSError as error:
         print(error)

    time_evolution(direc,x, y, z, t, path, path_energies, length, b0x,b0y,b0z,v0x,v0y,v0z)

    return()


  #%%

main()
