#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 10 15:38:10 2022

@author: miquelmiravet
"""

#############################################
#                                           #
#       STRESS TENSORS EVOLUTION KHI        #
#                                           #
#############################################

"""
Calculation of the energy density of the stresses directly from the data of the
numerical simulations
"""

import os
import numpy as np
import sys
import itertools
import numpy.ctypeslib as npct
import ctypes
from ctypes import c_int, c_float,c_char_p
import h5py
import concurrent.futures

#=================================================================================
#=================================================================================
# input type for the function
# must be a double array, with single dimension that is contiguous
array_3d_float = npct.ndpointer(dtype=np.float64, ndim=3, flags='CONTIGUOUS')
array_4d_float = npct.ndpointer(dtype=np.float64, ndim=4, flags='CONTIGUOUS')
array_1d_float = npct.ndpointer(dtype=np.float64, ndim=1, flags='CONTIGUOUS')

libstresses = npct.load_library("/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/pablo-model/stresses_scripts/box-filter/libstresses", ".")
libstresses.box_kernel_cart.restype = None
libstresses.box_kernel_cart.argtypes = [c_int, c_int, c_int, array_1d_float, array_1d_float, array_1d_float, c_int, c_int, c_int, array_1d_float, array_1d_float, array_1d_float, c_float, array_3d_float, array_3d_float]
libstresses.stresses.restype = None
libstresses.stresses.argtypes = [c_int, c_int, c_int,c_int,c_int,c_int, array_4d_float,array_4d_float,array_4d_float,array_4d_float,array_3d_float, c_float, array_1d_float,
array_1d_float, array_1d_float, array_1d_float, array_1d_float, array_1d_float,array_4d_float,array_4d_float,array_4d_float,array_3d_float,array_3d_float,array_3d_float]

#===========================================================================================

def readh5files(file,leng):

    ind0 = 3
    ind1 = leng+3

    print('DATA FROM FILE:      ',file)
    hf = h5py.File(file, 'r')
    g_params = hf.get('Parameters')
    t = np.array(g_params.get('t'))
    t0 = np.array(g_params.get('t0'))
    tf = np.array(g_params.get('tf'))
    print('time     = ', t)

    g_x = hf.get('X')
    x = np.array(g_x.get('znc'))
    x = x[ind0:ind1]
    g_y = hf.get('Y')
    y = np.array(g_y.get('znc'))
    y = y[ind0:ind1]
    g_z = hf.get('Z')
    z = np.array(g_z.get('znc'))
    z = z[ind0:ind1]

    g_hydro = hf.get('hydro')
    data_hydro = np.array(g_hydro.get('data'))
    data_hydro = data_hydro[ind0:ind1,ind0:ind1,ind0:ind1,:]
    rho = data_hydro[:,:,:,0]
    U = data_hydro[:,:,:,1]
    v_x = data_hydro[:,:,:,2]/rho
    v_y = data_hydro[:,:,:,3]/rho
    v_z = data_hydro[:,:,:,4]/rho
    g_thd = hf.get('thd')
    data_thd = np.array(g_thd.get('data'))
    data_thd = data_thd[ind0:ind1,ind0:ind1,ind0:ind1,:]
    press = data_thd[:,:,:,0]
    c_s = data_thd[:,:,:,1]

    g_mag = hf.get('mag_vol')
    B = np.array(g_mag.get('data'))
    B = B[ind0:ind1,ind0:ind1,ind0:ind1,:]
    hf.close()

    Bx = B[:,:,:,0]
    By = B[:,:,:,1]
    Bz = B[:,:,:,2]

    return [t,x,y,z,Bx.T, By.T, Bz.T, v_x.T, v_y.T, v_z.T,rho.T]

#===========================================================================================

def h5test(file,leng):
    print('DATA FROM FILE:      ',file)
    hf = h5py.File(file, 'r')
    rho = np.array(hf.get('rho'))
    vx = np.array(hf.get('vx'))
    vy = np.array(hf.get('vy'))
    vz = np.array(hf.get('vz'))
    Bx = np.array(hf.get('Bx'))
    By = np.array(hf.get('By'))
    Bz = np.array(hf.get('Bz'))
    hf.close()

    x0 = np.linspace(-0.5,0.5,leng*2)
    dx = x0[1]-x0[0]
    x = np.linspace(-0.5+dx,0.5-dx,leng)
    y = np.linspace(-0.5+dx,0.5-dx,leng)
    z = np.linspace(-0.5+dx,0.5-dx,leng)
    t = 0.0

    return [t,x,y,z,Bx, By, Bz, vx, vy, vz,rho]
#===========================================================================================


def stresses_ev(items):

    i,leng,Sf,bx0,direc,filtersize,t,path_files, x,y,z,xfilter,yfilter,zfilter = items
    basename = "stresses--"+str(leng)+"--bx"+bx0+"--Sf-"+str(Sf)
    output_counter = format(int(10*t[i]), "04")

    arx = format(int(10*t[i]), "08")
    listvar = h5test(direc+'KHI-'+str(arx)+'.h5',leng)
    v = np.array([listvar[7],listvar[8],listvar[9]])
    B = np.array([listvar[4],listvar[5],listvar[6]])
    rho = listvar[10]
    rho = np.asarray(rho, order='C')

    bmean = np.zeros((3,len(xfilter),len(yfilter),len(zfilter)))
    vmean = np.zeros((3,len(xfilter),len(yfilter),len(zfilter)))
    Mmean = np.zeros((9,len(xfilter),len(yfilter),len(zfilter)))
    Rmean = np.zeros((9,len(xfilter),len(yfilter),len(zfilter)))
    Fmean = np.zeros((9,len(xfilter),len(yfilter),len(zfilter)))
    rhomean = np.zeros((len(xfilter),len(yfilter),len(zfilter)))
    aux = np.zeros((len(x),len(y),len(z)))
    aux2 = np.zeros((len(xfilter),len(yfilter),len(zfilter)))

    libstresses.stresses(len(x),len(y),len(z),len(xfilter),len(yfilter),len(zfilter),v,vmean,B,bmean,rho,filtersize, x,y,z,xfilter,yfilter,zfilter, Mmean, Rmean, Fmean, rhomean,aux,aux2)

    hf = h5py.File(path_files+'/'+basename+'-'+str(output_counter)+'.h5', 'w')
    hf.create_dataset('time', data=t[i])
    hf.create_dataset('S_f', data = Sf)
    hf.create_dataset('x', data=x)
    hf.create_dataset('y', data=y)
    hf.create_dataset('z', data=z)
    hf.create_dataset('x_f', data=xfilter)
    hf.create_dataset('y_f', data=yfilter)
    hf.create_dataset('z_f', data=zfilter)
    hf.create_dataset('stress_Mxx', data=Mmean[0])
    hf.create_dataset('stress_Mxy', data=Mmean[1])
    hf.create_dataset('stress_Mxz', data=Mmean[2])
    hf.create_dataset('stress_Myy', data=Mmean[4])
    hf.create_dataset('stress_Myz', data=Mmean[5])
    hf.create_dataset('stress_Mzz', data=Mmean[8])
    hf.create_dataset('stress_Rxx', data=Rmean[0])
    hf.create_dataset('stress_Rxy', data=Rmean[1])
    hf.create_dataset('stress_Rxz', data=Rmean[2])
    hf.create_dataset('stress_Ryy', data=Rmean[4])
    hf.create_dataset('stress_Ryz', data=Rmean[5])
    hf.create_dataset('stress_Rzz', data=Rmean[8])
    hf.create_dataset('stress_Fxy', data=Fmean[1])
    hf.create_dataset('stress_Fxz', data=Fmean[2])
    hf.create_dataset('stress_Fyz', data=Fmean[5])
    hf.create_dataset('stress_Fxx', data=Fmean[0])
    hf.create_dataset('stress_Fzz', data=Fmean[8])
    hf.create_dataset('stress_Fyy', data=Fmean[4])
    hf.create_dataset('stress_Fyx', data=Fmean[3])
    hf.create_dataset('stress_Fzx', data=Fmean[6])
    hf.create_dataset('stress_Fzy', data=Fmean[7])
    hf.create_dataset('rho_mean', data=rhomean)

    hf.close()

    return()
#===========================================================================================

def main():

    Sf = int(sys.argv[1])
    bx0 = sys.argv[2]

    length = int(sys.argv[3])


    path_data = "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/DATA/KHI/res_"+str(length)+"_"+str(length)+"_"+str(length)+"/CVP--bx"+bx0+"--"+str(length)+"--rndom/"
    direc = path_data+"data/"

    t0 = 0
    tf = 40
    t = np.linspace(t0,tf,401)
    rang = np.arange(0,202)


    var = h5test(direc+'KHI-00000000.h5',length)

    x = var[1].astype(np.float64)
    y = var[2].astype(np.float64)
    z = var[3].astype(np.float64)

    cellsize = x[1]-x[0]

    filtersize = Sf*cellsize

    a = int(len(x)/4)
    b = int(len(x)*3/4)
    n = int(128/4)
    xfilter= np.linspace(x[0],x[-1],15).astype(np.float64)
    yfilter = np.array([y[a],y[b]])
    zfilter = np.linspace(z[0],z[-1],15).astype(np.float64)

    path_files = path_data+"results/stress_tensors/Sf_"+str(Sf)

    try:
        os.makedirs(path_files)
    except OSError as error:
        print(error)

    items = ((i,length,Sf,bx0,direc,filtersize,t,path_files, x,y,z,xfilter,yfilter,zfilter) for i in rang)
    items = tuple(items)

    itera = int(len(rang)/2)

    for it in range(0,itera):

        ind0 = 2*it
        ind1 = 2*(it+1)
        inputs = items[ind0:ind1]

        with concurrent.futures.ProcessPoolExecutor(max_workers=2) as executor:
            res = executor.map(stresses_ev,inputs)

            executor.shutdown()

    return()

if __name__ == '__main__':
    main()
