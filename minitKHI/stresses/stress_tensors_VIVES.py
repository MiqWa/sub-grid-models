#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 19 12:07:40 2020

@author: miquelmiravet
"""

"""

COMPUTATION OF THE STRESS TENSORS

"""

import os
import numpy as np
import h5py
import sys
import statistics
from statistics import stdev
import concurrent.futures

#READ DATA FUNCTIONS:

def readh5files(file,leng):

    print('DATA FROM FILE:      ',file)
    hf = h5py.File(file, 'r')

    rho = np.array(hf.get('rho'))
    vx = np.array(hf.get('vx'))
    vy = np.array(hf.get('vy'))
    vz = np.array(hf.get('vz'))
    Bx = np.array(hf.get('Bx'))
    By = np.array(hf.get('By'))
    Bz = np.array(hf.get('Bz'))

    hf.close()

    return [Bx, By, Bz, vx, vy, vz,rho]
#===========================================================================================

#AVERAGING AND STRESS TENSORS FUNCTION:

def averaging(leng,B,v,rho):

    #leng = len(x)
    #dx = x[1]-x[0]
    #dy = y[1]-y[0]
    #dz= z[1]-z[0]
    #dV = dx*dy*dz
    #dVmatrix = dV*np.ones((len(x),len(y),len(z)))
    #V = np.sum(dVmatrix)

    #averaging

    var_dec = np.array([B[0],B[1],B[2],v[0],v[1],v[2],rho])
#    print(var_dec)
    print('Averaging of fields...')

    meanvar = np.mean(var_dec,axis=(1,2,3))

    bmean = np.array([meanvar[0],meanvar[1],meanvar[2]])
    vmean = np.array([meanvar[3],meanvar[4],meanvar[5]])
    rhomean = meanvar[6]
    print('vx mean = ',vmean[0])
    #energies

    print('Energies...')

    Bmodulus = np.sqrt(B[0]**2+B[1]**2+B[2]**2)
    magener = 0.5*Bmodulus**2
    magenerx = 0.5*B[0]**2
    magenery = 0.5*B[1]**2
    magenerz = 0.5*B[2]**2

    vmodulus = np.sqrt(v[0]**2+v[1]**2+v[2]**2)
    kinener = 0.5*rho*vmodulus**2
    kinenerx = 0.5*rho*v[0]**2
    kinenery = 0.5*rho*v[1]**2
    kinenerz = 0.5*rho*v[2]**2

    var_ener = np.array([magener,magenerx,magenery,magenerz,kinener,kinenerx,kinenery,kinenerz])

    meanener = np.sum(var_ener,axis=(1,2,3))/leng**3

    #v_x cut

    #dS = dx*dz
    #dSmatrix = dS*np.ones((len(x),len(z)))
    #S = np.sum(dSmatrix)
    #print('Velocity cut...')

#    ycut = y
#    vcut = v[0]

#    vxcut = np.mean(vcut,axis = (0,2))

    #turbulent parts:

    #var_dec = np.moveaxis(var_dec,0,-1)
    turb = np.zeros((7,leng,leng,leng))
    for i in range(0,7):
        turb[i,:,:,:] = var_dec[i,:,:,:]-meanvar[i]

    #stress tensors:

    print('Stress tensors...')

    bturb = np.array([turb[0], turb[1], turb[2]])
    vturb = np.array([turb[3], turb[4], turb[5]])
    M = np.zeros((3,3,leng,leng,leng))
    R = np.zeros((3,3,leng,leng,leng))
    F = np.zeros((3,3,leng,leng,leng))
#    F9 = np.zeros((3,3,leng,leng,leng))

    for i in range(0,3):
        for j in range(0,3):
            M[i,j] = bturb[i]*bturb[j]
            R[i,j] = vturb[i]*vturb[j]
            F[i,j] = bturb[j]*vturb[i]-bturb[i]*vturb[j]
#            F9[i,j] = bturb[i]*vturb[j]


    sigmarho = np.sum((rho-rhomean)**2)/(leng**3)

    #averaging of the turbulent stress tensors:

    Mmean = np.sum(M,axis = (2,3,4))/leng**3
    Rmean = np.sum(R,axis = (2,3,4))/leng**3
    Fmean = np.sum(F,axis = (2,3,4))/leng**3
#    F9mean = np.sum(F9,axis = (2,3,4))/leng**3

    Mturb = np.zeros((3,3,leng,leng,leng))
    Rturb = np.zeros((3,3,leng,leng,leng))
    Fturb = np.zeros((3,3,leng,leng,leng))
#    F9turb = np.zeros((3,3,leng,leng,leng))

#    sigmaM = np.zeros((3,3,leng,leng,leng))
#    sigmaR = np.zeros((3,3,leng,leng,leng))
#    sigmaF = np.zeros((3,3,leng,leng,leng))
#    sigmaF9 = np.zeros((3,3,leng,leng,leng))

    for i in range(0,3):
        for j in range(0,3):
            Mturb[i,j]=M[i,j,:,:,:]-Mmean[i,j]
            Fturb[i,j]=F[i,j,:,:,:]-Fmean[i,j]
#            F9turb[i,j]=F9[i,j,:,:,:]-F9mean[i,j]
            Rturb[i,j]=R[i,j,:,:,:]-Rmean[i,j]


 #           sigmaM[i,j] = stdev(M[i,j].flatten())
 #           sigmaR[i,j] = stdev(R[i,j].flatten())
 #           sigmaF[i,j] = stdev(F[i,j].flatten())
 #           sigmaF9[i,j] = stdev(F9[i,j].flatten())

    sigmaM = np.sum(Mturb**2,axis = (2,3,4))/leng**3
    sigmaR = np.sum(Rturb**2,axis = (2,3,4))/leng**3
    sigmaF = np.sum(Fturb**2,axis = (2,3,4))/leng**3
#    sigmaF9 = np.sum(F9turb**2,axis = (2,3,4))/leng**3


    print('DONE')

    return bmean, vmean, rhomean, sigmarho,  meanener, Mmean, Rmean, Fmean, sigmaM, sigmaR, sigmaF

#    return bmean, vmean, rhomean, meanener, vxcut, ycut

#Time evolution:


def time_evolution(i,t,path_data,path,path_energies,leng,b0x,b0y,b0z,v0x,v0y,v0z):

#    outM = open(path+'/maxwell_glob.dat','w')
#    outM.write('# t \t M_xx \t M_xy \t M_xz \t M_yy \t M_yz \t M_zz #\n')
#    outR = open(path+'/reynolds_glob.dat','w')
#    outR.write('# t \t R_xx \t R_xy \t R_xz \t R_yy \t R_yz \t R_zz #\n')
#    outF = open(path+'/faraday_glob.dat','w')
#    outF.write('# t \t F_xy \t F_xz \t F_yz #\n')
#    outF9 = open(path+'/faraday9_glob.dat','w')
#    outF9.write('# t \t F9_xx \t F9_xy \t F9_xz \t F9_yx \t F9_yy \t F9_yz \t F9_zx \t F9_zy \t F9_zz #\n')
#    outsigM = open(path+'/maxwell_sigma_glob.dat','w')
#    outsigM.write('# t \t M_xx \t M_xy \t M_xz \t M_yy \t M_yz \t M_zz #\n')
#    outsigR = open(path+'/reynolds_sigma_glob.dat','w')
#    outsigR.write('# t \t R_xx \t R_xy \t R_yz \t R_yy \t R_yz \t R_zz #\n')
#    outsigF = open(path+'/faraday_sigma_glob.dat','w')
#    outsigF.write('# t \t F_xy \t F_xz \t F_yz #\n')
#    outsigF9 = open(path+'/faraday9_sigma_glob.dat','w')
#    outsigF9.write('# t \t F9_xx \t F9_xy \t F9_xz \t F9_yx \t F9_yy \t F9_yz \t F9_zx \t F9_zy \t F9_zz #\n')
#    outrho = open(path+'/faraday_glob.dat','w')
#    outrho.write('# t \t rho \t sig_rho_sq #\n')
   # outEmag = open(path_energies+'/mag_ener_glob.dat','w')
   # outEmag.write('# t \t Bmean_x \t Bmean_y \t Bmean_z \t Emag \t Emag_x \t Emag_y \t Emag_z #\n')
   # outEkin = open(path_energies+'/kin_ener_glob.dat','w')
   # outEkin.write('# t \t vmean_x \t vmean_y \t vmean_z \t Ekin \t Ekin_x \t Ekin_y \t Ekin_z \t rho #\n')
   # outvcut = open(path_energies+'/vcut.dat','w')
   # outvcut.write('#t \t ycut \t vxcut #\n')

 #   i,t,path_data,path,path_energies,leng = items

    arx = format(int(i), "08")
    listvar = readh5files(path_data+'/data/KHI-'+str(arx)+'.h5',leng)

    B = [listvar[0]-b0x, listvar[1]-b0y, listvar[2]-b0z]
    B = np.array(B)
    #B = np.array(B[:,:,:,:])
    v = [listvar[3],listvar[4]-v0y,listvar[5]-v0z]
    v = np.array(v)
    #v = np.array(v[:,:,:,:])
    rho = listvar[6]
    rho = np.asarray(rho, order='C')
    rho = rho[:,:,:]
    time = t[int(i)]

    #averaging:
    print('AVERAGING')

    bmean_t, vmean_t, rhomean_t, sigmarho_t, energies, Mmean_t, Rmean_t, Fmean_t,sigmaM_t, sigmaR_t,sigmaF_t = averaging(leng,B,v,rho)

#        bmean_t, vmean_t, rhomean_t,energies, vxcut, ycut = averaging(x,y,z,B,v,Bener,vener,rho)
#        outM.write('%f \t %.7e \t %.7e \t %.7e \t %.7e \t %.7e \t %.7e \n'%(time,Mmean_t[0,0],Mmean_t[0,1],Mmean_t[0,2],Mmean_t[1,1],Mmean_t[1,2],Mmean_t[2,2]))
#        outR.write('%f \t %.7e \t %.7e \t %.7e \t %.7e \t %.7e \t %.7e \n'%(time,Rmean_t[0,0],Rmean_t[0,1],Rmean_t[0,2],Rmean_t[1,1],Rmean_t[1,2],Rmean_t[2,2]))
#        outF.write('%f \t %.7e \t %.7e \t %.7e \n'%(time,Fmean_t[0,1],Fmean_t[0,2],Fmean_t[1,2]))
#        outsigM.write('%f \t %.7e \t %.7e \t %.7e \t %.7e \t %.7e \t %.7e \n'%(time,sigmaM_t[0,0]**2,sigmaM_t[0,1]**2,sigmaM_t[0,2]**2,sigmaM_t[1,1]**2,sigmaM_t[1,2]**2,sigmaM_t[2,2]**2))
#        outsigR.write('%f \t %.7e \t %.7e \t %.7e \t %.7e \t %.7e \t %.7e \n'%(time,sigmaR_t[0,0]**2,sigmaR_t[0,1]**2,sigmaR_t[0,2]**2,sigmaR_t[1,1]**2,sigmaR_t[1,2]**2,sigmaR_t[2,2]**2))
#        outsigF.write('%f \t %.7e \t %.7e \t %.7e \n'%(time,sigmaF_t[0,1]**2,sigmaF_t[0,2]**2,sigmaF_t[1,2]**2))
#        outrho.write('%f \t %.7e \t %.7e \n'%(time,rhomean_t,sigmarho_t**2))

    arxiu = format(int(time*10),"04")
    hf = h5py.File(path+'stresses_sigma_r0_KHI-'+str(arxiu)+'.h5', 'w')
    hf.create_dataset('time', data=time)
    hf.create_dataset('rhomean', data = rhomean_t)
    hf.create_dataset('sigma_rho', data = sigmarho_t)
    hf.create_dataset('Mmean', data = Mmean_t)
    hf.create_dataset('Rmean', data = Rmean_t)
    hf.create_dataset('Fmean', data = Fmean_t)
    hf.create_dataset('F9mean', data = F9mean_t)
    hf.create_dataset('sigma_M', data = sigmaM_t)
    hf.create_dataset('sigma_R', data = sigmaR_t)
    hf.create_dataset('sigma_F', data = sigmaF_t)
    hf.create_dataset('sigma_F9', data = sigmaF9_t)

#    hf.close()

        #outEmag.write('%f \t %.7e \t %.7e \t %.7e \t %.7e \t %.7e \t %.7e \t %.7e \n'%(time,bmean_t[0],bmean_t[1],bmean_t[2],energies[0],energies[1],energies[2],energies[3]))
        #outEkin.write('%f \t %.7e \t %.7e \t %.7e \t %.7e \t %.7e \t %.7e \t %.7e \t %.7e \n'%(time,vmean_t[0],vmean_t[1],vmean_t[2],energies[4],energies[5],energies[6],energies[7],rhomean_t))
        #outvcut.write('%f \t %.7f \t %.7e \n'%(time,ycut,vxcut))

#    hf2 = h5py.File(path_energies+'energies_vxcut_KHI-'+str(arxiu)+'.h5','w')
#    hf2.create_dataset('time', data=time)
#    hf2.create_dataset('Emag', data=energies[0])
#    hf2.create_dataset('Emagx', data=energies[1])
#    hf2.create_dataset('Emagy', data=energies[2])
#    hf2.create_dataset('Emagz', data=energies[3])
#    hf2.create_dataset('Ekin', data=energies[4])
#    hf2.create_dataset('Ekinx', data=energies[5])
#    hf2.create_dataset('Ekiny', data=energies[6])
#    hf2.create_dataset('Ekinz', data=energies[7])
#    hf2.create_dataset('vxcut', data=vxcut)
#    hf2.create_dataset('ycut', data=ycut)
#    hf2.create_dataset('Bmean', data=bmean_t)
#    hf2.create_dataset('vmean', data=vmean_t)
#    hf2.create_dataset('rhomean',data=rhomean_t)
#    hf2.close()

#    outM.close()
#    outR.close()
#    outF.close()
#    outsigM.close()
#    outsigR.close()
#    outsigF.close()
#    outrho.close()
    #outEmag.close()
    #outEkin.close()
    #outvcut.close()

    return()

#%%

def main():


    length= int(sys.argv[1])
    b0x = '3e-4'

    if length == 128:
        resu = '128_128_128'
    elif length == 256:
        resu = '256_256_256'
    elif length == 512:
        resu = '512_512_512'

    path_data = '/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/DATA/KHI/res_'+resu+'/CVP--bx'+b0x+'--'+str(length)+'--rndom'

    path = path_data+'/stress_tensors_global-TEST/'
    path_energies = path_data+'/mean_quants-TEST/'

    try:
        os.makedirs(path)
    except OSError as error:
        print(error)

    try:
         os.makedirs(path_energies)
    except OSError as error:
         print(error)

    t = np.linspace(0,40,401)

    var = readh5files(path_data+'/data/KHI-00000000.h5',length)

    b0x = var[0]
    b0y = var[1]
    b0z = var[2]
    v0x = var[3]
    v0y = var[4]
    v0z = var[5]

  #  rang = np.arange(0,1)

  #  items = ((i,t,path_data,path,path_energies,length) for i in rang)
  #  items = tuple(items)

#    itera = int((rang[-1]-rang[0])/1)
   # itera = 1
#    for it in range(0,itera):

#        ind0 = it
#        ind1 = 1*(it+1)
#        inputs = items[ind0:ind1]

#        with concurrent.futures.ProcessPoolExecutor(max_workers=1) as executor:
#            res = executor.map(time_evolution,inputs)
#            print(val for val in res)
#            executor.shutdown()

    for i in range(0,201):

        time_evolution(2*i,t,path_data,path,path_energies,length,b0x,b0y,b0z,v0x,v0y,v0z)

    return()


if __name__ == '__main__':
    main()
