#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 28 14:38:10 2021

@author: miquelmiravet
"""

#############################################
#                                           #
#       STRESS TENSORS EVOLUTION KHI        #
#                                           #
#############################################

"""
Calculation of the energy density of the stresses directly from the data of the
numerical simulations
"""

import os
import numpy as np
import sys
import itertools
import numpy.ctypeslib as npct
from ctypes import c_int, c_float
import h5py
import concurrent.futures

#=================================================================================
#=================================================================================
# input type for the function
# must be a double array, with single dimension that is contiguous
array_3d_float = npct.ndpointer(dtype=np.float64, ndim=3, flags='CONTIGUOUS')
array_1d_float = npct.ndpointer(dtype=np.float64, ndim=1, flags='CONTIGUOUS')

libboxkernel = npct.load_library("/home/lv43/lv43072/MInIT/KHI/khi_kernels/libboxkernel", ".")
libboxkernel.box_kernel_cart.restype = None
libboxkernel.box_kernel_cart.argtypes = [c_int, c_int, c_int, array_1d_float, array_1d_float, array_1d_float, c_int, c_int, c_int, array_1d_float, array_1d_float, array_1d_float, c_float, array_3d_float, array_3d_float]
#===========================================================================================

def readh5files(file,leng):

    ind0 = 3
    ind1 = leng+3

    print('DATA FROM FILE:      ',file)
    hf = h5py.File(file, 'r')
    g_params = hf.get('Parameters')
    t = np.array(g_params.get('t'))
    t0 = np.array(g_params.get('t0'))
    tf = np.array(g_params.get('tf'))
    print('time     = ', t)

    g_x = hf.get('X')
    x = np.array(g_x.get('znc'))
    x = x[ind0:ind1]
    g_y = hf.get('Y')
    y = np.array(g_y.get('znc'))
    y = y[ind0:ind1]
    g_z = hf.get('Z')
    z = np.array(g_z.get('znc'))
    z = z[ind0:ind1]

    g_hydro = hf.get('hydro')
    data_hydro = np.array(g_hydro.get('data'))
    data_hydro = data_hydro[ind0:ind1,ind0:ind1,ind0:ind1,:]
    rho = data_hydro[:,:,:,0]
    U = data_hydro[:,:,:,1]
    v_x = data_hydro[:,:,:,2]/rho
    v_y = data_hydro[:,:,:,3]/rho
    v_z = data_hydro[:,:,:,4]/rho
    g_thd = hf.get('thd')
    data_thd = np.array(g_thd.get('data'))
    data_thd = data_thd[ind0:ind1,ind0:ind1,ind0:ind1,:]
    press = data_thd[:,:,:,0]
    c_s = data_thd[:,:,:,1]

    g_mag = hf.get('mag_vol')
    B = np.array(g_mag.get('data'))
    B = B[ind0:ind1,ind0:ind1,ind0:ind1,:]
    hf.close()

    Bx = B[:,:,:,0]
    By = B[:,:,:,1]
    Bz = B[:,:,:,2]

    return [t,x,y,z,Bx.T, By.T, Bz.T, vx.T, vy.T, vz.T,rho.T]

#===========================================================================================

def box_filter(items):

    var,filtersize,x,y,z,xfilter,yfilter,zfilter = items

    var = np.asarray(var, order = 'C')
    varmean = np.zeros((len(xfilter),len(yfilter),len(zfilter)))

    libboxkernel.box_kernel_cart(len(x), len(y),len(z), x, y, z, len(xfilter), len(yfilter), len(zfilter), xfilter, yfilter, zfilter, filtersize, var, varmean)

    return varmean

#===========================================================================================

def stresses(v, b, rho, filtersize, x,y, z, xfilter, yfilter, zfilter):

    vars = [rho,v[0],v[1],v[2],b[0],b[1],b[2]]

    items = ((var,filtersize,x,y,z,xfilter,yfilter,zfilter) for var in vars)
    items = list(items)

    resmean = []

    for it in items:
        resmean.append(box_filter(it))

    rhomean = resmean[0]
    vmean = np.array([resmean[1],resmean[2],resmean[3]])
    bmean = np.array([resmean[4],resmean[5],resmean[6]])

    vsqmean = np.zeros((3,3,len(xfilter),len(yfilter),len(zfilter)))

    Rmean = np.zeros((3,3,len(xfilter),len(yfilter),len(zfilter)))

    bsqmean = np.zeros((3,3,len(xfilter),len(yfilter),len(zfilter)))

    Mmean = np.zeros((3,3,len(xfilter),len(yfilter),len(zfilter)))

    bvmean = np.zeros((3,3,len(xfilter),len(yfilter),len(zfilter)))

    Fmean = np.zeros((3,3,len(xfilter),len(yfilter),len(zfilter)))

    for i in range(0,3):
        for j in range(0,3):

            item1 = (b[i,:,:,:]*b[j,:,:,:],filtersize,x,y,z,xfilter, yfilter, zfilter)
            bsqmean[i,j] = box_filter(item1)
            Mmean[i,j] = bsqmean[i,j]-bmean[i]*bmean[j]

            item2 = (v[i,:,:,:]*v[j,:,:,:],filtersize, x,y,z,xfilter, yfilter, zfilter)
            vsqmean[i,j] = box_filter(item2)
            Rmean[i,j] = vsqmean[i,j]-vmean[i]*vmean[j]

            item3=(b[i,:,:,:]*v[j,:,:,:],filtersize,x,y,z, xfilter, yfilter, zfilter)
            bvmean[i,j] = box_filter(item3)


    for i in range(0,3):
    	for j in range(0,3):

            Fmean[i,j] = bvmean[j,i]-bmean[j]*vmean[i]-(bvmean[i,j]-bmean[i]*vmean[j])


    return Mmean, Rmean, Fmean, rhomean

#===========================================================================================

def stresses_ev(items):

    i,leng,direc,filtersize,t,path_files, x,y,z,xfilter,yfilter,zfilter, b0x,b0y,b0z,v0x,v0y,v0z = items

    arx = format(int(10*t[i]), "08")
    listvar = readh5files(direc+'h'+str(arx)+'.h5',leng)
    B = [listvar[4]-b0x, listvar[5]-b0y, listvar[6]-b0z]
    B = np.array(B)
    v = [listvar[7]-v0x,listvar[8]-v0y,listvar[9]-v0z]
    v = np.array(v)
    rho = listvar[10]
    rho = np.asarray(rho, order='C')

    Mmean, Rmean, Fmean, rhomean = stresses(v, B, rho, filtersize, x,y, z, xfilter, yfilter, zfilter)

    arxiu = format(int(10*t[i]), "08")
    hf = h5py.File(path_files+'/stresses_sim_res_'+str(leng)+'_'+str(leng)+'_'+str(leng)+'-'+str(arxiu)+'.h5', 'w')
    hf.create_dataset('time', data=t[i])
    hf.create_dataset('S_f', data = Sf)
    hf.create_dataset('x', data=x)
    hf.create_dataset('y', data=y)
    hf.create_dataset('z', data=z)
    hf.create_dataset('x_f', data=xfilter)
    hf.create_dataset('y_f', data=yfilter)
    hf.create_dataset('z_f', data=zfilter)
    hf.create_dataset('stress_Mxx', data=Mmean[0,0])
    hf.create_dataset('stress_Mxy', data=Mmean[0,1])
    hf.create_dataset('stress_Mxz', data=Mmean[0,2])
    hf.create_dataset('stress_Myy', data=Mmean[1,1])
    hf.create_dataset('stress_Myz', data=Mmean[1,2])
    hf.create_dataset('stress_Mzz', data=Mmean[2,2])
    hf.create_dataset('stress_Rxx', data=Rmean[0,0])
    hf.create_dataset('stress_Rxy', data=Rmean[0,1])
    hf.create_dataset('stress_Rxz', data=Rmean[0,2])
    hf.create_dataset('stress_Ryy', data=Rmean[1,1])
    hf.create_dataset('stress_Ryz', data=Rmean[1,2])
    hf.create_dataset('stress_Rzz', data=Rmean[2,2])
    hf.create_dataset('stress_Fxy', data=Fmean[0,1])
    hf.create_dataset('stress_Fxz', data=Fmean[0,2])
    hf.create_dataset('stress_Fyz', data=Fmean[1,2])
    hf.create_dataset('rho_mean', data=rhomean)

    hf.close()

    return i

#===========================================================================================

def main():

    path_data = "/storage/scratch/lv43/lv43072/KHI/Carrasco--512--Std--bx1e-3--rndm-1/"
    direc = path_data+"data/"

    Sf = int(sys.argv[1])

    t0 = 0
    tf = 40
    t = np.linspace(t0,tf,201)
    rang = np.arange(0,len(t))

    length = 512
    lenx = length
    leny = length
    lenz = length

    var = readh5files(direc+'h00000000.h5',length)

    x = var[1].astype(np.float64)
    y = var[2].astype(np.float64)
    z = var[3].astype(np.float64)
    b0x = var[4]
    b0y = var[5]
    b0z = var[6]
    v0x = var[7]
    v0y = var[8]
    v0z = var[9]

    cellsize = x[1]-x[0]

    filtersize = Sf*cellsize

    a = int(len(x)/2)
    amin = a-5
    amax = a+5
    xfilter= x[amin:amax]
    yfilter = y[amin:amax]
    zfilter = z[amin:amax]

    path_files = path_data+"results/stress_tensors/Sf_"+str(Sf)

    try:
        os.makedirs(path_files)
    except OSError as error:
        print(error)

    items = ((i,length,direc,filtersize,t,path_files, x,y,z,xfilter,yfilter,zfilter, b0x,b0y,b0z,v0x,v0y,v0z) for i in rang)
    items = tuple(items)

    itera = int(200/4)

    print('Stresses...')


    for it in range(0,itera):

        ind0 = it
        ind1 = it+4
        inputs = items[ind0:ind1]

        with concurrent.futures.ProcessPoolExecutor(max_workers=4) as executor:
            res = executor.map(stresses_ev,inputs)

            executor.shutdown()

            for rr in res:
                print(f'Iteration {rr} ended')

    return()

if __name__ == '__main__':
    main()
