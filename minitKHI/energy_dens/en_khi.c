//
//  box_kernel_cart.c
//
//
//  Created by Miquel Miravet on 23/06/2021.
//

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stdbool.h>
//#include "khi_kernel.c"

void box_kernel_cart(int lenx, int leny, int lenz, double x[], double y[], double z[], int lenxfilter, int lenyfilter, int lenzfilter, double xfilter[], double yfilter[], double
zfilter[], float filtersize, double var[][leny][lenz], double varmean[][lenyfilter][lenzfilter]){

  float difx, dify, difz,diflx1,diflx2,difly1,difly2,diflz1,diflz2;
  int i, j, k, p, q, l;
  float dx = x[1]-x[0];
  float lbox = lenx*dx;
  double sum= 0;
  int sum2= 0;

/*  double *** G = (double ***)malloc(lenx*sizeof(double**));
    for (i = 0; i< lenx; i++) {
      G[i] = (double **) malloc(leny*sizeof(double *));
      for (j = 0; j < leny; j++) {
        G[i][j] = (double *)malloc(lenz*sizeof(double));
        }
      }
*/
  double deltax = filtersize*0.5000001;
  double deltay = filtersize*0.5000001;
  double deltaz = filtersize*0.5000001;

  int G = 0;

  for (p = 0 ; p < lenxfilter ; p++){
    for (q = 0 ; q < lenyfilter ; q++){
      for (l = 0 ; l < lenzfilter ; l++){

        for (i = 0 ; i < lenx ; i++){
          difx = fabs(xfilter[p]-x[i]);
          diflx1 = fabs(xfilter[p]-(x[i]+lbox));
          diflx2 = fabs(xfilter[p]-(x[i]-lbox));
          for (j = 0; j < leny ; j++){
            dify = fabs(yfilter[q]-y[j]);
            difly1 = fabs(yfilter[q]-(y[j]+lbox));
            difly2 = fabs(yfilter[q]-(y[j]-lbox));
            for (k = 0; k < lenz ; k++){

			        difz = fabs(zfilter[l]-z[k]);
              diflz1 = fabs(zfilter[l]-(z[k]+lbox));
              diflz2 = fabs(zfilter[l]-(z[k]-lbox));

			        if((difx <= deltax || diflx1 <= deltax || diflx2 <= deltax) && (dify <= deltay || difly1 <= deltay || difly2 <= deltay) && (difz <= deltaz || diflz1 <= deltaz || diflz2 <= deltaz)){
                G = 1;
              }
				      else{
                G = 0;
				      }

              sum2 += G;
              sum += var[i][j][k]*G;

              G = 0;
            }
          }
        }
        varmean[p][q][l] = sum/sum2;
        sum = 0;
		    sum2 = 0;

      }
    }
  }

/*  for (i = 0; i< lenx; i++) {
    for (j = 0; j < leny; j++) {
      free(G[i][j]);
    }
	  free(G[i]);
  }
  free(G);
  */
}

void stresses(int lenx, int leny, int lenz, int lenxfilter, int lenyfilter, int lenzfilter, double v[][lenx][leny][lenz], double vm[][lenxfilter][lenyfilter][lenzfilter], double rho[][leny][lenz], float filtersize, double x[], double y[], double z[], double xfilter[], double yfilter[],
double zfilter[], double stressR[][lenxfilter][lenyfilter][lenzfilter], double
rhomean[][lenyfilter][lenzfilter], double aux[][leny][lenz], double aux2[][lenyfilter][lenzfilter]){


  int i,j,k,l,m,n;

  box_kernel_cart(lenx, leny,lenz, x, y, z, lenxfilter, lenyfilter, lenzfilter, xfilter, yfilter, zfilter, filtersize, v[0], vm[0]);
  box_kernel_cart(lenx, leny,lenz, x, y, z, lenxfilter, lenyfilter, lenzfilter, xfilter, yfilter, zfilter, filtersize, v[1], vm[1]);
  box_kernel_cart(lenx, leny,lenz, x, y, z, lenxfilter, lenyfilter, lenzfilter, xfilter, yfilter, zfilter, filtersize, v[2], vm[2]);


  k = 0;
  for (i = 0; i < 3; i++){
    for (j = 0; j < 3; j++){

      for (m = 0; m < lenx; m++){
        for (n = 0; n < leny; n++){
          for (l = 0; l < lenz; l++){
            aux[m][n][l] = v[i][m][n][l]*v[j][m][n][l];
          }
        }
      }

      box_kernel_cart(lenx, leny,lenz, x, y, z, lenxfilter, lenyfilter, lenzfilter, xfilter, yfilter, zfilter, filtersize, aux, aux2);

      for (m = 0; m < lenxfilter; m++){
        for (n = 0; n < lenyfilter; n++){
          for (l = 0; l < lenzfilter; l++){
            stressR[k][m][n][l] = aux2[m][n][l]-vm[i][m][n][l]*vm[j][m][n][l];
          }
        }
      }
      k++;
    }
  }


  box_kernel_cart(lenx, leny,lenz, x, y, z, lenxfilter, lenyfilter, lenzfilter, xfilter, yfilter, zfilter, filtersize, rho, rhomean);




}
