import os
import numpy as np
import h5py
import sys

import numpy.ctypeslib as npct
import ctypes
from ctypes import c_int, c_float,c_char_p
import concurrent.futures

array_3d_float = npct.ndpointer(dtype=np.float64, ndim=3, flags='CONTIGUOUS')
array_1d_float = npct.ndpointer(dtype=np.float64, ndim=1, flags='CONTIGUOUS')

lib_growth = npct.load_library("lib_growth", ".")
lib_growth.box_kernel_cart.restype = None
lib_growth.box_kernel_cart.argtypes = [c_int, c_int, c_int, array_1d_float, array_1d_float, array_1d_float, c_int, c_int, c_int, array_1d_float, array_1d_float, array_1d_float, c_float, array_3d_float, array_3d_float]

lib_growth.growth_rate.restype = None
lib_growth.growth_rate.argtypes = [c_int, c_int, c_int, array_1d_float, array_1d_float, array_1d_float, c_int, c_int, c_int,array_1d_float, array_1d_float, array_1d_float, array_1d_float,c_float, array_3d_float, array_3d_float,array_3d_float, array_3d_float]


#===============================================================================================

def readh5files(file,leng):

    print('DATA FROM FILE:      ',file)
    hf = h5py.File(file, 'r')
    rho = np.array(hf.get('rho'))
    vx = np.array(hf.get('vx'))
    vy = np.array(hf.get('vy'))
    vz = np.array(hf.get('vz'))
    Bx = np.array(hf.get('Bx'))
    By = np.array(hf.get('By'))
    Bz = np.array(hf.get('Bz'))
    hf.close()

    x0 = np.linspace(-0.5,0.5,leng*2)
    dx = x0[1]-x0[0]
    x = np.linspace(-0.5+dx,0.5-dx,leng)
    y = np.linspace(-0.5+dx,0.5-dx,leng)
    z = np.linspace(-0.5+dx,0.5-dx,leng)
    t = 0.0

    return [x,y,z,vx]

#===============================================================================================

def grate(items):

    i,path,path_save,basename,leng,x,y,z,xfilter,yfilter,yfilter1,yfilter2,zfilter,filtersize,t,Sf = items

    arx = format(int(i), "08")
    var = readh5files(path+'data/KHI-'+arx+'.h5',leng)
    vx = np.array(var[3])
    vxm1 = np.zeros((len(xfilter),2,len(zfilter)))
    vxm2 = np.zeros((len(xfilter),2,len(zfilter)))
    growth = np.zeros((len(xfilter),2,len(zfilter)))

    lib_growth.growth_rate(len(x),len(y),len(z),x,y,z,len(xfilter),2,len(zfilter),xfilter,yfilter1,yfilter2,zfilter,filtersize,vx,vxm1,vxm2,growth)

    print(growth)

 #   output_counter = format(int(10*t[i]),"04")

 #   hf = h5py.File(path_save+'/'+basename+'-'+output_counter+'.h5', 'w')
 #   hf.create_dataset('time', data=t[i])
 #   hf.create_dataset('S_f', data = Sf)
 #   hf.create_dataset('x', data=x)
 #   hf.create_dataset('y', data=y)
 #   hf.create_dataset('z', data=z)
 #   hf.create_dataset('x_f', data=xfilter)
 #   hf.create_dataset('y_f', data=yfilter)
 #   hf.create_dataset('z_f', data=zfilter)
 #   hf.create_dataset('growth', data=growth)

 #   hf.close()
    
    return()

#===============================================================================================

def main():

    Sf = int(sys.argv[1])

    length = int(sys.argv[3])
    b0x = sys.argv[2]

    path= "/home/miquelmiravet/University/Doctorat/Projects/sub-grid_models/DATA/KHI/res_"+str(length)+"_"+str(length)+"_"+str(length)+"/CVP--bx"+b0x+"--"+str(length)+"--rndom/"
    path_save = path+'results/growth_rates/Sf_'+str(Sf)

    basename = 'growth--'+str(length)+'--bx'+b0x+'--Sf-'+str(Sf)

    try:
        os.makedirs(path_save)
    except OSError as error:
        print(error)

    file0 = path+'data/KHI-00000000.h5'

    var = readh5files(file0,length)

    t = np.linspace(0,40,401)
    rang = np.arange(0,2)
#    rang = np.array([56,58,71,78,80,140,151,157,183,187,189,192,195])
#    rang = np.array([200])

    x = var[0].astype(np.float64)
    y = var[1].astype(np.float64)
    z = var[2].astype(np.float64)

    cellsize = x[1]-x[0]
    filtersize = Sf*cellsize

    a = int(len(y)/4)
    b = int(len(y)*3/4)
    n = 15

    xfilter = np.linspace(x[0],x[-1],n).astype(np.float64)
    indy2a = int(a+Sf)
    indy1a = int(a-Sf)
    indy2b = int(b+Sf)
    indy1b = int(b-Sf)
    yfilter1 = np.array([y[indy1a],y[indy2a]]).astype(np.float64)
    yfilter2 = np.array([y[indy1b],y[indy2b]]).astype(np.float64)
    print(yfilter1)
    print(yfilter2)
    yfilter = np.array([y[a],y[b]]).astype(np.float64)
    zfilter = np.linspace(z[0],z[-1],n).astype(np.float64)


    items = ((i,path,path_save,basename,length,x,y,z,xfilter,yfilter,yfilter1,yfilter2,zfilter,filtersize,t,Sf) for i in rang)
    items = tuple(items)
    itera = int(len(rang)/2)

    for it in range(0,itera):

        ind0 = 2*it
        ind1 = 2*(it+1)
        inputs = items[ind0:ind1]

        with concurrent.futures.ProcessPoolExecutor(max_workers=2) as executor:

            res = executor.map(grate,inputs)

            executor.shutdown()

    return()


if __name__ == "__main__":
    main()

