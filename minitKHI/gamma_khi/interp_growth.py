import os
import numpy as np
from scipy import interpolate
from matplotlib import pyplot as plt
from matplotlib import rc
import seaborn as sns

rc('text', usetex=True)
rc('font', family='serif')

t = np.linspace(0,40,201)
gamma_khi = np.loadtxt('KHI/growth_rates/av_growth--bx3e-4--128.dat', usecols=2)

x = np.linspace(0,40,800001)

interp2 = interpolate.interp1d(t, gamma_khi, kind = "quadratic")
interp3 = interpolate.interp1d(t, gamma_khi, kind = "cubic")
y_quad = interp2(x)
y_cubic = interp3(x)


plt.plot(t,gamma_khi, 'o', label = "$Pi$")
plt.plot(x, y_quad,    "-", label = "Quadratic")
plt.plot(x, y_cubic,   "-", label = "Cubic")
plt.grid()
plt.xlabel("x")
plt.ylabel("y")
plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
plt.show()
