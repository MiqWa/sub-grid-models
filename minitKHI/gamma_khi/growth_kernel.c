#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stdbool.h>

void box_kernel_cart(int lenx, int leny, int lenz, double x[], double y[], double z[], int lenxfilter, int lenyfilter, int lenzfilter, double xfilter[], double yfilter[], double
zfilter[], float filtersize, double var[][leny][lenz], double varmean[][lenyfilter][lenzfilter]){

  float difx, dify, difz,diflx1,diflx2,difly1,difly2,diflz1,diflz2;
  int i, j, k, p, q, l;
  float dx = x[1]-x[0];
  float lbox = lenx*dx;
  double sum= 0;
  int sum2= 0;

/*  double *** G = (double ***)malloc(lenx*sizeof(double**));
    for (i = 0; i< lenx; i++) {
      G[i] = (double **) malloc(leny*sizeof(double *));
      for (j = 0; j < leny; j++) {
        G[i][j] = (double *)malloc(lenz*sizeof(double));
        }
      }
*/
  double deltax = filtersize*0.5000001;
  double deltay = filtersize*0.5000001;
  double deltaz = filtersize*0.5000001;

  int G = 0;

  for (p = 0 ; p < lenxfilter ; p++){
    for (q = 0 ; q < lenyfilter ; q++){
      for (l = 0 ; l < lenzfilter ; l++){

        for (i = 0 ; i < lenx ; i++){
          difx = fabs(xfilter[p]-x[i]);
          diflx1 = fabs(xfilter[p]-(x[i]+lbox));
          diflx2 = fabs(xfilter[p]-(x[i]-lbox));
          for (j = 0; j < leny ; j++){
            dify = fabs(yfilter[q]-y[j]);
            difly1 = fabs(yfilter[q]-(y[j]+lbox));
            difly2 = fabs(yfilter[q]-(y[j]-lbox));
            for (k = 0; k < lenz ; k++){

			        difz = fabs(zfilter[l]-z[k]);
              diflz1 = fabs(zfilter[l]-(z[k]+lbox));
              diflz2 = fabs(zfilter[l]-(z[k]-lbox));

			        if((difx <= deltax || diflx1 <= deltax || diflx2 <= deltax) && (dify <= deltay || difly1 <= deltay || difly2 <= deltay) && (difz <= deltaz || diflz1 <= deltaz || diflz2 <= deltaz)){
                G = 1;
              }
				      else{
                G = 0;
				      }

              sum2 += G;
              sum += var[i][j][k]*G;

              G = 0;
            }
          }
        }
        varmean[p][q][l] = sum/sum2;
        sum = 0;
		    sum2 = 0;

      }
    }
  }
}


void growth_rate(int lenx, int leny, int lenz, double x[], double y[], double z[], int lenxfilter, int lenyfilter, int lenzfilter, double xfilter[], double yfilter1[], double yfilter2[], double
zfilter[], float filtersize, double vx[][leny][lenz], double vxmean1[][2][lenzfilter], double vxmean2[][2][lenzfilter], double dv[][2][lenzfilter]){

  int i,j;

  box_kernel_cart(lenx, leny,lenz, x, y, z, lenxfilter,lenyfilter , lenzfilter, xfilter, yfilter1, zfilter, filtersize, vx, vxmean1);
  box_kernel_cart(lenx, leny,lenz, x, y, z, lenxfilter,lenyfilter , lenzfilter, xfilter, yfilter2, zfilter, filtersize, vx, vxmean2);


  for (i = 0; i < lenxfilter; i++){
    for (j = 0; j < lenzfilter; j++){
        dv[i][0][j] = (vxmean1[i][1][j]-vxmean1[i][0][j])/(yfilter1[1]-yfilter1[0]);
        dv[i][1][j] = (vxmean2[i][1][j]-vxmean2[i][0][j])/(yfilter2[1]-yfilter2[0]);
        dv[i][0][j] = fabs(dv[i][0][j]);
        dv[i][1][j] = fabs(dv[i][1][j]);

    }
  }

}
